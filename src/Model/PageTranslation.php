<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Model;

use Rozyn\Model\TranslationModel;

class PageTranslation extends TranslationModel {
	protected $fields = ['id', 'language', 'page_id', 'title', 'content'];
}