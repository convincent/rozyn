<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Model;

use Rozyn\Model\TranslatableModel;

class Page extends TranslatableModel {
	protected $fields = ['id', 'author_id'];
	
	public function author() {
		return $this->belongsTo('Model\Author');
	}
}