<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Model;

use Rozyn\Model\Model;

class Post extends Model {
	protected $soft = false;
	
	protected $fields = ['id', 'author_id', 'title', 'content'];
	
	public function comments() {
		return $this->hasMany('Model\Comment');
	}
	
	public function author() {
		return $this->belongsTo('Model\Author');
	}
	
	public function tags() {
		return $this->hasAndBelongsToMany('Model\Tag', ['joinTable' => 'posts_tags']);
	}
}