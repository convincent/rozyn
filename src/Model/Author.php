<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Model;

use Rozyn\Model\Model;

class Author extends Model {
	protected $soft = false;
	
	protected $fields = ['id', 'name'];
	
	public function comments() {
		return $this->hasMany('Model\Comment');
	}
	
	public function posts() {
		return $this->hasMany('Model\Post');
	}
}