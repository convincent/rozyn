<?php

namespace Controller;

use Model\Post;
use Model\Author;
use Model\Comment;
use Rozyn\Model\Auth\User;
use Rozyn\Controller\Controller;

class PageController extends Controller {
	
	public function test() {
		
	}
	
	/**
	 * 
	 */
	public function index($msg = 'Hello world!') {
		$this->set('msg', $msg);
	}
	
	public function post() {
		
	}
	
	public function xml() {
		$this->set(array(
			'course' => 'Fundamental Programming',
			'participants' => array(
				array(
					'student' => array(
						'gender'	=> 'male',
						'name'		=> 'John',
					)
				),
				
				array(
					'student' => array(
						'gender'	=> 'female',
						'name'		=> 'Jane',
					)
				)
			)
		));
	}
	
}