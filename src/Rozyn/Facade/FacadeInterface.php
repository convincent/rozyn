<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Facade;

/**
 * This interface forces each static subclass of our Facade class to implement
 * a getClass() method, which should return the name of the class that is to
 * be accessed through the Facade.
 * 
 * We have to use an Interface for this, because we can't declare abstract
 * static methods in our abstract Facade class.
 */
interface FacadeInterface {
	/**
	 * Returns the full namespaced name of the class that is to be accessed
	 * through this Facade.
	 * 
	 * @return	string
	 */
	static public function getClass();
}