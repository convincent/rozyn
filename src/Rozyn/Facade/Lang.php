<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Facade;

class Lang extends Facade {
	public static function getClass() {
		return 'Rozyn\I18n\I18nHandler';
	}
}