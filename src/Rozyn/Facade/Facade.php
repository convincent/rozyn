<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Facade;

use Rozyn\Composition\DI;

/**
 * Our Facade class provides a way of interacting with our (singleton) objects
 * in a static manner, even though the objects themselves aren't static. This
 * makes code a lot more readable while maintaining the property of testablity
 * for all our classes.
 */
abstract class Facade implements FacadeInterface {
	/**
	 * An array with all the instances that have been instantiated so far. 
	 * Serves as a sort of cache for each child facade so that we don't have to
	 * do a full lookup through our DI every time we request a singleton 
	 * instance of a class.
	 * 
	 * @var	array
	 */
	protected static $instances = [];
	
	/**
	 * Returns an instance of the given class (name). If no such instance has
	 * been requested before, the instance is looked up through our DI and a
	 * reference to that instance is then stored in our class cache variable,
	 * meaning that the next time this same class is requested, we'll only 
	 * have to return the stored reference corresponding to the class name
	 * (which will be used as a unique key).
	 * 
	 * @param	string	$name
	 * @return	object
	 */
	public static function getInstanceOf($name) {
		if (!isset(static::$instances[$name])) {
			static::$instances[$name] = DI::getInstanceOf($name);
		}
		
		return static::$instances[$name];
	}
	
	/**
	 * This magic __callStatic method forwards every static call on the object
	 * that is being accessed through this Facade to a singleton instance of
	 * said object that is stored inside this Facade.
	 * 
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	mixed
	 */
	public static function __callStatic($name, $arguments) {
		return call_user_func_array(array(static::getInstanceOf(static::getClass()), $name), $arguments);
	}
}