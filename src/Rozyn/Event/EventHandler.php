<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Event;

class EventHandler {
	/**
	 * The name of the event that is fired when a database query is executed.
	 * 
	 * @var	string
	 */
	const EVENT_QUERY_EXECUTED = 'query_executed';
	
	/**
	 * The name of the event that is fired when a View is constructed.
	 * 
	 * @var	string
	 */
	const EVENT_VIEW_CONSTRUCTED = 'view_constructed';
	
	/**
	 * Tells our Dependency Injector that this class can be treated as a 
	 * singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds the listeners for this Dispatcher
	 * 
	 * @var	array
	 */
	protected $listeners = [];
	
	/**
	 * Fires a specific event and provides every listener with the specified
	 * parameters.
	 * 
	 * @param	string	$event
	 * @param	array	$params
	 */
	public function fire($event, array $params = []) {
		// Store the result of each listener, just in case we need this info.
		$res = [];
		
		// Execute all listeners for the given event.
		foreach ($this->getListeners($event) as $listener) {
			$res[] = $tmp = call_user_func_array($listener, $params);
			
			// If any of the listeners returns false, stop propagation.
			if ($tmp === false) {
				break;
			}
		}
		
		// Return an array of all results of the listeners.
		return $res;
	}
	
	/**
	 * Adds a single listener to a specified event. A listener can be a callable
	 * like a function or it can be an Action object which will be ran when the
	 * event is fired.
	 * 
	 * An optional $priority argument can be passed along which tells the 
	 * dispatcher when the listener should be called in relation to other 
	 * listeners for the same event.
	 * 
	 * @param	string							$event
	 * @param	callable|Rozyn\Request\Action	$listener
	 * @param	int								$priority
	 */
	public function listen($event, $listener, $priority = 0) {
		// If no listeners exist for this event yet, create a new entry in our
		// $listeners variable that keeps track of this event's listeners.
		if (!$this->hasListeners($event)) {
			$this->listeners[$event] = [];
		}
		
		// If no listeners have been added with this priority level yet, add
		// an empty array for this priority level now so that we can add any
		// number of listeners to it.
		if (!isset($this->listeners[$event][$priority])) {
			$this->listeners[$event][$priority] = [];
		}

		// If there exists a cached sorted array of listeners for this event,
		// unset this list now, so that the next time all the listeners for this
		// event are requested, the sorted list will be computed again.
		if (isset($this->sorted[$event])) {
			unset($this->sorted[$event]);
		}
									
		// Add the (newly created) callback to the event's listeners.
		$this->listeners[$event][$priority][] = $cb;
	}
	
	/**
	 * Returns whether or not a listener is registered for the given event.
	 * 
	 * @param	string	$event
	 * @return	boolean
	 */
	public function hasListeners($event) {
		return (isset($this->listeners[$event]));
	}
	
	/**
	 * Returns all listeners for a given event in the correct priority order.
	 * 
	 * @param	string	$event
	 * @return	array
	 */
	public function getListeners($event) {
		// If we have no cached result for this operation, sort the listeners.
		if (!isset($this->sorted[$event])) {
			$this->sortListeners($event);
		}
		
		// Now that we're sure to have a cached result, return that.
		return $this->sorted[$event];
	}
	
	/**
	 * Sorts all the listeners for a given event and returns a flat array 
	 * containing all the listeners in the correct priority order.
	 * 
	 * @param	string	$event
	 * @return	array
	 */
	protected function sortListeners($event) {
		// Iniitialize our sorted array of listeners by making it an empty array.
		// By caching this sorted array, we don't have to do the sort procedure
		// every single time a sorted array of all the listeners is required.
		$this->sorted[$event] = [];
		
		// If there are listeners for this event, order them by their priorities
		// and merge all these priority subarrays together to form a single
		// sorted array of listeners.
		if ($this->hasListeners($event)) {
			krsort($this->listeners[$event]);
			
			$this->sorted[$event] = call_user_func_array('array_merge', $this->listeners[$event]);
		}
	}
}