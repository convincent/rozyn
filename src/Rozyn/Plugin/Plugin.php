<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Plugin;

use Rozyn\App;

abstract class Plugin {
	/**
	 * The default bootstrap file for all plugins.
	 * 
	 * @var	string
	 */
	const DEFAULT_BOOTSTRAP = 'bootstrap.php';
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * The name of this plugin.
	 * 
	 * @var	string
	 */
	protected $name;
	
	/**
	 * Holds a reference to the App object that this Plugin was assigned.
	 * 
	 * @var	\Rozyn\App
	 */
	protected $app;
	
	/**
	 * The directory name of this plugin.
	 * 
	 * @var	string
	 */
	protected $dir;
	
	/**
	 * Specifies whether or not the plugin should be forcefully loaded.
	 * 
	 * @var	boolean
	 */
	protected $force = false;
	
	/**
	 * The path to the config directory of this plugin.
	 * 
	 * @var	string
	 */
	protected $configPath;
	
	/**
	 * Takes care of any setup logic that has to be executed whenever the plugin
	 * is loaded. The $force parameter tells us whether or not to forcefully
	 * load the plugin, even if it's not required per se.
	 * 
	 * @param	\Rozyn\App	$app
	 * @param	boolean		$force
	 */
	public function __construct(App $app, $force = false) {
		// Store the App instance.
		$this->app($app);
		
		// Store the $force value.
		$this->force($force);
	}
	
	/**
	 * Executes the bootstrap logic for this Plugin.
	 */
	public function bootstrap() {
		// Include the bootstrap file if it exists.
		if (file_exists($bootstrap = __PLUGINS__ . DS . $this->getDirName() . DS . $this->getBootstrap())) {
			// Create a $plugin variable that will be usable from inside the
			// bootstrap file.
			$plugin = $this;

			require_once $bootstrap;
		}

		// Import native translation files for this plugin if they exist.
		if (is_dir($dir = __PLUGINS__ . DS . $this->getDirName() . DS . LANG_DIR)) {
			$this->app()->lang()->import($dir);
		}
	}
	
	/**
	 * Returns whether or not this plugin is required. Defaults to true.
	 * 
	 * @return	boolean
	 */
	public function isRequired() {
		return true;
	}
	
	/**
	 * Returns the bootstrap file for this Plugin.
	 * 
	 * @return	string
	 */
	public function getBootstrap() {
		return self::DEFAULT_BOOTSTRAP;
	}
	
	/**
	 * Returns the name of the plugin.
	 * 
	 * @return	string
	 */
	public final function getName() {
		return $this->name ?: get_class_name($this);
	}
	
	/**
	 * Returns the directory name of the plugin
	 * 
	 * @return	string
	 */
	public final function getDirName() {
		return $this->dir ?: camel2snake($this->getName());
	}
	
	/**
	 * Returns the path to the folder that contains all this plugin's config
	 * files.
	 * 
	 * @return	string
	 */
	public final function getConfigPath() {
		return $this->configPath ?: __CONFIG__ . DS . PLUGINS_DIR . DS . $this->getDirName();
	}
	
	/**
	 * Specify whether or not this plugin should be forcefully loaded.
	 * 
	 * @param	boolean	$force
	 */
	public function force($force) {
		$this->force = $force;
	}
	
	/**
	 * Returns whether or not the plugin should be forcefully loaded.
	 * 
	 * @return	boolean
	 */
	public function isForced() {
		return $this->force;
	}
	
	/**
	 * Sets and returns the App instance.
	 * 
	 * @param	\Rozyn\App	$app
	 * @return	\Rozyn\App
	 */
	public function app(App $app = null) {
		if (func_num_args()) {
			$this->app = $app;
		}
		
		return $this->app;
	}
}