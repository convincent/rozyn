<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn;

use Exception;
use Rozyn\Cache\Cache;
use Rozyn\Config\Config;
use Rozyn\I18n\I18nHandler;
use Rozyn\Request\IncomingRequest;
use Rozyn\Routing\RedirectHandler;
use Rozyn\Plugin\Plugin;
use Rozyn\Auth\Authenticable;
use Rozyn\Routing\Router;
use Rozyn\Composition\DI;
use Rozyn\Routing\PageNotFoundException;

class App {
	/**
	 * Mark our App as a singleton class.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * An array with all plugins currently loaded for our App.
	 * 
	 * @var	array
	 */
	protected $plugins = [];
	
	/**
	 * A Route object associated with this App instance.
	 * 
	 * @var	\Rozyn\Routing\Route
	 */
	protected $route;
	
	/**
	 * Holds our Config.
	 * 
	 * @var	\Rozyn\Config\Config
	 */
	protected $config;
	
	/**
	 * Holds our Cache.
	 * 
	 * @var	\Rozyn\Cache\Cache
	 */
	protected $cache;
	
	/**
	 * Holds our Router.
	 * 
	 * @var	\Rozyn\Routing\Router
	 */
	protected $router;
	
	/**
	 * Holds our Internationalization Handler..
	 * 
	 * @var	\Rozyn\I18n\I18nHandler
	 */
	protected $lang;
	
	/**
	 * The Request object that is being handled by this App instance.
	 * 
	 * @var	\Rozyn\Request\IncomingRequest
	 */
	protected $request;
	
	/**
	 * A RedirectHandler object.
	 * 
	 * @var	\Rozyn\Routing\RedirectHandler
	 */
	protected $redirect;
	
	/**
	 * The Authenticable object associated with this App instance.
	 * 
	 * @var	\Rozyn\Auth\Authenticable
	 */
	protected $auth;

	/**
	 * Instantiates our App.
	 * 
	 * @param	\Rozyn\Config\Config			$config
	 * @param	\Rozyn\Cache\Cache				$cache
	 * @param	\Rozyn\Routing\Router			$router
	 * @param	\Rozyn\I18n\I18nHandler			$lang
	 * @param	\Rozyn\Auth\Authenticatable		$auth
	 * @param	\Rozyn\Request\IncomingRequest	$request
	 * @param	\Rozyn\Routing\RedirectHandler	$redirect
	 */
	public function __construct(Config $config,
								Cache $cache,
								Router $router,
								I18nHandler $lang,
								Authenticable $auth,
								IncomingRequest $request,
								RedirectHandler $redirect) {
		
		$this->config	= $config;
		$this->cache	= $cache;
		$this->router	= $router;
		$this->lang		= $lang;
		$this->auth		= $auth;
		$this->request	= $request;
		$this->redirect	= $redirect;
	}

	/**
	 * Sets everything in motion:
	 * - Imports all of our Routes into our Router.
	 * - Looks for a matching Route.
	 * - Executes all relevant logic for that Route.
	 * - Prints the Response for the client to see.
	 */
	public function run() {
		// Check if we need to redirect to force SSL.
		if ($this->config->read('force_ssl') && $this->request->isHttpGet() && !$this->request->isSecure()) {
			$this->redirect->to($this->redirect->getGenerator()->format($this->request->getUrl(), true));
		}
		
		// Check if the user has any cookies that should automatically log them
		// in.
		$this->auth->cookie();
		
		// Import all custom routes
		$this->router->import(_ROUTES_);
		
		try {
			// Pass on the Request to the Router so that it can determine which 
			// Route should be dispatched.
			$route = $this->router->match($this->request);
			
			// If no valid route was returned, throw a PageNotFoundException.
			if (false === $route) {
				throw new PageNotFoundException();
			}
			
			// Otherwise, proceed to handle the request.
			$this->route = $route;
			
			// Check if we have stored the result of this route in our cache.
			$response = ($route->isCacheEnabled() && true === ($isCached = $this->cache->has($key = $route->getCacheKey()))) ?
							$this->cache->read($key) :
							$this->router->dispatch($route);

			// Write the Response to the client.
			$response->write();
			
			// Write the response result to our cache if necessary.
			if ($route->isCacheEnabled() && !$isCached) {
				$this->cache->write($key, $response);
			}
		}
		
		// If we encounter any errors while running our application, pass the
		// corresponding Exception to our custom shutdown handler which will 
		// gracefully handle the error.
		catch (Exception $e) {
			shutdown_handler($e);
		}
	}

	/**
	 * Loads a plugin for our App. The first argument is the name of the plugin,
	 * the second argument is an optional index file. This is the file that will
	 * be included, so it should be responsible for loading all the other
	 * necessary files for the plugin to function properly. By default, this 
	 * method will look for a file called bootstrap.php in the plugin's folder.
	 * 
	 * @param	string	$plugin
	 * @param	array	$args
	 * @return	boolean
	 * @throws	\Rozyn\Plugin\PluginNotFoundException
	 */
	public function plug($plugin, array $args = []) {
		// Check if the plugin isn't loaded already.
		if ($this->plugged($plugin)) {
			return true;
		}

		// Add the plugin file to our plugins array. If the plugin
		// has a Plugin class, create an instance of that class and
		// store it. If not, simply store the name of the plugin so
		// that we can at least check whether or not the plugin is 
		// loaded or not.
		$instance = (class_exists(($class = $plugin . '\\' . $plugin))) ? 
						DI::getInstanceOf($class, $args) : 
						$plugin;
						
		// Compute the directory name of the plugin based on its name.
		$dir = camel2snake($plugin);
		
		// Start the bootstrap logic depending on what kind of plugin we've just
		// loaded (procedural or object-oriented).
		if ($instance instanceof Plugin && ($instance->isRequired() || 
											$instance->isForced())) {
			
			$instance->bootstrap();
		} 
		
		elseif (is_string($instance) && file_exists($bootstrap = __PLUGINS__ . DS . $dir . DS . 'bootstrap.php')) {
			require $bootstrap;
		}
		
		// Store the plugin in our plugins array so that we can check which 
		// plugins have been loaded.
		$this->plugins[$dir] = $instance;

		// Since the plugin was loaded successfully, return true.
		return true;
	}
	
	/**
	 * Returns whether or not a particular plugin has been loaded.
	 * 
	 * @param	string	$plugin
	 * @return	boolean
	 */
	public function plugged($plugin) {
		return array_key_exists(strtolower($plugin), $this->plugins);
	}
	
	/**
	 * Returns an array of all registered plugins.
	 * 
	 * @return	array
	 */
	public function getPlugins() {
		return $this->plugins;
	}
	
	/**
	 * Returns the Authenticable handler.
	 * 
	 * @return	\Rozyn\Auth\Authenticable
	 */
	public function auth() {
		return $this->auth;
	}
	
	/**
	 * Returns the Config instance.
	 * 
	 * @return	\Rozyn\Config\Config
	 */
	public function config() {
		return $this->config;
	}
	
	/**
	 * Returns the Router instance.
	 * 
	 * @return	\Rozyn\Routing\Router
	 */
	public function router() {
		return $this->router;
	}
	
	/**
	 * Returns the I18nHandler instance.
	 * 
	 * @return	\Rozyn\I18n\I18nHandler
	 */
	public function lang() {
		return $this->lang;
	}
	
	/**
	 * Returns the IncomingRequest instance.
	 * 
	 * @return	\Rozyn\Request\IncomingRequest
	 */
	public function request() {
		return $this->request;
	}
	
	/**
	 * Sets a Route object for this App instance to keep a reference of the 
	 * Route that was matched to the request that is currently being handled.
	 * 
	 * @param	\Rozyn\Routing\Route	$route
	 */
	public function setRoute(Route $route) {
		$this->route = $route;
	}
	
	/**
	 * Returns the Route object for this App instance to keep a reference of the 
	 * Route that was matched to the request that is currently being handled.
	 * 
	 * @return	\Rozyn\Routing\Route
	 */
	public function getRoute() {
		return $this->route;
	}
}