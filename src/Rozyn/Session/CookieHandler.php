<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Session;

use Rozyn\Encryption\Encrypter;
use Rozyn\Config\Config;

Class CookieHandler {
	/**
	 * Mark the class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds the default path for all the cookies.
	 * 
	 * @var	string
	 */
	protected $path;
	
	/**
	 * Holds the default domain for all cookies.
	 * 
	 * @var	string
	 */
	protected $domain;
	
	/**
	 * Holds the default expiration time in days for all cookies.
	 * 
	 * @var	int
	 */
	protected $days;
	
	/**
	 * An Encrypter that takes care of encrypting all the data storted in the
	 * Cookies.
	 * 
	 * @var	\Rozyn\Encryption\Encrypter
	 */
	protected $encrypter;
	
	/**
	 * Holds all configuration settings for this App.
	 * 
	 * @var	\Rozyn\Config\Config
	 */
	protected $config;

	/**
	 * Constructs the CookieHandler object.
	 * 
	 * @param	\Rozyn\Encryption\Encrypter	$encrypter
	 * @param	\Rozyn\Config\Config			$config
	 * @param	int							$days
	 * @param	string						$path
	 * @param	string						$domain
	 */
	public function __construct(Encrypter $encrypter, 
								Config $config,
								$days = 2, 
								$path = null, 
								$domain = null) {
		
		$this->encrypter = $encrypter;
		$this->config = $config;
		
		// Set our default values.
		$this->days   = $days;
		$this->path	  = $path ?: suffix(__WWW_ROOT__, URI_SEPARATOR);
		$this->domain = $domain;
	}
	
	/**
	 * Read a cookie entry. Returns the specified $default value if the cookie
	 * entry could not be found.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function read($key, $default = null) {
		return ($this->has($key)) ? unserialize($this->encrypter->decrypt(filter_input(INPUT_COOKIE, $key))) : $default;
	}
	
	/**
	 * Store a new cookie entry.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 * @param	int		$days
	 * @param	string	$path
	 * @param	string	$domain
	 * @param	boolean	$secure
	 * @param	boolean	$httponly
	 * @return	boolean
	 */
	public function write($key, $value, $days = null, $path = null, $domain = null, $secure = false, $httponly = true) {
		if ($days === null) {
			$days = $this->days;
		}
		
		if ($path === null) {
			$path = $this->path;
		}
		
		if ($domain === null) {
			$domain = $this->domain;
		}
		
		// If this App is forced to use SSL for all requests, override whatever
		// value was specified for the $secure setting of our cookie and set it
		// to true instead.
		if (true === $this->config->read('force_ssl')) {
			$secure = true;
		}
		
		return setcookie($key, $this->encrypter->encrypt(serialize($value)), time() + $days * 60 * 60 * 24, $path, $domain, $secure, $httponly);
	}
	
	/**
	 * Deletes a cookie entry.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function delete($key) {
		return $this->has($key) && $this->write($key, $this->read($key), -1);
	}
	
	/**
	 * Check if a given cookie entry exists.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function has($key) {
		return filter_input(INPUT_COOKIE, $key) !== null;
	}
}