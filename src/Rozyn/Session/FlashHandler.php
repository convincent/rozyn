<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Session;

use Rozyn\Helper\HtmlHelper;

Class FlashHandler {
	/**
	 * The prefix used in the session variable name for flash messages.
	 * 
	 * @var string
	 */
	const SESSION_PREFIX = 'flash.';

	/**
	 * The default HTML tag used to wrap a flash message in.
	 * 
	 * @var string
	 */
	const HTML_TAG = 'div';
	
	/**
	 * The default html class used to identify a flash message.
	 * 
	 * @var string
	 */
	const HTML_CLASS = 'flash';
	
	/**
	 * The default html id used to identify a flash message.
	 * 
	 * @var string
	 */
	const HTML_ID = 'flash';
	
	/**
	 * The default error key.
	 * 
	 * @var string
	 */
	const ERROR_KEY = 'error';
	
	/**
	 * The default success key.
	 * 
	 * @var string
	 */
	const SUCCESS_KEY = 'success';
	
	/**
	 * Mark the class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * A SessionHandler that takes care of storing the messages temporarily.
	 * 
	 * @var	\Rozyn\Session\SessionHandler
	 */
	protected $session;
	
	/**
	 * A HtmlHelper which can be used to construct the HTML wrappers that
	 * contain our flash messages.
	 * 
	 * @var	\Rozyn\Helper\HtmlHelper
	 */
	protected $html;
	
	/**
	 * A prefix that makes it possible to distinguish flash session variables
	 * from regular session variables.
	 * 
	 * @var	string
	 */
	protected $prefix;
	
	/**
	 * A default class which is added to any HTML elements that contain a 
	 * flash message.
	 * 
	 * @var	string
	 */
	protected $class;
	
	/**
	 * A default tag which used as the HTML wrapper for flash messages.
	 * 
	 * @var	string
	 */
	protected $tag;

	/**
	 * Constructs the FlashHandler object.
	 * 
	 * @param	\Rozyn\Session\SessionHandler	$session
	 * @param	\Rozyn\Helper\HtmlHelper		$html
	 */
	public function __construct(SessionHandler $session, HtmlHelper $html) {
		$this->session	= $session;
		$this->html		= $html;
	}
	
	/**
	 * Tries to read a flash variable with the given key. If it exists, the
	 * flash message is removed from the session memory first and wrapped in 
	 * valid HTML markup before being returned afterwards. If $html is set to
	 * false, no HTML markup is added.
	 * 
	 * If it doesn't exist, a user-specified default value is returned instead.
	 * 
	 * @param	string		$key
	 * @param	string		$default
	 * @param	boolean		$html
	 * @param	string		$tag
	 * @param	string[]	$classes
	 * @return	mixed
	 */
	public function read($key, $default = null, $html = false, $tag = null, array $classes = []) {
		$value = ($this->has($key)) ? $this->session->read(static::SESSION_PREFIX . $key) : $default;
		
		// Clear the session data, since we want to delete flash data
		// after it's been read once.
		$this->delete($key);
		
		// Wrap the $value in HTML code if necessary.
		if ($value !== null && $html === true) {
			$value = $this->html->tag($tag ?: static::HTML_TAG, $value, ['id' => static::HTML_ID, 'class' => array_merge($classes, [static::HTML_CLASS])]);
		}
		
		return $value;
	}
	
	/**
	 * Stores a flash variable under the specified key.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 */
	public function write($key, $value) {
		$this->session->write(static::SESSION_PREFIX . $key, $value);
	}
	
	/**
	 * Stores a success message.
	 * 
	 * @param	string	$value
	 */
	public function writeSuccess($value) {
		$this->write(static::SUCCESS_KEY, $value);
	}
	
	/**
	 * Stores an error message.
	 * 
	 * @param	string	$value
	 */
	public function writeError($value) {
		$this->write(static::ERROR_KEY, $value);
	}
	
	/**
	 * Delets a certain flash message from the session memory.
	 * 
	 * @param	string $key
	 */
	public function delete($key) {
		$this->session->delete(static::SESSION_PREFIX. $key);
	}
	
	/**
	 * Checks whether or not a certain flash message exists.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function has($key) {
		return $this->session->has(static::SESSION_PREFIX . $key);
	}
	
	/**
	 * Returns the specified flash message and marks it as an error message.
	 * 
	 * @param	string	$key
	 * @param	string	$tag
	 * @return	string
	 */
	public function readError($key = null, $tag = null) {
		if ($key === null) {
			$key = static::ERROR_KEY;
		}
		
		return $this->read($key, null, true, $tag, ['error']);
	}
	
	/**
	 * Returns the specified flash message and marks it as an success message.
	 * 
	 * @param	string	$key
	 * @param	string	$tag
	 * @return	string
	 */
	public function readSuccess($key = null, $tag = null) {
		if ($key === null) {
			$key = static::SUCCESS_KEY;
		}
		
		return $this->read($key, null, true, $tag, ['success']);
	}
	
	/**
	 * Alias for readError().
	 * 
	 * @param	string	$key
	 * @param	string	$tag
	 * @return	string
	 */
	public function error($key, $tag = null) {
		return $this->readError($key, $tag);
	}
	
	/**
	 * Alias for readSuccess().
	 * 
	 * @param	string	$key
	 * @param	string	$tag
	 * @return	string
	 */
	public function success($key, $tag = null) {
		return $this->readSuccess($key, $tag);
	}
}