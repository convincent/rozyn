<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model;

use Rozyn\Model\Model;

class Collection extends \Rozyn\Data\Collection {
	/**
	 * An instance of the models contained within this Collection
	 * 
	 * @var	\Rozyn\Model\Model
	 */
	protected $instance;
	
	/**
	 * The constructor.
	 * 
	 * @param	array				$items
	 * @param	\Rozyn\Model\Model	$model
	 */
	public function __construct(array $items = [], Model $model = null) {
		parent::__construct($items);
		
		$this->setInstance($model);
	}
	
	/**
	 * Returns the model instance associated with this Collection.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function getModel() {
		return $this->instance;
	}
	
	/**
	 * Sets the model instance associated with this Collection.
	 * 
	 * @param	\Rozyn\Model\Model	$model
	 */
	public function setInstance(Model $model = null) {
		$this->instance = $model;
	}
	
	/**
	 * Returns the model instance associated with this Collection.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function getInstance() {
		return $this->instance;
	}
	
	/**
	 * Sorts the model instances so that they match the given array of $ids.
	 * 
	 * @param	int[]	$ids
	 * @return	\Rozyn\Model\Collection
	 */
	public function sortByIds(array $ids) {
		$ids_lookup = array_flip($ids);
		
		$sorted = $this->sort(function($a, $b) use ($ids_lookup) {
			return $ids_lookup[$a->id()] > $ids_lookup[$b->id()];
		}, ORDER_ASCENDING);
		
		$sorted->setInstance($this->getInstance());
		
		return $sorted;
	}
	
	/**
	 * Loads the data for a given relation for each model in the Collection.
	 * 
	 * @param	string	$name
	 */
	public function loadRelation($name) {
		if (!$this->isEmpty() && $this->first()->hasRelation($name)) {
			$this->first()->getRelation($name)->loadBatch($this->items);
		}
	}
	
	/**
	 * Writes all the models in this collection to the database.
	 */
	public function save() {
		$self = $this;
		
		$this->instance->transact(function() use ($self) {
			foreach ($self->items() as $model) {
				$model->save();
			}
		});
	}
	
	/**
	 * Writes all the models in this collection with their related data to the 
	 * database.
	 */
	public function saveAll() {
		foreach ($this->items() as $model) {
			$model->saveAll();
		}
	}
	
	/**
	 * Deletes all the models in this collection.
	 */
	public function delete() {
		$instance = $this->instance;
		$query = $instance->newQuery();
		
		if ($this->instance->isSoft()) {
			$query->update()
				  ->set([$instance::SOFT_DELETE_FIELD => 1])
				  ->whereIn($instance->getPrimaryKey(), $this->getIds())
				  ->execute();
		} 
		
		else {
			$query->delete()
				  ->whereIn($instance->getPrimaryKey(), $this->getIds())
				  ->execute();
		}
	}
	
	/**
	 * Get the IDs of all the models in the collection.
	 * 
	 * @return	array
	 */
	public function getIds() {
		$ids = [];
		
		foreach ($this->items() as $model) {
			$ids[] = $model->id();
		}
		
		return $ids;
	}
	
	/**
	 * Magic call method to pass along some method calls to the models inside 
	 * this Collection.
	 * 
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	mixed
	 */
	public function __call($name, $arguments) {
		// Check if the model instance associated with this collection knows the called method.
		if (method_exists($this->instance, $name)) {
			// If so, call that method on each model stored inside this collection.
			foreach ($this->items as $id => $model) {
				$tmp = call_user_func_array([$model, $name], $arguments);
				
				// If the model method didn't return anything (void OR null) or if it returned
				// the model itself, move on to the next model in the collection. Otherwise,
				// assume that the code wants to get the result of the method as if it were
				// executed on just the very first model stored inside this collection and return it.
				if ($tmp === null || $tmp === $model) {
					continue;
				}
				
				return $tmp;
			}
			
			// Return an array containing the result of each model operation
			// or an instance of this collection if the method only returned void/null.
			return $this;
		}
	}
}