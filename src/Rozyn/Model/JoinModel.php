<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model;

class JoinModel extends Model {
	/**
	 * A magic field that is used to identify data that belongs to a JoinModel.
	 * 
	 * @var string
	 */
	const JOIN_MODEL_KEY = '_joinModel';
	
	/**
	 * Mark all JoinModels as hard.
	 * 
	 * @var	boolean
	 */
	protected $soft = false;
}