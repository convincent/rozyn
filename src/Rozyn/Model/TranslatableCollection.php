<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model;

use Rozyn\Model\Collection as ModelCollection;

class TranslatableCollection extends ModelCollection {
	public function getTranslationInstance() {
		return $this->instance->getTranslation();
	}
}