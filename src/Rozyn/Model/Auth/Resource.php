<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model\Auth;

class Resource extends AuthModel {
	/**
	 * The fields for this model.
	 * 
	 * @var	array
	 */
	protected $fields = array(
		'id' => 'VARCHAR(255)',
	);
	
	/**
	 * Set this model's table.
	 * 
	 * @var	string
	 */
	protected $table = 'resource';
	
	/**
	 * Mark this model as hard.
	 * 
	 * @var	boolean
	 */
	protected $soft = false;
	
	/**
	 * Define the resources <-> users relationship.
	 * 
	 * @return	\Rozyn\Relation\HasAndBelongsToMany
	 */
	public function users() {
		return $this->hasAndBelongsToMany('Rozyn\Model\Auth\User', ['joinTable' => static::JOIN_TABLE_USER_RESOURCE]);
	}
	
	/**
	 * Define the resources <-> groups relationship.
	 * 
	 * @return	\Rozyn\Relation\HasAndBelongsToMany
	 */
	public function groups() {
		return $this->hasAndBelongsToMany('Rozyn\Model\Auth\Group',	['joinTable' => static::JOIN_TABLE_GROUP_RESOURCE]);
	}
}