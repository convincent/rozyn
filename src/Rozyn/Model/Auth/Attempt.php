<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model\Auth;

class Attempt extends AuthModel {
	/**
	 * Define the table used to store login attempts.
	 * 
	 * @var string
	 */
	protected $table = 'attempt';
	
	/**
	 * Mark this model as hard.
	 * 
	 * @var	boolean
	 */
	protected $soft = false;
	
	/**
	 * Define the columns used by this model's table.
	 * 
	 * @var	array
	 */
	protected $fields = array(
		'id' => 'INT(11)',
		'auth' => 'VARCHAR(255)',
		'ip' => 'VARCHAR(255)',
		'success' => 'TINYINT(1)',
	);
	
	/**
	 * Set up the model.
	 */
	public function setup() {
		parent::setup();
		
		$this->addField(static::CREATED_AT_FIELD, 'TIMESTAMP');
	}
	
	/**
	 * Returns the value of the auth field that was submitted. So if the auth
	 * type for this App is set to email, this could return something like
	 * test@example.com.
	 * 
	 * @return	mixed
	 */
	public function getAuthFieldValue() {
		return $this->get('auth_field_value');
	}
}