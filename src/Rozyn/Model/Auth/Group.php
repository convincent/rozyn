<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model\Auth;

class Group extends AuthModel {
	/**
	 * The fields for this model.
	 * 
	 * @var	array
	 */
	protected $fields = array(
		'id'	=> 'INT(11)',
		'name'	=> 'VARCHAR(255)',
	);
	
	/**
	 * Set this model's table.
	 * 
	 * @var	string
	 */
	protected $table = 'group';
	
	/**
	 * Set this model's default alias.
	 * 
	 * @var	string
	 */
	protected $alias = 'UserGroup';
	
	/**
	 * Mark this model as hard.
	 * 
	 * @var	boolean
	 */
	protected $soft = false;
	
	/**
	 * Define the groups <-> users relationship.
	 * 
	 * @return	\Rozyn\Relation\HasAndBelongsToMany
	 */
	public function users() {
		return $this->hasAndBelongsToMany('Rozyn\Model\Auth\User', ['joinTable' => static::JOIN_TABLE_USER_GROUP]);
	}
	
	/**
	 * Define the groups <-> resources relationship.
	 * 
	 * @return	\Rozyn\Relation\HasAndBelongsToMany
	 */
	public function resources() {
		return $this->hasAndBelongsToMany('Rozyn\Model\Auth\Resource', ['joinTable' => static::JOIN_TABLE_GROUP_RESOURCE]);
	}

	/**
	 * Returns the name of this group.
	 * 
	 * @return	string
	 */
	public function getName() {
		return $this->get('name');
	}
}