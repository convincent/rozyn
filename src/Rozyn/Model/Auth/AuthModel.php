<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model\Auth;

use Rozyn\Model\Model;
use Rozyn\Facade\Random;
use Rozyn\Facade\Hash;

class AuthModel extends Model {
	/**
	 * The id of the member group.
	 * 
	 * @var int
	 */
	 const GROUP_MEMBER = 1;
	 
	/**
	 * The id of the manager group.
	 * 
	 * @var int
	 */
	 const GROUP_MANAGER = 2;
	 
	/**
	 * The id of the moderator group.
	 * 
	 * @var int
	 */
	 const GROUP_MODERATOR = 3;
	 
	/**
	 * The id of the admin group.
	 * 
	 * @var int
	 */
	 const GROUP_ADMIN = 4;
	 
	/**
	 * The id of the superadmin group.
	 * 
	 * @var int
	 */
	 const GROUP_SUPERADMIN = 5;
	 
	/**
	 * Join table for the users <-> groups relationship.
	 * 
	 * @var string
	 */
	const JOIN_TABLE_USER_GROUP = 'auth_users_groups';
	
	/**
	 * Join table for the users <-> resources relationship.
	 * 
	 * @var string
	 */
	const JOIN_TABLE_USER_RESOURCE = 'auth_users_resources';
	
	/**
	 * Join table for the groups <-> resources relationship.
	 * 
	 * @var string
	 */
	const JOIN_TABLE_GROUP_RESOURCE = 'auth_groups_resources';
	
	/**
	 * Define a prefix for all auth models.
	 * 
	 * @var string
	 */
	protected $prefix = 'auth_';
	
	/**
	 * Generates a new token and stores its hashed value in one of the model's 
	 * data fields before returning it.
	 * 
	 * @param	string	$field
	 * @return	string
	 */
	protected function generate($field) {
		$token = Random::string();
		
		$this->set($field, Hash::hash($token));
		
		return $token;
	}
}