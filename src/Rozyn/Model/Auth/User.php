<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model\Auth;

class User extends AuthModel {
	/**
	 * The foreign key used to point to a record in this model's table.
	 * 
	 * @var	string
	 */
	protected $foreignKey = 'user_id';
	
	/**
	 * The name of the database table associated with this model.
	 * 
	 * @var	string
	 */
	protected $table = 'user';
	
	/**
	 * The fields we want to exclude from mass assignments.
	 * 
	 * @var	array
	 */
	protected $guarded = ['password'];
	
	/**
	 * Holds the names of all the resources that this user has access to.
	 * 
	 * @var	array
	 */
	protected $permissions;
	
	/**
	 * The user fields in the database table.
	 * 
	 * @var	array
	 */
	protected $fields = array(
		'id'		=> 'INT',
		'username'	=> 'VARCHAR',
		'firstname' => 'VARCHAR',
		'lastname'	=> 'VARCHAR',
		'password'	=> 'VARCHAR',
		'email'		=> 'VARCHAR',
		'avatar'	=> 'VARCHAR',
	);
	
	/**
	 * Define the user <-> auth_token relationship.
	 * 
	 * @return	\Rozyn\Relation\HasOne
	 */
	public function auth_token() {
		return $this->hasOne('Rozyn\Model\Auth\Token');
	}
	
	/**
	 * Define the users <-> resources relationship.
	 * 
	 * @return	\Rozyn\Relation\HasAndBelongsToMany
	 */
	public function resources() {
		return $this->hasAndBelongsToMany('Rozyn\Model\Auth\Resource', ['joinTable' => static::JOIN_TABLE_USER_RESOURCE]);
	}
	
	/**
	 * Define the users <-> groups relationship.
	 * 
	 * @return	\Rozyn\Relation\HasAndBelongsToMany
	 */
	public function groups() {
		return $this->hasAndBelongsToMany('Rozyn\Model\Auth\Group', ['joinTable' => static::JOIN_TABLE_USER_GROUP]);
	}
	
	/**
	 * Returns the full name of the user.
	 * 
	 * @return	string
	 */
	public function getFullName() {
		return $this->firstname . ' ' . $this->lastname;
	}
	
	/**
	 * Returns whether or not a user is cleared for a given resource.
	 * 
	 * @param	\Rozyn\Model\Auth\Resource|string	$resource
	 * @return	boolean
	 */
	public function clearance($resource) {
		if (null === $this->permissions) {
			// Load our permissions.
			$this->getPermissions();
		}
		
		return isset($this->permissions[($resource instanceof Resource) ? $resource->id() : $resource]);
	}
	
	/**
	 * Retrieves all the resources that this user has access to.
	 * 
	 * @return	\Rozyn\Model\Auth\Resource[]
	 */
	public function getPermissions() {
		if (null === $this->permissions) {
			$this->permissions = [];
			
			foreach ($this->getRelationData('resources') as $resource) {
				$this->permissions[$resource->id()] = $resource;
			}
			
			foreach ($this->getNestedRelationData('groups.resources') as $resource) {
				$this->permissions[$resource->id()] = $resource;
			}
		}
			
		return $this->permissions;
	}
}