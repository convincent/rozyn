<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model\Auth;

use DateTime;

class Token extends AuthModel {
	/**
	 * The foreign key used to point to a record in this model's table.
	 * 
	 * @var	string
	 */
	protected $foreignKey = 'auth_token_id';
	
	/**
	 * The name of the database table associated with this model.
	 * 
	 * @var	string
	 */
	protected $table = 'token';
	
	/**
	 * The default expiration time in days.
	 * 
	 * @var	int
	 */
	protected $expire_days = 2;
	
	/**
	 * Holds the validation rules for this model.
	 * 
	 * @var array
	 */
	protected $validation = array(
		'secret' => 'obligatory',
		'serial' => 'obligatory',
	);
	
	/**
	 * The auth token fields in the database table.
	 * 
	 * @var	array
	 */
	protected $fields = array(
		'id'			=> 'INT',
		'user_id'		=> 'VARCHAR',
		'session_token'	=> 'VARCHAR',
		'secret'		=> 'VARCHAR',
		'expires_at'	=> 'DATETIME',
	);
	
	/**
	 * Mark this model as hard.
	 * 
	 * @var	boolean
	 */
	protected $soft = false;
	
	/**
	 * Initialize our model.
	 */
	public function init() {
		$this->addField(static::CREATED_AT_FIELD, 'DATETIME');
	}
	
	/**
	 * Define the auth_token <-> user relationship.
	 * 
	 * @return	\Rozyn\Relation\BelongsTo
	 */
	public function user() {
		return $this->belongsTo('Rozyn\Model\Auth\User');
	}
	
	/**
	 * Renews the expiration date of the Token and saves it.
	 * 
	 * @return	boolean
	 */
	public function save() {
		$d = new DateTime();
		$d->modify(sprintf('+%d days', $this->getLifeTime()));
		$this->set('expires_at', $d->format(DATETIME_FORMAT_SQL));
		
		return parent::save();
	}
	
	/**
	 * Sets the default value for the expiration time in days.
	 * 
	 * @param	int		$days
	 */
	public function setLifeTime($days) {
		$this->expire_days = $days;
	}
	
	/**
	 * Returns the total life time of the Token in days.
	 * 
	 * @return	int
	 */
	public function getLifeTime() {
		return $this->expire_days;
	}
	
	/**
	 * Generates a secret token and returns it, hashing it internally and 
	 * storing the hashed value in the model's data so that the hash will be
	 * stored in the database when the Session is saved.
	 * 
	 * @return	string
	 */
	public function generateSecret() {
		return $this->generate('secret');
	}
	
	/**
	 * Returns the ID of the user who is associated with this AuthToken.
	 * 
	 * @return	int
	 */
	public function getUserId() {
		return $this->user_id;
	}
}