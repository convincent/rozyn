<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model\Auth;

use Rozyn\Facade\Hash;

class Session extends AuthModel {
	/**
	 * Define the table used to store login attempts.
	 * 
	 * @var string
	 */
	protected $table = 'session';
	
	/**
	 * Mark this model as hard.
	 * 
	 * @var	boolean
	 */
	protected $soft = false;
	
	/**
	 * Activate timestamp columns for this model.
	 * 
	 * @var	boolean
	 */
	protected $timestamps = true;
	
	/**
	 * Define the columns used by this model's table.
	 * 
	 * @var	array
	 */
	protected $fields = array(
		'id' => 'INT(11)',
		'user_id' => 'INT(11)',
		'token' => 'VARCHAR(255)',
		'csrf' => 'VARCHAR(255)',
		'ip' => 'VARCHAR(255)',
		'expires_at' => 'DATETIME',
	);
	
	/**
	 * Checks if the specified CSRF token matches the one stored in this 
	 * session.
	 * 
	 * @param	string	$csrf
	 * @return	boolean
	 */
	public function validateCsrfToken($csrf) {
		return Hash::verify($csrf, $this->csrf);
	}
	
	/**
	 * Generates a csrf token and returns it, hashing it internally and storing 
	 * the hashed value in the model's data so that the hash will be stored in 
	 * the database when the Session is saved.
	 * 
	 * @return	string
	 */
	public function generateCsrfToken() {
		return $this->generate('csrf');
	}
}