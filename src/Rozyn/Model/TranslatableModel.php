<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model;

use Rozyn\Composition\DI;

/**
 * Defines a model that contains translatable data. This class provides some
 * additional model functionality to make it easy to interact with translatable
 * models. Ideally, you should be able to interact with them in much the same
 * way as you would with regular models, so that you don't even need to know if
 * a model is Translatable or not.
 */
abstract class TranslatableModel extends Model {
	/**
	 * The language field.
	 * 
	 * @var	string
	 */
	const LANGUAGE_FIELD = 'language';
	
	/**
	 * Holds the language of the translation that should be associated with this
	 * specific instance.
	 * 
	 * @var	string
	 */
	protected $language = null;
	
	/**
	 * Holds all the fields which are translatable.
	 * 
	 * @var	array
	 */
	protected $translatable = [];
	
	/**
	 * An array containing eager relations for this model.
	 * 
	 * @var	array
	 */
	protected $eager = ['translation'];
	
	/**
	 * Returns the full name of the model that holds the translations.
	 * 
	 * @return	string
	 */
	public function getTranslationClass() {
		return get_class($this) . 'Translation';
	}
	
	/**
	 * Returns whether or not both the model and its TranslationModel (if it
	 * exists) pass their validation rules. If not, any error messages are 
	 * stored in the $errors variable which can be accessed using the 
	 * getErrors() method.
	 * 
	 * @return	boolean
	 */
	public function validates() {
		$res = parent::validates();
		
		if ($this->hasTranslation() && !$this->getTranslation()->validates()) {
			$this->errors = array_merge($this->errors, $this->getTranslation()->getErrors());
			
			return false;
		}
		
		return $res;
	}

	/**
	 * Returns a new TranslatableModelCollection based on this model.
	 * 
	 * @param	array	$items
	 * @return	\Rozyn\Model\TranslatableCollection
	 */
	public function newCollection(array $items = []) {
		return DI::getInstanceOf('Rozyn\Model\TranslatableCollection', [$items, $this]);
	}
	
	/**
	 * Returns the fillable properties of this model.
	 * 
	 * @return	array
	 */
	public function fillable() {
		if (null === $this->fillable) {
			$this->fillable = array_merge(parent::fillable(), $this->getTranslation()->fillable());
		}
		
		return $this->fillable;
	}

	/**
	 * Saves the model and its translation in the database. If no ID is 
	 * specified, an INSERT is performed. Otherwise UPDATE is used.
	 * 
	 * @return	boolean
	 */
	public function save() {
		if (true === ($res = parent::save()) && null !== ($translation = $this->getTranslation())) {
			// Set a reference to the current model on our translation model.
			$translation->set($this->translation()->getForeignKey(), $this->id());
			
			// Save our translation model and return the result.
			return $translation->save();
		}
		
		return $res;
	}
	
	/**
	 * Saves the related data for this model, except for the relations in $except.
	 * 
	 * @param	string[]		$except
	 */
	public function saveRelations(array $except = []) {
		parent::saveRelations(array_merge($except, [SINGLE_TRANSLATION_RELATION]));
	}

	/**
	 * Returns one of the attributes stored in this model. If the attribute is 
	 * not found, the translation model is checked for that attribute. If it is
	 * found there, the translation model's value for that attribute is returned.
	 * 
	 * If the translation model doesn't contain the attribute either, a user-
	 * specified default value is returned instead.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function get($key, $default = null) {
		$res = parent::get($key, null);
		
		if ($res !== null) {
			return $res;
		}
		
		if ($this->hasTranslation()) {
			return $this->getTranslation()->get($key, $default);
		}
		
		return $default;
	}

	/**
	 * Sets an attribute in our data array. If an array is specified, all 
	 * entries in there will be added to our data array instead.
	 * 
	 * If the attribute doesn't match one of the fields in our model, the 
	 * related translation model is checked for that field.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 * @return	\Rozyn\Model\TranslatableModel
	 */
	public function set($key, $value) {
		if (is_string($key) && !$this->hasField($key) && $this->hasTranslatableField($key)) {
			$this->dirty($key);
			
			return $this->getTranslation()->set($key, $value);
		}
		
		return parent::set($key, $value);
	}
	
	/**
	 * Checks both this model and its translation model for dirtiness and 
	 * returns true if either one is found dirty.
	 * 
	 * @param	string|null	$key
	 * @return	boolean
	 */
	public function isDirty($key = null) {
		return parent::isDirty($key) || $this->hasTranslation() && $this->getTranslation()->isDirty($key);
	}
	
	/**
	 * Returns the single translation relation.
	 * 
	 * @return	\Rozyn\Relation\HasOne
	 */
	final public function translation() {	
		$relation = $this->hasOne($this->getTranslationClass(), array(
						'alias'			=> $this->getTranslationAlias(),
						'conditions'	=> [$this->getTranslationAlias() . '.' . static::LANGUAGE_FIELD . ' = :lang'],
						'params'		=> ['lang' => $this->getLanguage()],
					));
		
		$relation->setForeignKey($relation->getForeignKey());
		
		return $relation;
	}
	
	/**
	 * Returns the single translation relation.
	 * 
	 * @return	\Rozyn\Relation\HasMany
	 */
	final public function translations() {
		// Make sure that the single translation relation is set before this one.
		$this->translation();
		
		return $this->hasMany($this->getTranslationClass(), array(
			'foreignKey' => $this->translation()->getForeignKey(),
		));
	}
	
	/**
	 * Check whether the model has a particular translatable field.
	 * 
	 * @param	string	$field
	 * @return	boolean
	 */
	final public function hasTranslatableField($field) {
		return $this->translation()->getRelated()->hasField($field);
	}
	
	/**
	 * Returns an alias that can be used for translation models in queries.
	 * 
	 * @return	string
	 */
	final public function getTranslationAlias() {
		return ucfirst(SINGLE_TRANSLATION_RELATION);
	}
	
	/**
	 * Returns the language code set for this instance.
	 * 
	 * @return	string
	 */
	final public function getLanguage() {
		if ($this->language === null) {
			$this->language = site_language();
		}
		
		return $this->language;
	}
	
	/**
	 * Sets the language associated with this model.
	 * 
	 * @param	$lang	string
	 */
	final public function setLanguage($lang) {
		$this->language = $lang;
		
		// Update the condition variables for this relation so that any future
		// queries use the newly updated language value.
		$this->translation()->setParams(['lang' => $lang]);
		
		// Update any existing translation data with the new language.
		if ($this->hasTranslation() && $this->getTranslation()->getLanguage() !== $lang) {
			$this->loadTranslation();
		}
		
		$this->getTranslation()->set(static::LANGUAGE_FIELD, $this->getLanguage());
	}
	
	/**
	 * Returns an instance of the translation model for this model. If no 
	 * translation data has been loaded into this model yet, the proxy instance
	 * of the translation model associated with the Relation.
	 * 
	 * @return	\Rozyn\Model\TranslationModel
	 */
	final public function getTranslation() {
		if (!isset($this->data[SINGLE_TRANSLATION_RELATION])) {
			$this->data[SINGLE_TRANSLATION_RELATION] = $this->translation()->getRelated();
		}
		
		return $this->data[SINGLE_TRANSLATION_RELATION];
	}
	
	/**
	 * Returns whether or not translation data has been loaded for this model.
	 * 
	 * @return	boolean
	 */
	final public function hasTranslation() {
		return isset($this->data[SINGLE_TRANSLATION_RELATION]);
	}
	
	/**
	 * Loads the translation from the database.
	 */
	final public function loadTranslation() {
		$this->loadRelation(SINGLE_TRANSLATION_RELATION);
	}
	
	/**
	 * Specifies how to encode this Model as JSON.
	 * 
	 * @return	array
	 */
	public function jsonSerialize() {
		return array_merge(parent::jsonSerialize(), $this->getTranslation()->jsonSerialize());
	}
}