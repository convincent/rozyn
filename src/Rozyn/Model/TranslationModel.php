<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model;

abstract class TranslationModel extends Model {
	/**
	 * The language field.
	 * 
	 * @var	string
	 */
	const LANGUAGE_FIELD = 'language';
	
	/**
	 * TranslationModels don't need to be soft.
	 * 
	 * @var	boolean
	 */
	protected $soft = false;
	
	/**
	 * Use this method to perform any instantiation logic. TranslationModels 
	 * shouldn't have any relations of their own, so don't set them here.
	 */
	public function init() {
		// Do nothing
	}

	/**
	 * Saves the model in the database. If no ID is specified, an INSERT is 
	 * performed. Otherwise UPDATE is used.
	 * 
	 * @return	boolean
	 */
	public function save() {
		if (!$this->has(static::LANGUAGE_FIELD)) {
			$this->set(static::LANGUAGE_FIELD, site_language());
		}
		
		return parent::save();
	}
	
	/**
	 * Returns the full name of the translatable model for which this model 
	 * holds the translations.
	 * 
	 * @return	string
	 */
	public function getTranslatableModel() {
		return preg_replace('/Translation$/', '', get_class($this));
	}
	
	/**
	 * Returns the language of this translation.
	 * 
	 * @return string
	 */
	final public function getLanguage() {
		return $this->get('language');
	}
}