<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Model;

use Closure;
use Rozyn\Model\Collection as ModelCollection;
use Rozyn\Database\ModelQuery;
use Rozyn\Relation\Relation;
use Rozyn\Composition\DI;
use Rozyn\Facade\Validator;
use Rozyn\Facade\Database;
use Rozyn\Facade\Auth;
use JsonSerializable;
use Rozyn\Relation\RelationNotFoundException;


abstract class Model implements JsonSerializable {
	/**
	 * A constant specifying ascending order.
	 * 
	 * @var	string
	 */
	const ORDER_ASC = 'ASC';
		
	/**
	 * A constants specifying descending order
	 * 
	 * @var	string
	 */
	const ORDER_DESC = 'DESC';
	
	/**
	 * Holds the name of the field that identifies whether this model instance
	 * is soft deleted.
	 * 
	 * @var	string
	 */
	const SOFT_DELETE_FIELD = 'is_deleted';
	
	/**
	 * Holds the name of the field that identifies whether this model instance
	 * has been published or not.
	 * 
	 * @var	string
	 */
	const IS_PUBLISHED_FIELD = 'is_published';
	
	/**
	 * Holds the name of the field that tells us when this model was created.
	 * 
	 * @var	string
	 */
	const CREATED_AT_FIELD = 'created_at';
	
	/**
	 * Holds the name of the field that tells when this model was last modified.
	 * 
	 * @var	string
	 */
	const MODIFIED_AT_FIELD = 'modified_at';
	
	/**
	 * Holds the name of the field that tells us which user created this model.
	 * 
	 * @var	string
	 */
	const CREATED_BY_FIELD = 'created_by';
	
	/**
	 * Holds the name of the field that tells which user last modified this 
	 * model.
	 * 
	 * @var	string
	 */
	const MODIFIED_BY_FIELD = 'modified_by';
	
	/**
	 * A standardized column name that defines how the models should be sorted.
	 * 
	 * @var string
	 */
	const ORDERING_FIELD = 'ordering';
	
	/**
	 * An array containing all function names defined in this class. This array
	 * can be used to differentiate between user-defined methods and system
	 * methods.
	 * 
	 * @var	string[]
	 */
	protected static $baseMethods;

	/**
	 * Keeps track of whether the validation rules have been requested at least
	 * once. If not, the first time they are requested, some preprocessing has 
	 * to take place to make sure all the validation rules are in the expected
	 * format.
	 * 
	 * @var	boolean
	 */
	protected $validationCached = false;
	
	/**
	 * Holds all the validation rules for this model.
	 * 
	 * @var	array
	 */
	protected $validation = [];
	
	/**
	 * Holds all the error messages generated as a result of the validation
	 * rules.
	 * 
	 * @var	array
	 */
	protected $errors = [];
	
	/**
	 * Holds all the fields of this model after we've parsed them, so as to
	 * ensure that they are formatted properly. Every key should be a column 
	 * name where every value says something about the data type. This variable
	 * should only be used internally and should therefore not be manually
	 * overwritten in custom child classes.
	 * 
	 * @var	array
	 */
	private $structure;
	
	/**
	 * Holds all the fields of this model. Every key should be a column name
	 * where every value is the corresponding SQL type.
	 * 
	 * @var	string[]
	 */
	protected $fields = [];
	
	/**
	 * Holds all the data for this model.
	 * 
	 * @var	array
	 */
	protected $data = [];
	
	/**
	 * Holds all the field names that should be fillable by mass assignment. By
	 * default, all fields are fillable.
	 * 
	 * @var	string[]
	 */
	protected $fillable;
	
	/**
	 * Holds all the field names that should not be fillable by mass assignment.
	 * By default, no fields are guarded.
	 * 
	 * @var	string[]
	 */
	protected $guarded;
	
	/**
	 * Holds an array of all fields that should be hidden by default from the
	 * application, meaning they are not retrieved from the database when the
	 * entire model instance is retrieved.
	 * 
	 * @var	string[]
	 */
	protected $hidden = [];
	
	/**
	 * Holds all the instantiated relations for this model.
	 * 
	 * @var	\Rozyn\Relation\Relation[]
	 */
	private $relations = [];
	
	/**
	 * An array containing all the relations that should be eagerly loaded. May
	 * contain nested relations as well, as long as the format of the array is
	 * compatible with the ModelQuery::with() method.
	 * 
	 * @var	array
	 */
	protected $eager = [];
	
	/**
	 * The name of the database table that should be used for this model.
	 * 
	 * @var	string
	 */
	protected $table;
	
	/**
	 * A prefix used to namespace this model.
	 * 
	 * @var	string
	 */
	protected $prefix = '';
	
	/**
	 * The alias used for this model in database queries.
	 * 
	 * @var	string
	 */
	protected $alias;
	
	/**
	 * The foreign key by which model instances can be identified in foreign
	 * tables.
	 * 
	 * @var	string
	 */
	protected $foreignKey;
	
	/**
	 * The name of the field that is used as the primary key.
	 * 
	 * @var	string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * An array with conditions that are applied to all queries that are
	 * executed from within this model.
	 * 
	 * @var	array
	 */
	protected $conditions = [];
	
	/**
	 * Holds an array of all dirty fields in the model so that we know which 
	 * columns to update when we save the model.
	 * 
	 * @var	string[]
	 */
	protected $dirty = [];
	
	/**
	 * Specifies the name of the field that is used for soft deletion. If set to
	 * false, no soft deletion is used for this model. If set to true, the 
	 * system's default soft deletion column name is used.
	 * 
	 * @var	mixed
	 */
	protected $soft = true;
	
	/**
	 * Specifies whether or not to add timestamp fields to this model.
	 * 
	 * @var	boolean
	 */
	protected $timestamps = false;
	
	/**
	 * Specifies whether or not to add authentication fields to this model.
	 * 
	 * @var	boolean
	 */
	protected $authenticated = false;
	
	/**
	 * Holds a JoinModel instance that represents the record in a join table 
	 * linking this model to another model. If this model was not retrieved
	 * through a parent of one of its HasAndBelongsToMany relations then this 
	 * attribute will be null.
	 * 
	 * @var	\Rozyn\Model\JoinModel
	 */
	protected $joinModel;
	
	/**
	 * The default column by which rows in this table need to be sorted.
	 * 
	 * @var	string
	 */
	protected $orderField;
	
	/**
	 * The default order direction in which rows in this table need to be sorted.
	 * 
	 * @var	string
	 */
	protected $orderDir;

	/**
	 * Constructs a new model instance by trying to mass assign the specified 
	 * attributes.
	 * 
	 * @param	array	$attributes
	 */
	public function __construct(array $attributes = []) {
		$this->setup();
		$this->init();

		$this->fill($attributes);
	}

	/**
	 * A function that can be extended in subclasses. This will be called every 
	 * time a new instance is created, so use it to perform any and all startup
	 * logic.
	 */
	public function init() {}
	
	/**
	 * A core setup function that is used to set up the model properly. We don't 
	 * use init() for this, because then we'd have to call parent::init() in 
	 * each and every custom child class.
	 */
	protected function setup() {
		// An array of all fields to be added dynamically based on this model's
		// properties.
		$fields = [];
		
		// If the model implements timestamps, add the appropriate fields.
		if ($this->timestamps === true) {
			$fields[static::CREATED_AT_FIELD] = 'DATETIME';
			$fields[static::MODIFIED_AT_FIELD] = 'DATETIME';
		}
		
		// If the model implements authentication, add the appropriate fields.
		if ($this->authenticated === true) {
			$fields[static::CREATED_BY_FIELD] = 'INT';
			$fields[static::MODIFIED_BY_FIELD] = 'INT';
		}

		// If the model uses soft-deletion, make sure that it contains a 
		// soft delete field.
		if ($this->soft === true) {
			$fields[static::SOFT_DELETE_FIELD] = 'TINYINT(1)';
		}
		
		// Add all additional fields.
		if (!empty($fields)) {
			$this->addFields($fields);
		}
	}
	
	/**
	 * Returns a new instance of this model by retrieving the corresponding record
	 * from the Database using the specified ID. If the specified $id is an array,
	 * all records that match one of the IDs in that array are returned as a
	 * ModelCollection.
	 * 
	 * @param	int|array	$id
	 * @param	array		$with
	 * @return	\Rozyn\Model\Model|\Rozyn\Model\Collection
	 */
	public static function read($id, array $with = []) {
		$self	= new static;
		$query	= $self->newQuery()->select()->with($with);
		
		return (is_array($id)) ? $query->whereIn($self->getAliasedPrimaryKey(), $id)->fetchModels() :
								 $query->whereEquals($self->getAliasedPrimaryKey(), $id)->fetchModel();
	}

	/**
	 * Returns a new ModelCollection containing all instances of this model stored in the database.
	 * 
	 * @return	\Rozyn\Model\Collection
	 */
	public static function all() {
		return static::query()->select()->fetchModels();
	}

	/**
	 * Returns a new ModelCollection containing all instances of this model that match the where clause.
	 * 
	 * @param	mixed					$conditions
	 * @param	array					$vars
	 * @param	string					$orderColumn
	 * @param	string					$orderDirection
	 * @return	\Rozyn\Model\Collection
	 */
	public static function find($conditions, array $vars = [], $orderColumn = null, $orderDirection = 'ASC') {
		$query	= static::query()->select();
		$self	= $query->getModel();
		
		if ($orderColumn === null) {
			$orderColumn = $self->getAliasedPrimaryKey();
		}
		
		return $query->where($conditions)
					 ->bind($vars)
					 ->orderBy($orderColumn, $orderDirection)
					 ->fetchModels();
	}
	
	/**
	 * Returns a list containing all the model ids as keys and a specified 
	 * column as the corresponding value.
	 * 
	 * @param	mixed	$conditions
	 * @param	array	$vars
	 * @return	\Rozyn\Model\Model
	 */
	public static function first($conditions = [], array $vars = []) {
		return self::instance()	->newQuery()
								->select()
								->where($conditions)
								->bind($vars)
								->limit(1)
								->fetchModel();
	}
	
	/**
	 * Returns the first model that matches the given conditions.
	 * 
	 * @param	string	$listColumn
	 * @param	mixed	$conditions
	 * @param	array	$vars
	 * @return	array
	 */
	public static function listing($listColumn = null, $orderColumn = null, $orderDirection = 'ASC', $conditions = [], array $vars = []) {
		$query = static::query()->select();
	
		if ($listColumn && !$orderColumn) {
			$orderColumn = $listColumn;
		}
		
		$query->orderBy($orderColumn, $orderDirection);
		
		return $query->where($conditions)->bind($vars)->fetchList($listColumn);
	}

	/**
	 * Returns a new instance of this model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public static function instance() {
		return new static;
	}
	
	/**
	 * Returns a new proxy instance of this model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public static function proxy() {
		return static::instance();
	}

	/**
	 * Returns a new ModelCollection based on this model.
	 * 
	 * @param	array					$items
	 * @return	\Rozyn\Model\Collection
	 */
	public static function collection(array $items = []) {
		return static::instance()->newCollection($items);
	}

	/**
	 * Returns a new ModelQuery based on this model.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public static function query() {
		return static::instance()->newQuery();
	}
	
	/**
	 * Returns a new Paginator based on this model.
	 * 
	 * @param	int		$perPage
	 * @param	string	$url
	 * @param	array	$conditions
	 * @return	\Rozyn\Pagination\QueryPaginator
	 */
	public static function paginate($perPage = null, $url = null, array $conditions = []) {
		return static::instance()->newPaginator($perPage, $url, $conditions);
	}
	
	/*
	 * Deletes the model from the database. If soft deletion is supported, the
	 * model is not actually deleted from the database, only its soft delete
	 * column is set to 1, unless $hard is set to TRUE. In that case, it will 
	 * still be deleted from the database completely.
	 * 
	 * @param	boolean	$hard
	 * @return	boolean
	 */
	public function delete($hard = false) {
		if (!$this->id()) {
			return false;
		}
		
		if (!$this->isSoft() || $hard) {
			$success = $this->newQuery()->delete()->where($this->getPrimaryKey() . ' = ' . $this->id())->limit(1)->execute();
			unset($this->data[$this->getPrimaryKey()]);
			
			return $success;
		}
		
		$this->set(static::SOFT_DELETE_FIELD, true);
		
		return $this->save();
	}
	
	/**
	 * Restores a model if it has been previously soft deleted.
	 * 
	 * @return	boolean
	 */
	public function restore() {
		if ($this->isDeleted()) {
			$this->set(static::SOFT_DELETE_FIELD, 0);
			
			return $this->save();
		}
		
		return true;
	}
	
	/*
	 * Deletes the model and all of its related models from the database in much
	 * the same way the regular delete() method deletes a single model.
	 * 
	 * @param	boolean	$hard
	 * @return	boolean
	 */
	public function deleteAll($hard = false) {
		// First, delete all related instances.
		$this->transact(function($self) use ($hard) {
			// Loop through the relations and delete all relations plus their
			// models.
			foreach ($self->getRelationData() as $name => $related) {
				$self->getRelation($name)->clear();

				if (null !== $related) {
					$related->deleteAll($hard);
				}
			}
		});
		
		// Delete this model.
		return $this->delete($hard);
	}

	/**
	 * Saves the model in the database. If no ID is specified, an INSERT is 
	 * performed. Otherwise UPDATE is used.
	 * 
	 * @return	boolean
	 */
	public function save() {
		// Only perform an SQL query if the model is actually dirty or if it 
		// doesn't exist yet.
		if (!$this->isDirty() && $this->exists()) {
			return true;
		}
		
		$query = $this->newQuery();

		// Set some magic fields.
		$this->set(static::MODIFIED_AT_FIELD, now(DATETIME_FORMAT_SQL));
		$this->set(static::MODIFIED_BY_FIELD, Auth::id());

		// Check if the model already exists.
		if ($this->exists()) {
			// Compute which updates need to be executed based on which
			// fields have been marked as dirty.
			$updates = $this->getDirtyData();
			
			// Remove the primary key from the $updates array if it's in there.
			if (isset($updates[$this->getPrimaryKey()])) {
				unset($updates[$this->getPrimaryKey()]);
			}
			
			// Make sure there is something to update before we send the query.
			if (empty($updates)) {
				return true;
			}
			
			// Send an update query to update all the model's dirty fields.
			$query	->update()
					->set($updates)
					->where($this->getPrimaryKey() . '=:id')
					->bind('id', $this->id())
					->execute();
		}

		else {
			// Set some default values for so-called magic fields.
			$this->set(static::CREATED_AT_FIELD, now(DATETIME_FORMAT_SQL));
			$this->set(static::CREATED_BY_FIELD, Auth::id());

			// If no ID has been set for the model yet, that means the 
			// model is not yet stored in our database, meaning we have to
			// perform an INSERT query (as opposed to an UPDATE query).
			$query	->insert()
					->values($this->getDataForQuery())
					->execute();
		}

		// Get the last inserted ID
		$id = $query->lastInsertId();

		// If the ID is valid, update the model's ID.
		if ((is_int($id) || is_string($id) && preg_match('/^\d+$/', $id) === 1) && intval($id) > 0) {
			$this->forceSet($this->getPrimaryKey(), $id);
		}

		// Mark the model as clean.
		$this->clean();

		// Since our query was a success, return true.
		return true;
	}
	
	/**
	 * Saves the related data for this model, except for the relations in $except.
	 * 
	 * @param	string[]		$except
	 */
	public function saveRelations(array $except = []) {
		foreach ($this->data as $key => $value) {
			// Make sure that if we have related data we also have its corresponding 
			// relation in our $relations array so that it is properly saved when this 
			// method is called. The line below forces the related data to be loaded.
			!$this->hasField($key) && $this->hasRelation($key);
		}
		
		$relations = array_diff_key ($this->getRelations(), array_flip($except));
		$this->transact(function() use ($relations) {
			// Loop through the relations.
			foreach ($relations as $name => $relation) {
				$relation->sync($this->getRelationData($name));
			}
		});
	}

	/**
	 * Saves the model and all of its related models in the database.
	 */
	public function saveAll() {
		// First, save our current model so that we can retrieve its ID for the 
		// related model(s).
		$this->save();
		
		// Save the related data
		$this->saveRelations();
	}
	
	/**
	 * Touches the model, updating its modified_at field if it exists.
	 */
	public function touch() {
		if ($this->hasField(static::MODIFIED_AT_FIELD)) {
			$this->dirty(static::MODIFIED_AT_FIELD);
		}
	}

	/**
	 * Returns the ID associated with this model, or NULL if no ID has been set 
	 * yet.
	 * 
	 * @return	int
	 */
	public function id() {
		return ($this->exists()) ? $this->data[$this->getPrimaryKey()] : null;
	}
	
	/**
	 * Returns whether or not the model exists in the database by checking if its
	 * primary key has a value associated with it.
	 * 
	 * @return	boolean
	 */
	public function exists() {
		return isset($this->data[$key = $this->getPrimaryKey()]);
	}

	/**
	 * Returns a new instance of this model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function newInstance() {
		return new static;
	}
	
	/**
	 * Returns a new proxy instance of this model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function newProxy() {
		return static::proxy();
	}

	/**
	 * Returns a new ModelCollection based on this model.
	 * 
	 * @param	array					$items
	 * @return	\Rozyn\Model\Collection
	 */
	public function newCollection(array $items = []) {
		return new ModelCollection($items, $this);
	}

	/**
	 * Returns a new ModelQuery based on this model.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function newQuery() {
		$query = new ModelQuery($this);
	
		$query->with($this->getEagerRelations());
		
		return $query;
	}
	
	/**
	 * Returns a new Query object unrelated to this model.
	 * 
	 * @return	\Rozyn\Database\Query
	 */
	public function newPlainQuery() {
		return DI::getInstanceOf('Rozyn\Database\Query');
	}
	
	/**
	 * Returns a new Paginator object for this model.
	 * 
	 * @param	int								$perPage
	 * @param	string							$url
	 * @param	array							$conditions
	 * @return	\Rozyn\Pagination\QueryPaginator
	 */
	public function newPaginator($perPage = null, $url = null, array $conditions = []) {
		$paginator = DI::getInstanceOf('Rozyn\Pagination\ModelPaginator', [$this->newQuery()->select(), $perPage, $url]);
		
		$paginator->getQuery()->where($conditions);
		
		return $paginator;
	}

	/**
	 * Starts a database transaction to group several database actions 
	 * efficiently.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function beginTransaction() {
		Database::beginTransaction();
		
		return $this;
	}

	/**
	 * Ends a database transaction to commit all database actions in the 
	 * transaction simultaneously.
	 *
	 * @return	\Rozyn\Model\Model
	 */
	public function endTransaction() {
		Database::endTransaction();
		
		return $this;
	}
	
	/**
	 * Executes the passed callback inside a new transaction so that all queries 
	 * generated inside this callback will be sent to the server simultaneously.
	 * 
	 * @param	\Closure	$cb
	 * @return	\Rozyn\Model\Model
	 */
	public function transact(Closure $cb) {
		$this->beginTransaction();
		
		$cb($this);
		
		return $this->endTransaction();
	}

	/**
	 * Creates a new hasOne relation for this model and returns it.
	 * 
	 * @param	\Rozyn\Model\Model|string	$related
	 * @param	array						$options
	 * @return	\Rozyn\Relation\HasOne
	 */
	public function hasOne($related, array $options = []) {
		return $this->relation('HasOne', $related, $options);
	}

	/**
	 * Creates a new hasMany relation for this model.
	 * 
	 * @param	\Rozyn\Model\Model|string	$related
	 * @param	array						$options
	 * @return	\Rozyn\Model\Model
	 */
	public function hasMany($related, array $options = []) {
		return $this->relation('HasMany', $related, $options);
	}

	/**
	 * Creates a new belongsTo relation for this model.
	 * 
	 * @param	\Rozyn\Model\Model|string	$related
	 * @param	array						$options
	 * @return	\Rozyn\Model\Model
	 */
	public function belongsTo($related, array $options = []) {
		return $this->relation('BelongsTo', $related, $options);
	}

	/**
	 * Creates a new hasManyToMany relation for this model.
	 * 
	 * @param	\Rozyn\Model\Model|string	$related
	 * @param	array						$options
	 * @return	\Rozyn\Model\Model
	 */
	public function hasAndBelongsToMany($related, array $options = []) {
		return $this->relation('HasAndBelongsToMany', $related, $options);
	}

	/**
	 * Creates a new relation for this model and returns it.
	 * 
	 * @param	string						$type
	 * @param	\Rozyn\Model\Model|string	$related
	 * @param	array						$options
	 * @return	\Rozyn\Model\Model
	 */
	protected function relation($type, $related, array $options = []) {
		$name = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS)[2]['function'];
		
		if (isset($this->relations[$name])) {
			return $this->relations[$name];
		}
		
		if (is_string($related)) {
			$related = new $related();
		}

		$relationClass = NS_RELATIONS . $type;

		return $this->relations[$name] = new $relationClass($name, 
															$this, 
															$related, 
															$options);
	}
	
	/**
	 * Set a relation without using one of the model's built-in relation methods.
	 * 
	 * @param	string						$name
	 * @param	\Rozyn\Relation\Relation	$relation
	 * @return	\Rozyn\Model\Model
	 */
	public function setRelation($name, Relation $relation) {
		$this->relations[$name] = $relation;
		
		return $this;
	}

	/**
	 * Returns an array with all the model's relations. Each key identifies the 
	 * relation by a name.
	 * 
	 * @return	\Rozyn\Relation\Relation[]
	 */
	public function getRelations() {
		return $this->relations;
	}

	/**
	 * Returns a Relation instance stored inside this model based on the given 
	 * name.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Relation\Relation
	 * @throws	\Rozyn\Relation\RelationNotFoundException
	 */
	public function getRelation($name) {
		if (isset($this->relations[$name])) {
			return $this->relations[$name];
		}
		
		if (method_exists($this, $name) && !static::isBaseMethod($name) && ($relation = $this->$name()) instanceof Relation) {
			return $relation;
		}
		
		throw new RelationNotFoundException($name);
	}

	/**
	 * Returns whether or not a Relation with the given name is stored inside 
	 * this model.
	 * 
	 * @param	string	$name
	 * @return	boolean
	 */
	public function hasRelation($name) {
		try {
			$this->getRelation($name);
		} catch (RelationNotFoundException $ex) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Loads the data for a given relation of this model and stores it.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Model\Model
	 */
	public function loadRelation($name) {
		$this->getRelation($name)->load();
		
		return $this;
	}
	
	/**
	 * A built-in relation for all models that implement authentication, i.e.
	 * that have $authenticated set to TRUE.
	 * 
	 * @throws	\Rozyn\Relation\RelationNotFoundException
	 * @return	\Rozyn\Relation\BelongsTo
	 */
	public function creator() {
		if ($this->authenticated === true) {
			return $this->belongsTo(DI::getClassName('Rozyn\Model\Auth\User'), ['foreignKey' => static::CREATED_BY_FIELD]);
		}
		
		throw new RelationNotFoundException("This model does not implement authentication and therefore does not support the creator relation.");
	}
	
	/**
	 * Returns whether or not the model passes its validation rules. If not, any
	 * error messages are stored in the $errors variable which can be accessed
	 * using the getErrors() method.
	 * 
	 * @return	boolean
	 */
	public function validates() {
		$this->errors = [];
		
		if (!Validator::validateModel($this)) {
			$this->errors = Validator::getErrors();
			return false;
		}
		
		return true;
	}
	
	/**
	 * Returns any error messages that were generated as a result of a validation
	 * check.
	 * 
	 * @return	array
	 */
	public function getErrors() {
		return $this->errors;
	}
	
	/**
	 * Returns whether or not this model contains error messages.
	 * 
	 * @return	boolean
	 */
	public function hasErrors() {
		return !empty($this->errors);
	}

	/**
	 * Returns all validation rules stored for this model, or just the rules for
	 * the specified attribute if one is provided.
	 * 
	 * @param	string	$attr
	 * @return	array
	 */
	public function getValidationRules($attr = null) {
		// If this is the first time the validation rules are requested, perform
		// some preprocessing logic.
		if (!$this->validationCached) {
			$this->cacheValidationRules();
		}
		
		if (null === $attr) {
			return $this->validation;
		}

		if (isset($this->validation[$attr])) {
			return $this->validation[$attr];
		}
		
		return [];
	}
	
	/**
	 * Performs the preprocessing logic for our validation rules. This allows us
	 * to specify our validation rules very freely, since they are sure to be
	 * parsed by this method before they are used anyway.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function cacheValidationRules() {
		foreach ($this->validation as &$rules) {
			if (!is_array($rules)) {
				$newRules = [];

				foreach (explode('|', $rules) as $i => $ruleAndParams) {
					$tmp = explode(':', $ruleAndParams);
					$newRules[$tmp[0]] = (isset($tmp[1])) ? explode(',', $tmp[1]) : [];
				}

				$rules = $newRules;
			}
			
			foreach ($rules as $rule => &$params) {
				if (is_int($rule)) {
					unset($rules[$rule]);
					$rules[$params] = [];
				}

				if (!is_array($params)) {
					$params = [$params];
				}
			}
		}

		$this->validationCached = true;
		
		return $this;
	}
	
	/**
	 * Returns whether or not the model has been soft deleted.
	 * 
	 * @return	boolean
	 */
	public function isDeleted() {
		return (bool)$this->get(static::SOFT_DELETE_FIELD, false);
	}
	
	/**
	 * Returns whether or not the model supports soft deletion.
	 * 
	 * @return	boolean
	 */
	public function isSoft() {
		return $this->soft === true;
	}
	
	/**
	 * Returns the aliased column name used for soft deletion.
	 * 
	 * @return	string
	 */
	public function getAliasedSoftDeleteField() {
		return $this->getAliasedField(static::SOFT_DELETE_FIELD);
	}
	
	/**
	 * Set the prefix for this model.
	 * 
	 * @param	string	$prefix
	 */
	public function setPrefix($prefix) {
		$this->prefix = $prefix;
	}
	
	/**
	 * Returns the prefix for this model.
	 * 
	 * @return	string
	 */
	public function getPrefix() {
		return $this->prefix;
	}

	/**
	 * Sets the name of the database table associated with this model.
	 * 
	 * @param	string	$table
	 */
	public function setTable($table) {
		$this->table = $table;
	}

	/**
	 * Returns the name of the database table associated with this model.
	 * 
	 * @return	string
	 */
	public function getTable() {
		if (null === $this->table) {
			$this->table = $this->inferTable();
		}
		
		return $this->prefix . $this->table;
	}
	
	/**
	 * Tries to guess the database table name for this model.
	 * 
	 * @return	string
	 */
	public function inferTable() {
		return camel2snake(get_class_name($this));
	}

	/**
	 * Returns the name of the table with its alias.
	 * 
	 * @return	string
	 */
	public function getAliasedTable() {
		return $this->getTable() . ' AS ' . $this->getAlias();
	}

	/**
	 * Sets the alias for the database table of this model.
	 * 
	 * @param	string
	 */
	public function setAlias($alias) {
		$this->alias = $alias;
	}

	/**
	 * Returns the alias of the database table associated with this model or the
	 * name of the model if no alias was set.
	 * 
	 * @return	string
	 */
	public function getAlias() {
		if (null === $this->alias) {
			$this->alias = get_class_name($this);
		}
		
		return $this->alias;
	}

	/**
	 * Returns the name of the primary key column.
	 * 
	 * @return	string
	 */
	public function getPrimaryKey() {
		return $this->primaryKey;
	}

	/**
	 * Fills the model's data array with attributes.
	 * 
	 * @param	array	$attributes
	 * @return	\Rozyn\Model\Model
	 */	
	public function fill(array $attributes) {
		if (!empty($attributes)) {
			foreach (array_intersect_key($attributes, $this->fillable()) as $key => $value) {
				$this->set($key, $value);
			}
		}

		return $this;
	}
	
	/**
	 * Returns the fillable properties of this model.
	 * 
	 * @return	array
	 */
	public function fillable() {
		if (null === $this->fillable) {
			$fields = $this->getFields();
			$this->fillable = array_combine($fields, $fields);
		}
		
		if (null !== $this->guarded) {
			$this->fillable = array_diff_key($this->fillable, array_merge([$this->getPrimaryKey => ''], array_flip($this->guarded)));
		}
		
		return $this->fillable;
	}

	/**
	 * Returns the structure of this model's database table.
	 * 
	 * @return	array
	 */
	public function getStructure() {
		if (null === $this->structure) {
			$this->structure = (is_int(key($this->fields))) ? array_fill_keys($this->fields, false) : $this->fields;
		}
		
		return $this->structure;
	}
	
	/**
	 * Returns the type of the given field.
	 * 
	 * @param	string	$field
	 * @return	string
	 */
	public function getFieldType($field) {
		$structure = $this->getStructure();
		
		if (!$this->hasField($field)) {
			throw new UnknownFieldException($field);
		}
		
		return ($structure[$field] === null) ? 
					$this->inferFieldType($field) :
					$structure[$field];
	}
	
	/**
	 * Tries to guess the type of a given field based on the field name.
	 * 
	 * @param	string	$field
	 * @return	string
	 */
	public function inferFieldType($field) {
		if (ends_with($field, '_id') || 
			ends_with($field, '_by')) {
				return 'INT';
		} 

		if (starts_with($field, 'is_') || 
			starts_with($field, 'has_')) {
				return'TINYINT';
		} 

		if (ends_with($field, '_at') || 
			ends_with($field, '_until') || 
			ends_with($field, '_from')) {
				return 'DATETIME';
		} 
		
		return 'VARCHAR';
	}

	/**
	 * Returns all the fields stored in this model.
	 * 
	 * @return	array
	 */
	public function getFields() {
		return array_keys($this->getStructure());
	}

	/**
	 * Returns whether or not the model has the specified field.
	 * 
	 * @param	string	$field
	 * @return	boolean
	 */
	public function hasField($field) {
		return array_key_exists($field, $this->getStructure());
	}

	/**
	 * Returns one of the attributes stored in this model. If the given 
	 * attribute is not found, a user-specified default value is returned 
	 * instead. If no key is given at all, the entire data array of the model is
	 * returned instead, minus the relationships.
	 * 
	 * @param	string			$key
	 * @param	mixed			$default
	 * @return	mixed|array
	 */
	public function getData($key = null, $default = null) {
		return (null === $key) ? 
					array_intersect_key($this->data, $this->getStructure()) : 
					$this->get($key, $default);
	}

	/**
	 * Returns the data associated with the specified relation.
	 * 
	 * @param	string	$key
	 * @param	boolean	$load
	 * @return	Rozyn\Model|Rozyn\Model\Collection
	 */
	public function getRelationData($key = null, $load = true) {
		// If no key was specified, return all data in our $data variable that
		// corresponds to relation data.
		if (null === $key) {
			return array_intersect_key($this->data, $this->relations);
		}
		
		// Make sure a method with the given $key exists, otherwise we can 
		// immediately conclude no relationship with this name exists.
		if (!method_exists($this, $key)) {
			throw new RelationNotFoundException($key);
		}
		
		// If no relation data exists yet, retrieve it.
		if (!isset($this->data[$key])) {
			// Find the requested relation. If the relation does not exist, a
			// RelationNotFoundException will be thrown from inside this
			// function.
			$relation = $this->getRelation($key);
			
			// Set the relation data.
			$this->forceSet($key, ($load === true) ? $relation->load() : $relation->newEmpty());
		}

		return $this->data[$key];		
	}

	/**
	 * Returns the relation fields that have been filled.
	 * 
	 * @return	array
	 */
	public function getRelationDataFields() {
		return array_keys($this->getRelationData());
	}
	
	/**
	 * Returns the instances of one particular nested relation that are 
	 * associated with this model.
	 * 
	 * @param	string			$args
	 * @return	\Rozyn\Model\Collection
	 */
	public function getNestedRelationData($args) {
		$nest = (func_num_args() === 1 && is_string($args) && strpos($args, '.') !== false) ?
					explode('.', $args) :
					func_get_args();
		
		if (count($nest) < 2) {
			return InvalidNestedRelationFormatException("Nested relations need at least 2 direct relations.");
		}
		
		// Convert the nest array to a key.
		$key = implode('.', $nest);
		
		// See if we've cached the result.
		if (!$this->has($key)) {
			// Initialize our variables.
			$models	= [$this->id() => null];
			$parent = $this;

			// Loop through all nesting levels and retrieve all related data
			// for each level.
			foreach ($nest as $relationName) {
				if (!is_string($relationName)) {
					throw new InvalidNestedRelationFormatException(json_encode($relationName));
				}
				
				$relation = $parent->getRelation($relationName);

				$models	= $relation->loadBatchArray(array_keys($models));
				$parent	= $relation->getRelated();
			}

			$this->set($key, $parent->newQuery()->objectify($models));
		}
		
		return $this->get($key);
	}
	
	/**
	 * Returns the eager relations for this Model.
	 * 
	 * @return	array
	 */
	public function getEagerRelations() {
		return $this->eager;
	}
	
	/**
	 * Add a single eager relation.
	 * 
	 * @param	string	$relation
	 */
	public function addEagerRelation($relation) {
		$this->addEagerRelations([$relation]);
	}
	
	/**
	 * Add eager relations.
	 * 
	 * @param	array	$relations
	 */
	public function addEagerRelations(array $relations) {
		$this->eager = array_merge_recursive($this->eager, $relations);
	}

	/**
	 * Returns all the values stored in this model that correspond to a database
	 * column. If a value hasn't been specifically set, NULL is used instead.
	 * In contrast to the getFieldsForQuery() method this method does take into
	 * account hidden fields. That means if a value is set for a hidden field,
	 * it will be saved to the database and thus overwrite the old value for 
	 * this hidden field.
	 * 
	 * @return	array
	 */
	public function getDataForQuery() {
		return array_replace(array_fill_keys($this->getFields(), null), array_intersect_key($this->data, $this->getStructure()));
	}

	/**
	 * Returns one of the attributes stored in this model. If the given attribute is not found,
	 * a user-specified default value is returned instead.
	 * 
	 * @param	string	$key		The attribute you wish to retrieve.
	 * @param	mixed	$default	The default value to return for when the attribute doesn't exist.
	 * @return	mixed	Returns the attribute matching the key or the default value if the key doesn't exist.
	 */
	public function get($key, $default = null) {
		if (isset($this->data[$key])) {
			return $this->data[$key];
		}
		
		if (!$this->hasField($key)) {
			try {
				return $this->getRelationData($key);
			} catch (RelationNotFoundException $e) {}
		}
		
		return $default;
	}

	/**
	 * Sets an attribute in our data array.
	 * 
	 * @param	string|array	$key
	 * @param	mixed			$value
	 * @return	\Rozyn\Model\TranslatableModel
	 */
	public function set($key, $value) {
		// If the new value corresponds to a field (and not a Relation) and 
		// if it differs from the old value, mark the model as dirty. Here, we 
		// use the weak inequality check (!=) instead of the strong check (!==)
		// because that way we don't necessarily have to ensure that our PHP
		// data type is always exactly equal to the MySQL data type.
		if (!$this->isDirty($key) && $this->hasField($key) && $this->get($key) != $value) {
			$this->dirty($key);
		}

		$this->data[$key] = $value;

		return $this;
	}

	/**
	 * Returns whether or not this model has a data field with the given key.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function has($key) {
		return (isset($this->data[$key]) && $this->data[$key] !== null && (!$this->data[$key] instanceof \Rozyn\Model\Collection || !$this->data[$key]->isEmpty()));
	}

	/**
	 * Mark a model field as dirty.
	 * 
	 * @param	string|null	$key
	 * @return	\Rozyn\Model\Model
	 */
	public function dirty($key) {
		$this->dirty[$key] = true;

		return $this;
	}

	/**
	 * Mark the entire model as clean.
	 * 
	 * @param	string|null	$key
	 * @return	\Rozyn\Model\Model
	 */
	public function clean($key = null) {
		if ($key === null) {
			$this->dirty = [];
		} else {
			unset($this->dirty[$key]);
		}

		return $this;
	}

	/**
	 * Returns whether or not the model has been altered after being loaded initially.
	 * 
	 * @param	string|null	$key
	 * @return	boolean
	 */
	public function isDirty($key = null) {
		return (null === $key) ? 
					!empty($this->dirty) :
					isset($this->dirty[$key]);
	}

	/**
	 * Returns whether or not the model is clean. If $key is specified, it 
	 * checks if that particular $key is still clean.
	 * 
	 * @param	string|null	$key
	 * @return	boolean
	 */
	public function isClean($key = null) {
		return !$this->isDirty($key);
	}
	
	/**
	 * Sets the JoinModel for this model if it is defined.
	 * 
	 * @param	\Rozyn\Model\JoinModel	$joinModel
	 */
	public function setJoinModel(JoinModel $joinModel) {
		$this->joinModel = $joinModel;
	}
	
	/**
	 * Returns the JoinModel for this model if it is defined, otherwise returns
	 * null.
	 * 
	 * @return	\Rozyn\Model\JoinModel
	 */
	public function getJoinModel() {
		return $this->joinModel;
	}
	
	/**
	 * Returns whether or not this model has an associated JoinModel.
	 * 
	 * @return	boolean
	 */
	public function hasJoinModel() {
		return $this->joinModel !== null;
	}

	/**
	 * Returns the foreign key for this model. If no foreign key is specified, a foreign
	 * key is guessed based on the name of this model.
	 * 
	 * @return	string
	 */
	public function getForeignKey() {
		return $this->foreignKey ? : $this->foreignKey = $this->inferForeignKey();
	}

	/**
	 * Guesses the foreign key in another model's table based on this model's name.
	 * 
	 * @return	string
	 */
	public function inferForeignKey() {
		return camel2snake(get_class_name($this)) . '_id';
	}
	
	/**
	 * Returns the names of all dirty fields.
	 * 
	 * @return	string[]
	 */
	public function getDirtyFields() {
		return array_keys($this->dirty);
	}
	
	/**
	 * Returns all dirty data.
	 * 
	 * @return	array
	 */
	public function getDirtyData() {
		return array_intersect_key($this->data, $this->dirty);
	}

	/**
	 * Returns an array with all registered fields for this model, prefixed with
	 * the model's alias (excluding the hidden fields).
	 * 
	 * @return	string[]
	 */
	public function getFieldsForQuery() {
		$res = [];

		foreach (array_diff($this->getFields(), $this->getHiddenFields()) as $field) {
			$res[$this->getAliasedField($field)] = $field;
		}

		return $res;
	}
	
	/**
	 * Returns a list of all fields that should remain hidden from the
	 * application.
	 * 
	 * @return	string[]
	 */
	public function getHiddenFields() {
		return $this->hidden;
	}

	/**
	 * Returns an array with all registered fields for this model, prefixed with the
	 * model's alias.
	 * 
	 * @return	string[]
	 */
	public function getAliasedFields() {
		$res = [];

		foreach ($this->getFields() as $field) {
			$res[] = $this->getAliasedField($field);
		}

		return $res;
	}

	/**
	 * Returns the name of a field which can be used directly in an SQL query. It takes into
	 * account a possible Alias for this table and makes sure to prefix that field with that
	 * alias.
	 * 
	 * @param	string	$field
	 * @return	string
	 */
	public function getAliasedField($field) {
		return $this->getAlias() . '.' . $field;
	}

	/**
	 * Returns the field that holds the model's primary key, prefixed with the model's alias.
	 *  
	 * @return	string
	 */
	public function getAliasedPrimaryKey() {
		return $this->getAliasedField($this->getPrimaryKey());
	}

	/**
	 * Returns the field that holds the model's foreign key, prefixed with the model's alias.
	 *  
	 * @return	string
	 */
	public function getAliasedForeignKey() {
		return $this->getAliasedField($this->getForeignKey());
	}
	
	/**
	 * Returns the default ordering field.
	 * 
	 * @return	string
	 */
	public function getDefaultOrderField() {
		if ($this->orderField === null) {
			$this->orderField = ($this->hasField(static::ORDERING_FIELD)) ? static::ORDERING_FIELD : $this->getPrimaryKey();
		}
		
		return $this->orderField;
	}
	
	/**
	 * Returns the default order direction.
	 * 
	 * @return	string
	 */
	public function getDefaultOrderDirection() {
		return ($this->orderDir === static::ORDER_DESC) ? 
					static::ORDER_DESC : 
					static::ORDER_ASC;
	}
	
	/**
	 * Add a condition(s) for this model.
	 * 
	 * @param	mixed	$condition
	 * @return	\Rozyn\Model\Model
	 */
	public function addCondition($condition) {
		$this->conditions = array_merge($this->conditions, (is_string($condition)) ? [$condition] : $condition);
		
		return $this;
	}
	
	/**
	 * Add conditions for this model.
	 * 
	 * @param	mixed	$conditions
	 * @return	\Rozyn\Model\Model
	 */
	public function addConditions($conditions) {
		return $this->addCondition($conditions);
	}
	
	/**
	 * Sets the conditions for this model.
	 * 
	 * @param	mixed	$conditions
	 * @return	\Rozyn\Model\Model
	 */
	public function setConditions($conditions) {
		if ($conditions === null) {
			$this->conditions = [];
		}
		
		$this->conditions = (is_string($conditions)) ? [$conditions] : $conditions;
		
		return $this;
	}
	
	/**
	 * Returns an array of all conditions that were set on this model.
	 * 
	 * @return	string[]
	 */
	public function getConditions() {
		if ($this->conditions === null) {
			return [];
		}
		
		return $this->conditions;
	}
	
	/**
	 * Removes all conditions from this model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function dropConditions() {
		$this->conditions = [];
		
		return $this;
	}

	/**
	 * Fills the model's data array with attributes, even if some fields have been
	 * marked as unfillable. You should be very careful when using this function!
	 * 
	 * This function also doesn't mark the model as dirty.
	 * 
	 * @param	array	$attributes
	 * @return	\Rozyn\Model\Model
	 */
	public function forceFill(array $attributes) {
		$this->data = array_merge($this->data, normalize($attributes));

		return $this;
	}

	/**
	 * Sets a single model value.
	 * 
	 * This function also doesn't mark the model as dirty.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 * @return	\Rozyn\Model\Model
	 */
	public function forceSet($key, $value) {
		$this->data[$key] = $value;

		return $this;
	}

	/**
	 * Overwrites the entire $data attribute with the specified array.
	 * 
	 * This function also doesn't mark the model as dirty.
	 * 
	 * @param	array	$data
	 * @return	\Rozyn\Model\Model
	 */
	public function forceData($data) {
		$this->data = $data;

		return $this;
	}
	
	/**
	 * Sets the fields/columns for this model.
	 * 
	 * @param	array	$fields
	 * @param	boolean	$merge
	 * @return	\Rozyn\Model\Model
	 */
	public function setFields(array $fields, $merge = false) {
		return ($merge) ? $this->addFields($fields) : $this->structure = $fields;
	}
	
	/**
	 * Adds fields or columns to this model.
	 * 
	 * @param	array	$fields
	 * @return	\Rozyn\Model\Model
	 */
	public function addFields(array $fields) {
		return $this->structure = array_merge($this->getStructure(), $fields);
	}
	
	/**
	 * Adds a single field or column to this model.
	 * 
	 * @param	string	$field
	 * @param	string	$dataType
	 * @return	\Rozyn\Model\Model
	 */
	public function addField($field, $dataType = null) {
		if ($this->structure === null) {
			$this->structure = $this->getStructure();
		}
		
		return $this->structure[$field] = $dataType;
	}
	
	/**
	 * Removes fields or columns from this model.
	 * 
	 * @param	array	$fields
	 * @return	\Rozyn\Model\Model
	 */
	public function removeFields($fields) {
		$this->structure = array_diff_key($this->getStructure(), array_flip($fields));
		
		return $this;
	}
	
	/**
	 * Removes a single field or column from this model.
	 * 
	 * @param	string	$field
	 * @return	\Rozyn\Model\Model
	 */
	public function removeField($field) {
		unset($this->structure[$field]);
		
		return $this;
	}

	/**
	 * Provides easy access to this model's data by allowing you to access $data values as if they
	 * were public.
	 * 
	 * @param	string	$key
	 * @return	mixed
	 */
	public function __get($key) {
		return $this->get($key);
	}

	/**
	 * Provides easy access to this model's data by allowing you to access $data
	 * values as if they were public.
	 * 
	 * @param	string	$key
	 */
	public function __set($key, $value) {
		$this->set($key, $value);
	}

	/**
	 * Returns a string representation of the Model's data.
	 * 
	 * @return	string
	 */
	public function __toString() {
		return substr(str_replace(array('"', ':', ','), array('', ': ', ',' . PHP_EOL), json_encode($this->getData())), 1, -1);
	}
	
	/**
	 * Specifies how to encode this Model as JSON.
	 * 
	 * @return	array
	 */
	public function jsonSerialize() {
		return $this->getData();
	}
	
	/**
	 * Check if a method is a base method.
	 * 
	 * @return	boolean
	 */
	protected static function isBaseMethod($method) {
		return isset(static::getBaseMethods()[$method]);
	}
	
	/**
	 * Retrieve all function names defined in this class. 
	 * 
	 * @return	string[]
	 */
	protected static function getBaseMethods() {
		if (null === static::$baseMethods) {
			static::$baseMethods = array_combine(get_class_methods(__CLASS__), get_class_methods(__CLASS__));
		}
		
		return static::$baseMethods;
	}
}