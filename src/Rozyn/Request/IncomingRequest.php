<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

use Rozyn\Security\Sanitizer;
use Rozyn\Config\Config;

class IncomingRequest extends BaseRequest {
	/**
	 * Defines what form name should be used for automated CSRF testing.
	 * 
	 * @var	string
	 */
	const CSRF_INPUT_NAME = '_csrf';
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * A Sanitizer object used to sanitize user input associated with this
	 * request.
	 * 
	 * @var	\Rozyn\Security\Sanitizer
	 */
	protected $sanitizer;
	
	/**
	 * Holds all the configuration options for this request.
	 * 
	 * @var	\Rozyn\Config\Config
	 */
	protected $config;
	
	/**
	 * The IP address of the client that sent the Request.
	 * 
	 * @var	string
	 */
	protected $ip;

	/**
	 * The query associated with this request.
	 * 
	 * @var	string
	 */
	protected $query;
	
	/**
	 * Holds the FILES data that was sent along with the request.
	 * 
	 * @var	array
	 */
	protected $files;
	
	/**
	 * Holds the raw unfiltered and unvalidated GET data for this request.
	 * 
	 * @var	array
	 */
	protected $rawGet = [];
	
	/**
	 * Holds the raw unfiltered and unvalidated POST data for this request.
	 * 
	 * @var	array
	 */
	protected $rawPost = [];
	
	/**
	 * Constructs the IncomingRequest.
	 * 
	 * @param	\Rozyn\Security\Sanitizer		$sanitizer
	 * @param	\Rozyn\Config\Config			$config
	 */
	public function __construct(Sanitizer $sanitizer, Config $config) {
		$this->sanitizer = $sanitizer;
		$this->config = $config;
		
		$this->setup();
	}
	
	/**
	 * Retrieve filtered GET input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @param	mixed	$filters
	 * @return	mixed
	 */
	public function get($key = null, $default = null, $filters = null) {
		return $this->getInputData('get', $key, $default, $filters);
	}
	
	/**
	 * Retrieve filtered POST input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @param	mixed	$filters
	 * @return	mixed
	 */
	public function post($key = null, $default = null, $filters = null) {
		return $this->getInputData('post', $key, $default, $filters);
	}
	
	/**
	 * Retrieve raw GET input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @param	mixed	$filters
	 * @return	mixed
	 */
	public function rawGet($key = null, $default = null, $filters = null) {
		return $this->getInputData('rawGet', $key, $default, $filters);
	}
	
	/**
	 * Retrieve raw POST input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @param	mixed	$filters
	 * @return	mixed
	 */
	public function rawPost($key = null, $default = null, $filters = null) {
		return $this->getInputData('rawPost', $key, $default, $filters);
	}
	
	/**
	 * Infer and filter the GET data for this request internally.
	 * 
	 * @return	array
	 */
	public function inferGet() {
		// Filter the input data if necessary.
		return $this->sanitizer->sanitize($_GET, $this->config->read('input_filters'));
	}
	
	/**
	 * Infer and filter the POST data for this request internally.
	 * 
	 * @return	array
	 */
	public function inferPost() {
		// Filter the input data if necessary.
		return $this->sanitizer->sanitize($_POST, $this->config->read('input_filters'));
	}
	
	/**
	 * Retrieves file data for a single uploaded file from the request. If 
	 * multiple files are uploaded under the same name, only the first matching
	 * file is returned as an array. If the specified file is not found, null
	 * is returned.
	 * 
	 * @param	string	$key
	 * @return	mixed
	 */
	public function file($key = null) {
		$files = $this->files();
		
		// If no key was specified, simply return all files.
		if ($key === null) {
			return $files;
		}
		
		// If the key does not match a file, return null.
		if (!isset($files[$key])) {
			return null;
		}
		
		// If there are multiple files for the given key, return the first one.
		if (isset($files[$key][0]) && is_array($files[$key][0]) && $files[$key][0]['error'] === 0) {
			return $files[$key][0];
		}

		// If no errors occurred, return the file matching the key.
		if (isset($files[$key]['error']) && $files[$key]['error'] === 0) {
			return $files[$key];
		}

		// Sometimes the files may be structured differently, so we check for
		// that here.
		if (is_array($files[$key]) && count($files[$key]) === 1) {
			$file = current($files[$key]);
			
			if (is_array($file) && isset($file['error']) && $file['error'] === 0) {
				return $file;
			}
		}
		
		// If all else failed, return null.
		return null;
	}
	
	/**
	 * Retrieves all file data for a specific key, or the entire files array if
	 * no key is specified. The array that is returned will always have numeric 
	 * indices where each value is itself an array representing a single
	 * file.
	 * 
	 * If the specified key is not found, an empty array is returned.
	 * 
	 * @param	string	$key
	 * @return	array
	 */
	public function files($key = null) {
		if ($this->files === null) {
			$files = [];
			foreach ($_FILES as $k => $file) {
				$files[$k] = [];
				if (isset($file['name']) && is_array($file['name'])) {
					$count = count($file['name']);
					
					for ($i = 0; $i < $count; $i++) {
						$name = $file['name'][$i];
						
						if (is_array($name)) {
							break;
						}
						
						$files[$k][$name] = [];
						foreach ($file as $field => $data) {
							$files[$k][$name][$field] = $data[$i];
						}
					}
				}
				
				else {
					$files[$k] = $file;
				}
			}

			$this->files = $files;
		}

		if ($key === null) {
			return $this->files;
		}
		
		if (isset($files[$key])) {
			return (isset($this->files[$key]['error'])) ? [$this->files[$key]] : $this->files[$key];
		}
		
		return [];
	}
	
	/**
	 * Returns the IP address of the client.
	 * 
	 * @return	string
	 */
	public function getIp() {
		if ($this->ip === null) {
			$this->ip = (isset($_SERVER['REMOTE_ADDR']) && preg_match('/^\d+\.\d+\.\d+\.\d+$/', $_SERVER['REMOTE_ADDR'])) ? 
							$_SERVER['REMOTE_ADDR'] : 
							'0.0.0.0';
		}
		
		return $this->ip;
	}
	
	/**
	 * Returns whether or not the current request is secure.
	 * 
	 * @return	boolean
	 */
	public function isSecure() {
		return $this->secure = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || isset($_SERVER['SERVER_PORT']) && intval($_SERVER['SERVER_PORT']) === 443;
	}

	/**
	 * Returns the query associated with this request.
	 * 
	 * @return	string
	 */
	public function getQuery() {
		return $this->query;
	}
	
	/**
	 * Returns an array of allowed input types.
	 * 
	 * @return	string[]
	 */
	protected function getAllowedInputTypes() {
		return array_merge(parent::getAllowedInputTypes(), ['rawGet', 'rawPost']);
	}

	/**
	 * Tries to guess the host.
	 * 
	 * @return	string
	 */
	protected function inferHost() {
		return (isset($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : 'localhost';
	}

	/**
	 * Tries to guess the scheme.
	 * 
	 * @return	string
	 */
	protected function inferProtocol() {
		return ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $this->getPort() === 443) ?
					'https' :
					'http';
	}
	
	/**
	 * Tries to guess the uri.
	 * 
	 * @return type
	 */
	protected function inferUri() {
		return (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
	}
	
	/**
	 * Infers the port for the request.
	 * 
	 * @return	string
	 */
	protected function inferPort() {
		return (isset($_SERVER['SERVER_PORT'])) ? (int)$_SERVER['SERVER_PORT'] : 0;
	}
	
	/**
	 * Retrieve the request method from the request.
	 * 
	 * @return	string
	 */
	protected function inferRequestMethod() {
		$method = strtoupper(filter_input(INPUT_SERVER, 'REQUEST_METHOD'));
		
		if (!$method) {
			if (!isset($_SERVER['REQUEST_METHOD'])) {
				return null;
			}
			
			// Workaround for the PHP bug described at: 
			// http://stackoverflow.com/questions/25232975/php-filter-inputinput-server-request-method-returns-null
			$method = $_SERVER['REQUEST_METHOD'];
		}
		
		if (!$this->isValidRequestMethod($method)) {
			throw new InvalidRequestMethodException($method);
		}
		
		return $method;
	}
	
	/**
	 * Infers the query associated with this request.
	 * 
	 * @return	string
	 */
	public function inferQuery() {
		return preg_replace('/^' . preg_quote(__WWW_ROOT__, '/') . '/', '', (false === ($pos = strpos($this->getUri(), '?'))) ? $this->getUri() : substr($this->getUri(), 0, $pos));
	}
	
	/**
	 * Returns the value of the CSRF token that was submitted by the client or
	 * null if no such token was submitted.
	 * 
	 * @return	string
	 */
	public function getCsrfToken() {
		return $this->post(static::CSRF_INPUT_NAME, null);
	}
	
	/**
	 * Infer headers from global variables and return them.
	 * 
	 * @return	array
	 */
	protected function inferHeaders() {
		if (function_exists('getallheaders')) {
			return getallheaders();
		}
		
		return parent::inferHeaders();
	}
	
	/**
	 * Infer and set all different components that make up this request.
	 */
	protected function setup() {
		$this->rawGet = $_GET;
		$this->rawPost = $_POST;
		
		$this->get = $this->inferGet();
		$this->post = $this->inferPost();

		$this->headers = $this->inferHeaders();
		$this->protocol = $this->inferProtocol();
		$this->host = $this->inferHost();
		$this->uri = $this->inferUri();
		$this->port = $this->inferPort();
		$this->method = $this->inferRequestMethod();
		$this->query = $this->inferQuery();
	}
	
	/**
	 * Retrieve input data from the client of the given type. If $key is set to 
	 * null or if it is omitted, this method returns all input data of the given
	 * type.
	 * 
	 * @param	string	$type
	 * @param	string	$key
	 * @param	mixed	$default
	 * @param	mixed	$filters
	 * @return	mixed
	 */
	protected function getInputData($type, $key = null, $default = null, $filters = null) {
		// Call the parent function to determine the unfiltered result.
		$res = parent::getInputData($type, $key, $default);
		
		// Check if we need to apply any filters to the result and return the
		// (filtered) result afterwards.
		return ($filters === null) ? $res : $this->sanitizer->sanitize($res, $filters);
	}
	
	/**
	 * Alias for getIP().
	 * 
	 * @return	string
	 */
	final public function ip() {
		return $this->getIp();
	}
}