<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

abstract class Transporter implements RequestMethodsInterface {
	use RequestMethods;
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds an error number that helps determine what went wrong in the case of
	 * an error.
	 * 
	 * @var	int
	 */
	protected $errno;
	
	/**
	 * Holds an error message as a string that helps determine what went wrong 
	 * in the case of an error.
	 * 
	 * @var	string
	 */
	protected $errstr;
	
	/**
	 * The default timeout (in seconds) before failing.
	 * 
	 * @var	int
	 */
	protected $timeout = null;
	
	/**
	 * Holds the URL to which the next request should be sent.
	 * 
	 * @var	string
	 */
	protected $url;
	
	/**
	 * Holds all the data to be sent to the server.
	 * 
	 * @var	array
	 */
	protected $data = [];
	
	/**
	 * Sends a Request object.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	abstract public function request(Request $request);
	
	/**
	 * Make a GET request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	abstract public function get(Request $request = null);
	
	/**
	 * Make a POST request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	abstract public function post(Request $request = null);
	
	/**
	 * Make a PUT request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	abstract public function put(Request $request = null);
	
	/**
	 * Make a DELETE request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	abstract public function delete(Request $request = null);
	
	/**
	 * Returns the HTTP status code.
	 * 
	 * @return	int
	 */
	abstract public function getCode();
	
	/**
	 * Set data to be sent to the server for this resource.
	 * 
	 * @param	string|array	$key
	 * @param	mixed			$value
	 */
	public function setData($key, $value = null) {
		if (is_array($key) && func_num_args() === 1) {
			$this->data = $key;
		}
		
		elseif (is_string($key) && func_num_args() === 2) {
			$this->data[$key] = $value;
		}
	}
	
	/**
	 * Retrieve all the data that is to be sent to the server for this resource.
	 * 
	 * @return	array
	 */
	public function getData() {
		return $this->data;
	}
	
	/**
	 * Retrieve all the data that is to be sent to the server for this resource.
	 * 
	 * @return	array
	 */
	final public function data() {
		return $this->getData();
	}
	
	/**
	 * Sets the default timeout for this Transporter.
	 * 
	 * @param	int		$timeout
	 */
	public function setTimeout($timeout) {
		$this->timeout = $timeout;
	}
	
	/**
	 * Returns the default timeout for this Transporter.
	 * 
	 * @return	int
	 */
	public function getTimeout() {
		if (!$this->timeout) {
			$this->setTimeout(ini_get('default_socket_timeout'));
		}
		
		return $this->timeout;
	}
	
	/**
	 * Returns whether or not the response corresponds to a valid HTTP response.
	 * If the $acceptRedirects argument is set to true, redirects are also
	 * considered to be successful.
	 * 
	 * @param	boolean	$acceptRedirects
	 * @return	boolean
	 */
	public function isSuccess($acceptRedirects = true) {
		return ($this->getCode() >= 200 && $this->getCode() < 300) || 
					($acceptRedirects && $this->isRedirect());
	}
	
	/**
	 * Returns whether or not the response is a redirect.
	 * 
	 * @return	boolean
	 */
	public function isRedirect() {
		return $this->getCode() >= 300 && $this->getCode() < 400;
	}
	
	/**
	 * Returns whether or not the response's status code corresponds with a
	 * client error.
	 * 
	 * @return	boolean
	 */
	public function isClientError() {
		return $this->getCode() >= 400 && $this->getCode() < 500;
	}
	
	/**
	 * Returns whether or not the response's status code corresponds with a
	 * server error.
	 * 
	 * @return	boolean
	 */
	public function isServerError() {
		return $this->getCode() >= 500 && $this->getCode() < 600;
	}
	
	/**
	 * Returns whether or not the response's status code corresponds with an
	 * error (client or server).
	 * 
	 * @return	boolean
	 */
	public function isError() {
		return $this->isClientError() || $this->isServerError();
	}
	
	/**
	 * Set the URL for this resource.
	 * 
	 * @param	string	$url
	 */
	public function setUrl($url) {
		$this->reset();
		
		$this->url = $url;
	}
	
	/**
	 * Get the URL for this resource.
	 * 
	 * @return	string
	 */
	public function getUrl() {
		return $this->url;
	}
	
	/**
	 * Reset some of the object variables so that it'll be as if we haven't
	 * executed this curl resource yet.
	 */
	public function reset() {
		$this->data		= [];
		$this->method	= null;
		$this->url		= null;
	}
}