<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

class SocketTransporter extends Transporter {
	/**
	 * A file pointer used to write and read data from the socket stream.
	 * 
	 * @var	resource
	 */
	protected $fp;
	
	/**
	 * Sends a Request object.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 * @return	\Rozyn\Request\SocketTransporter
	 */
	public function request(Request $request) {
		$this->open($request);
		
		if ($this->isOpen()) {
			$headers = [];
			
			$headers[] = str_format("{method} {uri} HTTP/1.1", array(
				'method'	=> $request->getRequestMethod(),
				'uri'		=> $request->getUri(),
			));
			
			$headers[] = str_format("Host: {host}", array(
				'host'	=> $request->getHost(),
			));
			
			$headers[] = "Connection: Close";
			
			foreach ($request->getHeaders() as $label => $value) {
				$headers[] = $label . ': ' . $value;
			}
			
			$this->write(implode("\r\n", $headers) . "\r\n\r\n");
		}
		
		return $this;
	}
	
	/**
	 * Check if we've reached end of file.
	 * 
	 * @return	boolean
	 */
	public function isFinished() {
		return feof($this->fp);
	}
	
	/**
	 * Returns whether or not the socket connection is closed.
	 * 
	 * @return	boolean
	 */
	public function isClosed() {
		return !$this->isOpen();
	}
	
	/**
	 * Returns whether or not the socket connection is open.
	 * 
	 * @return	boolean
	 */
	public function isOpen() {
		return !!$this->fp;
	}
	
	/**
	 * Writes to the socket connection.
	 * 
	 * @param	string	$out
	 */
	public function write($out) {
		if ($this->isOpen()) {
			fwrite($this->fp, $out);
		}
	}
	
	/**
	 * Read a number of bytes from the socket connection.
	 * 
	 * @return	string
	 */
	public function read($length) {
		return fread($this->fp, $length);
	}
	
	/**
	 * Read a line from the socket connection.
	 * 
	 * @return	string
	 */
	public function line() {
		return fgets($this->fp);
	}
	
	/**
	 * Opens a socket connection.
	 * 
	 * @param	string|\Rozyn\Request\Request	$hostOrRequest
	 * @param	int								$port
	 * @return	
	 */
	public function open($hostOrRequest, $port = -1) {
		if ($hostOrRequest instanceof Request) {
			$host = $hostOrRequest->getHost();
			$port = $hostOrRequest->getPort();
		} else {
			$host = $hostOrRequest;
		}
		
		$this->fp = stream_socket_client(	$host . ':' . $port,
											$this->errno, 
											$this->errstr, 
											$this->getTimeout());
		
		return $this->fp;
	}
	
	/**
	 * Close the socket connection.
	 */
	public function close() {
		if ($this->fp && fclose($this->fp)) {
			$this->fp = null;
		}
		
		return $this->isClosed();
	}
	
	/**
	 * Make a GET request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function get(Request $request = null) {
		$request->setRequestMethod(static::HTTP_GET);
		
		return $this->request($request);
	}
	
	/**
	 * Make a POST request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function post(Request $request = null) {
		$request->setRequestMethod(static::HTTP_POST);
		
		$content = http_build_query($request->getData());
		
		return $this->request($request);
	}
	
	/**
	 * Make a PUT request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function put(Request $request = null) {
		return $this->post($request);
	}
	
	/**
	 * Make a DELETE request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function delete(Request $request = null) {
		return $this->post($request);
	}

	public function getCode() {
		throw new \Exception('The SocketTransporter class does not yet implement the getCode() method.', 501);
	}
}