<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

class Request extends BaseRequest {
	/**
	 * Constructs the Request.
	 * 
	 * @param	string	$url
	 */
	public function __construct($url = null) {
		$this->setUrl($url);
	}
	
	/**
	 * Retrieve GET input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function get($key = null, $default = null) {
		return $this->getInputData('get', $key, $default);
	}
	
	/**
	 * Retrieve POST input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function post($key = null, $default = null) {
		return $this->getInputData('post', $key, $default);
	}
	
	/**
	 * Store the POST data for this request internally.
	 * 
	 * @param	array	$data
	 */
	public function setPost(array $data) {
		// Store the data.
		$this->post = $data;
	}
	
	/**
	 * Store the GET data for this request internally.
	 * 
	 * @param	array	$data
	 */
	public function setGet(array $data) {
		// Store the data.
		$this->get = $data;
	}
	
	/**
	 * Set the request URL for this Request.
	 * 
	 * @param	string	$url
	 */
	public function setUrl($url) {
		$this->url = $url;
		
		// Infer the URL components based on this new URL.
		$this->setProtocol($this->inferProtocol());
		$this->setHost($this->inferHost());
		$this->setUri($this->inferUri());
	}
	
	/**
	 * Set the port of the Request.
	 * 
	 * @param	int		$port
	 */
	public function setPort($port) {
		$this->port = $port;
	}
	
	/**
	 * Set the host for this Request.
	 * 
	 * @param	string	$host
	 */
	public function setHost($host) {
		$this->host = $host;
	}
	
	/**
	 * Set the protocol for this Request.
	 * 
	 * @param	string	$protocol
	 */
	public function setProtocol($protocol) {
		$this->protocol = $protocol;
	}
	
	/**
	 * Set the URI for this Request.
	 * 
	 * @param	string	$uri
	 */
	public function setUri($uri) {
		$this->uri = $uri;
	}
	
	/**
	 * Returns whether or not the current request is secure.
	 * 
	 * @return	boolean
	 */
	public function isSecure() {
		return $this->getProtocol() === 'https' || $this->getProtocol() === 'ssl';
	}
	
	/**
	 * Infers the scheme from the URL.
	 * 
	 * @return	string
	 */
	protected function inferProtocol() {
		$matches = [];
		preg_match('/^[a-zA-Z]+(?=:\/\/)/', $this->url, $matches);
		
		return (empty($matches)) ? null : $matches[0];
	}
	
	/**
	 * Infers the host from the URL.
	 * 
	 * @return	string
	 */
	protected function inferHost() {
		$matches = [];
		preg_match('/^(?:[a-zA-Z]+:\/\/)?([^\/]+\/?)/', $this->url, $matches);
		
		return (!isset($matches[1])) ? null : $matches[1];
	}
	
	/**
	 * Infers the URI from the URL.
	 * 
	 * @return	string
	 */
	protected function inferUri() {
		return preg_replace('/^(?:[a-zA-Z]+:\/\/)?[^\/]+/', '', $this->url);
	}
}