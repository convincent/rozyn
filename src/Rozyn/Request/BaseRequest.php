<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

abstract class BaseRequest implements RequestMethodsInterface {
	use RequestMethods;
	
	/**
	 * The request URI for this Request.
	 * 
	 * @var	string
	 */
	protected $uri;
	
	/**
	 * The request URL for this Request.
	 * 
	 * @var	string
	 */
	protected $url;
	
	/**
	 * The host for this Request.
	 * 
	 * @var	string
	 */
	protected $host;
	
	/**
	 * Whether or not this request is secure.
	 * 
	 * @var	boolean
	 */
	protected $secure;
	
	/**
	 * The protocol for this Request.
	 * 
	 * @var	string
	 */
	protected $protocol;
	
	/**
	 * Holds the port of the request.
	 * 
	 * @var	int
	 */
	protected $port;
	
	/**
	 * Holds the headers for the current request.
	 * 
	 * @var	array
	 */
	protected $headers = [];
	
	/**
	 * Holds all the data for this request.
	 * 
	 * @var	array
	 */
	protected $data;
	
	/**
	 * Holds the GET data for this request.
	 * 
	 * @var	array
	 */
	protected $get = [];
	
	/**
	 * Holds the POST data for this request.
	 * 
	 * @var	array
	 */
	protected $post = [];
	
	/**
	 * Returns the full URL associated with the request.
	 * 
	 * @return	string
	 */
	public function getUrl() {
		return $this->getScheme() . $this->getHost() . $this->getUri();
	}
	
	/**
	 * Returns the query string 
	 * 
	 * @return	string
	 */
	public function getUri() {
		return $this->uri;
	}
	
	/**
	 * Returns the host of the request.
	 * 
	 * @return	string
	 */
	public function getHost() {
		return $this->host;
	}
	
	/**
	 * Returns the protocol for this request.
	 * 
	 * @return	string
	 */
	public function getProtocol() {
		return $this->protocol;
	}
	
	/**
	 * Returns the scheme for this request.
	 * 
	 * @return	string
	 */
	public function getScheme() {
		return $this->getProtocol() . '://';
	}
	
	/**
	 * Returns the port.
	 * 
	 * @return	int
	 */
	public function getPort() {
		return $this->port;
	}
	
	/**
	 * Sets the headers for the current request.
	 * 
	 * @param	array $headers
	 */
	public function setHeaders(array $headers) {
		$this->headers = $headers;
	}
	
	/**
	 * Returns all headers for this Request.
	 * 
	 * @return	string[]
	 */
	public function getHeaders() {
		return $this->headers;
	}
	
	/**
	 * Retrieve the value of a particular header or $default if the header 
	 * could not be found.
	 * 
	 * @param	string	$header
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function getHeader($header, $default = null) {
		if (isset($this->headers[$header])) {
			return $this->headers[$header];
		}
		
		$lcheader = strtolower($header);
		foreach ($this->headers as $key => $value) {
			if (strtolower($key) === $lcheader) {
				return $value;
			}
		}
		
		return $default;
	}
	
	/**
	 * Checks if the request was made using the HTTP GET request method.
	 * 
	 * @return	boolean
	 */
	public function isHttpGet() {
		return $this->getRequestMethod() === static::HTTP_GET;
	}
	
	/**
	 * Checks if the request was made using the HTTP POST request method.
	 * 
	 * @return	boolean
	 */
	public function isHttpPost() {
		return $this->getRequestMethod() === static::HTTP_POST;
	}
	
	/**
	 * Checks if the request was made using the HTTP PATCH request method.
	 * 
	 * @return	boolean
	 */
	public function isHttpPatch() {
		return $this->getRequestMethod() === static::HTTP_PATCH;
	}
	
	/**
	 * Checks if the request was made using the HTTP PUT request method.
	 * 
	 * @return	boolean
	 */
	public function isHttpPut() {
		return $this->getRequestMethod() === static::HTTP_PUT;
	}
	
	/**
	 * Checks if the request was made using the HTTP DELETE request method.
	 * 
	 * @return	boolean
	 */
	public function isHttpDelete() {
		return $this->getRequestMethod() === static::HTTP_DELETE;
	}
	
	/**
	 * Checks if the request was made using the HTTP HEAD request method.
	 * 
	 * @return	boolean
	 */
	public function isHttpHead() {
		return $this->getRequestMethod() === static::HTTP_HEAD;
	}
	
	/**
	 * Retrieve input data from the client of the given type. If $key is set to 
	 * null or if it is omitted, this method returns all input data of the given
	 * type.
	 * 
	 * @param	string	$type
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	protected function getInputData($type, $key = null, $default = null) {
		// Make sure we accept the specified type.
		if (!in_array($type, $this->getAllowedInputTypes())) {
			throw new InvalidInputTypeException($type);
		}
		
		// Retrieve the correct input data.
		$input = $this->{$type};

		// If no key was specified (or if it was explicitly set to NULL), return
		// all relevant input data.
		if ($key === null) {
			return $input;
		}
		
		// If the given key is not set in the input data, return our default
		// value.
		if (!isset($input[$key])) {
			return $default;
		}
		
		return $input[$key];
	}
	
	/**
	 * Returns an array of allowed input types.
	 * 
	 * @return	string[]
	 */
	protected function getAllowedInputTypes() {
		return ['get', 'post'];
	}
	
	/**
	 * Tries to guess the headers for this request.
	 * 
	 * @return	string[]
	 */
	protected function inferHeaders() {
		return [];
	}
	
	/**
	 * Alias for getUrl().
	 * 
	 * @return	string
	 */
	final public function url() {
		return $this->getUrl();
	}
	
	/**
	 * Alias for getUri().
	 */
	final public function uri() {
		return $this->getUri();
	}
	
	/**
	 * Alias for getHost().
	 * 
	 * @return	string
	 */
	final public function host() {
		return $this->getHost();
	}
	
	/**
	 * Alias for getPort().
	 * 
	 * @return	int
	 */
	final public function port() {
		return $this->getPort();
	}
	
	/**
	 * Alias for isSecure().
	 * 
	 * @return	boolean
	 */
	final public function secure() {
		return $this->isSecure();
	}
	
	/**
	 * Alias for getScheme().
	 * 
	 * @return	string
	 */
	final public function scheme() {
		return $this->getScheme();
	}
	
	/**
	 * Alias for getMethod().
	 * 
	 * @return	string
	 */
	final public function method() {
		return $this->getRequestMethod();
	}
	
	/**
	 * Retrieve GET input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	abstract public function get($key = null, $default = null);
	
	/**
	 * Retrieve POST input data.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	abstract public function post($key = null, $default = null);
}