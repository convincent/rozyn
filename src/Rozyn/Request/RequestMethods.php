<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

trait RequestMethods {
	/**
	 * Holds the request method for this object.
	 * 
	 * @var	string
	 */
	protected $method;
	
	/**
	 * Holds the names of all allowed request methods.
	 * 
	 * @var	string[]
	 */
	protected static $requestMethods = ['GET', 'POST', 'DELETE', 'PUT', 'PATCH'];
	
	/**
	 * Sets the request method for this object.
	 * 
	 * @param	string	$method
	 * @throws	\Rozyn\Request\InvalidRequestMethodException
	 */
	public function setRequestMethod($method) {
		if (!$this->isValidRequestMethod($method)) {
			throw new InvalidRequestMethodException($method);
		}
		
		$this->method = $method;
	}
	
	/**
	 * Returns the request method for this object.
	 * 
	 * @return	string
	 */
	public function getRequestMethod() {
		return $this->method;
	}
	
	/**
	 * Returns whether or not the specified request method is valid.
	 * 
	 * @param	string	$method
	 * @return	boolean
	 */
	public function isValidRequestMethod($method) {
		return $method === null || in_array(strtoupper($method), static::$requestMethods);
	}
}