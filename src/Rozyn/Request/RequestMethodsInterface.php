<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

interface RequestMethodsInterface {
	/**
	 * The GET request method.
	 * 
	 * @var	string
	 */
	const HTTP_GET = 'GET';
	
	/**
	 * The POST request method.
	 * 
	 * @var	string
	 */
	const HTTP_POST = 'POST';
	
	/**
	 * The PATCH request method.
	 * 
	 * @var	string
	 */
	const HTTP_PATCH = 'PATCH';
	
	/**
	 * The DELETE request method.
	 * 
	 * @var	string
	 */
	const HTTP_DELETE = 'DELETE';
	
	/**
	 * The PUT request method.
	 * 
	 * @var	string
	 */
	const HTTP_PUT = 'PUT';
	
	/**
	 * The HEAD request method.
	 * 
	 * @var	string
	 */
	const HTTP_HEAD = 'HEAD';
	
	/**
	 * Sets the request method for this object.
	 * 
	 * @param	string	$method
	 * @throws	\Rozyn\Request\InvalidRequestMethodException
	 */
	function setRequestMethod($method);
	
	/**
	 * Returns the request method for this object.
	 * 
	 * @return	string
	 */
	function getRequestMethod();
	
	/**
	 * Returns whether or not the specified request method is valid.
	 * 
	 * @param	string	$method
	 * @return	boolean
	 */
	function isValidRequestMethod($method);
}