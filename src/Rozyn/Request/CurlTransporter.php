<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Request;

class CurlTransporter extends Transporter {
	/**
	 * Holds our cURL handle.
	 * 
	 * @var	resource
	 */
	protected $ch;
	
	/**
	 * Keeps track of all the options we set for this cURL resource.
	 * 
	 * @var	array
	 */
	protected $options = [];
	
	/**
	 * Keeps track of whether or not the current resource has been executed at
	 * least once.
	 * 
	 * @var	boolean
	 */
	protected $sent = false;
	
	/**
	 * Initializes our cURL handle and returns it.
	 * 
	 * @param	string	$url
	 * @return	resource
	 */
	public function init($url = null) {
		// Create the cURL resource 
        $this->ch = curl_init();
		
		if ($url !== null) {
			$this->setUrl($url);
		}
		
		return $this->ch;
	}
	
	/**
	 * Checks whether or not the cURL resource has been initialized.
	 * 
	 * @return	boolean 
	 */
	public function isInit() {
		return !!$this->ch;
	}
	
	/**
	 * Closes the cURL resource.
	 * 
	 * @return	boolean
	 */
	public function close() {
        // Close the cURL resource to free up system resources.
        if (curl_close($this->ch)) {
			// Reset our resource.
			$this->ch = null;
			
			// Also reset some other variables
			$this->reset();
		}
		
		// Return whether or not we successfully closed our resource.
		return $this->ch === null;
	}
	
	/**
	 * Reset some of the object variables so that it'll be as if we haven't
	 * executed this curl resource yet.
	 */
	public function reset() {
		parent::reset();
		
		$this->options	= [];
		$this->sent		= false;
	}
	
	/**
	 * Grab the URL and pass it to the browser.
	 * 
	 * @return	string
	 * @throws	\Rozyn\Request\CurlResourceNotInitializedException
	 */
	public function send() {
		if (!$this->isInit()) {
			throw new CurlResourceNotInitializedException();
		}
		
		if (!$this->hasOption(CURLOPT_RETURNTRANSFER)) {
			$this->setOption(CURLOPT_RETURNTRANSFER, true);
		}

		if ($this->getRequestMethod() === null) {
			$this->setRequestMethod(static::HTTP_GET);
		}

		$response = curl_exec($this->ch);
		
		if ($response === false) {
			throw new InvalidRequestException(curl_error($this->ch), curl_errno($this->ch));
		}
		
		return $response;
	}
	
	/**
	 * Sends a Request object.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function request(Request $request) {
		$this->setUrl($request->getUrl());
		$this->setRequestMethod($request->getRequestMethod());
		
		return $this->send();
	}
	
	/**
	 * Make a DELETE request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function delete(Request $request = null) {
		return $this->post($request);
	}
	
	/**
	 * Make a GET request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function get(Request $request = null) {
		$this->setOption(CURLOPT_HTTPGET, true);
		
		$urlParamString = (!empty($this->getData())) ? http_build_query($this->getData()) : '';
		
		if ($request) {
			$request->setRequestMethod(static::HTTP_GET);
			$request->setUrl($request->getUrl() . '?' . $urlParamString);
			
			return $this->request($request);
		}
		
		$this->setUrl($this->getUrl() . '?' . $urlParamString);
		return $this->send();
	}
	
	/**
	 * Make a POST request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function post(Request $request = null) {
		if ($request) {
			$request->setRequestMethod(static::HTTP_POST);
			$this->setOption(CURLOPT_POSTFIELDS, $request->post());
			return $this->request($request);
		}
		
		$this->setOption(CURLOPT_POSTFIELDS, $this->getData());
		$this->setRequestMethod(static::HTTP_POST);
		return $this->send();
	}
	
	/**
	 * Make a PUT request.
	 * 
	 * @param	\Rozyn\Request\Request	$request
	 */
	public function put(Request $request = null) {
		return $this->post($request);
	}
	
	/**
	 * Returns the HTTP status code.
	 * 
	 * @return	int
	 */
	public function getCode() {
		if (!$this->sent()) {
			$this->setOption(CURLOPT_NOBODY, true);
			$this->send();
		}
		
		return intval(curl_getinfo($this->ch, CURLINFO_HTTP_CODE));
	}
	
	/**
	 * Returns whether or not the current resource has been executed at least
	 * once.
	 * 
	 * @return	boolean
	 */
	public function sent() {
		return !!$this->sent;
	}
	
	/**
	 * Set an option for our cURL resource.
	 * 
	 * @param	int		$option
	 * @param	mixed	$value
	 * @return	boolean
	 */
	public function setOption($option, $value) {
		$this->options[$option] = $value;
		
		if (!$this->isInit()) {
			$this->init();
		}
		
		return curl_setopt($this->ch, $option, $value);
	}
	
	/**
	 * Set a number of options.
	 * 
	 * @param	array	$options
	 */
	public function setOptions($options) {
		foreach ($options as $option => $value) {
			$this->setOption($option, $value);
		}
	}
	
	/**
	 * Check if a particular option has been set for this cURL resource.
	 * 
	 * @param	int		$option
	 * @return	boolean
	 */
	public function hasOption($option) {
		return isset($this->options[$option]);
	}
	
	/**
	 * Return the value that was set for a particular option.
	 * 
	 * @param	int		$option
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function getOption($option, $default = null) {
		return ($this->hasOption($option)) ? $this->options[$option] : $default;
	}
	
	/**
	 * Set the URL for this resource.
	 * 
	 * @param	string	$url
	 */
	public function setUrl($url) {
		parent::setUrl($url);
		
		return $this->setOption(CURLOPT_URL, $url);
	}
	
	/**
	 * Get the URL for this resource.
	 * 
	 * @return	string
	 */
	public function getUrl() {
		return $this->getOption(CURLOPT_URL);
	}
	
	/**
	 * Set the method for this resource.
	 * 
	 * @param	string	$method
	 */
	public function setRequestMethod($method) {
		parent::setRequestMethod($method);
		
		return $this->setOption(CURLOPT_CUSTOMREQUEST, $method);
	}
}