<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Filesystem;

class CsvFileIterator implements FileIterator {
	/**
	 * Reads the next entry from the file and returns it.
	 * 
	 * @return	array
	 */
	protected function read() {
		return fgetcsv($this->file);
	}
}