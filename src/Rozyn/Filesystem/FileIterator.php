<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Filesystem;

class FileIterator implements \Iterator {
	/**
	 * The file resource that is to be iterated.
	 * 
	 * @var	resource
	 */
    protected $file;
	
	/**
	 * The position of our iterator.
	 * 
	 * @var	int
	 */
    protected $key = 0;
	
	/**
	 * The current element of our iterator.
	 * 
	 * @var	mixed
	 */
    protected $current;

	/**
	 * Constructs our Iterator object.
	 * 
	 * @param	string	$file
	 */
    public function __construct($file) {
        $this->file = fopen($file, 'r');
    }

	/**
	 * Cleans up any leftover resources.
	 */
    public function __destruct() {
        fclose($this->file);
    }

	/**
	 * Reset our iterator to the start of the file.
	 */
    public function rewind() {
        rewind($this->file);
        $this->current = $this->read();
        $this->key = 0;
    }

	/**
	 * 
	 * 
	 * @return	boolean
	 */
    public function valid() {
        return $this->current !== false;
    }

	/**
	 * Return the key of our iterator.
	 * 
	 * @return	int
	 */
    public function key() {
        return $this->key;
    }

	/**
	 * Return the current element.
	 * 
	 * @return	mixed
	 */
    public function current() {
        return rtrim($this->current, "\n");
    }

	/**
	 * Move to the next element.
	 */
    public function next() {
        $this->current = $this->read();
        $this->key++;
    }
	
	/**
	 * Returns the file resource associated with this iterator.
	 * 
	 * @return	resource
	 */
	public function getFile() {
		return $this->file;
	}
	
	/**
	 * Reads the next entry from the file and returns it.
	 * 
	 * @return	mixed
	 */
	protected function read() {
		return fgets($this->file);
	}
}