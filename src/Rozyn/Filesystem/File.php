<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Filesystem;

use Rozyn\Facade\Url;

class File {
	
	use MimeTypes;
	
	/**
	 * The full path to the file.
	 * 
	 * @var	string
	 */
	protected $path;
	
	/**
	 * The name of the file.
	 * 
	 * @var	string
	 */
	protected $name;
	
	/**
	 * The mime type associated with this file.
	 * 
	 * @var	string
	 */
	protected $mime = null;
	
	/**
	 * Constructs the object. The given path is used to locate the file. If 
	 * $create is set to true, a new file will be created if it doesn't exist
	 * yet.
	 * 
	 * @param	mixed		$path
	 * @param	boolean		$create
	 */
	public function __construct($path, $create = false) {
		// Check if $path is an array. If so, assume that it is originally an
		// entry in the $_FILES superglobal, meaning that the element matching
		// the "tmp_name" key is the actual (temporary) path that we're looking
		// for.
		if (is_array($path) && isset($path['tmp_name'])) {
			$path = $path['tmp_name'];
		}
		
		// Store the full path.
		$this->setPath($path);
		
		// If the file doesn't exist and $create it set to true, create the file
		// on the fly.
		if (!$this->exists() && $create) {
			$this->create();
		}
		
		// Store the mime type of the file so that if the path or name get
		// changed later on, the mime type can still be read without having to
		// deduce it from the actual file.
		$this->mime();
	}
	
	/**
	 * Returns whether or not the file exists. If $strict is set to true, this
	 * method will also check whether or not the file is indeed a file and not
	 * a directory.
	 * 
	 * @param	boolean	$strict
	 * @return	boolean
	 */
	public function exists($strict = true) {
		return file_exists($this->inclpath()) && (!$strict || is_file($this->inclpath()));
	}
	
	/**
	 * Creates the file if it doesn't exist yet. 
	 * 
	 * Returns TRUE if the file was created succesfully, FALSE if the file 
	 * already exists or if another error occured.
	 * 
	 * @return	boolean
	 */
	public function create() {
		$dir = dirname($this->path);
		if (is_dir($dir) && is_writable($dir) && !$this->exists()) {
			return touch($this->path);
		}
		
		return false;
	}
	
	/**
	 * Returns the content of the file.
	 * 
	 * @return	string
	 */
	public function content() {
		return file_get_contents($this->path());
	}
	
	/**
	 * Returns the last modification time of the file.
	 * 
	 * @return	int
	 */
	public function getModificationTime() {
		return filemtime($this->path());
	}
	
	/**
	 * Returns the last change time of the file.
	 * 
	 * @return	int
	 */
	public function getChangeTime() {
		return filectime($this->path());
	}
	
	/**
	 * Returns the MIME type of the file or null if the MIME type could not be 
	 * read.
	 * 
	 * @return	string|false
	 */
	public function mime() {
		if ($this->exists() && false !== ($finfo = finfo_open(FILEINFO_MIME_TYPE))) {
			$this->mime = finfo_file($finfo, $this->inclpath());
			finfo_close($finfo);
		}
		
		elseif (isset($this->mimeTypes[$ext = strtolower($this->ext())])) {
			$this->mime = $this->mimeTypes[$ext];
		}
		
		return $this->mime;
	}
	
	/**
	 * Returns the size of the file in bytes unless $simplify is set to true. In
	 * that case the size will be returned in an appropriate unit and the unit
	 * itself will also be included in the return string.
	 * 
	 * @param	boolean	$simplify
	 * @return	int | string
	 */
	public function size($simplify = false) {
		$path = $this->inclpath();
		
		if (file_exists($path)) {
			$size = filesize($path);

			if ($simplify === true) {
				$i = 0;
				$units = ['B', 'KB', 'MB', 'GB'];

				while ($size > 1024 && $i < count($units) - 1) {
					$size /= 1024;
					$i++;
				}

				$size = round($size, 2) . $units[$i];
			}

			return $size;
		}
		
		return 0;
	}
	
	/**
	 * Returns the extension of the file or null if the file has no extension.
	 * 
	 * @param	boolean		$dot	Specifies whether or not the dot should be included in the extension.
	 * @return	string|null
	 */
	public function ext($dot = false) {
		return (false !== ($pos = strrpos($this->name, '.'))) ? strtolower(substr($this->name, $pos + intval(!$dot))) : null;
	}
	
	/**
	 * Changes the name of the file. If the file exists, the file will be 
	 * renamed on the file system too.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Filesystem\File
	 */
	public function rename($name) {
		$existed = $this->exists();
		$old	 = $this->inclpath();
		
		if (strpos($name, '/') === false && strpos($name, '\\') === false) {
			$this->setName($name);
		}
		
		else {
			$this->setPath($name);
		}
		
		if ($existed) {
			rename($old, $this->inclpath());
		}
		
		return $this;
	}
	
	/**
	 * Moves the file to the new location. If $path refers to a directory, the
	 * file will be placed inside that directory. If $path refers to a file
	 * location, the file will be renamed and then moved into the correct
	 * directory.
	 * 
	 * @param	string	$path
	 * @return	\Rozyn\Filesystem\File
	 */
	public function move($path) {
		return (is_dir($path)) ? 
					$this->rename($path . DS . $this->name()) : 
					$this->rename($path);
	}
	
	/**
	 * Returns the url to the given file.
	 * 
	 * @return	string
	 */
	public function url($secure = false, $absolute = false) {
		return Url::format(preg_replace('/^' . preg_quote(__ROOT__, '/') . '/', '', $this->path()), $secure, $absolute);
	}
	
	/**
	 * Returns the include path to the given file.
	 * 
	 * @return	string
	 */
	public function inclpath() {
		return (__WWW_ROOT__ === '') ? 
					prefix($this->path, __ROOT__) : 
					preg_replace('/^' . preg_quote(__WWW_ROOT__, '/') . '/', __ROOT__, $this->path);
	}
	
	/**
	 * Returns the path to the file.
	 * 
	 * @return	string
	 */
	public function path() {
		return $this->path;
	}
	
	/**
	 * Returns the filename of the file.
	 * 
	 * @return	string
	 */
	public function name() {
		return $this->name;
	}
	
	/**
	 * Applies a given callback function to this File object.
	 * 
	 * @param	callable				$callback
	 * @return	\Rozyn\Filesystem\File
	 */
	public function apply(callable $callback) {
		$callback($this);
		
		return $this;
	}

	/**
	 * Saves a new copy of the file if a $path is specified. If $avoidCollisions
	 * is set to TRUE, the filename is changed so that no other files will be
	 * overwritten. In that case, the internal $path and $name variables of this
	 * object are also updated.
	 * 
	 * @param	string	$path
	 * @param	boolean	$avoidCollisions
	 * @return	\Rozyn\Filesystem\File
	 */
	public function save($path = null, $avoidCollisions = true) {
		if ($path !== null) {
			$old = $this->path();
			
			$this->setPath($path);
			
			while ($avoidCollisions === true && file_exists($this->path())) {
				$this->setName(substr(md5(uniqid()), 0, 6) . $this->name());
			}
			
			copy($old, $this->path());
		}
		
		return $this;
	}
	
	/**
	 * Sets the path for this file. Note that this method doesn't actually move
	 * the file to the designated path, it merely sets the internal $path and
	 * $name variables of this object.
	 * 
	 * @param	string	$path
	 * @return	\Rozyn\Filesystem\File
	 */
	public function setPath($path) {
		if (!is_dir($path)) {
			$this->path = $path;
			$this->name = basename($path);
		}
		
		return $this;
	}
	
	/**
	 * Sets the name of the file and updates the $path variable accordingly.
	 * Note that this method doesn't actually rename the file, it merely updates 
	 * the internal $path and $name variables of this object.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Filesystem\File
	 */
	public function setName($name) {
		$this->path = preg_replace('/' . preg_quote($this->name, '/') . '$/', $name, $this->path);
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Prepends a prefix to the file name.
	 * 
	 * @param	string	$prefix
	 * @return	\Rozyn\Filesystem\File
	 */
	public function prefixName($prefix) {
		return $this->setName($prefix . $this->name());
	}
	
	/**
	 * Appends a suffix to the file name.
	 * 
	 * @param	string	$suffix
	 * @return	\Rozyn\Filesystem\File
	 */
	public function suffixName($suffix) {
		return $this->setName(preg_replace('/(?=' . preg_quote($this->ext(true), '/') . ')$/', $suffix, $this->name()));
	}
	
	/**
	 * Returns a string representation of the file, which will be its location.
	 * 
	 * @return	string
	 */
	public function __toString() {
		return $this->path ?: '';
	}
}