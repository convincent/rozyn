<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Filesystem;

class Image extends File {
	/**
	 * Holds the image resource.
	 * 
	 * @var	
	 */
	protected $image;
	
	/**
	 * Constructs the object.
	 * 
	 * @param	string	$path
	 * @param	boolean $create
	 */
    public function __construct($path, $create = false) {
		parent::__construct($path, $create);
    }

	/**
	 * A clean-up function that helps free up memory whenever this object is
	 * discarded.
	 */
    public function __destruct() {
        $this->destroy();
    }
	
	/** 
	 * A clone method that takes care of copying the image resource.
	 */
	public function __clone() {
        $copy = $this->createtruecolor($this->width(), $this->height());

		imagecopy($copy, $this->image, 0, 0, 0, 0, $this->width(), $this->height());

		$this->image = $copy;
	}
	
	/**
	 * Resizes the image to the given dimensions, ignoring the aspect ratio.
	 * 
	 * @param	int		$width
	 * @param	int		$height
	 * @return	\Rozyn\Filesystem\Image
	 */
	public function resize($width, $height) {
		$this->load();
		
        $image = $this->createtruecolor($width, $height);
		
        imagecopyresampled($image, $this->image, 0, 0, 0, 0, $width, $height, $this->width(), $this->height());

        $this->destroy();
        $this->image = $image;
		
		return $this;
	}

	/**
	 * Crops the image, resulting in an image with the specified $width and
	 * $height. By default, the center of the image is assumed to be the
	 * desired result, but you can specify an $xOffset and $yOffset manually to
	 * force a different part of the image to be cropped instead.
	 * 
	 * @param	int		$width
	 * @param	int		$height
	 * @param	int		$xOffset
	 * @param	int		$yOffset
	 * @return	\Rozyn\Filesystem\Image
	 */
    public function crop($width, $height, $xOffset = null, $yOffset = null) {
		$this->load();
		
        if ($xOffset === null) {
            $xOffset = round(($this->width() - $width) / 2);
		}
		
        if ($yOffset === null) {
            $yOffset = round(($this->height() - $height) / 2);
		}	
		
        $image = $this->createtruecolor($width, $height);
		
        imagecopy($image, $this->image, 0, 0, $xOffset, $yOffset, $width, $height);

        $this->destroy();
        $this->image = $image;
		
		return $this;
    }

	/**
	 * Returns the width of the image if no arguments are specified. If a $width
	 * argument is passed, the image is instead resized to that width and the
	 * instance itself is returned.
	 * 
	 * @param	int		$width
	 * @param	boolean $preserve
	 * @return	int | Rozyn\Filesystem\Image
	 */
    public function width($width = null, $preserve = true) {
		$this->load();
		
		return ($width === null) ?
					imagesx($this->image) :
					$this->resize($width, ($preserve) ? round($this->height() * ($width / $this->width())) : $this->height());
    }

	/**
	 * Returns the height of the image if no arguments are specified. If a
	 * $height argument is passed, the image is instead resized to that height 
	 * and the instance itself is returned.
	 * 
	 * @param	int		$height
	 * @param	boolean $preserve
	 * @return	int | Rozyn\Filesystem\Image
	 */
    public function height($height = null, $preserve = true) {
		$this->load();
		
		return ($height === null) ?
					imagesy($this->image) :
					$this->resize(($preserve) ? round($this->width() * ($height / $this->height())) : $this->width(), $height);
    }
    
	/**
	 * Resizes the image to the given width or height, making the image as large
	 * as possible while retaining aspect ratio and making sure that neither the
	 * width nor the height are greater than the specified $width and $height,
	 * respectively.
	 * 
	 * @param	int		$width
	 * @param	int		$height
	 * @return	\Rozyn\Filesystem\Image
	 */
    public function resizeMax($width, $height) {
        return ($width / $this->width() > $height / $this->height()) ?
					$this->height($height) :
					$this->width($width);
    }
    
	/**
	 * Resizes the image to the given width or height, making the image as large
	 * as possible while retaining aspect ratio and making sure that both the
	 * width and the height are greater than or equal to the specified $width 
	 * and $height, respectively.
	 * 
	 * @param	int		$width
	 * @param	int		$height
	 * @return	\Rozyn\Filesystem\Image
	 */
    public function resizeMin($width, $height) {
        return ($width / $this->width() > $height / $this->height()) ? 
					$this->width($width) :
					$this->height($height);
    }

	/**
	 * Crops the image after resizing the image in such a way that the result
	 * contains as much as possible from the original image.
	 * 
	 * @param	int		$width
	 * @param	int		$height
	 * @param	int		$xOffset
	 * @param	int		$yOffset
	 * @return	\Rozyn\Filesystem\Image
	 */
    public function resizeAndCrop($width, $height, $xOffset = null, $yOffset = null) {
        return $this->resizeMin($width, $height)
					->crop($width, $height, $xOffset, $yOffset);
    }

	/**
	 * Saves the image resource to the file that it was loaded from, or stores
	 * it as a new file if a new $path is specified.
	 * 
	 * @param	string	$path
	 * @param	boolean	$avoidCollisions
	 * @return	\Rozyn\Filesystem\Image
	 */
    public function save($path = null, $avoidCollisions = true) {
		parent::save($path, $avoidCollisions);
		
		$this->load();
		
        switch ($this->mime()) {
            case 'image/png':
                imagepng($this->image, $this->path(), 9);
                break;

            case 'image/gif':
                imagegif($this->image, $this->path(), 100);
                break;

            case 'image/jpeg':
            case 'image/pjpeg':
                imagejpeg($this->image, $this->path(), 100);
                break;

            default:
				throw new UnsupportedMimeTypeException($this->mime());
        }
		
		return $this;
    }
	
	/**
	 * Creates a new image resource that can be filled with (a part of) the 
	 * image resource already associated with this object.
	 * 
	 * @param	int		$width
	 * @param	int		$height
	 * @return	resource
	 */
	protected function createtruecolor($width, $height) {
		return ($this->supportsTransparency()) ?	imagecreatetruecolortransparent($width, $height) :
													imagecreatetruecolor($width, $height);
	}
	
	/**
	 * Returns if the image type supports transparency.
	 * 
	 * @return	boolean
	 */
	protected function supportsTransparency() {
		return in_array($this->mime(), ['image/png', 'image/jpeg', 'image/pjpeg']);
	}

	/**
	 * Loads the image resource from the file.
	 * 
	 * @return	\Rozyn\Filesystem\Image
	 */
    protected function load() {
		if (!$this->isLoaded() && $this->exists()) {
			switch ($this->mime()) {
				case 'image/png':
					$this->image = imagecreatefrompng($this->path());
					imagealphablending($this->image, false);
					imagesavealpha($this->image, true);
					break;

				case 'image/gif':
					$this->image = imagecreatefromgif($this->path());
					break;

				case 'image/jpeg':
				case 'image/pjpeg':
					$this->image = imagecreatefromjpeg($this->path());
					break;

				default:
					throw new UnsupportedMimeTypeException($this->mime());
			}
        }
		
		return $this;
    }
	
	/**
	 * Returns whether or not the image file has been loaded as a resource yet.
	 * 
	 * @return	boolean
	 */
	protected function isLoaded() {
		return $this->image !== null;
	}

	/**
	 * Destroys the image resource to free up memory.
	 * 
	 * @return	\Rozyn\Filesystem\Image
	 */
    public function destroy() {
        if (isset($this->image)) {
            imagedestroy($this->image);
			$this->image = null;
        }
		
		return $this;
    }

	/**
	 * Returns the image resource.
	 * 
	 * @return	resource 
	 */
    public function resource() {
		return $this->getResource();
    }

	/**
	 * Returns the image resource.
	 * 
	 * @return	resource 
	 */
    public function getResource() {
        return $this->image;
    }
}