<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Security;

use Rozyn\Config\Config;

class Sanitizer {	
	/**
	 * The filter strategy used to sanitize email addresses.
	 * 
	 * @var	string
	 */
	const FILTER_EMAIL = 'email';
	
	/**
	 * The filter strategy used to sanitize integers.
	 * 
	 * @var	string
	 */
	const FILTER_INT = 'int';
	
	/**
	 * The filter strategy used to sanitize float values.
	 * 
	 * @var	string
	 */
	const FILTER_FLOAT = 'float';
	
	/**
	 * The filter strategy used to sanitize strings.
	 * 
	 * @var	string
	 */
	const FILTER_STRING = 'string';
	
	/**
	 * The filter strategy to make sure a certain variable can be safely used 
	 * inside an HTML document.
	 * 
	 * @var	string
	 */
	const FILTER_HTML = 'html';
	
	/**
	 * The filter strategy used to sanitize URLs.
	 * 
	 * @var	string
	 */
	const FILTER_URL = 'url';
	
	/**
	 * The filter strategy to make sure a certain variable can be safely used
	 * inside <script> tags.
	 * 
	 * @var	string
	 */
	const FILTER_JS = 'js';
	
	/**
	 * Holds the configuration for this App.
	 * 
	 * @var	\Rozyn\Config\Config	$config
	 */
	protected $config;
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Constructs our Sanitizer object and takes care of its initialization.
	 * 
	 * @param	\Rozyn\Config\Config	$config
	 */
	public function __construct(Config $config) {
		$this->config = $config;
	}
	
	/**
	 * A sanitize function that can be used to sanitize variables.
	 * 
	 * @param	mixed			$var
	 * @param	string|string[]	$filters
	 * @return	mixed
	 */
	public function sanitize($var, $filters = null) {
		// If an array is passed as variable, apply the specified filters to
		// each value in that array recursively.
		if (is_array($var)) {
			foreach ($var as $key => $value) {
				$var[$key] = $this->sanitize($value, $filters);
			}
			
			return $var;
		}
		
		// Format the $filters argument so that we can easily parse it.
		if (!is_array($filters)) {
			$filters = [$filters];
		}
		
		// Loop through all specified filters and apply them to the variable one
		// after the other.
		foreach ($filters as $filter) {
			$var = $this->filter($var, $filter);
		}
		
		// Return the result.
		return $var;
	}
	
	/**
	 * Applies a single filter to a value and returns the result.
	 * 
	 * @param	mixed	$var
	 * @param	string	$filter
	 */
	protected function filter($var, $filter) {
		switch ($filter) {
			case static::FILTER_EMAIL:
				return filter_var($var, FILTER_SANITIZE_EMAIL);
				
			case static::FILTER_INT:
				return filter_var($var, FILTER_SANITIZE_NUMBER_INT);
				
			case static::FILTER_FLOAT:
				return filter_var($var, FILTER_SANITIZE_NUMBER_FLOAT);
				
			case static::FILTER_STRING:
				return filter_var($var, FILTER_SANITIZE_STRING);
			
			case static::FILTER_HTML:
				return htmlspecialchars($var, ENT_NOQUOTES, $this->config->read('default_charset'), false);
				
			case static::FILTER_JS:
				return preg_replace('(^"|"$)', '', json_encode($var));
				
			case static::FILTER_URL:
				return filter_var($var, FILTER_SANITIZE_URL);
				
			default:
				return filter_var($var, FILTER_UNSAFE_RAW);
		}
	}
}