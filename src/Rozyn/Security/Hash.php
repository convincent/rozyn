<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Security;

class Hash {
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * A Randomizer object used to generate random and unique salts and the
	 * like.
	 * 
	 * @var	\Rozyn\Security\Randomizer
	 */
	protected $randomizer;
	
	/**
	 * Construct the Hash instance.
	 * 
	 * @param	\Rozyn\Security\Randomizer	$randomizer
	 */
	public function __construct(Randomizer $randomizer) {
		$this->setRandomizer($randomizer);
	}
	
	/**
	 * Hash a given password using the specified algorithm. By default, the
	 * blowfish algorithm is used.
	 * 
	 * @param	string	$password
	 * @param	string	$algorithm
	 * @return	string
	 * @throws	\Rozyn\Security\HashException
	 */
	public function hash($password, $algorithm = 'blowfish') {
		// Check if the specified algorithm exists and if it supported by PHP
		// and this class.
		$crypt = 'CRYPT_' . strtoupper($algorithm);
		if (defined($crypt) && constant($crypt) === 1 && method_exists($this, $algorithm)) {
			return call_user_func([$this, $algorithm], $password);
		}
		
		throw new HashException("Hashing algorithm {$algorithm} not supported or not implemented.");
	}
	
	/**
	 * Hash a password using the blowfish algorithm.
	 * 
	 * @param	string	$password
	 * @param	int		$cost
	 * @return	string
	 * @throws	\Rozyn\Security\HashException
	 */
	public function blowfish($password, $cost = 10) {
		// Make sure the $cost variable matches the requirements of the blowfish
		// algorithm.
		if ($cost < 4 || $cost > 31) {
			throw new HashException("Invalid cost specified for BLOWFISH algorithm ({$cost}). Should be between 4 and 31.");
		}
		
		// Generate a 22-character salt.
		$salt = $this->salt(22);
		
		// Prefix information about the hash so PHP knows how to verify it 
		// later. "$2y$" means we're using the Blowfish algorithm. The following
		// two digits are the cost parameter. After that comes the automatically 
		// generated salt.
		$salt = sprintf("$2y$%02d$", $cost) . $salt;
		
		// Hash the password with the salt
		return crypt($password, $salt);
	}
	
	/**
	 * Hash a password using the md5 algorithm.
	 * 
	 * @param	string	$password
	 * @return	string
	 */
	public function md5($password) {
		// Generate a random salt
		$salt = '$1$' . $this->salt(9);
		
		return crypt($password, $salt);
	}
	
	/**
	 * Generates a random salt string of $length characters. We can use base64-
	 * encoding for this, but we will have to replace the + characters with a 
	 * dot (.) character, since + characters are not accepted by all algorithms
	 * as a valid salt character.
	 * 
	 * @param	int		$length
	 * @return	string
	 */
	public function salt($length) {
		return strtr($this->randomizer->base64($length), '+', '.');
	}
	
	/**
	 * Verifies if the given $password matches the specified $hash.
	 * 
	 * @param	string	$password
	 * @param	string	$hash
	 * @return	boolean
	 */
	public function verify($password, $hash) {
		return crypt($password, $hash) === $hash;
	}
	
	/**
	 * Set the randomizer for this object.
	 * 
	 * @param	\Rozyn\Security\Randomizer	$randomizer
	 */
	public function setRandomizer(Randomizer $randomizer) {
		$this->randomizer = $randomizer;
	}
	
	/**
	 * Returns the randomizer for this object.
	 * 
	 * @return	\Rozyn\Security\Randomizer
	 */
	public function getRandomizer() {
		return $this->randomizer;
	}
}