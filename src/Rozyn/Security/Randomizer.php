<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Security;

class Randomizer {
	/**
	 * Mark the class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;

	/**
	 * Generates and returns a random string of length $l.
	 * 
	 * @param	int		$length
	 * @return	string
	 */
	public function string($length = 128) {
		$secure_length = (int)$length;
		
		return openssl_random_pseudo_bytes($secure_length);
	}
	
	/**
	 * Returns a random, base64 encoded string of the specified $length.
	 * 
	 * @param	int		$length
	 * @return	string
	 */
	public function base64($length = 128) {
		return substr(base64_encode($this->string($length)), 0, $length);
	}
	
	/**
	 * Generates and returns a random integer between $min and $max (inclusive).
	 * 
	 * @param	int		$min
	 * @param	int		$max
	 * @return	int
	 */
	public function integer($min, $max) {
		return rand($min, $max);
	}
}