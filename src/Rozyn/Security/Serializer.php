<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Security;

use Rozyn\Config\Config;
use Rozyn\Encryption\Encrypter;

class Serializer {
	/**
	 * Holds the configuration for this App.
	 * 
	 * @var	\Rozyn\Config\Config	$config
	 */
	protected $config;
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Specifies whether cache entries should be encrypted.
	 * 
	 * @var	boolean
	 */
	protected $encrypted = true;
	
	/**
	 * An Encrypter object responsible for encrypting this cache's entries.
	 * 
	 * @var	\Rozyn\Encryption\Encrypter
	 */
	protected $encrypter;
	
	/**
	 * A key to use when we need to create a dictionary object to json_encode.
	 * 
	 * @var	string
	 */
	const SERIALIZE_KEY = '__SERIALIZE';
	
	/**
	 * Constructs our Serializer object and takes care of its initialization.
	 * 
	 * @param	\Rozyn\Config\Config	$config
	 * @param	\Rozyn\Encryption\Encrypter	$encrypter
	 */
	public function __construct(Config $config, Encrypter $encrypter) {
		$this->config = $config;
		$this->encrypter = $encrypter;
	}
	
	/**
	 * Serializes the value.
	 * 
	 * @param	mixed	$value
	 * @return	string
	 */
	public function serialize($value) {
		$serialized = json_encode([static::SERIALIZE_KEY => $value]);
		
		if ($this->isEncrypted()) {
			$serialized = $this->encrypter->encrypt($serialized);
		}
		
		return $serialized;
	}
	
	/**
	 * Unserializes the value.
	 * 
	 * @param	string	$value
	 * @return	mixed
	 */
	public function unserialize($value) {
		if ($this->isEncrypted()) {
			$value = $this->encrypter->decrypt($value);
		}
		
		return json_decode($value, true)[static::SERIALIZE_KEY];
	}
	
	/**
	 * Returns whether or not this Serializer uses encryption.
	 * 
	 * @return	boolean
	 */
	public function isEncrypted() {
		return $this->encrypted;
	}
}