<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Error;

use Exception;
use Rozyn\App;
use Rozyn\Routing\Router;
use Rozyn\Routing\PageNotFoundException;
use Rozyn\Logging\FileLogger;

class ShutdownHandler {
	/**
	 * The default error route name.
	 * 
	 * @var	string
	 */
	const ERROR_ROUTE = 'error';
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var boolean
	 */
	protected $singleton = true;
	
	/**
	 * The app instance for which we handle the errors.
	 * 
	 * @var	\Rozyn\App
	 */
	protected $app;
	
	/**
	 * A logger used to log errors.
	 * 
	 * @var	\Rozyn\Logging\LoggerInterface
	 */
	protected $logger;
	
	/**
	 * A router used to route to fallback pages.
	 * 
	 * @var	\Rozyn\Routing\Router
	 */
	protected $router;
	
	/**
	 * The current Exception being handled by this ShutdownHandler.
	 * 
	 * @var	\Exception
	 */
	protected $exception;
	
	/**
	 * The code for the current Exception.
	 * 
	 * @var	int
	 */
	protected $code;
	
	/**
	 * Initialize the class.
	 * 
	 * @param	\Rozyn\App					$app
	 * @param	\Rozyn\Logging\FileLogger	$logger
	 * @param	\Rozyn\Routing\Router		$router
	 */
	public function __construct(App $app, FileLogger $logger, Router $router) {
		$this->app		= $app;
		$this->logger	= $logger;
		$this->router	= $router;
	}
	
	/**
	 * Handles an Exception that was thrown during execution. If a valid 
	 * Response could be generated as a fallback, that response is returned. If 
	 * there is no way to recover from the error, the Exception is rethrown 
	 * again.
	 * 
	 * @return	\Rozyn\Response\Response
	 * @throws	\Exception
	 */
	public function handle() {
		// Check if an error route has been specified. If so, run that route and
		// feed it our exception as an argument.
		if (null !== ($route = $this->router->getRoute(static::ERROR_ROUTE))) {
			$response = $this->router->dispatch($route, array(
				'code'		=> $this->getCode(),
				'exception' => $this->getException(),
			));

			$response->setCode($this->getCode());
			return $response;
		}
		
		throw $this->getException();
	}
	
	/**
	 * Set the exception for this handler.
	 * 
	 * @param	\Exception	$exception
	 */
	public function setException(Exception $exception = null) {
		if ($exception === null) {
			$error = error_get_last();
			if (null === $error) {
				exit;
			}
			
			$this->code		 = 500;
			$this->exception = new PHPRuntimeErrorException("{$error['message']} in {$error['file']} (line {$error['line']})", 500);
		} else {
			$code = intval($exception->getCode());
			
			$this->code		 = ($code < 400) ? 500 : $code;
			$this->exception = $exception;
			
			if (!($exception instanceof PageNotFoundException)) {
				$this->logger->writeException('error', $exception);
			}
		}
	}
	
	/**
	 * Returns the current Exception for this handler.
	 * 
	 * @return	\Exception
	 */
	public function getException() {
		return $this->exception;
	}
	
	/**
	 * Returns the app instance for this handler.
	 * 
	 * @return	\Rozyn\App
	 */
	protected function getApp() {
		return $this->app;
	}
	
	/**
	 * Returns the HTTP error code associated with the given exception.
	 * 
	 * @return	int
	 */
	public function getCode() {
		return $this->code;
	}
	
	/**
	 * Returns the Logger for this handler.
	 * 
	 * @return	\Rozyn\Logging\LoggerInterface
	 */
	protected function getLogger() {
		return $this->logger;
	}
	
	/**
	 * Returns the Router for this handler.
	 * 
	 * @return	\Rozyn\Routing\Router
	 */
	protected function getRouter() {
		return $this->router;
	}
}