<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Composition;

use ReflectionException;

class DI {
	/**
	 * The $map variable holds a key => value mapping of keys and classNames 
	 * respectively
	 * 
	 * @var	string[]
	 */
	public static $map = [];

	/**
	 * The $singletons variable holds a key => value mapping of keys and 
	 * singleton instances respectively.
	 *
	 * @var	object[]
	 */
	public static $singletons = [];

	/**
	 * Returns an instance of the class mapped to the passed $key using the 
	 * elements in $args as arguments for its constructor. If no mapping of $key
	 * to a class is found, the $key is treated as a className itself and the DI
	 * tries to look for a class matching that name, loading the necessary files
	 * dynamically.
	 *
	 * If the class that is mapped to the $key is a singleton, this method 
	 * checks if an instance of that class already exists. If so, that existing 
	 * instance is returned. If no instance is registered yet, a new instance is
	 * created and stored so that when the same singleton class is requested 
	 * later on, we can simply return this newly created instance without having
	 * to do the instantiation again.
	 *
	 * Whenever a new instance of a class that depends on other classes is 
	 * created, this method tries to inject all of the dependencies into the 
	 * constructor by recursively calling itself. For example, if a requested 
	 * class A depends on class B, an instance of class A will be created by 
	 * passing an instance of class B to it as a constructor parameter. If class
	 * B itself has more dependencies, those will be loaded first and injected
	 * into the constructor of class B, until all dependencies have been 
	 * resolved.
	 *
	 * @param	string
	 * @param	array
	 * @return	object
	 */
	public static function getInstanceOf($key, $args = array()) {
		// Check if there is already a Singleton instance registered within our 
		// Dependency Injector. If so: return it.
		if (isset(self::$singletons[$key])) {
			return self::$singletons[$key];
		}

		// Get the className that is registered for the given key. This will be 
		// the key itself by default, unless a mapping is found.
		$className  = self::getClassName($key);

		// We use a ReflectionClass to get all the necessary information about 
		// our className.
		$reflection = new \ReflectionClass($className);
		
		// Make sure our $reflection is instantiable.
		if ($reflection->isAbstract()) {
			throw new AbstractClassException($reflection->name);
		}

		$arguments = [];
		// Loop through all the class' constructor parameters to see which 
		// dependencies we need to inject.
		if (null !== ($constructor = $reflection->getConstructor())) {
			// We only need to loop through the parameters that we haven't 
			// received an argument for, so subtract those we do have an 
			// argument for.
			foreach ($constructor->getParameters() as $parameter) {
				// If a type hinted class is found, recursively inject that 
				// dependency class.
				if (null !== ($class = $parameter->getClass()) && !$parameter->isDefaultValueAvailable()) {
					try {
						$arguments[] = self::getInstanceOf($class->name);
					} catch (AbstractClassException $e) {
						break;
					}
				}

				// If no class was found, we've stopped looping through the 
				// dependencies (only "simple" parameters with default values 
				// remain), so we break.
				else {
					break;
				}
			}
			
			// After all of the class' dependencies have been created, merge 
			// those with the remaining arguments and create a new instance.
			$instance = $reflection->newInstanceArgs(array_merge($arguments, $args));
		} else {
			// If the class didn't have a constructor method, we can create a 
			// new instance of the class without invoking a constructor method.
			$instance = $reflection->newInstanceWithoutConstructor();
		}

		// If the class is supposed to be a singleton, we register this newly 
		// created object into the DI so that it can be quickly returned when 
		// it's requested again.
		try {
			$reflection->getProperty('singleton');
			self::$singletons[$key] = $instance;
		} catch (ReflectionException $e) {}

		// Finally, return our newly created object.
		return $instance;
	}

	/**
	 * Returns a new Proxy instance of a class, meaning it is created without 
	 * invoking its constructor.
	 *
	 * @param	string	$key
	 * @return	object
	 */
	public static function getProxyInstanceOf($key) {
		// Get the className that is registered for the given key. This will be 
		// the key itself by default, unless a mapping is found.
		$className  = self::getClassName($key);

		// We use a ReflectionClass to get all the necessary information about 
		// our className.
		$reflection = new \ReflectionClass($className);

		// If the class didn't have a constructor method, we can create a new 
		// instance of the class without invoking a constructor method.
		return $reflection->newInstanceWithoutConstructor();
	}

	/**
	 * Maps a class to a given key. This way, every time the key is requested, 
	 * an instance of its mapped class is returned instead. The key is first 
	 * formatted before anything else is done to make sure that the passed $key 
	 * satisfies the implicit conditions we've placed upon it.
	 *
	 * @param	string
	 * @param	string
	 */
	public static function mapClass($key, $className) {
		self::$map[$key] = $className;
	}

	/**
	 * Returns the full name of a class based on the passed key. If no key is 
	 * found in the mapping, the key itself is treated as a className and it 
	 * will be formatted correctly before being returned.
	 *
	 * @param	string
	 * @return	string
	 */
	public static function getClassName($key) {
		return (isset(self::$map[$key])) ? self::$map[$key] : $key;
	}
}