<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Composition;

class ModelFactory extends Factory {
	public function build($arg = null, $args = []) {
		return DI::getInstanceOf((strpos($arg, '\\') === false) ? NS_MODELS . $arg : $arg, $args);
	}

	public function proxy($arg) {
		return DI::getProxyInstanceOf((strpos($arg, '\\') === false) ? NS_MODELS . $arg : $arg);
	}
}