<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Composition;

class ResponseFactory extends Factory {
	/**
	 * The default Response type that is used if no response type is explicitly
	 * specified.
	 * 
	 * @var string
	 */
	const DEFAULT_RESPONSE_TYPE = 'html';
	
	/**
	 * The identifier for a custom Response type that is used if the given 
	 * response type does not match a known class. The custom response type can
	 * be used to create a response with a non-supported content-type and will
	 * not use a template file.
	 * 
	 * @var string
	 */
	const CUSTOM_RESPONSE_TYPE = 'custom';
	
	/**
	 * The namespace prefix that should be prepended to a simple Response class
	 * name so that it forms a valid class path. Because this is prepended to a
	 * class name, we need a trailing backslash!
	 * 
	 * @var	string
	 */
	const RESPONSE_NAMESPACE = 'Rozyn\\Response\\';
	
	/**
	 * Returns a Response object based on the specified arguments.
	 * 
	 * @param	string	$arg
	 * @param	array	$args
	 * @return	\Rozyn\Response\Response
	 */
	public function build($arg = null, array $args = []) {
		// Check if $arg contains a forward slash, in which case we'll assume
		// it's referring to a Content-type instead of a Response subclass. That
		// means we need to load our custom Response class instead of trying to
		// figure out which particular subclass we should load.
		if (strpos($arg, '/') === false) {
			// Extract the requested class name from the given argument.
			$className = (null === $arg) ? $this->getDefaultResponseClass() : $this->format($arg);

			// Convert that class name to a full namespaced class path.
			$classPath = ($this->isRozyn($className)) ? 
							static::RESPONSE_NAMESPACE . $className : 
							$className;

			// Check if the inferred class path matches a known class.
			if (class_exists($classPath)) {
				// If so, instantiate it and return it.
				return DI::getInstanceOf($classPath, $args);
			}
		}
		
		// If we haven't found a valid subclass up until this point, we'll treat 
		// the original argument as the requested content type for our custom 
		// response class.
		$response = DI::getInstanceOf(static::RESPONSE_NAMESPACE . $this->format(static::CUSTOM_RESPONSE_TYPE), $args);
		
		// Set the proper Content-type header for this response and return it.
		$response->setHeader('Content-type', $arg);
		return $response;
	}

	/**
	 * Check if the given response type matches a core response class file of
	 * our App.
	 * 
	 * @param	string	$arg
	 * @return	boolean
	 */
	private function isRozyn($arg) {
		return file_exists(get_class_path(static::RESPONSE_NAMESPACE . $arg));
	}

	/**
	 * Turns a potentially malformed response class name into a valid one.
	 * 
	 * @param	string	$arg
	 * @return	string
	 */
	private function format($arg) {
		return suffix(snake2camel($arg), 'Response');
	}
	
	/**
	 * Returns the class corresponding to the default response type.
	 * 
	 * @return	string
	 */
	private function getDefaultResponseClass() {
		return str_replace(' ', '', ucwords(str_replace('_', ' ', static::DEFAULT_RESPONSE_TYPE))) . 'Response';
	}
}