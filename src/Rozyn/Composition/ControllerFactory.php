<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Composition;

class ControllerFactory extends Factory {
	/**
	 * A response factory.
	 * 
	 * @var	\Rozyn\Factory\ResponseFactory
	 */
	protected $responseFactory;
	
	/**
	 * 
	 * @param	\Rozyn\Composition\ResponseFactory	$responseFactory
	 */
	public function __construct(ResponseFactory $responseFactory) {
		$this->responseFactory = $responseFactory;
	}
	
	/**
	 * Converts a controller name to a full Controller object, passing along the
	 * $controllerArgs to its constructor.
	 * 
	 * @param	string	$name
	 * @param	array	$args
	 * @return	\Rozyn\Controller\Controller
	 */
	public function build($name = null, $args = []) {
		if (empty($args)) {
			$args = [$this->responseFactory->build()];
		}
		
		return DI::getInstanceOf($this->format($name), $args);
	}

	/**
	 * Formats the controller name so that it's guaranteed to match the expected
	 * format.
	 * 
	 * @param	string	$arg
	 * @return	type
	 */
	private function format($arg) {
		$controllerName = suffix(ucfirst($arg), 'Controller');

		return (starts_with($controllerName, '\\')) ? $controllerName : prefix($controllerName, NS_CONTROLLERS);
	}
}