<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Composition;

class CollectionFactory extends Factory {
	public function build($arg = null, $args = []) {
		return DI::getInstanceOf(($arg) ? 'Rozyn\Collections\\' . $arg : 'Rozyn\Data\Collection', $args);
	}
}