<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use Rozyn\Config\Config;
use Rozyn\Helper\HtmlHelper;
use Rozyn\Helper\FormHelper;
use Rozyn\Composition\ControllerFactory;

abstract class Response extends ResponseDocument implements ResponseDocumentInterface {	
	/**
	 * The View object associated with this Response.
	 * 
	 * @var	\Rozyn\Response\View
	 */
	protected $view;
	
	/**
	 * An array of all headers for this Response.
	 * 
	 * @var	string[] 
	 */
	protected $headers = [];
	
	/**
	 * The status code of the response.
	 * 
	 * @var	int
	 */
	protected $code;
	
	/**
	 * Holds a PreProcessor instance that can be used to perform some methods on
	 * this Response object before it is rendered.
	 * 
	 * @var	\Rozyn\Response\PreProcessor
	 */
	protected $preProcessor;
	
	/**
	 * Holds a PostProcessor instance that can be used to perform last minute
	 * editing after the response string has been rendered.
	 * 
	 * @var	\Rozyn\Response\PostProcessor
	 */
	protected $postProcessor;
	
	/**
	 * Returns the full output for this Response's content.
	 * 
	 * @return	string
	 */
	public abstract function render();
	
	/**
	 * Object constructor.
	 * 
	 * @param	\Rozyn\Config\Config					$config
	 * @param	\Rozyn\Response\PreProcessor			$preProcessor
	 * @param	\Rozyn\Response\PostProcessor			$postProcessor
	 * @param	\Rozyn\Composition\ControllerFactory	$controllerFactory
	 * @param	\Rozyn\Helper\HtmlHelper				$htmlHelper
	 * @param	\Rozyn\Helper\FormHelper				$formHelper
	 * @param	\Rozyn\Response\View					$view
	 * @param	string									$file
	 */
	public function __construct(Config $config,
								PreProcessor $preProcessor,
								PostProcessor $postProcessor,
								ControllerFactory $controllerFactory,
								HtmlHelper $htmlHelper,
								FormHelper $formHelper,
								View $view,
								$file = null) {
		
		// Set up our basics.
		$this->config = $config;
		$this->controllerFactory = $controllerFactory;
		$this->Html = $htmlHelper;
		$this->Form = $formHelper;
		
		$this->setPreProcessor($preProcessor);
		$this->setPostProcessor($postProcessor);
		$this->setView($view);
		$this->setFile($file);
	}
	
	/**
	 * Write the response to the client's browser.
	 */
	public function write() {
		$this->writeHeaders();
		$this->writeBody();
	}
	
	/**
	 * Write the response headers to the client's browser.
	 */
	public function writeHeaders() {
		// Output the response code.
		http_response_code($this->getCode());
		
		// Output the other headers.
		foreach ($this->getHeaders() as $field => $description) {
			// If this header has a numeric index, assume that the description
			// specifies both the field and its corresponding value.
			if (is_int($field)) {
				header($description);
			} 
			
			// If not, the field holds the name of the header field and 
			// description will hold the corresponding value.
			else {
				header($field . ': ' . $description);
			}
		}
	}
	
	/**
	 * Write the response content to the client's browser.
	 */
	public function writeBody() {
		if (null === $this->body) {
			$this->setBody($this->render());
		}
		
		echo $this->getBody();
	}
	
	/**
	 * Sets the template file of this Response.
	 * 
	 * @param	string $file
	 */
	public function setFile($file) {
		return parent::setFile($this->formatTemplateFileName($file));
	}
	
	/**
	 * Sets the View object associated with this Response.
	 * 
	 * @param	\Rozyn\Response\View	$view
	 */
	public function setView(View $view) {
		$this->view = $view;
	}
	
	/**
	 * Returns the View for this Response.
	 * 
	 * @return	\Rozyn\Response\View
	 */
	public function getView() {
		return $this->view;
	}
	
	/**
	 * Sets the status code of the Response.
	 * 
	 * @param	int	$code
	 */
	public function setCode($code) {
		$this->code = $code;
	}
	
	/**
	 * Returns the status code of the Response.
	 * 
	 * @return	int
	 */
	public function getCode() {
		return $this->code ?: 200;
	}
	
	/**
	 * Returns the type of Response.
	 * 
	 * @return	string
	 */
	public function getType() {
		return strtr(get_class_name($this), 'Response', '');
	}
	
	/**
	 * Returns the data type of this Response.
	 * 
	 * @return	string
	 */
	public function getDataType() {
		return strtolower($this->getType());
	}
	
	/**
	 * Set the headers for this Response.
	 * 
	 * @param	array	$headers
	 */
	public function setHeaders(array $headers) {
		$this->headers = $headers;
	}
	
	/**
	 * Add a single header to this Response.
	 * 
	 * @param	string	$header
	 * @param	string	$value
	 */
	public function setHeader($header, $value) {
		$this->headers[$header] = $value;
	}
	
	/**
	 * Add a single header to this Response.
	 * 
	 * @param	string	$header
	 * @param	string	$value
	 */
	public function addHeader($header, $value) {
		$this->setHeader($header, $value);
	}
	
	/**
	 * Add multiple headers to this Response.
	 * 
	 * @param	string[]	$headers
	 */
	public function addHeaders(array $headers) {
		$this->headers = array_merge($this->headers, $headers);
	}
	
	/**
	 * Returns all the headers for this Response.
	 * 
	 * @return	array
	 */
	public function getHeaders() {
		return $this->headers;
	}
	
	/**
	 * Assign a PreProcessor to this Response.
	 * 
	 * @param	\Rozyn\Response\PreProcessor	$preProcessor
	 */
	public function setPreProcessor(PreProcessor $preProcessor) {
		$this->preProcessor = $preProcessor;
	}
	
	/**
	 * Assign a PostProcessor to this Response.
	 * 
	 * @param	\Rozyn\Response\PostProcessor	$postProcessor
	 */
	public function setPostProcessor(PostProcessor $postProcessor) {
		$this->postProcessor = $postProcessor;
	}
	
	/**
	 * Retrieve the stylesheets for this Response.
	 * 
	 * @return	array
	 */
	public function getStyleSheets() {
		return (null === $this->getView()) ? 
					parent::getStyleSheets() :
					array_unique(array_merge(parent::getStyleSheets(), $this->getView()->getStyleSheets()));
	}
	
	/**
	 * Retrieve the scripts for this Response.
	 * 
	 * @return	array
	 */
	public function getScripts() {
		return (null === $this->getView()) ? 
					parent::getScripts() : 
					array_unique(array_merge(parent::getScripts(), $this->getView()->getScripts()));
	}
	
	/**
	 * Retrieve the meta tags for this Response.
	 * 
	 * @return	array
	 */
	public function getMeta() {
		return (null === $this->getView()) ? 
					parent::getMeta() :
					array_replace(parent::getMeta(), $this->getView()->getMeta());
	}
	
	/**
	 * Returns a string ready to be used in an HTML document containing all the
	 * associated stylesheets.
	 * 
	 * @return	string
	 */
	public function getStyleSheetTags() {
		if ($this->preProcessor === null) {
			$res = '';
			
			foreach ($this->getStyleSheets() as $asset) {
				$res .= $this->Html->css(asset($asset));
			}
			
			return $res;
		}
		
		return $this->Html->css($this->preProcessor->compileStyleSheets($this));
	}
	
	/**
	 * Returns a string ready to be used in an HTML document containing all the
	 * associated scripts.
	 * 
	 * @return	string
	 */
	public function getScriptTags() {
		if ($this->preProcessor === null) {
			$res = '';
			
			foreach ($this->getScripts() as $asset) {
				$res .= $this->Html->js(asset($asset));
			}
			
			return $res;
		}
		
		return $this->Html->js($this->preProcessor->compileScripts($this));
	}
	
	/**
	 * Returns a string ready to be used in an HTML document containing all the
	 * associated meta tags.
	 * 
	 * @return	string
	 */
	public function getMetaTags() {
		$res	= [];
		$meta	= $this->getMeta();
		
		foreach ($meta as $key => $value) {
			$res[] = $this->Html->tag('meta', ['name' => $key, 'property' => $key, 'content' => $value]);
		}
		
		return implode("\n", $res);
	}
	
	/**
	 * Returns whether or not this View has a title associated with it.
	 * 
	 * @return	boolean
	 */
	public function hasTitle() {
		return $this->title !== null;
	}
	
	/**
	 * Pass unknown method calls to the first View in the Response.
	 * 
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	mixed
	 */
	public function __call($name, $arguments) {
		if (null !== $this->getView() && method_exists($this->getView(), $name)) {
			return call_user_func_array([$this->getView(), $name], $arguments);
		}
	}
	
	/**
	 * Prepare this Response for caching.
	 */
	public function __beforeCache() {
		unset($this->config);
		unset($this->scripts);
		unset($this->styles);
		unset($this->file);
		unset($this->view);
		unset($this->preProcessor);
		unset($this->postProcessor);
		unset($this->controllerFactory);
		unset($this->Html);
		unset($this->Form);
	}
}