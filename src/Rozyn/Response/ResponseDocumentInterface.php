<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use Rozyn\Config\Config;
use Rozyn\Event\EventHandler;
use Rozyn\Composition\ControllerFactory;
use Rozyn\Helper\HtmlHelper;
use Rozyn\Helper\FormHelper;

interface ResponseDocumentInterface {
	/**
	 * Returns the full output for this entire Response.
	 * 
	 * @return	string
	 */
	public function render();
	
	/**
	 * Sets the file of this object to the specified file.
	 * 
	 * @param	string $file
	 */
	public function setFile($file);
	
	/**
	 * Returns whether or not a file is associated with this ResponseBody.
	 * 
	 * @return	bool
	 */
	public function hasFile();
	
	/**
	 * Returns the file associated with this ResponseBody.
	 * 
	 * @return	\Rozyn\Filesystem\File
	 */
	public function getFile();

	/**
	 * Set the body for the response.
	 * 
	 * @param	string	$body
	 */
	public function setBody($body);

	/**
	 * Get the body for the response.
	 * 
	 * @return	string
	 */
	public function getBody();
	
	/**
	 * Returns whether or not the Response has body.
	 * 
	 * @return	boolean
	 */
	public function hasBody();
	
	/**
	 * Takes all the assets from another ResponseDocument and merges them with
	 * this object's assets.
	 * 
	 * @param	\Rozyn\Response\ResponseDocumentInterface $response
	 */
	public function mergeAssets(ResponseDocumentInterface $response);
	
	/**
	 * Add any number of stylesheets to this Document and returns a string that 
	 * is ready for use in your HTML templates.
	 * 
	 * @param	mixed
	 * @return	string
	 */
	public function style();
	
	/**
	 * Add any number of scripts to this Document and returns a string that is 
	 * ready for use in your HTML templates.
	 * 
	 * @param	mixed
	 * @return	string
	 */
	public function script();
	
	/**
	 * Retrieve the stylesheets for this Response.
	 * 
	 * @return	array
	 */
	public function getStyleSheets();
	
	/**
	 * Retrieve the scripts for this Response.
	 * 
	 * @return	array
	 */
	public function getScripts();
}