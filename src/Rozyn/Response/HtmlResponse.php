<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

class HtmlResponse extends Response {
	use Templatable;
	
	/**
	 * The default template file used for this response type.
	 * 
	 * @var string
	 */
	const DEFAULT_TEMPLATE_FILE = 'default';
	
	/**
	 * An array of all headers for this Response.
	 * 
	 * @var	string[] 
	 */
	protected $headers = ['Content-Type' => 'text/html; charset=utf-8'];
	
	/**
	 * Returns the full output for this Response's content.
	 * 
	 * @return	string
	 * @throws	\Rozyn\Response\TemplateNotFoundException
	 */
	public function render() {
		if (!$this->hasFile()) {
			$this->setFile(static::DEFAULT_TEMPLATE_FILE);
		}
		
		if (!file_exists($this->getFile())) {
			throw new TemplateNotFoundException($this->getFile());
		}
		
		$content = $this->getView()->render();
		extract($this->getView()->getVars());
		
		// Store the content so that it becomes accessible from within our 
		// template file.
		$this->setContent($content);
				
		// We use a buffer to store the contents of our view file in a variable 
		// while simultaneously parsing any PHP code in the view file. Because 
		// we use this approach, the magic $this variable can be used inside the
		// view file to refer back to this View object.
		ob_start();
		require($this->getFile());
		
		return ob_get_clean();
	}
}