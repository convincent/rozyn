<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use MatthiasMullie\Minify\CSS;
use JShrink\Minifier as JS;

class Minifier {
	/**
	 * The suffix used for files that are already minimized.
	 * 
	 * @var	string
	 */
	const MINIFIED_SUFFIX = '.min';
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds a CSS minifier.
	 * 
	 * @var	\MatthiasMullie\Minify\CSS
	 */
	private $css;
	
	/**
	 * Holds a JS minifier.
	 * 
	 * @var	\JShrink\Minifier
	 */
	private $js;
	
	/**
	 * Construct our minifier object.
	 * 
	 * @param	\MatthiasMullie\Minify\CSS	$css
	 * @param	\JShrink\Minifier			$js
	 */
	public function __construct(CSS $css, JS $js) {
		$this->css	= $css;
		$this->js	= $js;
	}
	
	/**
	 * Minifies a CSS string.
	 * 
	 * @param	string	$src
	 * @return	string
	 */
	public function css($src) {
		$css = new $this->css();
		$css->add($src);
		
		return $css->minify();
	}
	
	/**
	 * Minifies a JS string.
	 * 
	 * @param	string	$src
	 * @return	string
	 */
	public function js($src) {
		return $src;
	}
	
	/**
	 * Checks whether or not a given file is minimized already.
	 * 
	 * @param	string	$file
	 * @return	boolean
	 */
	public function isMinified($file) {
		return preg_match('/' . preg_quote(static::MINIFIED_SUFFIX, '/') . '\.[a-zA-Z0-9]+$/', $file);
	}
}