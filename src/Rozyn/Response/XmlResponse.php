<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use JsonSerializable;

class XmlResponse extends Response {
	// Since this Response supports template file, import the Templatable trait.
	use Templatable;
	
	/**
	 * An array of all headers for this Response.
	 * 
	 * @var	string[] 
	 */
	protected $headers = ['Content-Type' => 'application/xml; charset=utf-8'];
	
	/**
	 * Returns the full output for this Response's content.
	 * 
	 * @return	string
	 */
	public function render() {
		$vars = [];

		foreach ($this->getVars() as $key => $value) {
			// Check if the key would make for a valid XML tag.
			if (!preg_match('/^[a-zA-Z]/', $key)) {
				continue;
			}

			// If our value is an object, check to see if it can be serialized
			// into JSON. If so, we can do that to make sure we can properly
			// parse and format it for inclusion in our XML document. If for
			// some reason it cannot be properly serialized, ignore this 
			// key/value pair.
			if (is_object($value)) {
				if (!$value instanceof JsonSerializable) {
					continue;
				}

				$value = $value->jsonSerialize();
			}

			// Add the key/value pair to our XML document.
			$vars[$key] = $value;
		}

		// We use a buffer to store the contents of our template file. If no 
		// template file was specified, we use a default template instead.
		$xml = array_to_xml($vars);

		if (!$this->hasFile()) {
			return $xml->asXML();
		}
		
		$content = '';
		foreach ($xml->children() as $child) {
			$content .= $child->asXML();
		}

		$this->setContent($content);

		ob_start();
		require($this->getFile());

		return ob_get_clean();
	}
}