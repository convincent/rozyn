<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use Rozyn\Filesystem\FileNotFoundException;
use Rozyn\Config\Config;
use Rozyn\Composition\ControllerFactory;
use Rozyn\Helper\HtmlHelper;
use Rozyn\Helper\FormHelper;

class View extends ResponseDocument implements ResponseDocumentInterface {
	/**
	 * Holds all the variables associated with this View.
	 * 
	 * @var	array
	 */
	protected $vars = [];
	
	/**
	 * Holds the title associated with this View.
	 * 
	 * @var	string
	 */
	protected $title;
	
	/**
	 * Creates a new View instance.
	 * 
	 * @param	\Rozyn\Config\Config					$config
	 * @param	\Rozyn\Composition\ControllerFactory	$controllerFactory
	 * @param	\Rozyn\Helper\HtmlHelper				$htmlHelper
	 * @param	\Rozyn\Helper\FormHelper				$formHelper
	 * @param	\Rozyn\Filesystem\File|string			$file
	 */
	public function __construct(Config $config,
								ControllerFactory $controllerFactory,
								HtmlHelper $htmlHelper,
								FormHelper $formHelper,
								$file = null) {
		
		// Set up our basics.
		$this->config = $config;
		$this->controllerFactory = $controllerFactory;
		$this->Html = $htmlHelper;
		$this->Form = $formHelper;
				
		// Store the file associated with this View.
		$this->setFile($file);
	}
	
	/**
	 * Renders the View and returns the string result.
	 * 
	 * @return	string
	 * @throws	\Rozyn\Filesystem\FileNotFoundException
	 */
	public function render() {
		if (!$this->hasFile()) {
			throw new NoResponseFileSpecifiedException('View has no associated file.');
		}
		
		if (!file_exists($this->getFile())) {
			throw new FileNotFoundException('Response file ' . $this->getFile() . ' could not be found.');
		}
		
		// We use a buffer to store the contents of our view file in a variable 
		// while simultaneously parsing any PHP code in the view file. Because 
		// we use this approach, the magic $this variable can be used inside the
		// view file to refer back to this View object.
		extract($this->vars);

		ob_start();
		require $this->getFile();
		$this->setBody(ob_get_clean());

		return $this->getBody();
	}
	
	/**
	 * Sets the file of this object to the specified file.
	 * 
	 * @param	string	$file
	 */
	public function setFile($file) {
		return parent::setFile($this->formatViewFileName($file));
	}
	
	/**
	 * Set the title for any page rendering this View.
	 * 
	 * @param	string	$title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * Get the title associated with this View.
	 * 
	 * @return	string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Returns the variables assigned to this View.
	 * 
	 * @return	array
	 */
	public function getVars() {
		return $this->vars;
	}
	
	/**
	 * Returns the value of a single variable assigned to this View.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	array
	 */
	public function getVar($key, $default = null) {
		return (array_key_exists($key, $this->vars)) ? $this->vars[$key] : $default;
	}
	
	/**
	 * Add a single variables to this View.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 */
	public function addVar($key, $value) {
		$this->vars[$key] = $value;
	}
	
	/**
	 * Add a number of variables to this View.
	 * 
	 * @param	array	$vars
	 */
	public function addVars(array $vars) {
		$this->vars = array_merge($this->vars, $vars);
	}
	
	/**
	 * Reset the variables assigned to this View to the specified array.
	 * 
	 * @param	array	$vars
	 */
	public function setVars(array $vars) {
		$this->vars = $vars;
	}
	
	/**
	 * Unset a variable assigned to this View.
	 * 
	 * @param	string	$key
	 */
	public function deleteVar($key) {
		unset($this->vars[$key]);
	}
}