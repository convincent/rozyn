<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use Rozyn\Cache\PublicCache;

class PreProcessor {	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * A Cache handler that we can use to check whether or not certain assets
	 * have been cached.
	 * 
	 * @var	\Rozyn\Cache\PublicCache
	 */
	protected $cache;
	
	/**
	 * A Minifier that can minify our concatenated assets.
	 * 
	 * @var	\Rozyn\Response\Minifier
	 */
	protected $minifier;
	
	/**
	 * Constructs the PreProcessor object.
	 * 
	 * @param	\Rozyn\Cache\PublicCache	$cache
	 * @param	\Rozyn\Response\Minifier	$minifier
	 */
	public function __construct(PublicCache $cache, Minifier $minifier) {
		$this->cache	= $cache;
		$this->minifier	= $minifier;
	}
	
	/**
	 * Compile all stylesheets assets into one file and returns the path to said
	 * file.
	 * 
	 * @param	\Rozyn\Response\Response
	 * @return	string
	 */
	public function compileStyleSheets(Response $response) {
		return $this->compileAssets($response->getStyleSheets(), 'css');
	}
	
	/**
	 * Compile all script assets into one file and returns the path to said file.
	 * 
	 * @param	\Rozyn\Response\Response
	 * @return	string
	 */
	public function compileScripts(Response $response) {
		return $this->compileAssets($response->getScripts(), 'js');
	}
	
	/**
	 * Takes an array of assets of the given $type (css/js) and bundles the
	 * contents of all the assets into a single file. The path to that file is
	 * then returned.
	 * 
	 * @param	string[]	$assets
	 * @param	string		$type
	 * @return	string
	 */
	protected function compileAssets(array $assets, $type) {
		// Initialize a hash variable that we will use to later check if a cache
		// version of the compiled asset file exists.
		$hash = '';
		
		// Loop through each asset and determine each asset's separate hash.
		// This hash is based on the last time the asset was modified and its
		// name. This way, we ensure that if one of the assets has been modified
		// since it was last compiled, the entire asset collection will be 
		// recompiled.
		foreach ($assets as $asset) {
			$hash .= $asset . filemtime($this->getAssetPath($asset));
		}
		
		// Finalize our hash value by running its current value through md5()
		// and appending the proper file extension based on the $type of assets.
		$hash = md5($hash) . '.' . $type;
		
		// Check if we've previously compiled this list of assets. If not, time
		// to do so now.
		if (!$this->cache->has($hash)) {
			$str = '';
			
			// Loop through all the assets once more, this time reading the full
			// content of each one to write it to our compiled file.
			foreach ($assets as $asset) {
				$path = $this->getAssetPath($asset);
				
				// Read the content of the file.
				$content = file_get_contents($path);
				
				// Translate any labels in the asset file. Labels can be 
				// identified via double curly brackets preceding and succeeding
				// them, ex: {{label}}.
				$content = preg_replace_callback('/\{\{([^}]+)\}\}/', function($matches) {
								return __(trim($matches[1]));
							}, $content);
				
				// If we're dealing with CSS assets we need to correct relative
				// paths where necessary.
				if ($type === 'css') {
					$baseDirs = preg_split('/[\/' . PREG_DS . ']/', str_replace(__ROOT__, '', dirname($path)));
					$content = preg_replace_callback('/url\(["\']?((?:\.\.\/)+)(.*?)["\']?\)/', function($matches) use ($baseDirs) {
						return 'url(' . __WWW_ROOT__ . implode(URI_SEPARATOR, array_slice($baseDirs, 0, -strlen($matches[1]) / 3)) . URI_SEPARATOR . $matches[2] . ')';
					}, $content);
				}
			
				// Minify the result if it is not already minified (which can be 
				// checked based on whether or not the file name ends in ".min.js"
				// for example, where "js" is an example value of $type, but could
				// also be something like "css".
				if (!$this->minifier->isMinified($path) && method_exists($this->minifier, $type)) {
					$content = $this->minifier->$type($content);
				}
				
				$str .= $content . PHP_EOL;
			}
			
			// After all assets have been read, write the concatenated string
			// containing all the assets' content to our compiled file.
			$this->cache->write($hash, $str);
		}
		
		// Return the path to our compiled asset file.
		return $this->cache->asset($hash);
	}
	
	/**
	 * Returns the include path to a given asset.
	 * 
	 * @param	string
	 * @return	string
	 */
	protected function getAssetPath($asset) {
		return (($asset[0] === URI_SEPARATOR) ? __ROOT__ : __PUBLIC__ . DS) . $asset;
	}
}