<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use Rozyn\Filesystem\MimeTypes;

class FileResponse extends Response {
	use MimeTypes;
	
	/**
	 * Write the response headers to the client's browser.
	 */
	public function writeHeaders() {
		// Set the Content-type of the page to the mime type of this Response's 
		// file.
		header('Content-type: ' . $this->getContentType());
		
		// Write the other headers
		parent::writeHeaders();
	}
	
	/**
	 * Write the response content to the client's browser.
	 */
	public function writeBody() {
		echo $this->render();
		exit;
	}
	
	/**
	 * Returns the full output for this Response's content.
	 * 
	 * @return	string
	 */
	public function render() {
		// Any text-based files will be loaded through PHP's require function. 
		// This allows us to use PHP variables and constants in these files.
		if (starts_with($this->getContentType(), 'text/')) {
			require($this->file()->inclpath());
		}
		
		// Most other files should be loadable through file_get_contents().
		else {
			echo file_get_contents($this->file()->inclpath());
		}
	}
	
	/**
	 * Returns the file associated with this Response. In this case, the file 
	 * directly associated with the Response is irrelevant. Instead, the file 
	 * of the View associated with this Response should be returned, since that
	 * is the one that has to be rendered. 
	 * 
	 * @return	\Rozyn\Filesystem\File
	 */
	public function file() {
		if (null === $this->getView()) {
			return parent::file();
		}
		
		return $this->getView()->getFile();
	}
	
	/**
	 * Returns the content type for the file associated with this Response.
	 * 
	 * @return	string
	 */
	protected function getContentType() {
		return (isset($this->mimeTypes[$this->file()->ext()])) ? 
								$this->mimeTypes[$this->file()->ext()] : 
								$this->file()->mime();
	}
}