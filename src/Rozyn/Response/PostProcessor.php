<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use Rozyn\Cache\PublicCache;

class PostProcessor {
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * A Cache handler that we can use to check whether or not certain assets
	 * have been cached.
	 * 
	 * @var	\Rozyn\Cache\Cache
	 */
	protected $cache;
	
	/**
	 * Constructs the PostProcessor object.
	 * 
	 * @param	\Rozyn\Cache\PublicCache	$cache
	 */
	public function __construct(PublicCache $cache) {
		$this->cache = $cache;
	}
	
	/**
	 * Takes an HTML string as parameter and performs all necessary post-processing
	 * logic on it before returning the result.
	 * 
	 * @param	string	$html
	 * @return	string
	 */
	public function processHtml($html) {
		$html = preg_replace("/(\t)+/", ' ', $html);
		$html = preg_replace_callback('/\{\{([^}]+)\}\}/', function($matches) {
			return __(trim($matches[1]));
		}, $html);
		
		return $html;
	}
}