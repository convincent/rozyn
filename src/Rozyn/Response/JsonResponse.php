<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

class JsonResponse extends Response {
	/**
	 * An array of all headers for this Response.
	 * 
	 * @var	string[] 
	 */
	protected $headers = ['Content-type' => 'application/json; charset=utf-8'];
	/**
	 * An array of variables that should not be passed along to the response.
	 * 
	 * @var	array
	 */
	protected $secret = array(
		'_controller',
		'_method',
		'_args',
		'_alias',
	);
	
	/**
	 * Returns the full output for this Response's content.
	 * 
	 * @return	string
	 */
	public function render() {
		return json_encode(array_diff_key($this->getView()->getVars(), array_flip($this->secret)));
	}
}