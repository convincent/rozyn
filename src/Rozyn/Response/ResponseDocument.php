<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

use Rozyn\Composition\DI;

abstract class ResponseDocument {
	/**
	 * The file extension used by view files.
	 * 
	 * @var string
	 */
	const VIEW_EXTENSION = '.php';
	
	/**
	 * The file extension of template files.
	 * 
	 * @var string
	 */
	const TEMPLATE_EXTENSION = '.php';
	
	/**
	 * Holds a configuration object that contains relevant options.
	 * 
	 * @var	\Rozyn\Config\Config
	 */
	protected $config;
	
	/**
	 * Holds the entire body of this document.
	 * 
	 * @var	string
	 */
	protected $body;

	/**
	 * Holds all stylesheets associated with this view.
	 * 
	 * @var	string[]
	 */
	protected $styles = [];
	
	/**
	 * Holds all scripts associated with this view.
	 * 
	 * @var	string[]
	 */
	protected $scripts = [];
	
	/**
	 * Holds all raw, unprocessed style arguments for this view.
	 * 
	 * @var	string[]
	 */
	private $rawStyles = [];
	
	/**
	 * Holds all raw, unprocessed script arguments for this view.
	 * 
	 * @var	string[]
	 */
	private $rawScripts = [];
	
	/**
	 * Holds all meta tags associated with this view.
	 * 
	 * @var	string[]
	 */
	protected $meta = [];
	
	/**
	 * Holds file that is associated with this view.
	 * 
	 * @var	\Rozyn\Filesystem\File
	 */
	protected $file;
	
	/**
	 * Tells us whether the stylesheet tags have been printed yet.
	 * 
	 * @var	boolean
	 */
	protected $printedStyleSheets = false;
	
	/**
	 * Tells us whether the script tags have been printed yet.
	 * 
	 * @var	boolean
	 */
	protected $printedScripts	 = false;
	
	/**
	 * The Controller Factory used to produce Controllers.
	 * 
	 * @var	\Rozyn\Composition\ControllerFactory
	 */
	protected $controllerFactory;
	
	/**
	 * A class that can be used from the view file to make dealing with HTML
	 * in a dynamic setting a bit more easier.
	 * 
	 * @var	\Rozyn\Helper\HtmlHelper
	 */
	protected $Html;
	
	/**
	 * A class that can be used from the view file to make dealing with Forms
	 * in a dynamic setting a bit more easier.
	 * 
	 * @var	\Rozyn\Helper\FormHelper
	 */
	protected $Form;
	
	/**
	 * Send an additional request to the server form within a Response object. 
	 * This is useful for when you always want to execute a certain piece of 
	 * code when a particular Response is rendered.
	 * 
	 * @param	string	$controllerName
	 * @param	string	$method
	 * @param	array	$params
	 * @return	\Rozyn\Response\View
	 */
	public function requestAction($controllerName, $method, array $params = []) {
		// Create the Controller instance.
		$controller = $this->controllerFactory->build($controllerName);
		
		// Call the desired method with the given parameters.
		$controller->__callMethod($method, $params);
		
		// Extract the View that we want from the Controller.
		$view = $controller->getView();
		
		// Render the view.
		$view->render();
		
		// Make sure that the new view also has access to all assets from this
		// view.
		$this->mergeMeta($view);
		$this->mergeAssets($view);
		
		// Finally, return the newly created view.
		return $view;
	}
	
	/**
	 * Loads a view with a given set of variables without invoking a controller 
	 * method.
	 * 
	 * @param	string	$_file
	 * @param	array	$vars
	 * @return	string
	 */
	public function requestView($_file, array $vars = []) {
		extract($this->getVars());
		
		if (!empty($vars)) {
			extract($vars);
		}
		
		ob_start();
		include $this->formatViewFileName($_file);
		
		return ob_get_clean();
	}
	
	/**
	 * Sets the file of this object to the specified file.
	 * 
	 * @param	string $file
	 * @return	\Rozyn\Response\ResponseDocument
	 */
	public function setFile($file) {
		$this->file = ($file === null) ? 
							null : 
							DI::getInstanceOf('Rozyn\Filesystem\File', array(
									preg_replace('/['. PREG_DS . ']+/',
									DS, 
									$file)));
		
		return $this;
	}
	
	/**
	 * Returns whether or not a file is associated with this ResponseBody.
	 * 
	 * @return	bool
	 */
	public function hasFile() {
		return $this->getFile() !== null;
	}
	
	/**
	 * Returns the file associated with this ResponseBody.
	 * 
	 * @return	\Rozyn\Filesystem\File
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * Sets the body for this document.
	 * 
	 * @param	string	$body
	 */
	public function setBody($body) {
		$this->body = $body;
	}

	/**
	 * Retrieves the body of this document.
	 * 
	 * @return	string
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * Tells us whether this document has a body.
	 * 
	 * @return	boolean
	 */
	public function hasBody() {
		return !!$this->body;
	}
	
	/**
	 * Add any number of stylesheets to this Document and returns a string that
	 * is ready for use in your HTML templates.
	 * 
	 * @param	mixed
	 */
	public function style() {
		$files = (func_num_args() == 1 && is_array(func_get_arg(0))) ? 
					func_get_arg(0) : 
					func_get_args();
		
		$this->styles = array_merge($this->styles ?: [], $this->asset($files, 'css', '.css'));
		$this->rawStyles = array_merge($this->rawStyles ?: [], $files);
	}
	
	/**
	 * Returns all stylesheets associated with this Document.
	 * 
	 * @return	string[]
	 */
	public function getStyleSheets() {
		return $this->styles;
	}
	
	/**
	 * Returns all raw, unprocessed stylesheets associated with this Document.
	 * 
	 * @return	string[]
	 */
	public function getRawStyleSheets() {
		return $this->rawStyles;
	}
	
	/**
	 * Add any number of scripts to this Document and returns a string that is 
	 * ready for use in your HTML templates.
	 * 
	 * @param	mixed
	 */
	public function script() {
		$files = (func_num_args() == 1 && is_array(func_get_arg(0))) ? 
					func_get_arg(0) :
					func_get_args();
		
		$this->scripts = array_merge($this->scripts ?: [], $this->asset($files, 'js', '.js'));
		$this->rawScripts = array_merge($this->rawScripts ?: [], $files);
	}
	
	/**
	 * Returns all scripts associated with this Document.
	 * 
	 * @return	string[]
	 */
	public function getScripts() {
		return $this->scripts;
	}
	
	/**
	 * Returns all raw, unprocessed scripts associated with this Document.
	 * 
	 * @return	string[]
	 */
	public function getRawScripts() {
		return $this->rawScripts;
	}
	
	/**
	 * Add a meta tag for this view.
	 * 
	 * @param	array|string	$key
	 * @param	string			$value
	 */
	public function meta($key, $value = null) {
		if ($value === null && is_array($key)) {
			$this->meta = array_replace($this->meta ?: [], $key);
		} else {
			$this->meta[$key] = $value;
		}
	}
	
	/**
	 * Returns all the meta information associated with this Document.
	 * 
	 * @return	string[]
	 */
	public function getMeta() {
		return $this->meta;
	}
	
	/**
	 * Merges the assets of another ResponseDocument with the assets of this
	 * Document.
	 * 
	 * @param	\Rozyn\Response\ResponseDocumentInterface	$response
	 */
	public function mergeAssets(ResponseDocumentInterface $response) {
		$this->style($response->getRawStyleSheets());
		$this->script($response->getRawScripts());
	}
	
	/**
	 * Merges the meta information of another ResponseDocument with the assets 
	 * of this Document.
	 * 
	 * @param	\Rozyn\Response\ResponseDocumentInterface	$response
	 */
	public function mergeMeta(ResponseDocumentInterface $response) {
		$this->meta($response->getMeta());
	}

	/**
	 * Returns a string representation of this Document.
	 * 
	 * @return	string
	 */
	public function __toString() {
		return ($this->hasFile()) ? $this->render() : $this->getBody();
	}
	
	/**
	 * Add assets to this ResponseDocument.
	 * 
	 * @param	array	$files
	 * @param	string	$type
	 * @param	string	$ext
	 * @return	array
	 */
	protected function asset(array $files, $type, $ext = null) {
		// Initialize the array that will hold all newly added asset files.
		$new = [];
		
		// Set up base URLs and base paths so that we can easily construct full
		// paths to the asset files later on.
		$base_url  = $type . URI_SEPARATOR;		
		$base_path = PUBLIC_DIR . DS . $type . DS;
		
		// Check if the asset is loaded from within a plugin. If so, use that
		// plugins asset directories as the root for assets with a relative path.
		$plugin = null;
		
		if (strpos($this->getFile(), __PLUGINS__) !== false) {
			$m = [];
			preg_match('/plugins'. PREG_DS . '([^' . PREG_DS . ']+)' . PREG_DS . '/', $this->getFile(), $m);
			
			if (count($m) > 1) {
				$plugin = $m[1];
			}
			
			// Adjust the base paths we previously created, since we will assume
			// the provided assets paths are relative to the plugin's public
			// folder, not the project's public folder.
			$base_url  = URI_SEPARATOR . PLUGINS_DIR . URI_SEPARATOR . $plugin . URI_SEPARATOR . PUBLIC_DIR . URI_SEPARATOR . $base_url;
			$base_path = PLUGINS_DIR . DS . $plugin . DS . $base_path;
		}
		
		// Prepend the base path with the path to our root directory to make it
		// an absolute path.
		$base_path = __ROOT__ . DS . $base_path;
		
		// Loop through all the asset files.
		foreach ($files as $file) {
			// Construct the full URL and the path to the asset file that we're
			// currently iterating over.
			$url	=  ($file[0] === URI_SEPARATOR) ? $file : $base_url . $file;
			$path	= (($file[0] === URI_SEPARATOR) ? __ROOT__ : $base_path) . $file;
			
			// Check if the asset file is actually referring to a directory. We
			// can check this by looking at the last character of the filename. 
			// If it is an asterisk, it means all files within this directory
			// should be loaded.
			if (substr($file, -1) === '*') {
				// Remove the asterisk from our previously computed URL and path
				// variables
				$url = substr($url,  0, -1);
				$dir = substr($path, 0, -2);
				
				// Compute the length of the file extension. This way, we can 
				// easily determine if a file matches the desired extension 
				// using PHP's substr() function with a fixed offset. Otherwise
				// we'd have to recompute the length of the extension string
				// again in each iteration and that's just silly.
				$extOffset = ($ext === null) ? $ext : -strlen($ext);
				
				// Loop through the specified directory.
				dir_map($dir, function($f) use ($ext, $extOffset, $url, &$new) {
					// Make sure that the file we're currently iterating over
					// matches the desired extension. This way we only include
					// actual asset files and not just every file we encounter.
					if ($ext === null || substr($f, $extOffset) === $ext) {
						// If a valid asset file is found, add it to our array
						// of newly added asset files.
						$new[] = $url . $f;
					}
				});
			} 
			
			// Make sure the asset actually exists before adding it.
			elseif (file_exists(suffix($path, $ext))) {
				$new[] = suffix($url, $ext);
			}
		}
		
		return $new;
	}
	
	/**
	 * Parses a given filename and returns a valid path to the view file.
	 * 
	 * @param	string $file
	 * @return	string
	 */
	protected function formatViewFileName($file) {
		$formatted = $this->formatFileName($file);
		
		if ($formatted === null) {
			return null;
		}
		
		return (($formatted[0] === DS) ? __ROOT__ : __VIEWS__ . DS) . 
				suffix($formatted, static::VIEW_EXTENSION);
	}
	
	/**
	 * Parses a given filename and returns a valid path to the response file.
	 * 
	 * @param	string $file
	 * @return	string
	 */
	protected function formatTemplateFileName($file) {
		$formatted = $this->formatFileName($file);
		
		if ($formatted === null) {
			return null;
		}
		
		return (($formatted[0] === DS) ? __ROOT__ : __TEMPLATES__ . DS) . 
				suffix($formatted, static::TEMPLATE_EXTENSION);
	}
	
	/**
	 * Performs some basic formatting operations on a file name.
	 * 
	 * @param	string	$file
	 * @return	string
	 */
	protected function formatFileName($file) {
		if ($file === null) {
			return null;
		}
		
		if (DS !== URI_SEPARATOR) {
			$file = str_replace(URI_SEPARATOR, DS, $file);
		}
		
		return $file;
	}
}