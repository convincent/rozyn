<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Response;

trait Templatable {
	/**
	 * The content for this Response that will be loaded into the template file.
	 * 
	 * @var	string
	 */
	protected $content;
	
	/**
	 * Set the content that is to be rendered within the template file.
	 * 
	 * @param	string	$content
	 */
	public function setContent($content) {
		$this->content = $content;
	}
	
	/**
	 * Retrieve the content that is to be rendered within the template file.
	 * 
	 * @return	string
	 */
	public function getContent() {
		return $this->content;
	}
}