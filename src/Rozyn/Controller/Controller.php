<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Controller;

use Exception;
use ReflectionClass;
use ReflectionMethod;
use Rozyn\Request\IncomingRequest;
use Rozyn\Routing\RedirectHandler;
use Rozyn\Session\FlashHandler;
use Rozyn\Auth\Authenticable;
use Rozyn\Config\Config;
use Rozyn\Response\Response;
use Rozyn\Composition\ModelFactory;
use Rozyn\Composition\ControllerFactory;
use Rozyn\Response\AjaxResponse;
use Rozyn\Response\JsonResponse;

abstract class Controller {
	/**
	 * The annotation name that identifies resources the client has to have
	 * access to in order to execute a given method.
	 * 
	 * @var	string
	 */
	const AUTH_ANNOTATION = 'auth';
	
	/**
	 * The name of the variable used to store any potential exception messages.
	 * 
	 * @var string
	 */
	const EXCEPTION_MESSAGE_VAR	= 'message';
	
	/**
	 * The name of the variable used to store any potential exception codes.
	 * 
	 * @var int
	 */
	const EXCEPTION_CODE_VAR = 'code';
	
	/**
	 * Holds a configuration object that contains relevant options.
	 * 
	 * @var	\Rozyn\Config\Config
	 */
	protected $config;
	
	/**
	 * Holds an Authenticable driver which we can use to check if the client has
	 * permissions to access a given method.
	 * 
	 * @var	\Rozyn\Auth\Authenticable
	 */
	protected $auth;
	
	/**
	 * Holds an IncomingRequest object that can be used to retrieve data that 
	 * was sent to the server for this request.
	 * 
	 * @var	\Rozyn\Request\IncomingRequest
	 */
	protected $request;
	
	/**
	 * Holds a RedirectHandler which can be used to route clients to other 
	 * pages upon completion of a request.
	 * 
	 * @var	\Rozyn\Routing\RedirectHandler
	 */
	protected $redirect;
	
	/**
	 * Holds a FlashHandler which can be used to temporarily store values that
	 * need to be carried over to the next request.
	 * 
	 * @var	\Rozyn\Session\FlashHandler
	 */
	protected $flash;
	
	/**
	 * Holds the name of the controller.
	 * 
	 * @var	string
	 */
	protected $name;

	/**
	 * Holds the name of the model associated with this controller.
	 * 
	 * @var	string
	 */
	protected $model;
	
	/**
	 * Holds a proxy instance of the Model associated with this Controller.
	 * 
	 * @var	\Rozyn\Model\Model
	 */
	protected $modelInstance;

	/**
	 * Holds a Response object that represents the result of a method in this 
	 * controller.
	 * 
	 * @var	\Rozyn\Response\Response
	 */
	protected $response;

	/**
	 * Holds the name of the folder in our views directory that holds all of the
	 * view files associated with this controller.
	 * 
	 * @var	string
	 */
	protected $views;

	/**
	 * Holds an array of all method names that are greenlighted to be accessed
	 * through a web interface. If set to null, all methods are considered to be
	 * accessible.
	 * 
	 * @var	string[]
	 */
	protected $accessible;

	/**
	 * Holds an array of all method names that clients are forbidden to access
	 * through a web interface. All methods native to this controller are 
	 * considered to be forbidden by default.
	 * 
	 * @var	string[]
	 */
	protected $forbidden;
	
	/**
	 * A Model Factory that can be used to generate model instances of the model
	 * associated with this Controller.
	 * 
	 * @var	\Rozyn\Composition\ModelFactory
	 */
	protected $modelFactory;
	
	/**
	 * A Controller Factory that can be used to generate new controllers on the
	 * fly should the current controller be a gateway for combining multiple
	 * other controllers.
	 * 
	 * @var	\Rozyn\Composition\ControllerFactory
	 */
	protected $controllerFactory;

	/**
	 * Build a new instance of our controller. Each instance of a controller 
	 * should only be responsible for one single rendering method. If two or 
	 * more methods from a same controller have to be called in a single 
	 * request, you should create new instances of this controller for each 
	 * separate method call.
	 * 
	 * @param	\Rozyn\Config\Config				$config
	 * @param	\Rozyn\Request\IncomingRequest		$request
	 * @param	\Rozyn\Routing\RedirectHandler		$redirect
	 * @param	\Rozyn\Auth\Authenticable			$auth
	 * @param	\Rozyn\Composition\ModelFactory		$modelFactory
	 * @param	\Rozyn\Response\Response			$response
	 */
	public function __construct(Config $config,
								IncomingRequest $request,
								RedirectHandler $redirect,
								FlashHandler $flash,
								Authenticable $auth, 
								ModelFactory $modelFactory,
								ControllerFactory $controllerFactory,
								Response $response = null) {
		
		$this->config				= $config;
		$this->auth					= $auth;
		$this->request				= $request;
		$this->redirect				= $redirect;
		$this->flash				= $flash;
		$this->modelFactory			= $modelFactory;
		$this->controllerFactory	= $controllerFactory;
		
		$this->setResponse($response);
		
		$this->setup();
		$this->init();
	}
	
	/**
	 * Sets the title on the View associated with this Controller.
	 * 
	 * @param	string	$title
	 */
	final public function setTitle($title) {
		$this->getResponse()->setTitle($title);
	}
	
	/**
	 * Gets the title of the View associated with this Controller.
	 * 
	 * @return	string
	 */
	final public function getTitle() {
		return $this->getResponse()->getTitle();
	}
	
	/**
	 * Returns whether or not the View of this Controller has a title associated
	 * with it.
	 * 
	 * @return	boolean
	 */
	final public function hasTitle() {
		return $this->getResponse()->hasTitle();
	}
	
	/**
	 * Sets the Response object for this Controller.
	 * 
	 * @param	\Rozyn\Response\Response	$response
	 */
	final public function setResponse(Response $response) {
		$this->response = $response;
	}
	
	/**
	 * Returns the Response object for this Controller.
	 * 
	 * @return	\Rozyn\Response\Response
	 */
	final public function getResponse() {
		return $this->response;
	}

	/**
	 * Retrieve this Controller's View object.
	 * 
	 * @return	\Rozyn\Response\View
	 */
	final public function getView() {
		return $this->response->getView();
	}
	
	/**
	 * Returns the base name of this Controller, without the "Controller" 
	 * substring.
	 * 
	 * @return	string
	 */
	final public function getName() {
		if (null === $this->name) {
			$this->name = preg_replace('/Controller$/', '', get_class_name($this));
		}
		
		return $this->name;
	}

	/**
	 * Specify a non-default file to be used for this Controller's View object.
	 * 
	 * @param	string	$file
	 */
	final protected function renders($file) {
		// Check if the given filename starts with a directory separator. If 
		// not, we prepend it with the path to this controller's view directory
		// to make sure we're dealing with a valid file path.
		if ($file[0] !== URI_SEPARATOR) {
			$file = $this->views . DS . $file;
		}
		
		$this->getView()->setFile($file);
	}

	/**
	 * Renders the Controller's View object and returns it. If a filename is 
	 * specified, that file is used as the View's source file instead of the 
	 * default source file.
	 * 
	 * @param	string	$file
	 * @param	array	$vars
	 * @return	\Rozyn\Response\View
	 */
	final protected function render($file = '', array $vars = []) {
		if ($file) {
			$this->renders($file);
		}
		
		$this->set($vars);
		
		return $this->getView()->render();
	}

	/**
	 * Adds one or more variables to the View's collection.
	 * 
	 * @param	array|string	$key
	 * @param	mixed			$value
	 */
	final protected function set($key, $value = null) {
		if (func_num_args() === 1) {
			$this->getView()->addVars($key);
		} else {
			$this->getView()->addVar($key, $value);			
		}
	}

	/**
	 * Retrieves a variable that was set on the View.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	final protected function get($key, $default = null) {
		return $this->getView()->getVar($key, $default);
	}

	/**
	 * Sets the title for the view.
	 * 
	 * @param	string	$title
	 */
	final protected function title($title) {
		return $this->getView()->setTitle($title);
	}

	/**
	 * Unsets one or more variables in the View's collection.
	 * 
	 * @param	string|string[]	$vars
	 */
	final protected function del($vars) {
		if (is_string($vars)) {
			$this->getView()->deleteVar($vars);
		} else {
			foreach ($vars as $var) {
				$this->getView()->deleteVar($var);
			}
		}
	}

	/**
	 * Unsets all variables in the View.
	 */
	final protected function delAll() {
		$this->delete(array_keys($this->getView()->getVars()));
	}
	
	/**
	 * Calls a method on the Model associated with this Controller.
	 * 
	 * @param	string	$method
	 * @param	array	$args
	 * @return	mixed
	 */
	final protected function callModel($method, array $args = []) {
		return call_user_func_array([$this->getModel(), $method], $args);
	}
	
	/**
	 * Calls a static method on the Model associated with this Controller.
	 * 
	 * @param	string	$method
	 * @param	array	$args
	 * @return	mixed
	 */
	final protected function callModelStatic($method, array $args = []) {
		return call_user_func_array($this->getModelName() . '::' . $method, $args);
	}
	
	/**
	 * Returns a proxy instance of the Model associated with this Controller.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	final protected function getModel() {
		if (!$this->modelInstance) {
			$this->modelInstance = $this->callModelStatic('proxy');
		}
		
		return $this->modelInstance;
	}
	
	/**
	 * Returns a new instance of the Model associated with this Controller.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	final protected function getNewModelInstance() {
		return $this->getModel()->newInstance();
	}
	
	/**
	 * Returns the name of the Model associated with this Controller.
	 * 
	 * @return	string
	 */
	final protected function getModelName() {
		return $this->model ?: $this->inferModelName();
	}
	
	/**
	 * Tries to intelligently guess the name of the Model associated with this
	 * Controller.
	 * 
	 * @return	string
	 */
	final private function inferModelName() {
		return prefix($this->getName(), NS_MODELS);
	}
	
	/**
	 * Alias for getNewModelInstance().
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	final protected function getModelInstance() {
		return $this->getNewModelInstance();
	}
	
	/**
	 * Alias for getModel().
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	final protected function model() {
		return $this->getModel();
	}
	
	/**
	 * Load and return another Controller.
	 * 
	 * @return	\Rozyn\Controller\Controller
	 */
	final protected function loadController($controller) {
		return $this->controllerFactory->build($controller);
	}
	
	/**
	 * Returns all accessible methods.
	 * 
	 * @return	array
	 */
	final protected function accessible() {
		if (null === $this->accessible) {
			// Retrieve all methods defined for this Controller.
			$methods = get_class_methods($this);
			
			// Automatically forbid all methods that are defined in this class.
			$this->accessible = array_diff($methods, get_class_methods(__CLASS__));
		}
		
		if (is_array($this->forbidden)) {
			$this->accessible = array_diff($this->accessible, $this->forbidden);
		}
		
		return $this->accessible;
	}
	
	/**
	 * Returns all forbidden methods.
	 * 
	 * @return	array
	 */
	final protected function forbidden() {
		return array_diff(get_class_methods($this), $this->accessible());
	}
	
	/**
	 * Returns whether or not a given method is accessible.
	 * 
	 * @param	string	$method
	 * @return	boolean
	 */
	final protected function isAccessible($method) {
		return in_array($method, $this->accessible());
	}
	
	/**
	 * Returns whether or not a given method is forbidden.
	 * 
	 * @param	string	$method
	 * @return	boolean
	 */
	final protected function isForbidden($method) {
		return !$this->isAccessible($method);
	}
	
	/**
	 * Make any number of methods accessible.
	 * 
	 * @param	string | array	$methods
	 */
	final protected function allow($methods) {
		if (!is_array($methods)) {
			$methods = func_get_args();
		}
		
		if (is_array($this->forbidden)) {
			$this->forbidden = array_diff($this->forbidden, $methods);
		}
		
		$this->accessible = array_unique(array_merge($this->accessible(), $methods));
	}
	
	/**
	 * Make any number of methods forbidden.
	 * 
	 * @param	string | array	$methods
	 */
	final protected function forbid($methods) {
		if (!is_array($methods)) {
			$methods = func_get_args();
		}
		
		if (null === $this->forbidden) {
			$this->forbidden = [];
		}
		
		$this->forbidden = array_unique(array_merge($this->forbidden, $methods));
	}
	
	/**
	 * Check whether or not this Controller is being called using an AJAX 
	 * request.
	 * 
	 * @param	boolean	$strict
	 * @return	boolean
	 */
	final protected function isAjax($strict = false) {
		return	$this->response instanceof AjaxResponse || 
				$strict === false && $this->isJson();
	}
	
	/**
	 * Check whether or not this Controller is being called using a JSON AJAX 
	 * request.
	 * 
	 * @return	boolean
	 */
	final protected function isJson() {
		return $this->response instanceof JsonResponse;
	}
	
	/**
	 * Our own magic call method which routes to the requested method with the 
	 * right parameters. The reason we want to do this is because we need a way 
	 * to execute certain code snippets before and after every method call and 
	 * this allows us to do so.
	 *  
	 * @param	string	$name	The name of the requested method
	 * @param	array	$args	The arguments to be passed on to the request method
	 * @return	mixed
	 */
	final public function __callMethod($name, array $args) {
		try {
			$this->__beforeMethod($name, $args);
			$res = call_user_func_array(array($this, $name), $args);
			$this->__afterMethod($name, $args, $res);
			return $res;
		} catch (Exception $exception) {
			return $this->__catchException($exception, $name, $args);
		}
	}
	
	/**
	 * Our magic method that is executed before every method call in our 
	 * controller, as long as that call is routed through __callMethod(). This
	 * method should be used to perform any logic that needs to be executed
	 * before the requested method is ever invoked. This includes things like
	 * authentication, authorization checks, last-minute modifications to the 
	 * $args array, etc.
	 * 
	 * @param	string	$name
	 * @param	array	$args
	 * @throws	\Rozyn\Controller\ControllerMethodNotFoundException
	 * @throws	\Rozyn\Controller\ControllerMethodForbiddenException
	 * @throws	\Rozyn\Controller\ControllerMethodAuthException
	 */
	protected function __beforeMethod($name, array $args) {
		if (!method_exists($this, $name)) {
			throw new ControllerMethodNotFoundException(get_class_name($this) . '->' . $name);
		}
		
		if ($this->isForbidden($name)) {
			throw new ControllerMethodForbiddenException($name);
		}
		
		if (!$this->isAuthorizedForMethod($name)) {
			throw new ControllerMethodAuthException($name);
		}
		
		// Set the rendered view file.
		$this->renders(camel2snake($name));

		// Set some magic variables that we want to have access to in every view.
		$this->set(array('_controller'	=> get_class($this),
						 '_method'		=> $name,
						 '_args'		=> $args));
	}
	
	/**
	 * Our magic method that is executed after every method call in our 
	 * controller, as long as that call is routed through __callMethod().
	 * 
	 * @param	string	$name
	 * @param	array	$args
	 * @param	mixed	$res	The result of the intended method call.
	 */
	protected function __afterMethod($name, array $args, $res) {
		
	}
	
	/**
	 * A magic method that is called when an Exception is thrown while executing
	 * a __callMethod() call. The default behavior of this method is to simply
	 * throw the same execution again so that it can be handled by our debugging
	 * further down the line. Child classes may extend this method to override
	 * this behavior and implement some custom exception handling before the 
	 * appropriate debugging logic takes over.
	 * 
	 * @param	\Exception	$exception
	 * @param	string		$name
	 * @param	array		$args
	 * @throws	\Exception
	 */
	protected function __catchException(Exception $exception, $name, array $args) {
		throw $exception;
	}
	
	/**
	 * A custom initialization function used to execute setup logic for this
	 * Controller. This method can be used to define certain methods as
	 * forbidden, etc.
	 */
	protected function init() {
		
	}
	
	/**
	 * A core setup function that contains most of the initialization logic for
	 * Controller objects. This method should only be extended by system classes
	 * and not by custom user-defined classes. Use init() for those.
	 */
	protected function setup() {
		// Sanity checks for view files.
		if ($this->views === null) {
			$this->views = camel2snake($this->getName(), '_');
		}
		
		if (DS !== URI_SEPARATOR) {
			$this->views = str_replace(URI_SEPARATOR, DS, $this->views);
		}
		
		$matches = [];
		if (!starts_with($this->views, DS) && preg_match('/^' . preg_quote(__PLUGINS__, '/') . PREG_DS . '([^' . PREG_DS . ']+)/',  get_class_path(get_class($this)), $matches)) {
			$this->views = merge_paths(DS . PLUGINS_DIR . DS . $matches[1] . DS . VIEWS_DIR, $this->views);
		}
		
		if ($this->model) {
			$this->{$this->model} = $this->modelFactory->build($this->model);
		}
	}
	
	/**
	 * Returns whether or not the client has access to the given method.
	 * 
	 * @param	string	$method
	 * @return	boolean
	 */
	final protected function isAuthorizedForMethod($method) {
		// First check if the client has any access to the Controller class at
		// all.
		if (!$this->isAuthorizedForController()) {
			return false;
		}
		
		// Retrieve the resources that the client has to have access to in order
		// to run this $method.
		$resources = get_annotations(new ReflectionMethod($this, $method), self::AUTH_ANNOTATION);
		
		// If the client is cleared to access all the prerequisite resources, we
		// can grant access to this controller method.
		return $this->isAuthorizedForResources($resources);
	}
	
	/**
	 * Returns whether or not the client has access to any of the methods in
	 * this controller.
	 * 
	 * @return	boolean
	 */
	final protected function isAuthorizedForController() {
		// Retrieve the resources that the client has to have access to in order
		// to run this $method.
		$resources = get_annotations(new ReflectionClass($this), self::AUTH_ANNOTATION);
		
		// If the client is cleared to access all the prerequisite resources, we
		// can grant access to this controller.
		return $this->isAuthorizedForResources($resources);
	}
	
	/**
	 * Returns whether or not the client has access to all of the given 
	 * resources.
	 * 
	 * @param	string[]	$resources
	 * @return	boolean
	 */
	final private function isAuthorizedForResources(array $resources) {
		// If no resources are required, the user is automatically authorized.
		if (empty($resources)) {
			return true;
		}
		
		// If resources are required, loop through each one of them and check
		// whether the client is cleared to access those resources. 
		foreach ($resources as $resource) {
			// If a single resource is found to be inaccessible to the client, 
			// we conclude that the client is not authorized to access this 
			// controller method.
			if (!$this->auth->clearance($resource)) {
				return false;
			}
		}
		
		return true;
	}
}