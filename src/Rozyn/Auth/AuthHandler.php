<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Auth;

use DateTime;
use Rozyn\Request\IncomingRequest;
use Rozyn\Config\Config;
use Rozyn\Security\Hash;
use Rozyn\Event\EventHandler;
use Rozyn\Session\CookieHandler;
use Rozyn\Session\SessionHandler;
use Rozyn\Session\PotentialHijackException;
use Rozyn\Model\Auth\Attempt;
use Rozyn\Model\Auth\User;
use Rozyn\Model\Auth\Token;
use Rozyn\Model\Auth\Session;
use Rozyn\Model\UnknownFieldException;

class AuthHandler implements Authenticable {
	
	/**
	 * The name of the session variable that holds the client's data.
	 * 
	 * @var	string
	 */
	const SESSION_CLIENT = '__rauth';
	
	/**
	 * The name of the cookie entry that holds auth data.
	 * 
	 * @var	string
	 */
	const COOKIE_AUTH = '__rauth';
	
	/**
	 * Holds the name of the field user for authentication (usually this will be
	 * either the e-mail address or the username.
	 * 
	 * @var string
	 */
	const AUTH_FIELD = 'email';
	
	/**
	 * Holds the name of the password field used for authentication.
	 * 
	 * @var string
	 */
	const PASSWORD_FIELD = 'password';
	
	/**
	 * Holds the IncomingRequest.
	 * 
	 * @var \Rozyn\Request\IncomingRequest
	 */
	protected $request;
	
	/**
	 * A CookieHandler instance.
	 * 
	 * @var	\Rozyn\Session\CookieHandler
	 */
	protected $cookie;
	
	/**
	 * A SessionHandler instance.
	 * 
	 * @var	\Rozyn\Session\SessionHandler
	 */
	protected $session;
	
	/**
	 * An EventHandler instance.
	 * 
	 * @var	\Rozyn\Event\EventHandler
	 */
	protected $event;
	
	/**
	 * Holds all this App's configuration settings.
	 * 
	 * @var	\Rozyn\Config\Config
	 */
	protected $config;
	
	/**
	 * A Hash instance.
	 * 
	 * @var	\Rozyn\Security\Hash
	 */
	protected $hash;
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * The User model that represents that client.
	 * 
	 * @var	\Rozyn\Model\Auth\User
	 */
	private $client;
	
	/**
	 * Keeps track of whether or not the client has been authenticated yet.
	 * 
	 * @var boolean
	 */
	private $auth;
	
	/**
	 * @param	\Rozyn\Request\IncomingRequest	$request
	 * @param	\Rozyn\Session\SessionHandler	$session
	 * @param	\Rozyn\Session\CookieHandler	$cookie
	 * @param	\Rozyn\Event\EventHandler		$event
	 * @param	\Rozyn\Config\Config			$config
	 * @param	\Rozyn\Security\Hash			$hash
	 * @param	\Rozyn\Model\Auth\User			$client
	 */
	public function __construct(IncomingRequest $request,
								SessionHandler $session,
								CookieHandler $cookie,
								EventHandler $event,
								Config $config,
								Hash $hash,
								User $client) {
		
		$this->request = $request;
		$this->session = $session;
		$this->cookie = $cookie;
		$this->event = $event;
		$this->config = $config;
		$this->hash = $hash;
		$this->client = $client;
	}
	
	/**
	 * Tries to login the client using the specified credentials. If the login
	 * was unsuccessful, a LoginFailedException will be thrown. The optional 
	 * second argument specifies whether the client should be remembered so that 
	 * they can be automatically logged in on future visits. This functionality
	 * is disabled by default.
	 * 
	 * @param	array		$credentials
	 * @param	boolean		$remember
	 * @throws	\Rozyn\Auth\LoginFailedException
	 */
	public function login(array $credentials, $remember = false) {
		// First, check if the user is already logged in. If so, terminate the
		// client's current session.
		if ($this->auth()) {
			$this->logout();
		}
		
		// Try to authenticate the client using the specified credentials.
		if (!$this->attempt($credentials)) {
			throw new LoginFailedException();
		}

		// If the client wishes to be remembered, store the correct
		// cookies to make sure that happens.
		if ($remember === true) {
			// Save the token.
			$this->remember(new Token());
		}

		return true;
	}
	
	/**
	 * Attempts to log in the client using any credentials stored in cookies.
	 * Returns a boolean value indicating whether or not the client has been
	 * successfully logged in.
	 * 
	 * @return	boolean
	 */
	public function cookie() {
		// If we're already logged in there's no need to try and reauthenticate.
		if ($this->auth()) {
			return true;
		}
		
		// Check if the right cookies have been set and retrieve the data stored
		// inside if they are.
		$cookie = $this->cookie->read(static::COOKIE_AUTH);
		if (!$cookie) {
			return false;
		}

		// Find the authentication token that matches the cookie.
		$token = $this->findToken($cookie);

		// If no token could be retrieved or if token points to an unknown user,
		// delete the cookie and return false.
		if (null === $token || null === ($user = $this->findUserById($token->getUserId()))) {
			$this->cookie->delete(static::COOKIE_AUTH);
			return false;
		}
		
		// Everything checks out so log in the retrieved user.
		$this->loginUser($user);
		
		// Since we know the user has specified they want to be remembered, 
		// refresh the information in the authentication token and its matching
		// cookie.
		$this->remember($token);

		// Return a boolean value indicating whether or not authentication was
		// successful.
		return $this->auth();
	}
	
	/**
	 * Logs out the client. Throws a LogoutFailedException if for some reason
	 * the user could not be logged out.
	 * 
	 * @throws	\Rozyn\Auth\LogoutFailedException
	 */
	public function logout() {
		// Fire the logout event before we delete the session data so that
		// any listeners can still use that data if necessary.
		$this->event->fire(EVENT_LOGOUT, [$this]);
		
		// Destroy all the data we've stored for the client.
		$this->destroy();
		
		// Make sure the user is actually logged out.
		if (false !== $this->auth()) {
			throw new LogoutFailedException();
		}
	}
	
	/**
	 * Allows the client to be remembered so they don't have to log in manually
	 * next time they need to be authenticated.
	 * 
	 * @param	\Rozyn\Model\Auth\Token	$token
	 * @throws	\Rozyn\Auth\ClientNotAuthenticatedException
	 */
	public function remember(Token $token) {
		// Make sure the client is already authenticated.
		if (!$this->auth()) {
			throw new ClientNotAuthenticatedException;
		}
		
		// Generate a secret value for this token.
		$secret = $token->generateSecret();

		// Set some default values for this authentication oken before we save 
		// it.
		$token->fill(array(
			'user_id'		=> $this->client()->id(),
			'session_token'	=> $this->session->getToken(),
		));
		
		$token->save();

		// Generate the cookie data that allows the client to be automatically
		// logged in.
		$cookie =  array(
			'id'			=> $token->id(),
			'uid'			=> $this->client()->id(),
			'secret'		=> $secret,
			'session_token' => $this->session->getToken(),
		);

		// Write a cookie so that we can retrieve the correct authentication 
		// token later on.
		$this->cookie->write(static::COOKIE_AUTH, $cookie);
		
		return true;
	}
	
	/**
	 * Attempts to login a user. Returns a boolean value indicating whether or
	 * not authentication was successful.
	 * 
	 * @param	array	$credentials
	 * @return	boolean
	 */
	public function attempt(array $credentials) {
		// Extract the value of the authentication field used to identify the 
		// user that should be logged in.
		$auth = $credentials[static::AUTH_FIELD];
		
		// Create the Attempt object which will be used to log this login 
		// attempt.
		$attempt = new Attempt(array(
			'ip' => $this->request->getIp(),
			'auth' => $auth,
			'success' => false,
		));
		
		// Check if the given credentials match a registered user.
		if (!$this->validate($credentials)) {
			// If not, log the failed login attempt.
			$attempt->save();
			return false;
		}
		
		// Retrieve the matching user and return it.
		$this->loginUser($this->findUserByAuthField($auth));
		
		// If the credentials check out, we can mark the login attempt as a 
		// success, so log it as such. 
		$attempt->set('success', true);
		$attempt->save();
		
		return true;
	}
	
	/**
	 * Returns whether or not the provided credentials are valid login 
	 * credentials for some user on our platform without actually logging in
	 * said user.
	 * 
	 * @param	array	$credentials
	 * @return	boolean
	 */
	public function validate(array $credentials) {
		if (isset($credentials[static::AUTH_FIELD]) && isset($credentials[static::PASSWORD_FIELD])) {
			$user = $this->findUserByAuthField($credentials[static::AUTH_FIELD]);
			
			// Make sure a user matching the specified authentication field is
			// found and that the supplied password matches the password hash
			// stored for this user.
			return	$user !== null && $this->hash->verify($credentials[static::PASSWORD_FIELD], $user->get(static::PASSWORD_FIELD));
		}
		
		return false;
	}
	
	/**
	 * Returns whether or not the user has been authenticated.
	 * 
	 * @return	boolean
	 */
	public function	auth() {
		// If we don't know whether or not the client is authenticated yet, we
		// have to apply some logic to figure it out.
		if ($this->auth === null) {
			// By default, assume the client is not authenticated.
			$this->auth = false;
			
			// Let's see if our session contains the client data, a basic
			// prerequisite for the client to be considered authenticated.
			if ($this->session->has(static::SESSION_CLIENT)) {
				try {
					// If so, run our integrity tests to make sure the client's 
					// session is still valid and has not been compromised.
					$this->runTests();
					
					// If no exceptions were thrown, the client passed all our
					// integrity tests, so we can now safely assume the client 
					// has been authenticated successfully.
					$this->auth = true;
				} catch (\Exception $e) {
					// If our tests fail, assume the client's session has been
					// compromised. To prevent a potential session hijack or 
					// account break-in, we destroy the client's current and
					// for the client to reauthenticate.
					$this->destroy();
				}
			}
		}
		
		return $this->auth;
	}
	
	/**
	 * Returns the User object that currently represents the client.
	 * 
	 * @return	\Rozyn\Model\Auth\User
	 */
	public function client() {
		if (!$this->client->id()) {
			$data = $this->session->read(static::SESSION_CLIENT);
			
			if (is_array($data)) {
				unset($data[static::PASSWORD_FIELD]);
				$this->client->forceFill($data);
			}
		}

		return $this->client;
	}
	
	/**
	 * Returns whether or not the client is cleared for a given resource.
	 * 
	 * @param	\Rozyn\Model\Auth\Resource|string	$resource
	 * @return	boolean
	 */
	public function clearance($resource) {
		return $this->client()->clearance($resource);
	}
	
	/**
	 * Returns the client id.
	 * 
	 * @return	int
	 */
	public function id() {
		return $this->client()->id();
	}
	
	/**
	 * Destroy the client's session data and force them to reauthenticate.
	 */
	protected function destroy() {
		// Check if a database log of the session exists, and if so, delete it.
		$session = $this->findSession();
		if ($session instanceof Session) {
			$session->delete();
		}

		// Clear the current session so that it is completely reset.
		$this->session->clear();

		// Check if any authentication cookies exist.
		$cookie = $this->cookie->read(static::COOKIE_AUTH);
		if ($cookie !== null) {
			// Delete the token matching this cookie from the database.
			$token = $this->findToken($cookie);
			if ($token !== null) {
				$token->delete();
			}

			// Delete the cookies as well.
			$this->cookie->delete(static::COOKIE_AUTH);
		}

		// Force the client to reauthenticate.
		$this->auth = false;
	}
	
	/**
	 * Finds the auth token matching the given criteria in the $cookie data.
	 * 
	 * @param	array	$cookie
	 * @return	\Rozyn\Model\Auth\Token
	 */
	protected function findToken(array $cookie) {
		$cookie['now'] = (new DateTime())->format(DATETIME_FORMAT_SQL);
		
		$token = Token::first(["Token.id			= :id",
							   "Token.user_id		= :uid",
							   "Token.session_token	= :session_token",
							   "Token.expires_at	> :now" ],
									$cookie);
		
		// Make sure the token exists, and if it does, also make sure that the 
		// secret value specified in the $data array matches the token's secret
		// value.
		if ($token === null || !$this->hash->verify($cookie['secret'], $token->secret)) {
			return null;
		}
		
		return $token;
	}
	
	/**
	 * Finds the session model in the database that matches the current session.
	 * 
	 * @return	\Rozyn\Model\Auth\Session
	 */
	protected function findSession() {
		// If we don't log sessions to the database, there's no point in trying
		// to see if a session model exists.
		if (false === $this->config->read('log_sessions')) {
			return null;
		}
		
		// Similarly, if the client does not have a valid ID, there will be no 
		// session to retrieve, so we can return null right away.
		if (!$this->client()->id()) {
			return null;
		}
		
		// Retrieve the Session model that matches the client's current session
		// based on the user id and the session token we generated.
		$session = Session::first(['Session.user_id = :user_id', 'Session.token = :token'], array(
			'user_id' => $this->client()->id(),
			'token' => $this->session->getToken(),
		));
		
		return $session;
	}
	
	/**
	 * Finds the user matching the given id.
	 * 
	 * @param	int		$id
	 * @return	\Rozyn\Model\Auth\User
	 */
	protected function findUserById($id) {
		return $this->findUserBy($this->client->getPrimaryKey(), $id);
	}
	
	/**
	 * Finds the first user whose auth field matches the given value.
	 * 
	 * @param	mixed	$value
	 * @return	\Rozyn\Model\Auth\User
	 */
	protected function findUserByAuthField($value) {
		return $this->findUserBy(static::AUTH_FIELD, $value);
	}
	
	/**
	 * Finds the first user whose $field matches the given $value.
	 * 
	 * @param	string	$field
	 * @param	mixed	$value
	 * @return	\Rozyn\Model\Auth\User
	 * @throws	\Rozyn\Model\UnknownFieldException
	 */
	protected function findUserBy($field, $value) {
		// Make sure the field is actually known to our User class.
		if ($this->client()->hasField($field)) {
			return $this->client()->first($field . '= :value', ['value' => $value]);
		}
		
		throw new UnknownFieldException("Trying to find a user based on {$field}, but User does not have a field called {$field}");
	}
	
	/**
	 * Verifies the client's session. If anything suspicious is found, the 
	 * session is cleared and an Exception will be thrown.
	 * 
	 * @throws	\Rozyn\Session\PotentialHijackException
	 */
	protected function runTests() {
		$this->testClientStillExists();
		$this->testSessionNotExpired();
		$this->testIpIsUnchanged();
	}
	
	/**
	 * Verifies the client that is currently identified by the session variable
	 * still exists in the database.
	 * 
	 * @throws	Rozyn\Auth\ClientNoLongerExistsException
	 */
	protected function testClientStillExists() {
		$client = $this->findUserById($this->client()->id());
		
		if ($client === null || $client->get(static::AUTH_FIELD) !== $this->client()->get(static::AUTH_FIELD)) {
			throw new ClientNoLongerExistsException();
		}
	}
	
	/**
	 * Checks if the client's session is still valid.
	 * 
	 * @throws	\Rozyn\Auth\DatabaseClientSessionNotFoundException
	 */
	protected function testSessionNotExpired() {
		// Check if we log sessions to the database.
		if (true === $this->config->read('log_sessions', false)) {
			// If so, retrieve the session.
			$session = $this->findSession();
			
			// If it does not exist, throw an Exception.
			if ($session === null) {
				throw new DatabaseClientSessionNotFoundException();
			}
		}
	}
	
	/**
	 * Checks if the session's IP address is still valid. If IP validiation is
	 * turned off through the configuration settings, this method will simply
	 * return true.
	 * 
	 * @throws	\Rozyn\Session\PotentialHijackException
	 */
	protected function testIpIsUnchanged() {
		// Test if the IP address has changed since the last request from this
		// session.
		if ($this->session->getIp() !== $this->request->getIp()) {
			// If so, check if this App forces IP addresses to remain the same
			// or not.
			$forceStaticIp = $this->config->read('force_static_ip', false);
			
			// If so, throw a PotentialHijackException.
			if ($forceStaticIp === true) {
				throw new PotentialHijackException("Client's IP addresses changed mid-session.");
			}
		
			// If our App only forces the IP addresses of some user groups to 
			// remain the same, check if our client belongs to any of those 
			// groups. If so, we throw a PotentialHijackException, since the IP
			// address should have stayed the same.
			if (is_array($forceStaticIp)) {
				foreach ($this->client()->groups as $group) {
					if (in_array($group, $forceStaticIp)) {
						throw new PotentialHijackException("Client's IP addresses changed mid-session.");
					}
				}
			}
		}
	}
	
	/**
	 * Logs in the given User model.
	 * 
	 * @param	\Rozyn\Model\Auth\User	$user
	 */
	final private function loginUser(User $user) {
		// Restart a new session so nothing of the old session will remain.
		$this->session->clear();

		// Store the user data in a session so that the data is 
		// accessible on every page that we visit.
		$this->session->write(static::SESSION_CLIENT, $user->getData());

		// Fire a login event so that we can write hooks that perform
		// some logic whenever a user is logged in.
		$this->event->fire(EVENT_LOGIN, [$this]);
		
		// Log the newly created session to the database if necessary.
		if (true === $this->config->read('log_sessions')) {
			$session = new Session(array(
				'user_id' => $user->id(),
				'token' => $this->session->getToken(),
				'csrf' => $this->hash->hash($this->session->getCsrfToken()),
				'ip' => $this->session->getIp(),
				'expires_at' => date(DATETIME_FORMAT_SQL, $this->session->expiresAt()),
			));
			
			$session->save();
		}
		
		// Set our internal $auth variable to true, since the client has been
		// successfully authenticated.
		$this->auth = true;
	}
}