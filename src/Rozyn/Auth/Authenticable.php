<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Auth;

interface Authenticable {
	/**
	 * Attempts to log in the client using the specified credentials. If the
	 * attempt is successful, a boolean value true will be returned. In all 
	 * other cases false will be returned.
	 * 
	 * The optional second argument specifies whether the client's credentials
	 * should be stored somewhere so that he/she can be automatically logged in
	 * in the future. Defaults to false.
	 * 
	 * @param	array	$credentials
	 * @param	boolean	$remember
	 * @return	boolean
	 */
	public function login(array $credentials, $remember = false);
	
	/**
	 * Attempts to log in the client using any credentials stored in cookies. If
	 * the attempt is successful, a boolean value true will be returned. In all
	 * other cases false will be returned.
	 * 
	 * @return	boolean
	 */
	public function cookie();
	
	/**
	 * Logs out the client. Returns true if successful, false otherwise.
	 * 
	 * @return	boolean
	 */
	public function logout();
	
	/**
	 * Returns whether or not the provided credentials are valid login 
	 * credentials for some user on our platform without actually logging in
	 * said user.
	 * 
	 * @param	array	$credentials
	 * @return	boolean
	 */
	public function validate(array $credentials);
	
	/**
	 * Returns whether or not the client has been authenticated.
	 * 
	 * @return	boolean
	 */
	public function	auth();
	
	/**
	 * Returns the currently authenticated user.
	 * 
	 * @return	\Rozyn\Model\Auth\User
	 */
	public function client();
	
	/**
	 * Returns whether or not a user is cleared for a given resource.
	 * 
	 * @param	\Rozyn\Model\Auth\Resource|string	$resource
	 * @return	boolean
	 */
	public function clearance($resource);
}