<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Logging;

use Rozyn\Facade\Request;
use Exception;

abstract class BaseLogger implements LoggerInterface {
	/**
	 * Mark every Logger class that extends this abstract class as a singleton
	 * instance.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Writes an Exception thrown by PHP to a log in a readable way for 
	 * debugging purposes.
	 * 
	 * @param	string	$key
	 * @param	Exception $exception
	 */
	public function writeException($key, Exception $exception) {
		$trace = [];
		foreach ($exception->getTrace() as $step) {
			if (isset($step['file']) && isset($step['line'])) {
				$trace[] = str_format("{file}:{line}", array(
					'file' => $step['file'],
					'line' => $step['line']
				));
			}
		}
		
		$content = str_format('{message} | URL: {url} | IP: {ip} | DATA: {data} | TRACE: {trace}', array(
			'message'	=> $exception->getMessage(),
			'url'		=> Request::url(),
			'ip'		=> Request::ip(),
			'data'		=> json_encode(Request::post()),
			'trace'		=> implode(', ', $trace)
		));
		
		$this->write($key, $content);
	}
}