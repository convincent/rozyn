<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Logging;

interface LoggerInterface {
	/**
	 * Writes a log entry under the specified key.
	 * 
	 * @param	string	$key
	 * @param	string	$content
	 */
	public function write($key, $content);
}