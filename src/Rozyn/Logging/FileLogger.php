<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Logging;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class FileLogger extends BaseLogger {
	/**
	 * Holds an array of Monolog\Logger instances that can write to a certain
	 * file so that we don't have to instantiate new ones all the time.
	 * 
	 * @var	array
	 */
	protected $workers = [];
	
	/**
	 * Writes a log entry under the specified key.
	 * 
	 * @param	string	$key
	 * @param	string	$content
	 */
	public function write($key, $content) {
		$worker = $this->getWorker($key);
		
		if ($worker) {
			$worker->addDebug($content);
		}
	}
	
	/**
	 * Returns whether or not this FileLogger already has a worker assigned to
	 * the $key file.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	protected function hasWorker($key) {
		return isset($this->workers[$key]);
	}
	
	/**
	 * Returns the worker assigned to file $key. If $create is set to TRUE, a 
	 * new worker is created if it doesn't exist yet.
	 * 
	 * @param	string	$key
	 * @param	boolean	$create
	 * @return	Monolog\Logger
	 */
	protected function getWorker($key, $create = true) {
		if ($this->hasWorker($key)) {
			return $this->workers[$key];
		}
		
		return ($create) ? $this->createWorker($key) : null;
	}
	
	/**
	 * Creates a new worker assigned to file $key.
	 * 
	 * @param	string	$key
	 * @return	Monolog\Logger
	 */
	protected function createWorker($key) {
		if (!$this->hasWorker($key)) {
			$logger = new Logger($key);
			$logger->pushHandler(new StreamHandler(__LOGS__ . DS . suffix($key, '.log')));
			
			$this->workers[$key] = $logger;
		}
		
		return $this->getWorker($key);
	}
}