<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

trait TableOperations {
	/**
	 * The name of the table on which the operations should be performed.
	 * 
	 * @var	string
	 */
	protected $table;
	
	/**
	 * Sets or returns the table name.
	 * 
	 * @param	string	$table
	 * @return	mixed
	 */
	public function table($table = null) {
		if (null === $table) {
			return $this->table;
		}
		
		$this->table = str_replace(['`'], '', $table);
		
		return $this;
	}
}