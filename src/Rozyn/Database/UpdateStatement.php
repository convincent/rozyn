<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class UpdateStatement extends ManipulationStatement {
	protected $values = [];
	
	public function __construct($table = null) {
		if (null !== $table) {
			$this->table($table);
		}
	}
	
	public function set($valuesOrColumn, $value = null) {
		$values = (is_string($valuesOrColumn) && $value !== null) ?
					[$valuesOrColumn => $value] :
					$valuesOrColumn;
		
		$this->fields = array_merge($this->fields, array_keys($values));
		$this->values = array_merge($this->values, array_values($values));
		
		return $this;
	}
	
	public function values() {
		return $this->values;
	}
	
	protected function compileUpdates() {
		return (is_array($this->fields) && count($this->fields) == count($this->values)) ? implode(',', array_map(function($v) { 
			return "{$v}=:{$v}";
		}, $this->fields)) : null;
	}
	
	public function compile() {
		return str_format('UPDATE `{table}` SET {updates} {where}', array(
			'table'		=> $this->compileTable(),
			'updates'	=> $this->compileUpdates(),
			'where'		=> $this->compileWhere(),
		));
	}
}