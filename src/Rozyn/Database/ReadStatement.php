<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

abstract class ReadStatement extends ReadOrDeleteStatement {
	protected function compileFields() {
		if (!empty($this->fields)) {
			$tmp = '';
			
			foreach ($this->fields as $field => $alias) {
				$tmp .= ',';
				$tmp .= ($field === $alias) ? $field : $field . ' AS "' . $alias . '"';
			}
			
			return substr($tmp, 1);
		}
		
		return null;
	}
}