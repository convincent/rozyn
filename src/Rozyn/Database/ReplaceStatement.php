<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class ReplaceStatement extends WriteStatement {	
	public function into($table) {
		$this->table($table);
		
		return $this;
	}
	
	public function compile() {
		return str_format('REPLACE INTO `{table}` ({fields}) VALUES {values}', array(
					'table'		=> $this->compileTable(),
					'fields'	=> $this->compileFields(),
					'values'	=> $this->compileValues(),
		));
	}
}