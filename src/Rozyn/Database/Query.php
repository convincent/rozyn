<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

use Closure;
use Rozyn\Facade\Log;
use Rozyn\Facade\Config;
use Rozyn\Composition\DI;

class Query {
	/**
	 * The substring that is used to separate duplicate parameter names from
	 * each other. This substring is added to each of those parameters, after
	 * which an integer counter is appended to the result. The reason we use a 
	 * custom separator string for this is that there may be a legitimate reason
	 * to have two distinct parameters that are named <param> and <param2>,
	 * respectively, without the two necessarily having the same corresponding
	 * value. A slightly more unorthodox separator than an empty string may
	 * prevent this problem, though in theory the possibility of parameter name
	 * clashes still remains. However, by using a substring such as "__dp__" you
	 * drastically lower these odds.
	 */
	const DUPLICATE_PARAM_SEPARATOR = '__dp__';
	
	/**
	 * A reference to our Database handler.
	 * 
	 * @var	\Rozyn\Database\Database
	 */
	protected $db;
	
	/**
	 * The statement associated with this query.
	 * 
	 * @var	\Rozyn\Database\Statement
	 */
	protected $statement;
	
	/**
	 * The result of our query. Will only be populated after execution.
	 * 
	 * @var	mixed
	 */
	protected $result;
	
	/**
	 * The values that should be bound to the query once it is compiled.
	 * 
	 * @var	array
	 */
	protected $bindings = [];
	
	/**
	 * Constructs a new Query object.
	 */
	public function __construct() {
		$this->db = DI::getInstanceOf('Rozyn\Database\Database');
	}
	
	/**
	 * Returns the (cached) result of this Query.
	 * 
	 * @return	mixed
	 */
	public function getResult() {
		return $this->result;
	}
	
	/**
	 * Returns whether or not this Query has a Statement associated with it.
	 * 
	 * @return	boolean
	 */
	public function hasStatement() {
		return $this->statement !== null;
	}
	
	/**
	 * Retrieve the Statement object associated with this Query.
	 * 
	 * @return	\Rozyn\Database\Statement
	 */
	public function getStatement() {
		return $this->statement;
	}
	
	/**
	 * Bind a Statement object to this Query.
	 * 
	 * @param	\Rozyn\Database\Statement	$statement
	 * @param	\Rozyn\Database\Query
	 */
	public function setStatement(Statement $statement) {
		$this->statement = $statement;
		
		return $this;
	}
	
	/**
	 * Executes the query.
	 * 
	 * @return	mixed
	 */
	public function execute() {
		$this->prepare();
		return $this->result = $this->db->execute();
	}
	
	/**
	 * Retrieve a single result row.
	 * 
	 * @param	int		$fetchStyle
	 * @param	mixed	$arg
	 * @return	array
	 */
	public function fetch($fetchStyle = null, $arg = null) {
		$this->prepare();
		
		$result = $this->db->fetch($fetchStyle, $arg);
		
		return ($result === false) ? null : $result;
	}
	
	/**
	 * Fetches all the results matching this query from the database and returns
	 * the results as an array.
	 * 
	 * @param	int		$fetchStyle
	 * @param	mixed	$arg
	 * @return	array
	 */
	public function fetchAll($fetchStyle = null, $arg = null) {
		$this->prepare();
		
		$result = $this->db->fetchAll($fetchStyle, $arg);
		
		return $result;
	}
	
	/**
	 * Fetches the result set as a list of key-value pairs, where one column
	 * makes up the keys and one column makes up the corresponding values. 
	 * 
	 * The argument $labelColumn contains the name of the column whose contents
	 * should be used as the values in the resulting list. An optional $idColumn
	 * argument specifies the column that should be used to populate our list's
	 * keys. If omitted, a column name of "id" is used for this purpose.
	 * 
	 * @param	string	$labelColumn
	 * @param	string	$idColumn
	 * @return	array
	 */
	public function fetchList($labelColumn = null, $idColumn = null) {
		// Store the fields that are to be retrieved by this query. We'll be
		// resetting the fields array, so we need to be able to set the query's
		// fields list back to its original value after we're done retrieving
		// the relevant rows for this method.
		$fields = array_keys($this->getStatement()->fields());
		
		// Define an array that will hold the field names that we actually want
		// to retrieve with this method. This array has to have a labelColumn,
		// but the idColumn is optional.
		$listFields = [];
		
		// If no id column was specified, shit an element off the $fields array
		// and treat that field as the id column. If the $fields array is empty,
		// no ID column will be used and the keys of the resulting array will
		// simply be the default numerical array keys ranging from 0 to n-1.
		if (null !== $idColumn || count($fields) > 1 && null !== ($idColumn = array_shift($fields))) {
			$listFields[$idColumn] = 'id';
		}
		
		// If no label column has been specified, shift an element off the
		// $fields array if possible. We'll use that field as the listColumn. If
		// no such element exists, throw an exception, because we need a 
		// listColumn to proceed.
		if (null === $labelColumn && null === ($labelColumn = array_shift($fields))) {
			throw new QueryException('No label column defined in fetchList()');
		}
		
		// Store the label column in our $listFields array, since we'll also
		// want to retrieve that column's values.
		$listFields[$labelColumn] = 'label';
		
		// Since we'll only be needing the $labelColumn and the $idColumn, reduce
		// our fieldset to just those two fields.
		$this->statement->fields($listFields);
		
		// Retrieve the results of the query.
		$result = $this->fetchAll();
		
		// Initialize the array that will hold the final result list.
		$list = [];
		
		// Check if an id column was specified or inferred. If so, use that 
		// column's value as the key for each result row.
		if (null !== $idColumn && isset($listFields[$idColumn]) && $listFields[$idColumn] === 'id') {
			foreach ($result as $row) {
				$list[$row['id']] = $row['label'];
			}
		} 
		
		// If not, we will simply add each label to our $list array using 
		// incremental indices.
		else {
			foreach ($result as $row) {
				$list[] = $row['label'];
			}
		}
		
		// Reset the fields of the statement to their original value.
		$this->statement->fields($fields);
		
		// Return the result list.
		return $list;
	}
	
	/**
	 * Compiles the Query object to form a properly SQL-formatted query.
	 * 
	 * @return	string
	 */
	public function compile() {
		return $this->getStatement()->compile();
	}
	
	/**
	 * Prepares the query.
	 */
	protected function prepare() {
		// First we turn the query object into a string.
		$query = $this->compile();
		 
		// We need to compile all the bindings that are required for this query,
		// so retrieve them.
		$bindings = $this->getBindings($query);
		
		// Since we cannot have duplicate parameter names in our final query, we
		// have to check if we currently have any duplicates, and if so, get rid
		// of them. For this purpose we first retrieve all named parameters in 
		// the final query.
		$parameters = $this->getParameters($query);
		
		// Afterwards we loop through each of them and as soon as a duplicate is
		// encountered, we replace the first instance of that parameter in the
		// query with a new parameter name. 
		$counts = [];
		foreach ($parameters as $parameter) {
			// Update the counter for the current parameter.
			$counts[$parameter] = (!isset($counts[$parameter])) ? 1 : $counts[$parameter] + 1;
			
			if ($counts[$parameter] > 1) {
				// If we've seen this parameter before, rename it.
				$new = $parameter . static::DUPLICATE_PARAM_SEPARATOR . $counts[$parameter];
				// Also copy the corresponding value from the $bindings array.
				$bindings[$new] = $bindings[$parameter];
				// Replace the first occurence of the duplicate parameter name
				// with the newly generated name.
				$query = preg_replace('/(?<=:)' . preg_quote($parameter, '/') . '(?![a-zA-Z0-9_])/', $new, $query, 1);
			}
		}
		
		// If we are to log database queries, do so here.
		if (Config::read('log_queries')) {
			Log::write('database', $query . ' ' . json_encode($bindings) . ' | Trace: ' . get_stack_trace());
		}

		// Now that our query has been finalized, we can prepare it.
		$this->db->query($query);
		
		// Bind the finalized values after the query has been prepared.
		$this->compileBindings($query, $bindings);
	}
	
	/**
	 * Bind a RawStatement to this Query.
	 * 
	 * @param	string	$query
	 * @return	\Rozyn\Database\Query
	 */
	public function raw($query) {
		$this->setStatement(DI::getInstanceOf('Rozyn\Database\RawStatement', [$query]));
		return $this;
	}
	
	/**
	 * Bind a SelectStatement to this Query.
	 * 
	 * @param	array	$fields
	 * @return	\Rozyn\Database\Query
	 */
	public function select($fields = []) {
		$this->setStatement(DI::getInstanceOf('Rozyn\Database\SelectStatement', [$fields]));
		return $this;
	}
	
	/**
	 * Bind an UpdateStatement to this Query.
	 * 
	 * @param	string	$table
	 * @return	\Rozyn\Database\Query
	 */
	public function update($table = null) {
		$this->setStatement(DI::getInstanceOf('Rozyn\Database\UpdateStatement', [$table]));
		return $this;
	}
	
	/**
	 * Bind an InsertStatement to this Query.
	 * 
	 * @return	\Rozyn\Database\Query
	 */
	public function insert() {
		$this->setStatement(DI::getInstanceOf('Rozyn\Database\InsertStatement'));
		return $this;
	}
	
	/**
	 * Bind a ReplaceStatement to this Query.
	 * 
	 * @return	\Rozyn\Database\Query
	 */
	public function replace() {
		$this->setStatement(DI::getInstanceOf('Rozyn\Database\ReplaceStatement'));
		return $this;
	}
	
	/**
	 * Bind a DeleteStatement to this Query.
	 * 
	 * @return	\Rozyn\Database\Query
	 */
	public function delete() {
		$this->setStatement(DI::getInstanceOf('Rozyn\Database\DeleteStatement'));
		return $this;
	}
	
	/**
	 * Bind a CreateTableStatement to this Query.
	 * 
	 * @return	\Rozyn\Database\Query
	 */
	public function createTable($table = null) {
		$this->setStatement(DI::getInstanceOf('Rozyn\Database\CreateTableStatement', [$table]));
		return $this;
	}
	
	/**
	 * Since our the where() method in our Statement class can accept a callback
	 * as its first parameter which in turn takes another Statement object as
	 * its parameter, we need to intercept any calls to the where() method if a
	 * callback is provided as the first parameter so that we can rewrite that
	 * callback a bit in order for it to accept a Query object instead of a 
	 * Statement object and still function properly.
	 * 
	 * @param	mixed	$conditions
	 * @param	string	$combine
	 * @return	\Rozyn\Database\Query
	 */
	public function where($conditions, $combine = null) {
		// First we check if the $conditions variable is indeed a callback.
		if ($conditions instanceof Closure) {
			// If so, clone the current query so that we can perform a number
			// of operations on it without modifying the actual object itself.
			$query = clone $this;
			
			// Also reset the statement since we're not deep cloning anything
			// and we don't want our Statement object to be corrupted either.
			$query->select();
			
			// Apply the callback which takes a Query object as a parameter by
			// feeding it the clone we just created.
			$conditions($query); 
			
			// Next, call the Statement's where() method by passing a modified 
			// callback to it as a parameter. This callback basically copies 
			// all of the conditions that were set on the cloned Query object
			// and assigns them to the Statement object that is passed to our
			// custom callback.
			$this->getStatement()->where(function($statement) use ($query) {
				$statement->addConditions($query->getStatement()->getConditions());
			}, $combine);
			
			if ($query instanceof ModelQuery && $this instanceof ModelQuery) {
				$this->setRelations($query->getRelations());
			}
			
			// Make sure we copy the attributes set on this temporary query
			// clone to our original query.
			$this->copy($query);
			
			// Return this object to allow for method chaining.
			return $this;
		}
		
		// If no callback was specified, simply pass along the method call to
		// this object's Statement instance and let it handle the where() call.
		$this->getStatement()->where($conditions, $combine);
		
		// Return this object to allow for method chaining.
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query using SQL's NULL function.
	 * 
	 * @param	string	$column
	 * @return	\Rozyn\Database\Query
	 */
	public function whereNull($column) {
		$this->getStatement()->whereNull($column);
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query using SQL's NOT NULL function.
	 * 
	 * @param	string	$column
	 * @return	\Rozyn\Database\Query
	 */
	public function whereNotNull($column) {
		$this->getStatement()->whereNotNull($column);
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query using SQL's IN function.
	 * 
	 * @param	string	$column
	 * @param	array	$values
	 * @param	boolean	$parameterize
	 * @return	\Rozyn\Database\Query
	 */
	public function whereIn($column, array $values, $parameterize = true) {
		if ($parameterize) {
			$params = $this->parameterize($values);
			
			$this->bind(array_combine($params, $values));
		}

		$this->getStatement()->whereIn($column, ($parameterize) ? array_map(function($v) {
				return ":{$v}";
			}, $params) : $values);
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query to check for equality.
	 * 
	 * @param	string	$column
	 * @param	mixed	$value
	 * @param	boolean	$parameterize
	 * @return	\Rozyn\Database\Query
	 */
	public function whereEquals($column, $value, $parameterize = true) {
		if ($parameterize) {
			$param = $this->parameterize($column);
			$this->bind($param, $value);
		}
		
		$this->getStatement()->whereEquals($column, ($parameterize) ? ':' . $param : $value);
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query to check for inequality.
	 * 
	 * @param	string	$column
	 * @param	mixed	$value
	 * @param	boolean	$parameterize
	 * @return	\Rozyn\Database\Query
	 */
	public function whereNotEquals($column, $value, $parameterize = true) {
		if ($parameterize) {
			$param = $this->parameterize($column);
			$this->bind($param, $value);
		}
		
		$this->getStatement()->whereNotEquals($column, ($parameterize) ? ':' . $param : $value);
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query to check for prefixes.
	 * 
	 * @param	string	$column
	 * @param	mixed	$value
	 * @param	boolean	$parameterize
	 * @return	\Rozyn\Database\Query
	 */
	public function whereStartsWith($column, $value, $parameterize = true) {
		if ($parameterize) {
			$param = $this->parameterize($column);
			$this->bind($param, "{$value}%");
		}
		
		$this->getStatement()->where($column . (($parameterize) ? " LIKE :{$param}" : " LIKE {$value}"));
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query to check for suffixes.
	 * 
	 * @param	string	$column
	 * @param	mixed	$value
	 * @param	boolean	$parameterize
	 * @return	\Rozyn\Database\Query
	 */
	public function whereEndsWith($column, $value, $parameterize = true) {
		if ($parameterize) {
			$param = $this->parameterize($column);
			$this->bind($param, "%{$value}");
		}
		
		$this->getStatement()->where($column . (($parameterize) ? " LIKE :{$param}" : " LIKE {$value}"));
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query to check for substrings.
	 * 
	 * @param	string	$column
	 * @param	mixed	$value
	 * @param	boolean	$parameterize
	 * @return	\Rozyn\Database\Query
	 */
	public function whereContains($column, $value, $parameterize = true) {
		if ($parameterize) {
			$param = $this->parameterize($column);
			$this->bind($param, "%{$value}%");
		}
		
		$this->getStatement()->where($column . (($parameterize) ? " LIKE :{$param}" : " LIKE {$value}"));
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query to check for existing rows.
	 * 
	 * @param	\Rozyn\Database\Query | Rozyn\Database\ReadStatement $query
	 * @return	\Rozyn\Database\Query
	 */
	public function whereExists($query) {		
		if ($query instanceof Query) {
			$this->bind($query->getBindings());
		}
		
		$this->getStatement()->whereExists($query);
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query to check for nonexisting rows.
	 * 
	 * @param	\Rozyn\Database\Query|Rozyn\Database\ReadStatement	$query
	 * @return	\Rozyn\Database\Query
	 */
	public function whereNotExists($query) {
		if ($query instanceof Query) {
			$this->bind($query->getBindings());
		}
		
		$this->getStatement()->whereNotExists($query);
		
		return $this;
	}
	
	/**
	 * Returns a safe parameter name to use in your queries based on a given
	 * salt. Do not call this function twice with the same column name if you
	 * need the parameter names to be the same, because this function will 
	 * return a different value each time it's called, even if you provide the
	 * same salt as before. Instead, store the returned value somewhere where
	 * you can easily retrieve it so that you can reuse it if necessary.
	 * 
	 * If an array of salts is provided, an array with equal length is returned.
	 * Each element in the return array represents a single safe parameter name.
	 * 
	 * @param	string|array	$salt
	 * @return	string|array
	 */
	public function parameterize($salt = null) {
		if (is_array($salt)) {
			$res = [];
			
			foreach ($salt as $field) {
				$res[] = $this->parameterize($field);
			}
			
			return $res;
		}
		
		return 'p' . base64_alphanumeric(random_bytes(8) . $salt);
	}
	
	/**
	 * Returns the maximum value of a column that would be returned by this
	 * query. 
	 * 
	 * @param	string	$column
	 * @return	int
	 */
	public function max($column) {
		// Cache the statement before we alter it so that we can reset the
		// statement after we're done to easily undo all of our changes.
		$statement = clone $this->getStatement();
		
		// Replace all fields with the MAX() function to retrieve the count.
		$this->fields(["MAX({$column})" => 'max'], true);
		
		// Retrieve all results using the parent's generic fetchAll() method.
		$result = $this->fetch();
		
		// After we've executed the statement, set the query's statement back
		// to its original value.
		$this->setStatement($statement);
		
		// If the query returned a valid result, return that. Otherwise return 0.
		return (isset($result['max'])) ? intval($result['max']) : 0;
	}
	
	/**
	 * Returns the amount of rows that would be returned by the query.
	 * 
	 * @return	int
	 */
	public function count() {
		// Cache the statement before we alter it so that we can reset the
		// statement after we're done to easily undo all of our changes.
		$statement = clone $this->getStatement();
		
		// Replace all fields with the COUNT() function to retrieve the count.
		$this->fields(['COUNT(*)' => 'count'], false);
		
		// Retrieve all results using the parent's generic fetchAll() method.
		$result = $this->fetch();
		
		// After we've executed the statement, set the query's statement back
		// to its original value.
		$this->setStatement($statement);
		
		// If the query returned a valid result, return that. Otherwise return 0.
		return (isset($result['count'])) ? intval($result['count']) : 0;
	}
	
	/**
	 * Executes a given callback on this query.
	 * 
	 * @param	callable					$cb
	 * @return	\Rozyn\Database\Query
	 */
	public function apply(callable $cb) {
		call_user_func($cb, $this);
		
		return $this;
	}
	
	/**
	 * Binds one or more parameters to the query.
	 * 
	 * @param	mixed	$param
	 * @param	mixed	$value
	 * @return	\Rozyn\Database\Query
	 */
	public function bind($param, $value = null) {
		if (is_array($param)) {
			$this->bindings = array_merge($this->bindings, $param);
		} 
		
		else {
			$this->bindings[$param] = $value;
		}
				
		return $this;
	}
	
	/**
	 * Returns the database handler associated with this Query.
	 * 
	 * @return	\Rozyn\Database\Database
	 */
	public function db() {
		return $this->db;
	}
	
	/**
	 * Gets all the bindings set explicitly for this query. A query string can 
	 * also be passed along, in which case only the bindings for the named 
	 * parameters in the compiled query are returned.
	 * 
	 * @return	array
	 */
	public function getBindings($query = null) {
		$bindings = $this->bindings;
		
		if ($this->statement instanceof WriteStatement || $this->statement instanceof UpdateStatement) {
			if (is_array(current($this->statement->values()))) {
				foreach ($this->statement->values() as $i => $values) {
					$k = 0;
					foreach ($this->statement->fields() as $j => $field) {
						$bindings[$field . $i] = (array_key_exists($j, $values)) ? 
													$values[$j] : 
													$values[$k];
						
						$k++;
					}
				}
			} else {
				$bindings = array_merge($bindings, 
										array_combine($this->statement->fields(), 
													  $this->statement->values()));
			}
		}
		
		// Check if a query was passed.
		if ($query !== null) {
			// If so, find all named parameters and include only the bindings 
			// for those parameters.
			$bindings = array_intersect_key($bindings, array_flip($this->getParameters($query)));
		}
		
		return $bindings;
	}
	
	/**
	 * Returns an array of all parameters used in the query.
	 * 
	 * @param	string	$query
	 * @return	array
	 */
	public function getParameters($query = null) {
		if (null === $query) {
			$query = $this->getStatement()->compile();
		}
		
		$parameters = [];
		preg_match_all('/(?<=:)[a-zA-Z0-9_]+/', $query, $parameters);
		
		return (empty($parameters)) ? [] : $parameters[0];
	}
	
	/**
	 * Compiles all the bindings that were set. You can choose to pass along a
	 * compiled query as an argument. In that case, all named parameters are 
	 * extracted from the query and only those parameters will be bound.
	 * 
	 * @param	string	$query
	 */
	protected function compileBindings($query = null, array $bindings = null) {		
		$this->db->bind($bindings ?: $this->getBindings($query));
	}
	
	/**
	 * Takes a query as a parameter and copies its attributes into the current
	 * query.
	 * 
	 * @param	\Rozyn\Database\Query	$query
	 * @return	\Rozyn\Database\Query
	 */
	protected function copy(Query $query) {
		// Bind any parameters that were set in the process to our original 
		// query.
		$this->bind($query->getBindings());
		
		return $this;
	}
	
	/**
	 * A magic call method that allows for flexible method calls.
	 * 
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	\Rozyn\Database\Query
	 */
	public function __call($name, $arguments) {
		if (method_exists($this->getStatement(), $name)) {
			// Pass the call along to the Statement object.
			$res = call_user_func_array([$this->getStatement(), $name], $arguments);

			return ($res instanceof Statement) ? $this : $res;
		}

		if (substr($name, 0, 7) === 'orWhere') {
			// Retrieve the current default WHERE operator so that we can 
			// temporarily store it. We're going to change it to OR before
			// we execute our WHERE method so that we're sure it gets added
			// correctly. Afterwards, we set the operator back to what it 
			// was before.
			$combine = $this->getStatement()->getWhereOperator();
			$this->getStatement()->setWhereOperator('OR');

			// Call the appropriate WHERE method.
			call_user_func_array([$this, preg_replace('/^orW/', 'w', $name)], $arguments);

			// Reset the WHERE operator.
			$this->getStatement()->setWhereOperator($combine);

			// Return this object to allow for flexible method chaining.
			return $this;
		}
		
		// If the method can't or shouldn't be passed to the Statement object, 
		// pass it on to the Database object instead.
		return call_user_func_array([$this->db, $name], $arguments);
	}
	
	/**
	 * Returns the string representation of this Query.
	 * 
	 * @return	string
	 */
	public function __toString() {
		return $this->compile();
	}
	
	/** 
	 * A clone method that takes care of copying the image resource.
	 */
	public function __clone() {
		if ($this->statement !== null) {
			$statement = clone $this->statement;
			$this->setStatement($statement);
		}
	}
}