<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

use Rozyn\Model\Model;
use Rozyn\Model\JoinModel;
use Rozyn\Relation\Relation;
use Rozyn\Relation\HasAndBelongsToMany;
use Rozyn\Relation\RelationNotFoundException;

class ModelQuery extends Query {
	/**
	 * An instance that identifies which model the query is supposed to act upon.
	 * 
	 * @var	\Rozyn\Model\Model
	 */
	protected $model;
	
	/**
	 * Holds all the query's model's Relations that are recognized by this query.
	 * 
	 * @var	array
	 */
	protected $relations = [];
	
	/**
	 * Holds all the query's model's nested relations that should be passed on
	 * to potential subqueries to ensure all the data is loaded.
	 * 
	 * @var	array
	 */
	protected $nestedRelations;
	
	/**
	 * Sets the model instance for this ModelQuery.
	 * 
	 * @param	\Rozyn\Model\Model	$model
	 */
	public function __construct(Model $model) {
		parent::__construct();
		
		$this->setModel($model);
	}
	
	/**
	 * Returns the model instance that is associated with this ModelQuery.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function getModel() {
		return $this->model;
	}
	
	/**
	 * Sets the model instance that is associated with this ModelQuery.
	 * 
	 * @param	\Rozyn\Model\Model
	 */
	public function setModel(Model $model) {
		$this->model = $model;
		
		// We need to be able to create new instances of our model on the fly
		// without invoking the constructor, so we store a ReflectionClass
		// for that purpose.
		$this->modelReflection = new \ReflectionClass($this->model);
	}
	
	/**
	 * Compiles the query and returns its string representation.
	 * 
	 * @return	string
	 */
	public function compile() {
		// For ModelQueries, we handle SELECT Statements differently from other
		// types of queries.
		if ($this->getStatement() instanceof SelectStatement) {
			// Cache the statement before we modify it temporarily to get the 
			// correct results. We want to set it back to its original value after
			// we've executed our query.
			$statement = clone $this->statement;

			// Exclude soft deleted models if necessary.
			if ($this->model->isSoft()) {
				$this->whereNotEquals($this->model->getAliasedSoftDeleteField(), 1);
			}

			// Loop through all single query relations and update the query to 
			// correctly retrieve the related model's data. We only need to loop
			// through single record relations because multi record relations are
			// retrieved through a separate query later on.
			foreach ($this->getSingleRecordRelations() as $relation) {
				$relation->prepareQueryJoins($this);
			}
			
			// Check if default ordering needs to be applied.
			if (!count($this->getStatement()->getOrderBy())) {
				$this->getStatement()
					 ->orderBy($this->model->getAliasedField($this->model->getDefaultOrderField()),
							   $this->model->getDefaultOrderDirection());
			}

			// Apply all the model's conditions to the query.
			$this->applyModelConditions();

			// Store the result that we're going to return before we actually return
			// it, because there are still some things left do after we've called
			// the parent's compile() method.
			$res = parent::compile();

			// After we've executed the statement, set the query's statement back
			// to its original value so that we reset any Relation constraints we
			// placed on it for this method.
			$this->setStatement($statement);
			
			// Now that everything that had to be done has been done, return the
			// previously computed result.
			return $res;
		}
		
		// If this ModelQuery doesn't have a SELECT Statement associated with it,
		// we can simply fall back on the parent's compile() functionality.
		return parent::compile();
	}
	
	/**
	 * Executes the query and returns the first matching result.
	 * 
	 * @return	array|null
	 */
	public function fetchArray() {
		// Clone the current statement so that we can set it back after we 
		// change the limit to 1 for this method's purposes.
		$statement = clone $this->getStatement();
		
		// Set the limit to 1 temporarily to ensure we only retrieve a single
		// row.
		$this->statement->limit(1);
		
		// Call our fetchArrays() method, which would normally retrieve all 
		// matching rows, but since we set the limit to 1, it now only retrieves
		// the first matching row.
		$res = current($this->fetchArrays());
		
		// Reset our statement to its original form.
		$this->setStatement($statement);
		
		// Return our result, or null if no result was found. 
		return ($res === false) ? null : $res;
	}
	
	/**
	 * Executes the query and returns the result. Also loads any potential
	 * related data for the model(s) that were retrieved initially.
	 * 
	 * @return	array
	 */
	public function fetchArrays() {
		$rows = $this->fetchAll();
		
		//If no rows have been passed, return an empty array.
		if (empty($rows)) {
			return [];
		}
		
		// Extract the primary key of the current model so we don't have to do
		// so during each iteration.
		$primaryKey = $this->model->getPrimaryKey();
		
		// Initialize our results array.
		$result = [];
		
		// Loop through each row and copy it to our $result array, filing it
		// under the primary key's value.
		foreach ($rows as $row) {
			$result[$row[$primaryKey]] = $row;
		}
		
		// Loop through all the model's relation and load the related data.
		foreach ($this->getRelations() as $name => $relation) {
			$relation->loadBatchArray(array_keys($result), 
									  $this->getNestedRelations($name), 
									  $result);
		}
		
		return $result;
	}
	
	/**
	 * Does the same as the fetch() method, but returns the result as a Model
	 * instance instead of an array. All of the related models are instantiated
	 * as well.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function fetchModel() {
		return $this->objectifyRow($this->fetchArray());
	}
	
	/**
	 * Does the same as the fetchAll() method, but returns the result as a
	 * ModelCollection instead of an array. All of the related models are 
	 * instantiated as well.
	 * 
	 * @return	\Rozyn\Model\Collection
	 */
	public function fetchModels() {
		return $this->objectify($this->fetchArrays());
	}
	
	/**
	 * Returns a list containing all the model ids as keys and a specified 
	 * column as the corresponding value.
	 * 
	 * @param	string	$labelColumn
	 * @param	string	$idColumn
	 * @return	array
	 */
	public function fetchList($labelColumn = null, $idColumn = null) {
		// Make sure that the $labelColumn is aliased.
		if (null !== $labelColumn && false === strpos($labelColumn, '.')) {
			$labelColumn = $this->model->getAliasedField($labelColumn);
		}
		
		// Provide a fallback for the $idColumn if it is omitted.
		if (null === $idColumn) {
			$idColumn = $this->model->getAliasedPrimaryKey();
		}
		
		return parent::fetchList($labelColumn, $idColumn);
	}
	
	/**
	 * Takes a single row returned by a fetch() or fetchAll() call as an argument
	 * and converts it into a fully instantiated Model instance. All of its 
	 * related data is converted as well.
	 * 
	 * @param	array|null					$row
	 * @param	\Rozyn\Model\Model			$proxy
	 * @param	\Rozyn\Model\Relation		$relation
	 * @return	\Rozyn\Model\Model|null
	 */
	public function objectifyRow(array $row = null, Model $proxy = null, Relation $relation = null) {
		// If no data is provided, return null.
		if (empty($row)) {
			return null;
		}
		
		// Make sure we have a valid proxy model.
		if (null === $proxy) {
			$proxy = $this->model;
		}
		
		// Use the proxy model to instantiate a new model of that class.
		$instance = new $proxy;
		
		// Check if we need to instantiate a join model as well. This is only
		// necessary if we're retrieving related data for a HasAndBelongsToMany
		// $relation, and if a join table is defined for that $relation.
		if (isset($row[JoinModel::JOIN_MODEL_KEY]) && $relation instanceof HasAndBelongsToMany && $relation->hasJoinModel()) {
			$joinModel = $relation->getJoinModel()->newInstance();
			$joinModel->forceData($row[JoinModel::JOIN_MODEL_KEY]);
			
			$instance->setJoinModel($joinModel);
			unset($row[JoinModel::JOIN_MODEL_KEY]);
		}
		
		// Treat any remaining data that does not correspond with one of the
		// model's fields as relationship data.
		$relations = array_keys(array_diff_key($row, $proxy->getStructure()));
		
		// Loop through all these supposed relations.
		foreach ($relations as $name) {
			if (null !== $row[$name]) {
				try {
					// Try to retrieve the relation matching the name of the
					// retrieved variable. If it does not correspond to a 
					// relation of this model, a RelationNotFoundException will
					// be thrown, but we catch those neatly.
					$relation = $proxy->getRelation($name);

					// If we are indeed dealing with related data, recursively
					// objectify that data using the related model as a proxy
					// and also passing along the relationship object itself.
					$row[$name] = ($relation->isSingleRecord()) ? 
										$this->objectifyRow($row[$name], $relation->getRelated(), $relation) :
										$this->objectify($row[$name], $relation->getRelated(), $relation);
				} catch(RelationNotFoundException $e) {}
			}
		}
		
		// Force the data on the model. This will automatically set the related
		// data as well, as we previously objectified that recursively.
		$instance->forceData($row);
		
		return $instance;
	}
	
	/**
	 * Takes any number of rows returned by a fetch() or fetchAll() call as an
	 * argument and converts them into fully instantiated Model instances 
	 * wrapped inside a containing Collection object. All of the related data is
	 * converted as well.
	 * 
	 * @param	array				$rows
	 * @param	\Rozyn\Model\Model	$proxy
	 * @return	\Rozyn\Model\Collection
	 */
	public function objectify(array $rows, Model $proxy = null, Relation $relation = null) {
		// An empty array should be converted to an empty Collection.
		if (empty($rows)) {
			return $this->model->newCollection();
		}
		
		if (null === $proxy) {
			$proxy = $this->model;
		}
		
		// If we're indeed dealing with multiple records here, loop through all
		// of them and pass them to the objectifyRow() method one by one.
		foreach ($rows as &$row) {
			$row = $this->objectifyRow($row, $proxy, $relation);
		}
		
		// Create a new Collection with the modified $result array as its items.
		return $this->model->newCollection($rows);
	}
	
	/**
	 * Execute the query.
	 * 
	 * @param	array	$params
	 * @return	boolean
	 */
	public function execute(array $params = []) {
		$res = parent::execute($params);
		
		return $res;
	}
	
	/**
	 * Initialize a SELECT query. You can provide an array of fields that will
	 * be retrieved, but if you omit it, all the fields defined in the model
	 * will be used.
	 * 
	 * @param	array	$fields
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function select($fields = null) {
		// Create our SELECT statement.
		parent::select($fields)->from($this->model->getAliasedTable());
		
		// Set the initial fields that should be retrieved.
		if ($fields === null) {
			$this->fields($this->model->getFieldsForQuery());
		}
		
		return $this;
	}
	
	/**
	 * Initialize an INSERT query.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function insert() {
		return parent::insert()->into($this->model->getTable())->fields($this->model->getFields());
	}
	
	/**
	 * Initialize an UPDATE query. By default, the model's table is used, but
	 * you can specify a different table to use. You should probably never ever
	 * specify your own table, but this method declaration has to be compatible
	 * with the parent class' declaration.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function update($table = null) {
		return parent::update($table ?: $this->model->getTable());
	}
	
	/**
	 * Initialize an DELETE query.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function delete() {
		return parent::delete()->from($this->model->getTable());
	}
	
	/**
	 * Initialize an REPLACE query.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function replace() {
		return parent::replace()->into($this->model->getTable())->fields($this->model->getFields());
	}
	
	/**
	 * A passthrough method that intercepts calls to the Statement's fields()
	 * method so that we can apply some additional logic whenever it is called.
	 * 
	 * @param	array		$fields
	 * @param	boolean		$append
	 * @return	array | Rozyn\Database\ModelQuery
	 */
	public function fields($fields = null, $append = false) {
		if ($fields === null) {
			return $this->getStatement()->fields();
		}
		
		$this->getStatement()->fields($fields, $append);
		
		return $this;
	}
	
	/**
	 * Turn on one of the model's relations. By default only the eager relations
	 * are loaded when this model's relations are requested by the query, but
	 * if you wish to load lazy relations as well, you can add them by using the
	 * with() method and specifying the relation's name as its argument.
	 * 
	 * To specify nested relations, pass along an array where each key
	 * corresponds to the name of a direct relation of the parent model and its
	 * values are the names of the relations in that child model that need to
	 * also be loaded. You can repeat this format recursively to go into even
	 * deeper relations.
	 * 
	 * @param	string|array				$relations
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function with($relations) {
		// If the $relations argument is empty, we don't have to do anything.
		if (empty($relations)) {
			return $this;
		}
		
		// If a string is passed, change it into an array so that we can use the
		// foreach loop below for every possible input.
		if (is_string($relations)) {
			$relations = func_get_args();
		}
		
		// Loop through all of the specified relations.
		foreach ($relations as $key => $value) {
			// If the key is numeric, this indicates that the corresponding value
			// is a relation of the model associated with this ModelQuery. That
			// means we have to "turn on" that relation in the model instance.
			if (is_int($key)) {
				// Store a reference to said relation in this ModelQuery.
				$this->relations[$value] = $relation = $this->model->getRelation($value);

				if (!$relation->getRelated()->getAlias()) {
					// Set the alias on the relation.
					$relation->getRelated()->setAlias(str_random($value, 32, 'a'));
				}
			}
			
			// If both the key and the value are strings, again create an array
			// based on the value string so that the next if statement is enough
			// to cover all possible inputs.
			else {
				// If the key is a string and its value an array, assume that the 
				// key is meant to refer to one of the intrinsic ("own") relations
				// of the model associated with this ModelQuery. The array, then, 
				// contains any number of relation names that refer to relations in
				// the related model of the relation that was identified by the key.
				// Got it? It gets better: the array can also itself contain even
				// more nested relations as long as they all follow the format
				// described above.
				$this->relations[$key] = $this->model->getRelation($key);
				$this->nestedRelations[$key] = (is_string($value)) ? [$value] : $value;
			}
		}
		
		return $this;
	}
	
	/**
	 * Turn off some of the model's relations. By default all the eager relations
	 * are loaded when this model's relations are requested by the query, but
	 * if you wish to exclude one or more of them, you can do so by using the
	 * without() method and specifying the relation's name as its argument.
	 * 
	 * To remove nested relations, pass along an array where each key
	 * corresponds to the name of a direct relation of the parent model and its
	 * values are the names of the relations in that child model that need to
	 * also be loaded. You can repeat this format recursively to go into even
	 * deeper relations.
	 * 
	 * @param	string | array				$relations
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function without($relations) {
		if (is_string($relations)) {
			$relations = [$relations];
		}
		
		$explore = [];
		foreach ($relations as $key => $value) {
			if (is_int($key) && isset($this->relations[$value])) {
				unset($this->relations[$value]);
			} elseif (is_string($key) && is_string($value)) {
				$explore[$key] = [$value];
			}
		}
		
		if (!empty($explore) && !empty($this->nestedRelations)) {
			$this->nestedRelations = array_inflate(array_diff(array_flatten($this->nestedRelations), array_flatten($explore)));
		}
		
		return $this;
	}
	
	/**
	 * Removes all relations from this query's model.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function lazy() {
		$this->relations = $this->nestedRelations = [];
		
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the query that checks if a certain condition is
	 * met by one of the relations of this query's Model.
	 * 
	 * @param	string		$name
	 * @param	callable	$callback
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function whereRelation($name, callable $callback = null) {
		$relation = $this->model->getRelation($name);

		if ($relation->isSingleRecord()) {
			if (!isset($this->relations[$name])) {
				// Add the relation that we want to filter on.
				$this->with([$name]);
			}

			if (null !== $callback) {
				$callback($this);
			} else {
				$this->whereNotNull($relation->getRelated()->getAliasedPrimaryKey());
			}
		}

		elseif ($relation->isMultiRecord()) {
			$query = $relation->newExistsSubQuery();

			if (null !== $callback) {
				$callback($query);
			}

			$this->whereExists($query);

			// Bind parameters from our subquery to our actual query.
			$this->bind($query->getBindings());
		}
		
		return $this;
	}
	
	/**
	 * A more readable alias for whereRelation without a callback. This 
	 * basically just checks whether a relation is filled with at least 1 
	 * related row and doesn't return the model instance if it doesn't fit that
	 * criterium.
	 * 
	 * @param	string		$relation
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function whereHas($relation) {
		return $this->whereRelation($relation, null);
	}
	
	/**
	 * Applies the given conditions to the query.
	 * 
	 * @param	array	$conditions
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function applyConditions(array $conditions) {
		foreach ($conditions as $column => $value) {
			if (is_int($column)) {
				$this->where($value);
				continue;
			}
			
			if (false === strpos($column, '.')) {
				$column = $this->model->getAliasedField($column);
			}

			if (is_array($value)) {
				$this->whereIn($column, $value);
			}
			
			else {
				$this->whereEquals($column, $value);
			}
		}
		
		return $this;
	}
	
	/**
	 * Applies the query's model's conditions to the query.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function applyModelConditions() {
		return $this->applyConditions($this->getModelConditions());
	}
	
	/**
	 * Gets the query's model's conditions.
	 * 
	 * @return	array
	 */
	public function getModelConditions() {
		return $this->model->getConditions();
	}
	
	/**
	 * Drops the query's model's conditions.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function dropModelConditions() {
		$this->model->dropConditions();
		
		return $this;
	}
	
	/**
	 * Sets the relations variable (overwriting any predefined relations).
	 * 
	 * @param	\Rozyn\Relation\Relation[]	$relations
	 */
	public function setRelations(array $relations) {
		$this->relations = $relations;
	}
	
	/**
	 * Returns all Relations for this Query.
	 * 
	 * @return	\Rozyn\Relation\Relation[]
	 */
	public function getRelations() {
		return $this->relations;
	}
	
	/**
	 * Returns whether or not the ModelQuery has a particular nested relation.
	 * You can specify deeper levels of nesting by separating the relation
	 * names with a dot (.).
	 * 
	 * @param	string	$name
	 * @return	boolean
	 */
	public function hasNestedRelation($name) {
		$context = $this->getNestedRelations();
		
		foreach (explode('.', $name) as $nest) {
			if (!isset($context[$nest])) {
				return false;
			}
			
			$context = $this->nestedRelations[$nest];
		}
		
		return true;
	}
	
	/**
	 * Returns any nested relation names that have been defined for a particular
	 * model relation of the model associated with this ModelQuery. If no 
	 * argument is provided, all nested relations are returned instead.
	 * 
	 * @param	string	$name
	 * @return	array
	 */
	public function getNestedRelations($name = null) {
		if ($name === null) {
			return $this->nestedRelations;
		}
		
		return ($this->hasNestedRelation($name)) ? $this->nestedRelations[$name] : [];
	}
	
	/**
	 * Returns all single record relations that are recognized by this query.
	 * 
	 * @return	\Rozyn\Relation\SingleRecordRelation[]
	 */
	public function getSingleRecordRelations() {
		return array_filter($this->getRelations(), function($relation) {
			return $relation->isSingleRecord();
		});
	}
	
	/**
	 * Returns the Statement object associated with this Query. If no Statement
	 * is yet associated with it, a SELECT statement is initiated by default.
	 * 
	 * @return	\Rozyn\Database\Statement
	 */
	public function getStatement() {
		if (!$this->statement) {
			$this->select();
		}
		
		return parent::getStatement();
	}
	
	/**
	 * Takes a query as a parameter and copies its attributes into the current
	 * query.
	 * 
	 * @param	\Rozyn\Database\Query	$query
	 * @return	\Rozyn\Database\Query
	 */
	protected function copy(Query $query) {
		if ($query instanceof ModelQuery) {
			$this->setRelations($query->getRelations());
		}
		
		return parent::copy($query);
	}
}