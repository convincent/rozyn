<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

abstract class WriteStatement extends ManipulationStatement {
	/**
	 * A multidimensional array containing all value sets for this Statement.
	 * Each set of values is itself an array representing all the values for
	 * a new row to be written to the database. Each element in a set represents
	 * a column value in its respective row. 
	 * 
	 * @var	array
	 */
	protected $values = [];
	
	/**
	 * Adds an array of values to the statement. Every set of values that you
	 * add through this method will be interpreted as a single row to be
	 * written to the database. If $append is set to false, all previously
	 * stored values will be overwritten instead.
	 * 
	 * @param	array		$valueSet
	 * @param	boolean		$append
	 * @return	\Rozyn\Database\WriteStatement
	 */
	public function values($valueSet = null, $append = true) {
		if ($valueSet === null) {
			return $this->values;
		}
		
		if (!is_array($valueSet)) {
			$valueSet = [$valueSet];
		}
		
		if (!$append) {
			$this->values = [];
		}
		
		$this->values[] = $valueSet;
		
		return $this;
	}
	
	/**
	 * Implodes all of the values stored in this Statement so that the resulting
	 * string can be used directly in a query.
	 * 
	 * @return	string
	 */
	protected function compileValues() {
		$setCount	= count($this->values);
		$values		= [];
		
		for ($i = 0; $i < $setCount; $i++) {
			$parameters = [];
			foreach ($this->fields as $field) {
				$parameters[] = ':' . $field . $i;
			}
			
			$values[] = '(' . implode(',', $parameters) . ')';
		}
		
		return implode(',', $values);
	}
}