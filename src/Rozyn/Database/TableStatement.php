<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

abstract class TableStatement extends DefinitionStatement {
	use TableOperations;
	
	/**
	 * Create a new TableStatement object.
	 * 
	 * @param	string	$table
	 */
	public function __construct($table = null) {
		$this->table($table);
	}
}