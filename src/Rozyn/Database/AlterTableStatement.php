<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class AlterTableStatement extends TableStatement {
	/**
	 * Holds all specifications for this statement.
	 * 
	 * @var	array
	 */
	protected $specifications = [];
	
	/**
	 * Compiles the entire statement so that it forms a valid SQL query.
	 * 
	 * @return	string
	 */
	public function compile() {
		return str_format("ALTER TABLE `{table}` {specifications}", array(
			'table'				=> $this->table(),
			'specifications'	=> $this->compileSpecifications(),
		));
	}
	
	/**
	 * Compiles all the specifications so that they can be used directly in an
	 * SQL query.
	 * 
	 * @return	string
	 */
	public function compileSpecifications() {
		return implode(', ', $this->getSpecifications());
	}
	
	/**
	 * Returns all specifications for this statement.
	 * 
	 * @return	array
	 */
	public function getSpecifications() {
		return $this->specifications;
	}
	
	/**
	 * Adds a specification to this statement.
	 * 
	 * @param	string	$specification
	 * @return	\Rozyn\Database\AlterTableStatement
	 */
	public function addSpecification($specification) {
		$this->specifications[] = $specification;
		
		return $this;
	}
	
	/**
	 * Adds multiple specifications to this statement.
	 * 
	 * @param	string[]	$specifications
	 * @return	\Rozyn\Database\AlterTableStatement
	 */
	public function addSpecifications(array $specifications) {
		foreach ($specifications as $specification) {
			$this->addSpecification($specification);
		}
		
		return $this;
	}
	
	/**
	 * Sets the specifications of this statement.
	 * 
	 * @param	array	$specifications
	 * @return	\Rozyn\Database\AlterTableStatement
	 */
	public function setSpecifications(array $specifications) {
		$this->specifications = $specifications;
		
		return $this;
	}
}