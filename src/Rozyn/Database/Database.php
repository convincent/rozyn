<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class Database {
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds all the parameters assigned to the next query.
	 * 
	 * @var	array
	 */
	protected $params;
	
	/**
	 * The hostname of the server where our database is located.
	 * 
	 * @var	string
	 */
	protected $host;
	
	/**
	 * The name of the database with which we want to connect.
	 * 
	 * @var	string
	 */
	protected $name;
	
	/**
	 * The full DSN used to connect to the database through PDO.
	 * 
	 * @var	string
	 */
	protected $dsn;
	
	/**
	 * An internal PDO object that we use for our database operations.
	 * 
	 * @var	PDO
	 */
	protected $dbh;
	
	/**
	 * Holds the potential error message that tells us about any problems
	 * encountered by this object.
	 * 
	 * @var	string
	 */
	protected $error;
	
	/**
	 * A PDOStatement used to store our queries in.
	 * 
	 * @var	PDOStatement
	 */
	protected $stmt;
	
	/**
	 * Holds a boolean value indicating whether we're currently inside a
	 * transaction.
	 * 
	 * @var	boolean
	 */
	protected $transaction;
	
	/**
	 * Construct a new Database object with the given login credentials.
	 * 
	 * @param	string	$host
	 * @param	string	$user
	 * @param	string	$pass
	 * @param	string	$name
	 */
	public function __construct($host = DB_HOST, $user = DB_USER, $pass = DB_PASS, $name = DB_NAME) {
		$this->host = $host;
		$this->name = $name;
		
        //Set DSN
        $this->dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->name . ';charset=utf8mb4';

		//Set options
        $options = array(
            \PDO::ATTR_PERSISTENT	=> false,
			\PDO::ATTR_EMULATE_PREPARES => false,
            \PDO::ATTR_ERRMODE		=> \PDO::ERRMODE_EXCEPTION,
			\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        );
		
        //Create a new PDO instance
        $this->dbh = new \PDO($this->dsn, $user, $pass, $options);
	}
	
	/**
	 * Prepare a raw query.
	 * 
	 * @param	string	$query
	 * @return	\Rozyn\Database\Database
	 */
	public function query($query) {
		$this->params = [];
		$this->stmt   = $this->dbh->prepare($query);
		
		return $this;
	}
	
	/**
	 * Binds one or more parameters to our next query.
	 * 
	 * @param	mixed	$param
	 * @param	mixed	$value
	 * @param	int		$type
	 * @return	\Rozyn\Database\Database
	 */
	public function bind($param, $value = null, $type = null) {
		if (is_array($param)) {
			foreach ($param as $key => $value) {
				$this->bindValue($key, $value, $this->determineType($value));
			}
		} else {
			$this->bindValue($param, $value, $type ?: $this->determineType($value));
		}
		
		return $this;
	}
	
	/**
	 * Executes the query.
	 * 
	 * @return	mixed
	 * @throws	\Rozyn\Database\QueryException
	 */
	public function execute() {
		try {
			$result = $this->stmt->execute();
		} catch (\PDOException $e) {
			throw new QueryException($e->getMessage() . ". Full query:" . PHP_EOL . PHP_EOL . $this->stmt->queryString);
		}
		
		return $result;
	}
	
	/**
	 * Executes the query and fetches the entire result set.
	 * 
	 * @param	int		$fetchStyle
	 * @param	mixed	$arg
	 * @return	type
	 */
	public function fetchAll($fetchStyle = null, $arg = null) {
		$this->execute();
		
		return ($arg) ? $this->stmt->fetchAll($fetchStyle ?: \PDO::FETCH_ASSOC, $arg) :
						$this->stmt->fetchAll($fetchStyle ?: \PDO::FETCH_ASSOC);
	}
	
	/**
	 * Executes the query and fetches a single result row.
	 * 
	 * @param	int		$fetchStyle
	 * @param	mixed	$arg
	 * @return	type
	 */
	public function fetch($fetchStyle = null, $arg = null) {
		$this->execute();
		
		return ($arg) ? $this->stmt->fetch($fetchStyle ?: \PDO::FETCH_ASSOC, $arg) :
						$this->stmt->fetch($fetchStyle ?: \PDO::FETCH_ASSOC);
	}
	
	/**
	 * Returns a row count.
	 * 
	 * @return	int
	 */
	public function rowCount() {
		return $this->stmt->rowCount();
	}
	
	/**
	 * Returns the last primary key value that was inserted through this object.
	 * 
	 * @return	int
	 */
	public function lastInsertId() {
		return $this->dbh->lastInsertId();
	}
	
	/**
	 * Begins a new transaction. Returns true if no transaction was active yet
	 * and one has now successfully been started. Returns false otherwise.
	 * 
	 * @return	boolean
	 */
	public function beginTransaction() {
		if (!$this->transactionStarted() && $this->dbh->beginTransaction()) {
			return $this->transaction = true;
		}
		
		return false;
	}

	/**
	 * Ends the current transaction. Returns true if a transactions was active
	 * and has been successfully ended. Returns false otherwise.
	 * 
	 * @return	boolean
	 */
	public function endTransaction() {
		if ($this->transactionStarted() && $this->dbh->commit()) {
			$this->transaction = false;
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns whether or not a transaction is currently active.
	 * 
	 * @return	boolean
	 */
	public function transactionStarted() {
		return $this->transaction;
	}
	
	/**
	 * Cancel a transaction. Returns true if a transaction was successfully
	 * cancelled, otherwise returns false.
	 * 
	 * @return	boolean
	 */
	public function cancelTransaction() {
		if ($this->dbh->rollBack()) {
			$this->transaction = false;
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns a debug dump of the PDOStatement associated with this Database 
	 * object. 
	 * 
	 * @return	array
	 */
	public function debugDumpParams() {
		return $this->stmt->debugDumpParams();
	}
	
	/**
	 * Binds a new value. The $key represents the placeholder key used in the
	 * query for the specified $value. The $type argument is an integer value 
	 * that specifies what kind of value we're dealing with (string, int, null,
	 * etc)
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 * @param	int		$type
	 */
	public function bindValue($key, $value, $type) {
		$this->params[$key] = $value;
		
		$this->stmt->bindValue($key, $value, $type);
	}
	
	/**
	 * Returns the PDOStatement object associated with this database object.
	 * 
	 * @return	PDOStatement
	 */	
	public function getStatement() {
		return $this->stmt;
	}
	
	/**
	 * Returns all the parameters bound to this database.
	 * 
	 * @return	array
	 */
	public function getParams() {
		return $this->params;
	}
	
	/**
	 * Returns whether or not this database object has an error message.
	 * 
	 * @return	boolean
	 */
	public function hasError() {
		return (bool)strlen($this->error);
	}
	
	/**
	 * Alias for hasError().
	 * 
	 * @return	boolean
	 */
	public function hasErrors() {
		return $this->hasError();
	}
	
	/**
	 * Returns the last error message associated with this database object.
	 * 
	 * @return	string
	 */
	public function getLastError() {
		return $this->error;
	}
	
	/**
	 * Provide a magic call method so that we can use this class as a gateway to
	 * core base classes.
	 * 
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	mixed
	 */
	public function __call($name, $arguments) {
		if (method_exists($this->dbh, $name)) {
			return call_user_func_array([$this->dbh, $name], $arguments);
		} elseif (method_exists($this->stmt, $name)) {
			return call_user_func_array([$this->stmt, $name], $arguments);			
		}
	}
	
	/**
	 * Returns the PDO instance used by this Database.
	 * 
	 * @return	PDO
	 */
	public function dbh() {
		return $this->dbh;
	}
	
	/**
	 * Returns the database host.
	 * 
	 * @return	string
	 */
	public function getHost() {
		return $this->host;
	}
	
	/**
	 * Returns the database name.
	 * 
	 * @return	string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Returns an integer representation of the type of $value.
	 * 
	 * @param	mixed $value
	 * @return	int
	 */
	protected function determineType($value) {
		$type = \PDO::PARAM_STR;
		
		switch (true) {
			case $value === (int)$value:
			case $value === (bool)$value:
				$type = \PDO::PARAM_INT;
				break;
			case null === $value:
				$type = \PDO::PARAM_NULL;
				break;
		}
		
		return $type;
	}
}