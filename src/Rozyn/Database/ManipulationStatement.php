<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

abstract class ManipulationStatement extends Statement {
	use TableOperations;
	
	protected $alias  = '';
	protected $where  = [];
	protected $fields = [];
	
	protected $defaultWhereOperator = 'AND';
	
	public function fields($fields = null, $append = false) {
		if (null === $fields) {
			return $this->fields;
		}
		
		if (!is_array($fields)) {
			$fields = [$fields];
		}
		
		if ($append === false) {
			$this->fields = [];
		}
		
		foreach ($fields as $key => $value) {
			$this->fields[($key === (int)$key) ? $value : $key] = $value;
		}

		return $this;
	}
	
	public function alias($alias) {
		$this->alias = $alias;
	}
	
	public function andWhere($conditions) {
		return $this->where($conditions, 'AND');
	}
	
	public function orWhere($conditions) {
		return $this->where($conditions, 'OR');
	}
	
	/**
	 * Add conditions to the WHERE clause of this Statement.
	 * 
	 * @param	mixed	$conditions
	 * @param	string	$combine
	 * @return	\Rozyn\Database\Statement
	 */
	public function where($conditions, $combine = null) {
		// If no $combine operator is specified, use the default operator for
		// this Statement.
		if ($combine === null) {
			$combine = $this->getWhereOperator();
		}
		
		// If an array of conditions is provided, apply them in sequence and
		// recursively.
		if (is_array($conditions)) {
			foreach ($conditions as $condition) {
				$this->where($condition, $combine);
			}
			
			return $this;
		}
		
		// If no array was specified, we need to add the conditions. First, we
		// generate a key that will be used to store the condition under. This
		// key will identify whether it's an AND or an OR condition, as we'll
		// prefix the key with the appropriate operator. This is done so that we
		// can construct a valid WHERE clause string based off our $where array
		// when the entire Statement needs to be compiled.
		do {
			$key = $combine . str_random($this->getConditions());
		} while (isset($this->where[$key]));

		// If the condition to be added is a simple string, just add the 
		// condition to our $where array under the previously computed  key.
		if (is_string($conditions)) {
			$this->addCondition($key, $conditions);
		} 

		// If the condition is a callback instead, we need to execute that 
		// callback. A callback can be used to group a bunch of conditions 
		// together, which makes it very easy to fully control the structure of
		// the finalized $where array. Each subarray in this $where array
		// represents a group of conditions that need to be enclosed with
		// parentheses in the final string representation of the WHERE clause.
		elseif (is_callable($conditions)) {
			// First, we clone the current Statement so that we'll be able to
			// modify it without destroying our original Statement.
			$statement = new static;
			
			// Next we apply the specified callback on our cloned Statement,
			// which will assign all the desired conditions to this cloned
			// query.
			$conditions($statement);

			// Finally, we add all the conditions that were assigned to our
			// original Statement in the callback. We file all of them together
			// under the previously computed key, so as to create a subarray
			// within our $where array. This way, as said previously, we will be
			// able to construct a properly formatted WHERE clause using 
			// parentheses where appropriate.
			$this->where[$key] = $statement->getConditions();
		}
		
		// Finally, return this Statement object to allow for method chaining.
		return $this;
	}
	
	public function addCondition($key, $condition) {
		$this->where[$key] = "({$condition})";
	}
	
	public function addConditions($conditions) {
		foreach ($conditions as $key => $condition) {
			$this->where[$key] = $condition;
		}
		
		return $this;
	}
	
	public function whereIn($column, $values) {
		return (empty($values)) ? $this->whereEquals('1', '2') : $this->where($column . ' IN (' . implode(',', $values) . ')');
	}
	
	public function whereEquals($column, $value) {
		return $this->where($column . ' = ' . $value);
	}
	
	public function whereNotEquals($column, $value) {
		return $this->where($column . ' <> ' . $value . ' OR ' . $column . ' IS NULL');
	}
	
	public function whereNull($column) {
		return $this->where($column . ' IS NULL');
	}
	
	public function whereNotNull($column) {
		return $this->where($column . ' IS NOT NULL');
	}
	
	public function whereContains($column, $value) {
		return $this->whereLike($column, "%{$value}%");
	}
	
	public function whereEndsWith($column, $value) {
		return $this->whereLike($column, '%' . $value);
	}
	
	public function whereStartsWith($column, $value) {
		return $this->whereLike($column, $value . '%');
	}
	
	public function whereLike($column, $value) {
		return $this->where($column . " LIKE '{$value}'");
	}
	
	public function whereExists($query) {
		if (!$query->hasFields()) {
			$query->fields([1]);
		}
		
		return $this->where("EXISTS ({$query->compile()})");
	}
	
	public function whereNotExists($query) {
		if (!$query->hasFields()) {
			$query->fields([1]);
		}
		
		return $this->where("NOT EXISTS ({$query->compile()})");
	}
	
	protected function compileTable() {
		return (!$this->alias) ? $this->table : $this->table . ' AS ' . $this->alias;
	}
	
	protected function compileFields() {
		if (!empty($this->fields)) {
			return implode(',', array_map(function($v) {
				return "`{$v}`";
			}, array_keys($this->fields)));
		}
		
		return null;
	}
	
	protected function compileWhere() {
		if (!empty($this->where)) {
			return 'WHERE ' . $this->compileConditions();
		}
		
		return '';
	}
	
	protected function compileConditions(array $conditions = null) {
		if ($conditions === null) {
			$conditions = $this->where;
		}
		
		$str = '';
		
		foreach ($conditions as $key => $condition) {
			$str .= (starts_with($key, 'OR')) ? ' OR ' : ' AND ';
			$str .= (is_array($condition)) ? 
						"({$this->compileConditions($condition)})" : 
						$condition;
		}
		
		return preg_replace("/^\s(AND|OR)/", '', $str);
	}
	
	public function setWhereOperator($operator) {
		$this->defaultWhereOperator = ($operator === 'OR') ? 'OR' : 'AND';
	}
	
	public function getWhereOperator() {
		return $this->defaultWhereOperator;
	}
	
	public function __call($name, $arguments) {
		if (starts_with($name, 'get') && null !== ($attr = camel2snake(substr($name, 3))) && property_exists($this, $attr)) {
			return $this->{$attr};
		}
	}
	
	/**
	 * Clear all conditions for this statement.
	 * 
	 * @return	\Rozyn\Database\Statement
	 */
	public function clearConditions() {
		$this->where = [];
	}
	
	/**
	 * Returns the WHERE conditions for this Statement.
	 * 
	 * @return	array
	 */
	public function getConditions() {
		return $this->where;
	}
	
	/**
	 * Returns whether or not this statement has any fields associated with it.
	 * 
	 * @return	boolean
	 */
	public function hasFields() {
		return !empty($this->fields);
	}
	
	public function __toString() {
		return $this->compile();
	}
}