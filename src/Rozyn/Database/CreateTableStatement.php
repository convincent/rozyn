<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class CreateTableStatement extends TableStatement {
	/**
	 * Holds the columns for this table.
	 * 
	 * @var	array
	 */
	protected $columns = [];
	
	/**
	 * Holds the name of the primary key column for this table.
	 * 
	 * @var	string
	 */
	protected $primaryKey;
	
	/**
	 * Compiles the query and returns it as a valid SQL query string.
	 * 
	 * @return	string
	 */
	public function compile() {
		if (!$this->table()) {
			throw new QueryException('Trying to compile a CREATE TABLE statement without a table name');
		}
		
		return str_format('CREATE TABLE `{table}` {columns}', array(
					'table'		=> $this->table(),
					'columns'	=> $this->compileColumns(),
		));
	}
	
	/**
	 * Sets or returns the table's columns.
	 * 
	 * @param	array $columns
	 * @return	mixed
	 */
	public function columns(array $columns = null) {
		if ($columns === null) {
			return $this->columns;
		}
		
		$this->columns = $columns;
		
		return $this;
	}
	
	/**
	 * Adds a single column to this statement.
	 * 
	 * @param	\Rozyn\Database\Column	$column
	 * @return	\Rozyn\Database\CreateTableStatement
	 */
	public function addColumn(Column $column) {
		$this->addColumns([$column->getName() => $column]);
		
		return $this;
	}
	
	/**
	 * Adds columns to this statement.
	 * 
	 * @param	array $columns
	 * @return	\Rozyn\Database\CreateTableStatement
	 */
	public function addColumns(array $columns) {
		$this->columns = array_merge($this->columns, $columns);
		
		return $this;
	}
	
	/**
	 * Returns the part of the query that's responsible for defining the columns.
	 * 
	 * @return	string
	 */
	protected function compileColumns() {
		$res = [];
		
		// Loop through all specified columns and convert them to a valid SQL
		// string.
		foreach ($this->columns as $column) {
			$res[] = $column->getDefinition();
		}
		
		// If a primary key has been defined for this table, add it to our 
		// column list.
		if ($this->primaryKey) {
			$res[] = $this->compilePrimaryKey();
		}
		
		// Return the SQL columns substring.
		return (empty($res)) ? '' : '(' . implode(', ', $res) . ')';
	}
	
	/**
	 * Sets or returns the primary key for this table query.
	 * 
	 * @param	string	$primaryKey
	 * @return	\Rozyn\Database\CreateTableStatement|string
	 */
	public function primaryKey($primaryKey = null) {
		if ($primaryKey === null) {
			return $this->primaryKey;
		}
		
		$this->primaryKey = $primaryKey;
		
		return $this;
	}
	
	/**
	 * Sets or returns the primary key for this table query.
	 * 
	 * @param	string	$primary
	 * @return	\Rozyn\Database\CreateTableStatement|string
	 */
	final public function primary($primary = null) {
		return $this->primaryKey($primary);
	}
	
	/**
	 * Returns the SQL to add the primary key to the table.
	 * 
	 * @return	string
	 */
	protected function compilePrimaryKey() {
		return ($this->primaryKey) ? "PRIMARY KEY ({$this->primaryKey})" : '';
	}
	
	/**
	 * Returns the SQL to add the primary key to the table.
	 * 
	 * @return	string
	 */
	final protected function compilePrimary() {
		return $this->compilePrimaryKey();
	}
}