<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class Column {
	/**
	 * The default collation used by all string-based columns.
	 * 
	 * @var	string
	 */
	const DEFAULT_COLLATION = 'utf8_general_ci';
	
	/**
	 * The name of this column.
	 * 
	 * @var	string
	 */
	protected $name;
	
	/**
	 * The type of this column.
	 * 
	 * @var	string
	 */
	protected $type;
	
	/**
	 * The default value of this column.
	 * 
	 * @var	mixed
	 */
	protected $default;
	
	/**
	 * The collation of this column.
	 * 
	 * @var	string
	 */
	protected $collation;
	
	/**
	 * The extra information of this column.
	 * 
	 * @var	string
	 */
	protected $extra;
	
	/**
	 * Whether or not this column accepts NULL values.
	 * 
	 * @var	boolean
	 */
	protected $null = false;
	
	/**
	 * Whether or not this column's value auto increments.
	 * 
	 * @var	boolean
	 */
	protected $increments = false;
	
	/**
	 * Constructs a new Column object.
	 * 
	 * @param	string					$name
	 * @param	string					$type
	 * @param	boolean					$null
	 * @param	mixed					$default
	 * @param	boolean					$increments
	 * @param	string					$collation
	 * @param	string					$extra
	 */
	public function __construct($name = null, $type = null, $null = false, $default = null, $increments = null, $collation = null, $extra = null) {
		$this->setName($name);
		$this->setType($type);
		$this->setNullable($null);
		$this->setDefault($default);
		$this->setIncrements($increments);
		$this->setCollation($collation);
		$this->setExtra($extra);
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Database\Column
	 */
	public function setName($name) {
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Sets the type.
	 * 
	 * @param	string	$type
	 * @return	\Rozyn\Database\Column
	 */
	public function setType($type) {
		$this->type = $type;
		
		return $this;
	}
	
	/**
	 * Sets the default value.
	 * 
	 * @param	mixed	$default
	 * @return	\Rozyn\Database\Column
	 */
	public function setDefault($default) {
		$this->default = $default;
		
		return $this;
	}
	
	/**
	 * Marks whether or not this column should accept NULL values.
	 * 
	 * @param	boolean		$null
	 * @return	\Rozyn\Database\Column
	 */
	public function setNullable($null) {
		$this->null = !!$null;
		
		return $this;
	}
	
	/**
	 * Marks whether or not this column's value auto increments.
	 * 
	 * @param	boolean		$increments
	 * @return	\Rozyn\Database\Column
	 */
	public function setIncrements($increments) {
		$this->increments = !!$increments;
		
		return $this;
	}
	
	/**
	 * Sets the collation.
	 * 
	 * @param	string	$collation
	 * @return	\Rozyn\Database\Column
	 */
	public function setCollation($collation) {
		$this->collation = $collation ?: static::DEFAULT_COLLATION;
		
		return $this;
	}
	
	/**
	 * Sets the extra information for this column.
	 * 
	 * @param	string	$extra
	 * @return	\Rozyn\Database\Column
	 */
	public function setExtra($extra) {
		$this->extra = $extra;
		
		return $this;
	}
	
	/**
	 * Returns the column's name.
	 * 
	 * @return	string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Returns the column's type.
	 * 
	 * @return	string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Returns the column's default value.
	 * 
	 * @return	mixed
	 */
	public function getDefault() {
		return $this->default;
	}
	
	/**
	 * Returns the column's collation.
	 * 
	 * @return	string
	 */
	public function getCollation() {
		return $this->collation ?: static::DEFAULT_COLLATION;
	}
	
	/**
	 * Returns the column's extra information.
	 * 
	 * @return	string
	 */
	public function getExtra() {
		return $this->extra;
	}
	
	/**
	 * Returns whether or not the column accepts NULL values.
	 * 
	 * @return	boolean
	 */
	public function isNullable() {
		return !!$this->null;
	}
	
	/**
	 * Returns whether or not the column's value auto increments.
	 * 
	 * @return	boolean
	 */
	public function isIncrements() {
		return !!$this->increments;
	}
	
	/**
	 * Returns whether or not this Column contains textual values/strings.
	 * 
	 * @return	boolean
	 */
	public function isString() {
		return starts_with(strtolower($this->getType()), array(
			'tinytext',
			'mediumtext',
			'longtext',
			'text',
			'char',
			'varchar',
		));
	}
	
	/**
	 * Returns whether or not this Column contains integer values. If the
	 * $includeBoolean value is set to true, TINYINT(1) will also be treated as
	 * an integer column, otherwise it will be treated as a column holding
	 * boolean values and thus it will return false.
	 * 
	 * @param	boolean	$includeBoolean
	 * @return	boolean
	 */
	public function isInteger($includeBoolean = false) {
		// Extract the type of this column.
		$type = strtolower($this->getType());
		
		// If we should exclude boolean columns, check if this column is a 
		// boolean column, and if so, return false.
		if (!$includeBoolean && $type === 'TINYINT(1)') {
			return false;
		}
		
		// In all other cases, we simply have to check if the type of this
		// column corresponds to a known integer column type.
		return starts_with($type, array(
			'tinyint',
			'smallint',
			'mediumint',
			'int',
			'integer',
			'bigint',
		));
	}
	
	/**
	 * Sets or returns whether this column accepts NULL values.
	 * 
	 * @param	boolean		$null
	 * @return	mixed
	 */
	final public function nullable($null = null) {
		if ($null === null) {
			return $this->isNullable();
		}
		
		return $this->setNullable($null);
	}
	
	/**
	 * Sets or returns whether this column's value is auto incremented.
	 * 
	 * @param	boolean		$increments
	 * @return	mixed
	 */
	final public function increments($increments = null) {
		if ($increments === null) {
			return $this->isIncrements();
		}
		
		return $this->setIncrements();
	}
	
	/**
	 * Returns the column definition as a valid SQL substring.
	 * 
	 * @return	string
	 */
	public function getDefinition() {
		return trim(str_format('{column} {type} {collation} {null} {default} {auto_increment} {key} {extra}', array(
				'column'			=> "`{$this->getName()}`",
				'type'				=> $this->getType(),
				'collation'			=> ($this->isString()) ? 'COLLATE ' . $this->getCollation() : '',
				'null'				=> ($this->isNullable()) ? 'NULL' : 'NOT NULL',
				'default'			=> (!$this->getDefault()) ?	(($this->isNullable()) ? 
																'DEFAULT NULL' : 
																'') 
														: 'DEFAULT ' . $this->getDefault(),
				'auto_increment'	=> ($this->isIncrements()) ? 'AUTO_INCREMENT' : '',
				'key'				=> '',
				'extra'				=> $this->getExtra()
		)));
	}
}