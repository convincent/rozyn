<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

use Rozyn\Model\Model;
use Rozyn\Relation\HasAndBelongsToMany;
use Rozyn\Composition\DI;

class Table {
	/**
	 * The columns in this table.
	 * 
	 * @var	\Rozyn\Database\Column[]
	 */
	protected $columns;
	
	/**
	 * The name of the database table.
	 * 
	 * @var	string
	 */
	protected $name;
	
	/**
	 * Holds whether a database table with this $name exists.
	 * 
	 * @var	boolean
	 */
	protected $exists;
	
	/**
	 * Holds the primary key of this table.
	 * 
	 * @var	string
	 */
	protected $primaryKey;
	
	/**
	 * Holds the next ALTER TABLE statement that is to be sent to the database.
	 * 
	 * @var	\Rozyn\Database\AlterTableStatement
	 */
	protected $alterTableStatement;
	
	/**
	 * Holds whether or not we want to immediately push any commits we make to 
	 * the cache of this Table.
	 * 
	 * @var	boolean
	 */
	protected $autoPush;
	
	/**
	 * A collection of callback funtions that should be executed after the next
	 * push action.
	 * 
	 * @var	\Closure[]
	 */
	protected $pushCallbacks = [];
	
	/**
	 * An array that maps model types to corresponding column creation methods
	 * in this class.
	 * 
	 * @var string[]
	 */
	protected $typeMethods = array(
		'int'		=> 'integer',
		'float'		=> 'float',
		'double'	=> 'float',
		'boolean'	=> 'boolean',
		'tinyint'	=> 'boolean',
		'datetime'	=> 'dateTime',
		'timestamp'	=> 'timestamp',
		'text'		=> 'text',
		'string'	=> 'string',
		'varchar'	=> 'string',
	);

	/**
	 * Constructs a new Table object.
	 * 
	 * @param	string						$name
	 * @param	\Rozyn\Database\Column[]	$columns
	 */
	public function __construct($name, array $columns = null) {
		$this->setName($name);
		
		$this->columns = $columns;
	}
	
	/**
	 * A static function that returns an instance of this object.
	 * 
	 * @param	string						$name
	 * @param	\Rozyn\Database\Column[]	$columns
	 * @return	\Rozyn\Database\Table
	 */
	static public function instance($name, array $columns = null) {
		return new static($name, $columns);
	}
	
	/**
	 * A static function that returns an instance of this object based on a 
	 * given Model instance.
	 * 
	 * @param	\Rozyn\Model\Model		$model
	 * @return	\Rozyn\Database\Table
	 */
	static public function instanceFromModel(Model $model) {
		$table = new static($model->getTable());
		$table->addModelColumns($model);
		
		return $table;
	}
	
	/**
	 * A static function that returns an instance of this object that represents
	 * a join table for a given HasAndBelongsToMany relation.
	 * 
	 * @param	\Rozyn\Relation\HasAndBelongsToMany	$relation
	 * @return	\Rozyn\Database\Table
	 */
	static public function instanceFromRelation(HasAndBelongsToMany $relation) {
		// If this relation has a JoinModel, just use that model to create the
		// table.
		if ($relation->hasJoinModel()) {
			return static::instanceFromModel($relation->getJoinModel());
		}
		
		// If not, we have to fall back on the default behaviour, so we first
		// derive the table name from the relation's join table property.
		$table = new static($relation->getJoinTable());
		
		// Next we create two columns, one for each model in the relation.
		$columns = array(
			$relation->getLocalKey()	=> $relation->getParent()->getFieldType($relation->getParent()->getPrimaryKey()),
			$relation->getForeignKey()	=> $relation->getRelated()->getFieldType($relation->getRelated()->getPrimaryKey())
		);
		
		// Finally we loop through all these columns and create them.
		foreach ($columns as $column => $type) {
			// Format the type so that it follows a consistent format.
			$type = strtolower(trim(preg_replace('/\(\d+\)$/', '', $type)));

			// Determine which class method we should use to create this field's
			// column and then call that method.
			$table->addColumnForType($type, $column, 'integer');
			
			// Add an index or this column.
			$table->index($column);
		}
		
		$table->primary('id');
		
		return $table;
	}
	
	/**
	 * Adds all columns that are defined inside a Model object.
	 * 
	 * @param	\Rozyn\Model\Model	$model
	 * @return	\Rozyn\Database\Table
	 */
	public function addModelColumns(Model $model) {
		// Define a mapping that maps field types to methods in this class that
		// create a table column matching the given field type.
		
		// Loop through all the model's fields.
		foreach ($model->getStructure() as $field => $type) {			
			// If no type was given for a field, try to infer that field's type.
			if ($type === null) {
				$type = $model->inferFieldType($field);
			}
			
			// Format the type so that it follows a consistent format.
			$type = strtolower(trim(preg_replace('/\(\d+\)$/', '', $type)));
			
			// Determine which class method we should use to create this field's
			// column and then call that method.
			$this->addColumnForType($type, $field, 'string');
			
			// Check if an index should be created for this column and if so,
			// create it.
			if ($type !== 'text') {
				$this->index($field);
			}
			
			// If the current field is the primary key of the model, mark the
			// corresponding column as such.
			if ($field === $model->getPrimaryKey()) {
				$this->primary($field);
			}
			
			// Make the column nullable if applicable.			
			if ($field !== $model->getPrimaryKey()) {
				$this->nullable($field);
			}
		}
		
		return $this;
	}
	
	/**
	 * Sets the name for this table.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Database\Table
	 */
	public function setName($name) {
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name for this table.
	 * 
	 * @return	string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Sets or gets the name for this table.
	 * 
	 * @param	string	$name
	 * @return	mixed
	 */
	final public function name($name = null) {
		if ($name === null) {
			return $this->getName();
		}
		
		return $this->setName($name);
	}
	
	/**
	 * Renames the table.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Database\Table
	 */
	public function rename($name) {
		// If the table already exists, we have to execute a RENAME action on 
		// the database.
		if ($this->exists()) {
			$this->commit("RENAME `{$name}`");
		
			$this->addPushCallback(function($table) use ($name) {
				$table->setName($name);
			});
		}
		
		// If the table doesn't exist yet, we can simply change the name of this
		// object and be done with it.
		else {
			$this->setName($name);
		}
		
		return $this;
	}
	
	/**
	 * Creates the table. 
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function create() {		
		if (!$this->exists()) {
			$query = $this->newQuery()->createTable($this->getName())->addColumns($this->getColumns());
			
			if ($this->primaryKey) {
				$query->primaryKey($this->primaryKey);
			}
			
			$query->execute();
			
			$this->exists = true;
			
			$this->push();
		}
		
		return $this;
	}
	
	/**
	 * Returns whether or not the table exists.
	 * 
	 * @return	boolean
	 */
	public function exists() {
		if ($this->exists === null && $this->getName()) {
			try {
				$this->newQuery()->raw("DESCRIBE `{$this->getName()}`;")->execute();
				
				$this->exists = true;
			} catch (QueryException $e) {
				$this->exists = false;
			}
		}
		
		return !!$this->exists;
	}
	
	/**
	 * Adds a new column to the table. You can either pass a Column object as
	 * the first argument or you can pass a number of arguments which will then
	 * be used to construct a new Column object on the fly (through our DI, so
	 * custom extensions of this class are still supported this way).
	 * 
	 * The reason we're not choosing for pure dependency injection in this case
	 * is because this class was made purely as an easy interface for interacting
	 * with database tables. While dependency injection would certainly make the
	 * code more flexible, in this particular case, I decided that forcing DI
	 * onto this class would complicate writing code for Table objects by hand 
	 * too much.
	 * 
	 * You should never be interacting with Column objects directly anyway, so
	 * in that sense it's probably best to just view this Table class as a sort 
	 * of Column Manager class that handles all the interaction with the Column
	 * objects for you.
	 * 
	 * @param	\Rozyn\Database\Column|string	$name
	 * @param	string							$type
	 * @param	boolean							$null
	 * @param	mixed							$default
	 * @param	boolean							$increments
	 * @param	string							$collation
	 * @param	string							$extra
	 * @return	\Rozyn\Database\Table
	 */
	public function addColumn($name = null, $type = null, $null = false, $default = null, $increments = null, $collation = null, $extra = null) {
		$column = ($name instanceof Column) ? $name : $this->createColumn($name, $type, $null, $default, $increments, $collation, $extra);
		
		if ($this->exists()) {
			$this->commitColumn($column);
		}
		
		$this->columns[$column->getName()] = $column;
		
		return $this;
	}
	
	/**
	 * Adds a new TEXT column of the specified size.
	 * 
	 * @param	string	$name
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function text($name, $null = false) {
		return $this->addColumn($name, 'TEXT', $null, '');
	}
	
	/**
	 * Adds a new VARCHAR column of the specified length. If no length is
	 * specifies, a default value of 255 is used.
	 * 
	 * @param	string	$name
	 * @param	int		$length
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function string($name, $length = 255, $null = false) {
		return $this->addColumn($name, "VARCHAR({$length})", $null, '');
	}
	
	/**
	 * Adds a new VARCHAR column of the specified length. If no length is
	 * specifies, a default value of 255 is used.
	 * 
	 * @param	string	$name
	 * @param	int		$length
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function varchar($name, $length = 255, $null = false) {
		return $this->string($name, $length, $null);
	}
	
	/**
	 * Adds a new INT column of the specified size. If no size is specified, 11
	 * is used as a default.
	 * 
	 * @param	string	$name
	 * @param	int		$size
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function integer($name, $size = 11, $null = false) {
		return $this->addColumn($name, "INT({$size})", $null, 0);
	}
	
	/**
	 * Adds a new FLOAT column.
	 * 
	 * @param	string	$name
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function float($name, $null = false) {
		return $this->addColumn($name, "FLOAT", $null, 0);
	}
	
	/**
	 * Adds a new TINYINT(1) column.
	 * 
	 * @param	string	$name
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function boolean($name, $null = true) {
		return $this->addColumn($name, "TINYINT(1)", $null, 0);
	}
	
	/**
	 * Adds a new DATE column.
	 * 
	 * @param	string	$name
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function date($name, $null = false) {
		return $this->addColumn($name, "DATE", $null);
	}
	
	/**
	 * Adds a new DATETIME column.
	 * 
	 * @param	string	$name
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function dateTime($name, $null = false) {
		return $this->addColumn($name, "DATETIME", $null);
	}
	
	/**
	 * Adds a new TIME column.
	 * 
	 * @param	string	$name
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function time($name, $null = false) {
		return $this->addColumn($name, "TIME", $null);
	}
	
	/**
	 * Adds a new TIMESTAMP column.
	 * 
	 * @param	string	$name
	 * @param	boolean	$null
	 * @return	\Rozyn\Database\Table
	 */
	public function timestamp($name, $null = true) {
		return $this->addColumn($name, "TIMESTAMP", $null);
	}
	
	/**
	 * Returns whether or not this table has the given column.
	 * 
	 * @param	string	$column
	 */
	public function hasColumn($column) {
		return array_key_exists($column, $this->getColumns());
	}
	
	/**
	 * Returns a column in this table.
	 * 
	 * @param	string	$column
	 * @return	\Rozyn\Database\Column
	 * @throws	\Rozyn\Database\TableException
	 */
	public function getColumn($column) {
		if (!$this->hasColumn($column)) {
			throw new TableException("Table {$this->getName()} does not have a column named {$column}");
		}
		
		return $this->getColumns()[$column];
	}
	
	/**
	 * Returns all columns in this table.
	 * 
	 * @return	array
	 */
	public function getColumns() {
		if ($this->columns === null) {
			$this->syncColumns();
		}
		
		return $this->columns;
	}
	
	/**
	 * Drops a column.
	 * 
	 * @param	string	$column
	 */
	public function dropColumn($column) {
		if ($this->hasColumn($column)) {
			unset($this->columns[$column]);

			if ($this->exists()) {
				$this->commit(sprintf("DROP COLUMN `{$column}`"));
			}
		}
		
		return $this;
	}
	
	/**
	 * Renames a column.
	 * 
	 * @param	string	$oldName
	 * @param	string	$newName
	 * @return	\Rozyn\Database\Table
	 */
	public function renameColumn($oldName, $newName) {
		$column = $this->getColumn($oldName);
		$column->setName($newName);
		
		$this->columns[$newName] = $column;
		unset($this->columns[$oldName]);
		
		if ($this->exists()) {
			$this->commit(sprintf("CHANGE COLUMN `%s` %s", $oldName, $column->getDefinition()));
		}
		
		return $this;
	}
	
	/**
	 * Changes several aspects of a column at once.
	 * 
	 * @param	string	$column
	 * @param	string	$type
	 * @param	boolean	$null
	 * @param	mixed	$default
	 * @param	boolean	$increments
	 * @param	string	$collation
	 * @param	string	$extra
	 * @return	\Rozyn\Database\Table
	 */
	public function modifyColumn($column, $type = null, $null = null, $default = null, $increments = null, $collation = null, $extra = null) {
		$argc = func_num_args();
		
		$specifications = $this->getCache()->getSpecifications();
		
		if ($argc >= 7) {
			$this->setColumnExtra($column, $extra);
		}
		
		if ($argc >= 6) {
			$this->setColumnCollation($column, $collation);
		}
		
		if ($argc >= 5) {
			$this->increments($column);
		}
		
		if ($argc >= 4) {
			$this->setColumnDefault($column, $default);
		}
		
		if ($argc >= 3) {
			$this->nullable($column, $null);
		}
		
		if ($argc >= 2) {
			$this->setColumnType($column, $type);
		}
		
		$this->getCache()->setSpecifications($specifications);
		$this->commitColumn($column);
		
		return $this;
	}
	
	/**
	 * Drops multiple columns at once.
	 * 
	 * @param	array	$columns
	 * @return	\Rozyn\Database\Table
	 */
	public function dropColumns(array $columns) {
		foreach ($columns as $column) {
			$this->dropColumn($column);
		}
		
		return $this;
	}
	
	/**
	 * Renames multiple columns at once.
	 * 
	 * @param	array	$columns
	 * @return	\Rozyn\Database\Table
	 */
	public function renameColumns(array $columns) {
		foreach ($columns as $oldName => $newName) {
			$this->renameColumn($oldName, $newName);
		}
		
		return $this;
	}

	/**
	 * Sets a column type.
	 * 
	 * @param	string	$column
	 * @param	string	$type
	 * @return	\Rozyn\Database\Table
	 */
	public function setColumnType($column, $type) {
		$this->getColumn($column)->setType($type);
		
		$this->commitColumn($column);
		
		return $this;
	}

	/**
	 * Sets a column's default value.
	 * 
	 * @param	string	$column
	 * @param	mixed	$default
	 * @return	\Rozyn\Database\Table
	 */
	public function setColumnDefault($column, $default) {
		$this->getColumn($column)->setDefault($default);
		
		$this->commitColumn($column);
		
		return $this;
	}

	/**
	 * Sets a column's collation.
	 * 
	 * @param	string	$column
	 * @param	string	$collation
	 * @return	\Rozyn\Database\Table
	 */
	public function setColumnCollation($column, $collation) {
		$this->getColumn($column)->setCollation($collation);
		
		$this->commitColumn($column);
		
		return $this;
	}

	/**
	 * Sets a column's extra information.
	 * 
	 * @param	string	$column
	 * @param	string	$extra
	 * @return	\Rozyn\Database\Table
	 */
	public function setColumnExtra($column, $extra) {
		$this->getColumn($column)->setExtra($extra);
		
		$this->commitColumn($column);
		
		return $this;
	}
	
	/**
	 * Marks 1 or more columns as nullable.
	 * 
	 * @param	string|array	$columns
	 * @param	boolean			$null
	 * @return	\Rozyn\Database\Table
	 */
	public function nullable($columns, $null = true) {
		if (!is_array($columns)) {
			$columns = [$columns];
		}
		
		foreach ($columns as $column) {
			$this->getColumn($column)->setNullable($null);
			$this->commitColumn($column);
		}
		
		return $this;
	}
	
	/**
	 * Returns the type of a given column.
	 * 
	 * @return	string
	 */
	public function getColumnType($column) {
		return $this->getColumn($column)->getType();
	}
	
	/**
	 * Returns the default value of a given column.
	 * 
	 * @return	mixed
	 */
	public function getColumnDefault($column) {
		return $this->getColumn($column)->getDefault();
	}
	
	/**
	 * Returns the collation of a given column.
	 * 
	 * @return	string
	 */
	public function getColumnCollation($column) {
		return $this->getColumn($column)->getCollation();
	}
	
	/**
	 * Returns the extra information of a given column.
	 * 
	 * @return	string
	 */
	public function getColumnExtra($column) {
		return $this->getColumn($column)->getExtra();
	}
	
	/**
	 * Returns whether or not this column accepts NULL values.
	 * 
	 * @return	boolean
	 */
	public function isNullable($column) {
		return $this->getColumn($column)->isNullable();
	}
	
	/**
	 * Drops the table.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function drop() {
		if ($this->exists()) {
			$this->newQuery()->raw("DROP TABLE IF EXISTS `{$this->getName()}`;")->execute();
			$this->exists = false;
		}
		
		return $this;
	}
	
	/**
	 * Truncates the table.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function truncate() {
		$this->newQuery()->raw("TRUNCATE TABLE `{$this->name}`;")->execute();
		
		return $this;
	}
	
	/**
	 * Adds an index for a given column. An optional $name argument may be
	 * provided to name the index.
	 * 
	 * @param	string	$column
	 * @param	string	$name
	 * @return	\Rozyn\Database\Table
	 */
	public function index($column, $name = null) {
		if ($name === null) {
			$name = $column;
		}
		
		$this->commit("ADD INDEX `{$name}` (`{$column}`)");
		
		return $this;
	}
	
	/**
	 * Adds a unique index for a given column. An optional $name argument may be
	 * provided to name the index.
	 * 
	 * @param	string	$column
	 * @param	string	$name
	 * @return	\Rozyn\Database\Table
	 */
	public function unique($column, $name = null) {
		if ($name === null) {
			$name = $column;
		}
		
		$this->commit("ADD UNIQUE `{$name}` (`{$column}`)");
		
		return $this;
	}
	
	/**
	 * Marks a column as the table's primary key. The second argument specifies
	 * whether or not this column should also be auto incremented.
	 * 
	 * @param	string	$column
	 * @param	boolean	$increments
	 * @return	\Rozyn\Database\Table
	 */
	public function primary($column, $increments = true) {
		$this->primaryKey = $column;
		
		if (!$this->hasColumn($column)) {
			$this->integer($column);
		}
		
		if ($this->exists()) {
			$this->commit("ADD PRIMARY KEY (`{$column}`)");
		}
		
		if ($increments && $this->getColumn($column)->isInteger()) {
			$this->increments($column);
		}
		
		return $this;
	}
	
	
	/**
	 * Drops an index on the table.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Database\Table
	 */
	public function dropIndex($name) {
		$this->commit("DROP INDEX `{$name}`");
		
		return $this;
	}
	/**
	 * Drops a unique index on the table.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Database\Table
	 */
	public function dropUnique($name) {
		return $this->dropIndex($name);
	}
	
	/**
	 * Drops the table's primary key.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function dropPrimary() {
		$this->commit("DROP PRIMARY KEY");
		
		return $this;
	}
	
	/**
	 * Marks a column as AUTO_INCREMENT.
	 * 
	 * @param	string	$column
	 * @return	\Rozyn\Database\Table
	 */
	public function increments($column) {
		if (!$this->hasColumn($column)) {
			$this->integer($column);
		}
		
		$this->getColumn($column)->setIncrements(true);
		
		if ($this->exists()) {
			$this->commitColumn($column);
		}
		
		return $this;
	}
	
	/**
	 * Makes this table implement soft deletion.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function soft() {
		$this->addColumn(Model::SOFT_DELETE_FIELD, 'TINYINT(1)', true, 0);
		$this->index(Model::SOFT_DELETE_FIELD);
		
		return $this;
	}
	
	/**
	 * Drops any soft deletion columns for this column.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function hard() {
		$this->dropColumn(Model::SOFT_DELETE_FIELD);
		$this->dropIndex(Model::SOFT_DELETE_FIELD);
		
		return $this;
	}
	
	/**
	 * Automatically sets timestamp columns for this table.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function timestamps() {
		$this->addColumn(Model::CREATED_AT_FIELD, 'TIMESTAMP', false, 'CURRENT_TIMESTAMP');
		$this->addColumn(Model::MODIFIED_AT_FIELD, 'TIMESTAMP', true, null, false, null, 'ON UPDATE CURRENT_TIMESTAMP');
		
		$this->index(Model::CREATED_AT_FIELD);
		$this->index(Model::MODIFIED_AT_FIELD);
		
		return $this;
	}
	
	/**
	 * Drop any automated timestamps columns that were added through the 
	 * timestamps() method.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function dropTimestamps() {
		$this->dropColumn(Model::CREATED_AT_FIELD);
		$this->dropColumn(Model::MODIFIED_AT_FIELD);
		
		$this->dropIndex(Model::CREATED_AT_FIELD);
		$this->dropIndex(Model::MODIFIED_AT_FIELD);
		
		return $this;
	}
	
	/**
	 * Implements authentication columns for this table. These columns are meant
	 * to keep track of who created each record in this table and who last 
	 * modified it.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function authentication() {
		$this->addColumn(Model::CREATED_BY_FIELD, 'INT(11)', true);
		$this->addColumn(Model::MODIFIED_BY_FIELD, 'INT(11)', true);
		
		$this->index(Model::CREATED_BY_FIELD);
		$this->index(Model::MODIFIED_BY_FIELD);
		
		return $this;
	}
	
	/**
	 * Drops any authentication columns that were added through the 
	 * authentication() method.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function dropAuthentication() {
		$this->dropColumn(Model::CREATED_BY_FIELD);
		$this->dropColumn(Model::MODIFIED_BY_FIELD);
		
		$this->dropIndex(Model::CREATED_BY_FIELD);
		$this->dropIndex(Model::MODIFIED_BY_FIELD);
		
		return $this;
	}
	
	/**
	 * Returns an array containing detailed information about the table.
	 * 
	 * @return	array
	 */
	public function schema() {
		$schema = [];
		
		foreach ($this->getColumns() as $name => $column) {
			$schema[$name] = $column->getDefinition();
		}
		
		return $schema;
	}
	
	/**
	 * Returns whether or not we want to immediately push any commits we make
	 * to the cache of this Table.
	 * 
	 * @param	boolean	$autoPush
	 * @return	mixed
	 */
	public function autoPush($autoPush = null) {
		if ($autoPush === null) {
			return $this->autoPush || $this->exists();
		}
		
		$this->autoPush = !!$autoPush;
		
		return $this;
	}
	
	/**
	 * Adds a callback function to be executed on the next push.
	 * 
	 * @param	callable	$cb
	 * @return	\Rozyn\Database\Table
	 */
	public function addPushCallback(callable $cb) {
		$this->pushCallbacks[] = $cb;
		
		return $this;
	}
	
	/**
	 * Clears all push callbacks.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function clearPushCallbacks() {
		$this->pushCallbacks = [];
		
		return $this;
	}
	
	/**
	 * Returns all push callbacks.
	 * 
	 * @return	array
	 */
	public function getPushCallbacks() {
		return $this->pushCallbacks;
	}
	
	/**
	 * Pushes all the changes made to the database.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function push() {
		if (!$this->exists()) {
			$this->create();
		}
		
		if (count($this->getCache()->getSpecifications())) {
			$this->newQuery()->setStatement($this->getCache())->execute();
		}
		
		// Execute all registered push callbacks.
		foreach ($this->getPushCallbacks() as $cb) {
			$cb($this);
		}
		
		// Reset the specifications cache.
		$this->getCache()->setSpecifications([]);
		
		// Reset our push callbacks.
		$this->clearPushCallbacks();
		
		return $this;
	}
	
	/**
	 * Saves all pending changes made to the database. Alias of push().
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	final public function save() {
		return $this->push();
	}
	
	/**
	 * Commits a new alteration to this Table.
	 * 
	 * @param	string	$commit
	 * @return	\Rozyn\Database\Table
	 */
	public function commit($commit) {
		$this->getCache()->addSpecification($commit);
		
		if ($this->autoPush()) {
			$this->push();
		}
	}
	
	/**
	 * Creates a new specification in this Table's ALTER TABLE Statement to 
	 * update the given column.
	 * 
	 * @param	\Rozyn\Database\Column | string	$column
	 * @return	\Rozyn\Database\Table
	 */
	public function commitColumn($column) {
		$action = ($column instanceof Column && !$this->hasColumn($column->getName())) ? 'ADD' : 'MODIFY';
		$definition = $this->getColumnObject($column)->getDefinition();
		
		$this->commit(sprintf("%s COLUMN %s",	$action, 
												$definition));
		
		return $this;
	}
	
	/**
	 * Returns all committed changes.
	 * 
	 * @return	array
	 */
	public function getCommits() {
		return $this->getCache()->getSpecifications();
	}
	
	/**
	 * Synchronizes the columns in this Table object with the ones in the actual
	 * database table.
	 * 
	 * @return	\Rozyn\Database\Table
	 */
	public function syncColumns() {
		$this->columns = [];
				
		if ($this->exists()) {
			$stmt = $this->newQuery()->db()->prepare("SHOW COLUMNS FROM {$this->getName()};");

			if ($stmt->execute()) {
				$columns = $stmt->fetchAll();
				$stringColumns = [];
				
				foreach ($columns as $i => $column) {
					if (isset($column['Field'])) {
						$info		= explode(' ', $column['Type']);
						$name		= $column['Field'];
						$type		= $info[0];
						$default	= $column['Default'];
						$null		= $column['Null'] === 'YES';
						$increments = !!strpos($column['Extra'], 'auto_increment');
						$extra		= trim(str_replace('auto_increment', '', implode(' ', array_slice($info, 1)) . ' ' . $column['Extra']));

						$this->columns[$name] = $columnObj = $this->createColumn($name, $type, $null, $default, $increments, null, $extra);
						
						// We also need to store our string columns in a second
						// array because we need to retrieve collations later on.
						if ($columnObj->isString()) {
							$stringColumns['column' . $i] = $name;
						}
					}
				}
				
				// Find out the collations of our string columns.
				if (!empty($stringColumns)) {
					$statement =  "SELECT COLLATION_NAME, COLUMN_NAME "
								. "FROM information_schema.columns "
								. "WHERE TABLE_NAME = :table "
								. "AND TABLE_SCHEMA = :database "
								. "AND COLUMN_NAME IN ({columns});";
					
					$query = $this->newQuery();
					$query->raw(str_format($statement, array(
						'columns' => implode(',', array_map(function($v) {
							return ':' . $v;
						}, array_keys($stringColumns))),
					)));
					
					$query->bind('table', $this->getName());
					$query->bind('database', $query->db()->getName());
					$query->bind($stringColumns);
					
					$result = $query->fetchAll();
					
					foreach ($result as $row) {
						$this->getColumnObject($row['COLUMN_NAME'])->setCollation($row['COLLATION_NAME']);
					}
				}
			}
		}
		
		return $this;
	}
	
	/**
	 * Dynamically calls the proper column creation method based on a given 
	 * model data type if it exists. If it does not exist, the $default method
	 * will be used.
	 * 
	 * @param	string	$type
	 * @param	string	$column
	 * @param	string	$default
	 * @return	\Rozyn\Database\Table
	 */
	protected function addColumnForType($type, $column, $default = 'string') {
		$method = (isset($this->typeMethods[$type])) ? $this->typeMethods[$type] : $default;
		
		return $this->{$method}($column);
	}
	
	/**
	 * Tries to convert a column name string to a Column object.
	 * 
	 * @param	\Rozyn\Database\Column | string	$name
	 * @return	\Rozyn\Database\Column
	 */
	protected function getColumnObject($name) {
		if ($name instanceof Column) {
			return $name;
		}
		
		return $this->columns[$name];
	}
	
	/**
	 * Returns a new query to be used for this Table object.
	 * 
	 * @return	\Rozyn\Database\Query
	 */
	protected function newQuery() {
		return DI::getInstanceOf('Rozyn\Database\Query');
	}
	
	/**
	 * Creates and returns a Column object.
	 * 
	 * @param	string					$name
	 * @param	string					$type
	 * @param	boolean					$null
	 * @param	mixed					$default
	 * @param	boolean					$increments
	 * @param	string					$collation
	 * @param	string					$extra
	 *
	 * @return	\Rozyn\Database\Column
	 */
	protected function createColumn($name = null, $type = null, $null = false, $default = null, $increments = null, $collation = null, $extra = null) {
		return DI::getInstanceOf('Rozyn\Database\Column', func_get_args());
	}
	
	/**
	 * Returns the cached ALTER TABLE statement that will be sent to the 
	 * database.
	 * 
	 * @return	\Rozyn\Database\AlterTableStatement
	 */
	protected function getAlterTableStatement() {
		return $this->alterTableStatement ?: $this->alterTableStatement = DI::getInstanceOf('Rozyn\Database\AlterTableStatement', [$this->getName()]);
	}
	
	/**
	 * Returns the cached ALTER TABLE statement that will be sent to the 
	 * database.
	 * 
	 * @return	\Rozyn\Database\AlterTableStatement
	 */
	final protected function getCache() {
		return $this->getAlterTableStatement();
	}
}