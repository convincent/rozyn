<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class SelectStatement extends ReadStatement {
	/**
	 * Holds the ORDER BY clause columns and their respective order directions.
	 * 
	 * @var	string[]
	 */
	protected $orderBy = [];
	
	/**
	 * Holds the columns for the GROUP BY clause.
	 * 
	 * @var	string[]
	 */
	protected $groupBy = [];
	
	/**
	 * Holds the JOIN conditions.
	 * 
	 * @var	string[]
	 */
	protected $joins = [];
	
	/**
	 * Holds the HAVING conditions.
	 * 
	 * @var	string[]
	 */
	protected $having = [];
	
	/**
	 * Holds the offset.
	 * 
	 * @var	int
	 */
	protected $offset;
	
	/**
	 * Creates a new SelectStatement object for the given fields.
	 * 
	 * @param	string[]	$fields
	 */
	public function __construct(array $fields = null) {
		$this->fields($fields);
	}
	
	/**
	 * Compiles the statement and returns the resulting string.
	 * 
	 * @return	string
	 */
	public function compile() {
		return sprintf('SELECT %s FROM %s %s %s %s %s %s %s',
					$this->compileFields() ?: '*',
					$this->compileTable(),
					$this->compileJoins(),
					$this->compileWhere(),
					$this->compileGroupBy(),
					$this->compileHaving(),
					$this->compileOrderBy(),
					$this->compileLimit());
	}
	
	/**
	 * Specify a join table with the corresponding join conditions.
	 * 
	 * @param	string			$table
	 * @param	string|string[] $condition
	 * @param	string			$type
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function join($table, $condition, $type = 'INNER') {
		$this->joins[] = ['table'		=> $table,
						  'type'		=> $type,
						  'conditions'	=> (is_array($condition)) ? $condition : [$condition]];
		
		return $this;
	}
	
	/**
	 * Returns the order columns and their respective order directions.
	 * 
	 * @return	string[]
	 */
	public function getOrderBy() {
		return $this->orderBy;
	}
	
	/**
	 * Returns the columns in the GROUP BY clause.
	 * 
	 * @return	string[]
	 */
	public function getGroupBy() {
		return $this->groupBy;
	}
	
	/**
	 * Returns the JOIN conditions for this statement.
	 * 
	 * @return	string[]
	 */
	public function getJoins() {
		return $this->joins;
	}
	
	/**
	 * Returns the HAVING conditions.
	 * 
	 * @return	string[]
	 */
	public function getHaving() {
		return $this->having;
	}
	
	/**
	 * Returns the offset.
	 * 
	 * @return	int
	 */
	public function getOffset() {
		return $this->offset;
	}
	
	/**
	 * Specify the columns for the GROUP BY clause.
	 * 
	 * @param	string|string[]	$column
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function groupBy($column) {
		if (is_array($column)) {
			$this->groupBy = array_merge($this->groupBy, $column);
		} else {
			$this->groupBy[] = $column;
		}
		
		return $this;
	}
	
	/**
	 * Specify a HAVING condition.
	 * 
	 * @param	string|string[]	$condition
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function having($condition) {
		if (is_array($condition)) {
			$this->having = array_merge($this->having, $condition);
		} else {
			$this->having[] = $condition;
		}
		
		return $this;
	}
	
	/**
	 * Set an ORDER BY clause.
	 * 
	 * @param	string	$field
	 * @param	string	$order
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function orderBy($field, $order = null) {
		if (is_array($field)) {
			$this->orderBy = array_merge($this->orderBy, $field);
		} else {
			$this->orderBy[$field] = $order;
		}
		
		return $this;
	}
	
	/**
	 * Remove an ORDER BY clause.
	 * 
	 * @param	string	$field
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function removeOrderBy($field) {
		if (array_key_exists($field, $this->orderBy)) {
			unset($this->orderBy[$field]);
		}
		
		return $this;
	}
	
	/**
	 * Remove a GROUP BY clause.
	 * 
	 * @param	string	$field
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function removeGroupBy($field) {
		if (array_key_exists($field, $this->groupBy)) {
			unset($this->groupBy[$field]);
		}
		
		return $this;
	}
	
	/**
	 * Remove a HAVING clause.
	 * 
	 * @param	string	$field
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function removeHaving($field) {
		if (array_key_exists($field, $this->having)) {
			unset($this->having[$field]);
		}
		
		return $this;
	}
	
	/**
	 * Specify an OFFSET.
	 * 
	 * @param	int	$offset
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function offset($offset) {
		$this->offset = ($offset === null) ? null : (int)$offset;
		
		return $this;
	}
	
	/**
	 * Compiles the joins so that they can be used directly in the final
	 * query.
	 * 
	 * @return	string
	 */
	protected function compileJoins() {
		$str = '';
		
		foreach ($this->joins as $join) {
			$str .= sprintf('%s JOIN %s ON %s ',
							strtoupper($join['type']),
							$join['table'],
							implode(' AND ', $join['conditions']));
		}
		
		return $str;
	}
	
	/**
	 * Compiles the GROUP BY clause so that it can be used directly in the final
	 * query.
	 * 
	 * @return	string
	 */
	protected function compileGroupBy() {
		return (!empty($this->groupBy)) ? 'GROUP BY ' . implode(',', $this->groupBy) : '';
	}
	
	/**
	 * Compiles the HAVING clause so that it can be used directly in the final
	 * query.
	 * 
	 * @return	string
	 */
	protected function compileHaving() {
		return (!empty($this->having)) ? 'HAVING ' . implode(' AND ', $this->having) : '';
	}
	
	/**
	 * Compiles the ORDER BY clause so that it can be used directly in the final
	 * query.
	 * 
	 * @return	string
	 */
	protected function compileOrderBy() {
		if (!empty($this->orderBy)) {
			$res = [];
			foreach ($this->orderBy as $column => $sort) {
				$res[] = str_format('{column} {sort}', array(
					'column'	=> $column,
					'sort'		=> (strtolower($sort) === 'desc') ? 'DESC' : 'ASC',
				));
			}

			return 'ORDER BY ' . implode(',', $res);
		}
		
		return '';
	}
	
	/**
	 * Compiles the LIMIT and OFFSET clauses together so that they can be used 
	 * directly in the final query.
	 * 
	 * @return	string
	 */
	protected function compileLimit() {
		if (null !== $this->limit) {
			$limit = 'LIMIT ' . $this->limit;
		} elseif (null !== $this->offset) {
			$limit = 'LIMIT ' . PHP_INT_MAX;
		} else {
			$limit = '';
		}
		
		if (null !== $this->offset) {
			$limit .= ' OFFSET ' . $this->offset;
		}
		
		return trim($limit);
	}
		
	/**
	 * Specify a LEFT JOIN.
	 * 
	 * @param	string			$table
	 * @param	string|string[] $condition
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function leftJoin($table, $condition) {
		return $this->join($table, $condition, 'LEFT');
	}
		
	/**
	 * Specify a RIGHT JOIN.
	 * 
	 * @param	string			$table
	 * @param	string|string[] $condition
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function rightJoin($table, $condition) {
		return $this->join($table, $condition, 'RIGHT');
	}
		
	/**
	 * Specify an INNER JOIN.
	 * 
	 * @param	string			$table
	 * @param	string|string[] $condition
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function innerJoin($table, $condition) {
		return $this->join($table, $condition, 'INNER');
	}
		
	/**
	 * Specify any other type of JOIN.
	 * 
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	\Rozyn\Database\SelectStatement
	 */
	public function __call($name, $arguments) {
		if (substr($name, -4) === 'Join' && method_exists($this, ($method = str_replace('Join', '', $name)))) {
			$arguments[] = strtoupper($method);
			return call_user_func_array([$this, 'join'], $arguments);
		}
	}
}