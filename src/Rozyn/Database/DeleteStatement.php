<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

class DeleteStatement extends ReadOrDeleteStatement {
	public function compile() {
		return str_format('DELETE FROM `{table}` {where} {limit}', array(
					'table'	=> $this->compileTable(),
					'where' => $this->compileWhere(),
					'limit' => $this->compileLimit(),
		));
	}
}