<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Database;

abstract class ReadOrDeleteStatement extends ManipulationStatement {
	protected $limit = null;
	
	public function from($table) {
		$this->table($table);
		
		return $this;
	}
	
	public function limit($limit) {
		$this->limit  = ($limit === null) ? null : intval($limit);
		
		return $this;
	}
	
	protected function compileLimit() {
		return ($this->limit !== null) ? 'LIMIT ' . $this->limit : null;
	}
}