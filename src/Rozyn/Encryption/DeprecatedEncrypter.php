<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Encryption;

use Crypto;

class DeprecatedEncrypter extends Encrypter {
	/**
	 * Mark the class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds the default key used for this app's encryption.
	 * 
	 * @var	string
	 */
	protected $key;
	
	/**
	 * Creates a new instance of our Encrypter.
	 * 
	 * @param	string	$key
	 * @throws	\Rozyn\Encryption\NullKeyException
	 */
	public function __construct($key = null) {
		$this->key = $key ?: secret_key();
		
		if ($this->key === null) {
			throw new NullKeyException();
		}
	}
	
	/**
	 * Encrypts a message.
	 * 
	 * @param	string	$message
	 * @param	string	$key
	 * @return	string
	 */
	public function encrypt($message, $key = null) {
		return base64_encode(Crypto::Encrypt($message, $key ?: $this->key));
	}
	
	/**
	 * Decrypts a message.
	 * 
	 * @param	string	$message
	 * @param	string	$key
	 * @return	string
	 * @throws	\Rozyn\Encryption\InvalidCipherTextException
	 */
	public function decrypt($message, $key = null) {
		try {
			return Crypto::Decrypt(base64_decode($message), $key ?: $this->key);
		} catch (\InvalidCiphertextException $e) {
			throw new InvalidCipherTextException($message);
		}
	}
}