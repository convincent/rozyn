<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 * 
 * Inspired by http://www.zimuel.it/authenticated-encrypt-with-openssl-and-php-7-1/
 */

namespace Rozyn\Encryption;

class Encrypter {
    /**
     * The tag length
     * 
     * @var int
     */
     const TAG_LENGTH = 16;
    
	/**
	 * Mark the class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds the default key used for this app's encryption.
	 * 
	 * @var	string
	 */
	protected $key;
    
    /**
     * The encryption algorithm used.
     * 
     * @var string
     */
    protected $algo = 'aes-256-gcm';
	
	/**
	 * Creates a new instance of our Encrypter.
	 * 
	 * @param	string	$key
	 * @throws	\Rozyn\Encryption\NullKeyException
	 */
	public function __construct($key = null) {
		$this->key = $key ?: secret_key();
		
		if ($this->key === null) {
			throw new NullKeyException();
		}
	}
	
	/**
	 * Encrypts a message.
	 * 
	 * @param	string	$message
	 * @param	string	$key
	 * @return	string
	 */
	public function encrypt($message, $key = null) {
        $tag    = null;
        $iv     = random_bytes($this->getIvLength());
        
        $ciphertext = openssl_encrypt(
            $message, 
            $this->getAlgorithm(), 
            $key ?: $this->getKey(), 
            OPENSSL_RAW_DATA, 
            $iv, 
            $tag,
            '',
            static::TAG_LENGTH
        );
        
        return base64_encode($iv . $tag . $ciphertext);
	}
	
	/**
	 * Decrypts a message.
	 * 
	 * @param	string	$message
	 * @param	string	$key
	 * @return	string
	 * @throws	\Rozyn\Encryption\InvalidCipherTextException
	 */
	public function decrypt($message, $key = null) {
        $data = base64_decode($message);
        
        $iv = substr($data, 0, $this->getIvLength());
        $tag = substr($data, $this->getIvLength(), static::TAG_LENGTH);
        $ciphertext = substr($data, $this->getIvLength() + static::TAG_LENGTH);
        
        $decrypt = openssl_decrypt(
            $ciphertext, 
            $this->getAlgorithm(), 
            $key ?: $this->getKey(), 
            OPENSSL_RAW_DATA, 
            $iv,
            $tag,
			''
        );
        
		if ($decrypt === false) {
			throw new InvalidCipherTextException($message);
		}
		
		return $decrypt;
	}
    
    /**
     * Returns the algorithm used by this encrypter.
     * 
     * @return  string
     */
    protected function getAlgorithm() {
        return $this->algo;
    }
    
    /**
     * Returns the key by this encrypter.
     * 
     * @return  string
     */
    protected function getKey() {
        return $this->key;
    }
    
    /**
     * Returns the IV length for the given algorithm
     * 
     * @param   string  $algo
     * @return  int
     */
    protected function getIvLength($algo = null) {
        return openssl_cipher_iv_length($algo ?: $this->getAlgorithm());
    }
}