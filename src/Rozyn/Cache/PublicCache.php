<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Cache;

class PublicCache extends FileCache {
	/**
	 * Specifies whether cache entries should be encrypted.
	 * 
	 * @var	boolean
	 */
	protected $encrypted = false;
	
	/**
	 * Turns a given key into a relative asset path to the corresponding file.
	 * 
	 * @param	string	$key
	 * @return	string
	 */
	public function asset($key) {
		return __WWW_PUBLIC__ . URI_SEPARATOR . CACHE_DIR . URI_SEPARATOR . $key;
	}
	
	/**
	 * Returns the root path for this cache directory.
	 * 
	 * @return	string
	 */
	protected function getRootPath() {
		return __PUBLIC__ . DS . CACHE_DIR;
	}
	
	/**
	 * Returns the full path to the cache file associated with a given key.
	 * 
	 * @param	string	$key
	 * @return	string
	 */
	protected function getPath($key) {
		return $this->getRootPath() . DS . $key;
	}
}