<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Cache;

class FileCache extends Cache {
	/**
	 * Returns a timestamp that corresponds to the last time the specified
	 * cache entry was updated.
	 * 
	 * @param	string	$key
	 * @return	int
	 */
	public function time($key) {
		return filemtime($this->getPath($key));
	}
	
	/**
	 * Write something to our cache.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 */
	public function write($key, $value) {
		$path = $this->getPath($key);
		
		if (!is_scalar($value)) {
			$value = serialize($this->prepare($value));
		}
		
		if ($this->isEncrypted()) {
			$value = $this->encrypter->encrypt($this->prepare($value));
		}
		
		file_put_contents($path, $value);
	}

	/**
	 * Returns the value of a given cache item if it exists, $default otherwise.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function read($key, $default = null) {
		if (!$this->has($key)) {
			return $default;
		}
		
		$value = pf($this->getPath($key));
		
		if ($this->isEncrypted()) {
			$value = $this->encrypter->decrypt($value);
		}
		
		if (is_serialized($value)) {
			$value = unserialize($value);
		}
		
		return $value;
	}

	/**
	 * Check if a given cache item exists.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function has($key) {
		return file_exists($this->getPath($key));
	}
	
	/**
	 * Delete a cache entry.
	 * 
	 * @param	string	$key
	 */
	public function delete($key) {
		$path = $this->getPath($key);
		
		if (is_file($path)) {
			unlink($path);
		}
	}
	
	/**
	 * Clears the cache.
	 */
	public function clear() {
		dir_map($this->getRootPath(), function($file, $dir, $path) {
			unlink($path);
		});
	}
	
	/**
	 * Dumps the cache.
	 * 
	 * @return	array
	 */
	public function dump() {
		$result = [];
		
		dir_map($this->getRootPath(), function($file, $dir, $path) use (&$result) {
			$result[$file] = array(
				'size' => filesize($path),
			);
		});
		
		return $result;
	}
	
	/**
	 * Checks the current state of the cache and makes sure it's initialized 
	 * properly.
	 */
	protected function setup() {
		if (!is_dir($this->getRootPath())) {
			mkdir($this->getRootPath());
		}
		
		if (!is_writable($this->getRootPath())) {
			throw new CacheException(sprintf('Cache directory %s is not writeable', $this->getRootPath()));
		}
	}
	
	/**
	 * Returns the root path for this cache directory.
	 * 
	 * @return	string
	 */
	protected function getRootPath() {
		return __CACHE__;
	}
	
	/**
	 * Returns the full path to the cache file associated with a given key.
	 * 
	 * @param	string	$key
	 * @return	string
	 */
	protected function getPath($key) {
		return $this->getRootPath() . DS . md5($key) . md5(base64_encode($key));
	}
}