<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Cache;

class ApcCache extends Cache {
	/**
	 * A constant that represents the suffix that is appended to a given key to
	 * store its last modification time.
	 * 
	 * @var	string
	 */
	const TIME_SUFFIX = '-time';
	
	/**
	 * Specifies whether cache entries should be encrypted.
	 * 
	 * @var	boolean
	 */
	protected $encrypted = false;
	
	/**
	 * Returns a timestamp that corresponds to the last time the specified
	 * cache entry was updated.
	 * 
	 * @param	string	$key
	 * @return	int
	 */
	public function time($key) {
		return apc_fetch($this->formatKey($key . self::TIME_SUFFIX));
	}
	
	/**
	 * Write something to our cache.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 */
	public function write($key, $value) {
		// Store the value in our cache.
		apc_store($this->formatKey($key), ($this->isEncrypted()) ? 
					// If the value needs to be encrypted, do so before saving
					// it (after preparing the value for caching).
					$this->encrypter->encrypt($this->prepare($value)) : 
					// If not, just store the value as is, after preparing it.
					$this->prepare($value));
		
		// Store a separate cache entry that holds the last modification time
		// of the cache element matching the given key. This can be used to 
		// check if the cache entry has passed a certain age.
		apc_store($this->formatKey($key . self::TIME_SUFFIX), time());
	}

	/**
	 * Returns the value of a given cache item if it exists, $default otherwise.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function read($key, $default = null) {
		if ($this->has($key)) {
			return ($this->isEncrypted()) ? 
						$this->encrypter->decrypt(apc_fetch($this->formatKey($key))) : 
						apc_fetch($this->formatKey($key));
		}
		
		return $default;
	}

	/**
	 * Check if a given cache item exists.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function has($key) {
		return apc_exists($this->formatKey($key));
	}
	
	/**
	 * Clears the cache.
	 */
	public function clear() {
		apc_clear_cache();
	}
	
	/**
	 * Delete a cache entry.
	 * 
	 * @param	string	$key
	 */
	public function delete($key) {
		apc_delete($this->formatKey($key));
		apc_delete($this->formatKey($key . self::TIME_SUFFIX));
	}
	
	/**
	 * Dumps the cache.
	 * 
	 * @return	array
	 */
	public function dump() {
		return apc_cache_info('user');
	}

	/**
	 * Checks the current state of the cache and makes sure it's initialized 
	 * properly.
	 */
	protected function setup() {
		if (!function_exists('apc_fetch')) {
			throw new CacheException('APC functionality not supported.');
		}
	}
	
	/**
	 * Formats the key so that we can be sure it follows all our necessary
	 * guidelines.
	 * 
	 * @param	string	$key
	 * @return	string
	 */
	protected function formatKey($key) {
		return PROJECT_DIR . DS . $key;
	}
}