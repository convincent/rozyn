<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Cache;

use Rozyn\Encryption\Encrypter;

abstract class Cache {
	/**
	 * A magic class method that should be called before we try to cache a class
	 * instance. If the object does not implement this method, simply ignore it.
	 * 
	 * @var	string
	 */
	const MAGIC_CACHE_METHOD = '__beforeCache';
	
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Specifies whether cache entries should be encrypted.
	 * 
	 * @var	boolean
	 */
	protected $encrypted = true;
	
	/**
	 * An Encrypter object responsible for encrypting this cache's entries.
	 * 
	 * @var	\Rozyn\Encryption\Encrypter
	 */
	protected $encrypter;

	/**
	 * Create our cache object.
	 * 
	 * @param	\Rozyn\Encryption\Encrypter	$encrypter
	 */
	public function __construct(Encrypter $encrypter) {
		$this->setEncrypter($encrypter);
		
		$this->setup();
	}
	
	/**
	 * Returns true if the given timestamp is more recent than the last 
	 * modification date of the cache entry corresponding to the given $key.
	 * This method can be used to determine whether or not a certain cache entry
	 * should be overwritten yet.
	 * 
	 * @param	string	$key
	 * @param	int		$time
	 * @return	int
	 */
	public function outdated($key, $time) {
		return $this->time($key) <= $time;
	}

	/**
	 * Checks the current state of the cache and makes sure it's initialized 
	 * properly.
	 */
	abstract protected function setup();
	
	/**
	 * Returns a timestamp that corresponds to the last time the specified
	 * cache entry was updated.
	 * 
	 * @param	string	$key
	 * @return	int
	 */
	abstract public function time($key);

	/**
	 * Write something to our cache.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 */
	abstract public function write($key, $value);

	/**
	 * Returns the value of a given cache item if it exists, $default otherwise.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	abstract public function read($key, $default = null);

	/**
	 * Check if a given cache item exists.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	abstract public function has($key);
	
	/**
	 * Delete a cache entry.
	 * 
	 * @param	string	$key
	 */
	abstract public function delete($key);
	
	/**
	 * Clears the cache.
	 */
	abstract public function clear();
	
	/**
	 * Dumps the cache.
	 * 
	 * @return	array
	 */
	abstract public function dump();
	
	/**
	 * This method can be used to apply some last-minute logic to a soon-to-be
	 * cache entry. This is useful to unset certain properties that you don't
	 * need per se to save space and execution time when the cache entry is to
	 * be retrieved later on.
	 * 
	 * @param	mixed	$entry
	 * @return	mixed
	 */
	protected function prepare($entry) {
		if (is_object($entry) && method_exists($entry, $method = static::MAGIC_CACHE_METHOD)) {
			$entry->$method();
		}
		
		return $entry;
	}
	
	/**
	 * Sets an Encrypter for this Cache.
	 * 
	 * @param	\Rozyn\Encryption\Encrypter	$encrypter
	 */
	public function setEncrypter(Encrypter $encrypter) {
		$this->encrypter = $encrypter;
	}
	
	/**
	 * Returns the Encrypter object associated with this Cache.
	 * 
	 * @return	\Rozyn\Encryption\Encrypter
	 */
	public function getEncrypter() {
		return $this->encrypter;
	}
	
	/**
	 * Returns whether or not cache entries should be encrypted.
	 * 
	 * @return	boolean
	 */
	protected function isEncrypted() {
		return $this->encrypted;
	}
	
	/**
	 * Clears the cache.
	 */
	final public function flush() {
		return $this->clear();
	}
}