<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Config;

class Config {
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;

	/**
	 * Holds all the configuration options held by this class.
	 * 
	 * @var	array
	 */
	protected $config = [];

	/**
	 * Stores one or more config options.
	 * 
	 * @param	string|array	$key
	 * @param	mixed			$value
	 */
	public function write($key, $value = null) {
		if (is_array($key)) {
			$this->config = array_merge($this->config, $key);
		}
		
		else {
			$this->config[$key] = $value;
		}
	}

	/**
	 * Read from our config class.
	 * 
	 * @param	string	$key
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function read($key, $default = null) {
		if ($this->has($key)) {
			return $this->config[$key];
		}
		
		if (defined($constant = strtoupper($key))) {
			return constant($constant);
		}
		
		return $default;
	}

	/**
	 * Check if our config class has a value for the given key.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function has($key) {
		return array_key_exists($key, $this->config);
	}

	/**
	 * Delete entries from our config class.
	 * 
	 * @param	string	$key
	 */
	public function delete($key) {
		if ($this->has($key)) {
			unset($this->config[$key]);
		}
	}
	
	/**
	 * Import a file with configuration options.
	 * 
	 * @param	string	$path
	 */
	public function import($path) {
		$this->write(require $path);
	}
	
	/**
	 * Dumps all the config options stored in this class.
	 * 
	 * @return	array
	 */
	public function dump() {
		return $this->config;
	}
}