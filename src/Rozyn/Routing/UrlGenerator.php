<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Routing;

use Rozyn\Request\IncomingRequest;

class UrlGenerator {
	/**
	 * Marks the class as a singleton, which alerts our dependency injector to 
	 * the fact that it only has to load an instance of this class once.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds the Router object that in turn contains all the Routes for which
	 * this class can generate URLs.
	 * 
	 * @var	\Rozyn\Routing\Router
	 */
	protected $router;
	
	/**
	 * Holds the current HTTP Request, which is used to determine various 
	 * variables that are relevant to the generation of new URLs.
	 * 
	 * @var	\Rozyn\Request\IncomingRequest
	 */
	protected $request;
	
	/**
	 * Constructs the object.
	 * 
	 * @param	\Rozyn\Routing\Router			$router
	 * @param	\Rozyn\Request\IncomingRequest	$request
	 */
	public function __construct(Router $router, IncomingRequest $request) {
		$this->router  = $router;
		$this->request = $request;
	}
	
	/**
	 * Generates the URL to a named Route or a Route object. Takes into account
	 * whether or not this URL should be via the HTTP or HTTPS protocol based
	 * on the current request and the route itself (if the route can only be
	 * accessed over HTTPS, then HTTPS is forced). You can also specify an 
	 * array of parameters to be passed along to the Route.
	 * 
	 * @param	string|Rozyn\Routing\Route	$route
	 * @param	array						$params
	 * @return	string
	 */
	public function route($route, array $params = []) {
		if (is_string($route) && ($route = $this->router->getRoute($route)) || $route instanceof Route) {
			return $route->url($params);
		}
		
		return null;
	}
	
	/**
	 * Takes a URL string or relative path as a parameter and formats it so that 
	 * it matches all the criteria placed upon it.
	 * 
	 * @param	string		$path
	 * @param	boolean		$secure
	 * @param	boolean		$absolute
	 * @return	string
	 */
	public function format($path, $secure = null, $absolute = true) {		
		// Remove the inclpath root in case an include path is passed along.
		$path = preg_replace('/^' . preg_quote(__ROOT__, '/') . '/', '', $path);
		
		if ($secure === true) {
			return $this->makeSecure($path);
		}
		
		if ($absolute === true) {
			return $this->makeAbsolute($path, $secure);
		}
		
		return ($this->isValidUrl($path)) ? $path : merge_paths(__WWW_ROOT__, $path);
	}
	
	/**
	 * Returns the URL to the previous page.
	 * 
	 * @return	string
	 */
	public function previous() {
		if (null !== ($referer = $this->request->getHeader('referer', null))) {
			return $referer;
		}
		
		return $this->format('/');
	}
	
	/**
	 * Returns whether or not a given URL is secure.
	 * 
	 * @param	string	$url
	 * @return	boolean
	 */
	protected function isSecure($url) {
		return (starts_with($url, $this->getScheme(true)));
	}
	
	/**
	 * Takes a URL string as a parameter and makes it absolute.
	 * 
	 * @param	string	$path
	 * @param	boolean	$secure
	 * @return	string
	 */
	protected function makeAbsolute($path, $secure = null) {
		if ($this->isValidUrl($path)) {
			return ($secure === null) ? 
						$path : 
						preg_replace('/^(https?:\/\/)?/', $this->getScheme($secure), $path);
		}
		
		$path = prefix($path, '/');
		
		return (starts_with($path, __WWW_ROOT__)) ?	$this->getHostPath($secure) . $path :
													$this->getRootPath($secure) . $path;
	}
	
	/**
	 * Merges multiple URL components together by concatenating them using a 
	 * forward slash as glue. Returns a relative path that is prefixed with a
	 * forward slash.
	 * 
	 * @param	array	$components
	 * @return	string
	 */
	protected function buildPath($components) {
		return prefix(preg_replace('/' . PREG_URI_SEPARATOR . '+/', '/', implode(URI_SEPARATOR, (!is_array($components)) ? func_get_args() : $components), URI_SEPARATOR));
	}
	
	/**
	 * Returns whether or not a given path is a valid absolute URL.
	 * 
	 * @param	string	$path
	 * @return	boolean
	 */
	protected function isValidUrl($path) {
		return starts_with($path, ['http://', 'https://']);
	}
	
	/**
	 * Turns a path into an absolute, secure URL.
	 * 
	 * @param	string	$path
	 * @return	string
	 */
	protected function makeSecure($path) {
		return $this->makeAbsolute($path, true);
	}
	
	/**
	 * Returns the full host path, which consists of the scheme and the 
	 * hostname.
	 * 
	 * @param	boolean	$secure
	 * @return	string
	 */
	protected function getHostPath($secure = null) {
		return $this->getScheme($secure) . str_replace(['http://', 'https://'], '', __HOST__);
	}
	
	/**
	 * Returns the full secure host path, which consists of the https scheme and 
	 * the hostname.
	 * 
	 * @return	string
	 */
	protected function getSecureHostPath() {
		return $this->getHostPath(true);
	}
	
	/**
	 * Returns the URL path to the root directory of this app.
	 * 
	 * @param	boolean$secure
	 * @return	string
	 */
	protected function getRootPath($secure = null) {
		return $this->getHostPath($secure) . __WWW_ROOT__;
	}
	
	/**
	 * Returns the secure URL path to the root directory of this app.
	 * 
	 * @return	string
	 */
	protected function getSecureRootPath() {
		return $this->getRootPath(true);
	}
	
	/**
	 * Returns the proper scheme based on the given $secure setting.
	 * 
	 * @param	boolean	$secure
	 * @return	string
	 */
	protected function getScheme($secure = null) {
		if ($secure === null) {
			return $this->request->getScheme();
		}
		
		return ($secure) ? 'https://' : 'http://';
	}
}