<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Routing;

use Rozyn\Session\FlashHandler;

class RedirectHandler {
	/**
	 * Marks the class as a singleton, which alerts our dependency injector to 
	 * the fact that it only has to load an instance of this class once.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds the UrlGenerator object used to generate the redirect URLs.
	 * 
	 * @var	\Rozyn\Routing\UrlGenerator
	 */
	protected $generator;
	
	/**
	 * Holds the FlashHandler object used to store flash messages for a redirect.
	 * 
	 * @var	\Rozyn\Session\FlashHandler
	 */
	protected $flash;
	
	/**
	 * Creates an instance of this class.
	 * 
	 * @param	\Rozyn\Routing\UrlGenerator	$generator
	 * @param	\Rozyn\Session\FlashHandler	$flash
	 */
	public function __construct(UrlGenerator $generator, FlashHandler $flash) {
		$this->generator = $generator;
		$this->flash = $flash;
	}
	
	/**
	 * Sets a flash message that will be passed along with the Redirect. Returns
	 * itself to allow for easy method chaining.
	 * 
	 * @param	string	$key
	 * @param	string	$value
	 * @return	\Rozyn\Routing\RedirectHandler
	 */
	public function flash($key, $value) {
		$this->flash->write($key, $value);
		
		return $this;
	}
	
	/**
	 * Redirects the client to a given URL with an optional status code or
	 * headers to be sent along.
	 * 
	 * @param	string	$url
	 * @param	int		$status
	 * @param	headers	$headers
	 */
	protected function redirect($url, $status = 302, array $headers = []) {
		foreach ($headers as $header => $value) {
			header($header . ': ' . $value);
		}
		
		header('Location: ' . $url, true, $status);
		exit;
	}
	
	/**
	 * Redirects the client to a given URL or path. The URL/path is first run
	 * through the UrlGenerator to make sure it's valid before the redirection
	 * takes place.
	 * 
	 * @param	string	$path
	 * @param	int		$status
	 * @param	headers	$headers
	 */
	public function to($path, $status = 302, array $headers = []) {
		return $this->redirect($this->generator->format($path), $status, $headers);
	}
	
	/**
	 * Redirects the client to a given route.
	 * 
	 * @param	string	$route
	 */
	public function route($route, array $params = [], $status = 302, array $headers = []) {
		return $this->redirect($this->generator->route($route, $params), $status, $headers);
	}
	
	/**
	 * Redirects the client to the refering page.
	 * 
	 * @param	int		$status
	 * @param	array	$headers
	 */
	public function referer($status = 302, array $headers = []) {
		$this->redirect($this->generator->previous(), $status, $headers);
	}
	
	/**
	 * Redirects the client to the previous page.
	 * 
	 * @param	int		$status
	 * @param	array	$headers
	 */
	public function back($status = 302, array $headers = []) {
		$this->referer($status, $headers);
	}
	
	/**
	 * Redirects the client to the home page.
	 * 
	 * @param	int		$status
	 * @param	array	$headers
	 */
	public function home($status = 302, array $headers = []) {
		$this->redirect($this->generator->format(URI_SEPARATOR), $status, $headers);
	}
	
	/**
	 * Sets the URL generator associated with this RedirectHandler.
	 * 
	 * @param	\Rozyn\Routing\UrlGenerator	$generator
	 */
	public function setGenerator(UrlGenerator $generator) {
		$this->generator = $generator;
	}
	
	/**
	 * Returns the URL generator associated with this RedirectHandler.
	 * 
	 * @return	\Rozyn\Routing\UrlGenerator
	 */
	public function getGenerator() {
		return $this->generator;
	}
}