<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Routing;

use Rozyn\Request\IncomingRequest;

class Route {	
	/**
	 * The format (URL) that should match this Route.
	 * 
	 * @var	string
	 */
	protected $format;
	
	/**
	 * The type of response that is returned by this Route when it is dispatched.
	 * 
	 * @var	string
	 */
	protected $type;
	
	/**
	 * Returns whether or not this Route supports caching.
	 * 
	 * @var	boolean
	 */
	protected $cache = false;
	
	/**
	 * The name of the file that should be used as a template for the response
	 * that is returned by this Route when it is dispatched.
	 * 
	 * @var	string
	 */
	protected $file;
	
	/**
	 * An array of URL parameters with their respective values that were matched
	 * by this Route. Will remain empty until the Route actually matches a 
	 * client's request.
	 * 
	 * @var	array
	 */
	protected $matches = [];
	
	/**
	 * The name of the Controller class that should be called when this route
	 * is dispatched.
	 * 
	 * @var	string
	 */
	protected $controller;
	
	/**
	 * The name of the method in the Controller class that should be called when
	 * this route is dispatched.
	 * 
	 * @var	string
	 */
	protected $method;
	
	/**
	 * An array of arguments that should be passed to the Controller method when
	 * this route is dispatched.
	 * 
	 * @var	array
	 */
	protected $args = [];
	
	/**
	 * An array of filter names that correspond to filter functions registered
	 * with a Router. The actual functions matching these filters will be
	 * executed by the Router's match() function after the Route itself has
	 * confirmed a match. This way, you can apply some last minute logic on the
	 * URL arguments that were matched by the Route's format for example.
	 * 
	 * @var	string[]
	 */
	protected $filters = [];
	
	/**
	 * An array containing all named parameters that this route accepts.
	 * 
	 * @var	string[]
	 */
	protected $params;
	
	/**
	 * Specifies whether the Route should use the secure HTTPS protocol as 
	 * opposed to the standard HTTP.
	 * 
	 * @var	boolean|null
	 */
	protected $secure;
	
	/**
	 * The language for this Route.
	 * 
	 * @var	string
	 */
	protected $lang;
	
	/**
	 * Holds the request method associated with this Route.
	 * 
	 * @var	string
	 */
	protected $requestMethod;
	
	/**
	 * Holds the name of this Route.
	 * 
	 * @var	string
	 */
	protected $name;
	
	/**
	 * Specifies whether or not this Route is CSRF protected.
	 * 
	 * @var	boolean
	 */
	protected $csrf = false;

	/**
	 * Set all the route variables. Each key should correspond to one of the
	 * class variables of this class, which will be set to the corresponding
	 * value.
	 * 
	 * @param	array	$routeData
	 */
	public function __construct(array $routeData) {
		$this->setRouteData($routeData);
	}

	/**
	 * Tries to match the route to a given request. If matches are found, an 
	 * array akin to what is created through a preg_match_all call is returned. 
	 * If no match is found, a boolean value of false will be returned instead.
	 *
	 * @param	\Rozyn\Request\IncomingRequest	$request
	 * @return	array|false
	 */
	public function match(IncomingRequest $request) {
		// Retrieve the query from the request.
		$query = $request->getQuery();
		
		// Retrieve the request method from the request.
		$method = $request->getRequestMethod();
		
		// Check if the request method matches the one associated with this 
		// Route. If no request method is associated with this Route, all
		// request methods will be accepted.
		if ($this->getRequestMethod() && $this->getRequestMethod() !== $method) {
			return false;
		}
		
		// If the Route doesn't have a format, a match is impossible.
		if (null === $this->getFormat()) {
			return false;
		}
		
		// Check if the Route only accepts secure Requests and, if so, if the
		// Request is indeed secure.
		if ($this->isSecure() && !$request->isSecure()) {
			return false;
		}
		
		$matches = [];
		
		// Convert all named parameters to named regex submatches which we can
		// retrieve later on.
		$pattern = preg_replace('/\:([a-zA-Z]+)\/?/', '(?<$1>[^\/]+?)', str_replace('/', '\/', $this->getFormat()));
		
		// If the format ends with a forward slash followed by an asterisk 
		// ("/*"), we assume that the format takes any number of (additional)
		// parameters, so we replace this asterisk by a custom named parameter
		// that will hold all additional parameter values. We can then extract
		// each individual parameter value by exploding on a forward slash (/).
		// We make sure that these additional parameters are optional though,
		// so if for example you have a route with the following format:
		//		/index/*
		// that would mean that the url "/index" would still match this route,
		// as well as the url "/index/param1/param2/", etc.
		$pattern = preg_replace('/\\\\\/\*$/', '(?:\/(?<PARAMS>.+))?', $pattern);
		
		// Add optional leading or trailing directory separators to the pattern.
		$pattern = '/^\/?' . $pattern . '\/?$/';
				
		preg_match($pattern, $query, $matches);
		
		if (!empty($matches)) {
			// Unset all the numeric matches.
			foreach (array_keys($matches) as $key) {
				if (is_int($key)) {
					unset($matches[$key]);
				}
			}
			
			// Explode any additional, unnamed parameters and add them to the
			// matches array under numeric indices.
			if (isset($matches['PARAMS'])) {
				$matches = array_merge($matches, explode(URI_SEPARATOR, $matches['PARAMS']));
				unset($matches['PARAMS']);
			}
			
			// Store the matches for future reference.
			$this->setMatches($matches);

			// If the $matches array contains entries for the "method" parameter, 
			// extract the corresponding value and use it to set our method.
			if (array_key_exists('method', $matches)) {
				$this->setMethod($matches['method']);
				unset($matches['method']);
			}

			// If the $matches array contains entries for the "controller" 
			// parameter, extract the corresponding value and use it to set our
			// Constroller.
			if (array_key_exists('controller', $matches)) {
				$this->setController($matches['controller']);
				unset($matches['controller']);
			}
			
			// Add all the remaining matches to our $args array so that they're
			// also passed along to the controller method when this Route is
			// dispatched.
			$this->addArgs($matches);

			// Return all matches
			return $this->getMatches();
		}

		return false;
	}

	/**
	 * Generates a URL that, when visited, will be routed to this route. This way, you can link to
	 * specific pages on the website based on the routes you have defined.
	 *
	 * @param	array
	 * @return	string
	 */
	public function url($params = []) {
		$named = [];
		foreach ($params as $key => $value) {
			if (!is_int($key)) {
				$named[':' . $key] = $value;
				unset($params[$key]);
			}
		}
		
		// Remove any asterisks from the URL that were left over from the
		// original route's format, where we allow asterisks as wildcards.
		return __WWW_ROOT__ . URI_SEPARATOR . str_replace('*', '', strtr($this->getFormat(), $named)) . implode(URI_SEPARATOR, $params);
	}
	
	/**
	 * Returns whether or not the Route is secure.
	 * 
	 * @return	boolean
	 */
	public function isSecure() {
		return $this->secure;
	}
	
	/**
	 * Stores any matches made by this Route.
	 * 
	 * @param	array $matches
	 */
	public function setMatches(array $matches) {
		$this->matches = $matches;
	}
	
	/**
	 * Gets all matches made by this Route.
	 * 
	 * @return	array
	 */
	public function getMatches() {
		return $this->matches;
	}
	
	/**
	 * Retrieve a single matched URL param's value.
	 * 
	 * @param	string	$param
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function getMatch($param, $default = null) {
		return (isset($this->matches[$param])) ? $this->matches[$param] : $default;
	}
	
	/**
	 * Sets a bunch of different variables for this route at once.
	 * 
	 * @param	array	$routeData
	 */
	public function setRouteData(array $routeData) {
		foreach ($routeData as $key => $value) {
			$method = 'set' . ucfirst($key);
			
			if (!method_exists($this, $method)) {
				throw new InvalidRouteException("{$value} is not recognized as a valid Route parameter (no method {$method} was found)");
			}
			
			$this->{$method}($value);
		}
	}

	/**
	 * Sets this Route's format.
	 * 
	 * @param	string $format
	 */
	public function setFormat($format) {
		$this->format = $format;
		
		// Reset the params array, so that the next time getParams() is called, 
		// the params array will be recomputed.
		$this->params = null;
	}

	/**
	 * Retrieve this Route's format.
	 * 
	 * @return	string
	 */
	public function getFormat() {
		return $this->format;
	}
	
	/**
	 * Sets the Controller method for this Route.
	 * 
	 * @param	string	$method
	 */
	public function setMethod($method) {
		$this->method = $method;
	}
	
	/**
	 * Retrieves the Controller method for this Route.
	 * 
	 * @return	string
	 */
	public function getMethod() {
		return $this->method;
	}
	
	/**
	 * Sets the name of the Controller for this Route.
	 * 
	 * @param	string	$controller
	 */
	public function setController($controller) {
		$this->controller = $controller;
	}
	
	/**
	 * Retrieves the Controller name for this Route.
	 * 
	 * @return	string
	 */
	public function getController() {
		return $this->controller;
	}
	
	/**
	 * Retrieves the Controller name for this Route.
	 * 
	 * @return	string
	 */
	public function getControllerName() {
		return $this->controller;
	}
	
	/**
	 * Sets the request method for this Route.
	 * 
	 * @param	string	$requestMethod
	 */
	public function setRequestMethod($requestMethod) {
		$this->requestMethod = $requestMethod;
	}
	
	/**
	 * Retrieves the request method for this Route.
	 * 
	 * @return	string
	 */
	public function getRequestMethod() {
		return $this->requestMethod;
	}
	
	/**
	 * Sets the file for this Route.
	 * 
	 * @param	string	$file
	 */
	public function setFile($file) {
		if (DS !== URI_SEPARATOR) {
			$file = str_replace(URI_SEPARATOR, DS, $file);
		}
		
		$this->file = $file;
	}
	
	/**
	 * Returns the file for this Route.
	 * 
	 * @return	string
	 */
	public function getFile() {
		return $this->file;
	}
	
	/**
	 * Sets the type of Response to be returned by this Route when it is 
	 * dispatched.
	 * 
	 * @param	string	$type
	 */
	public function setType($type) {
		$this->type = $type;
	}
	
	/**
	 * Returns the type of Response to be returned by this Route when it is 
	 * dispatched.
	 * 
	 * @return	string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Defines the language that should be set for this site if this Route is
	 * dispatched.
	 * 
	 * @param	string	$lang
	 */
	public function setLang($lang) {
		$this->lang = $lang;
	}
	
	/**
	 * Returns the lang of this Route.
	 * 
	 * @return	string
	 */
	public function getLang() {
		return $this->lang;
	}
	
	/**
	 * Sets the arguments to be passed to the Controller method associated with 
	 * this Route when it is dispatched.
	 * 
	 * @param	array	$args
	 */
	public function setArgs(array $args) {
		$this->args = $args;
	}
	
	/**
	 * Returns the arguments to be passed to the Controller method associated
	 * with this Route when it is dispatched.
	 * 
	 * @return	array
	 */
	public function getArgs() {
		return $this->args;
	}
	
	/**
	 * Add a number of arguments to the existing list of arguments.
	 * 
	 * @param	array	$args
	 */
	public function addArgs(array $args) {
		$this->args = array_merge($this->args, $args);
	}
	
	/**
	 * Mark this Route as secure or insecure.
	 * 
	 * @param	boolean	$secure
	 */
	public function setSecure($secure) {
		$this->secure = $secure;
	}
	
	/**
	 * Set the filters for this Route.
	 * 
	 * @param	string[]	$filters
	 */
	public function setFilters(array $filters) {
		$this->filters = $filters;
	}
	
	/**
	 * Retrieve all the filters for this Route.
	 * 
	 * @return	string[]
	 */
	public function getFilters() {
		return $this->filters;
	}
	
	/**
	 * Returns an array of all named parameters that this route accepts.
	 * 
	 * @return	array
	 */
	public function getParams() {
		if ($this->params === null) {
			$matches = [];

			preg_match_all('/:([^*\/]+)/', $this->getFormat(), $matches);

			$this->params = (isset($matches[1])) ? $matches[1] : [];
		}
		
		return $this->params;
	}
	
	/**
	 * Sets the name for this Route.
	 * 
	 * @param	string	$name
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	/**
	 * Returns the name for this Route.
	 * 
	 * @return	string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Returns whether or not this Route supports caching.
	 * 
	 * @var	boolean
	 */
	public function isCacheEnabled() {
		return $this->cache;
	}
	
	/**
	 * Returns whether or not this Route is protected from CSRF.
	 * 
	 * @var	boolean
	 */
	public function isCsrfProtected() {
		return $this->csrf;
	}
	
	/**
	 * Get a unique cache key for this Route.
	 * 
	 * @return	string
	 */
	public function getCacheKey() {
		return 'route_' . $this->getName() . '_' . serialize($this);
	}
}