<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Routing;

use Closure;
use Rozyn\Cache\Cache;
use Rozyn\Config\Config;
use Rozyn\Composition\ControllerFactory;
use Rozyn\Composition\ResponseFactory;
use Rozyn\Composition\RouteFactory;
use Rozyn\Request\IncomingRequest;
use Rozyn\Session\SessionHandler;
use Rozyn\Session\PotentialHijackException;

class Router {
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds a reference to a Cache Handler that we can use to return cached
	 * route results.
	 * 
	 * @var	\Rozyn\Cache\Cache
	 */
	protected $cache;
	
	/**
	 * An array of all routes in this Router.
	 * 
	 * @var	\Rozyn\Routing\Route[]
	 */
	protected $routes = [];
	
	/**
	 * An array of all route proxies in this Router.
	 * 
	 * @var	array[]
	 */
	protected $proxies = [];
	
	/**
	 * Holds a config object containing relevant configuration options for this
	 * Router or routes in general.
	 * 
	 * @var	\Rozyn\Config\Config
	 */
	protected $config;
	
	/**
	 * Holds a SessionHandler object that we can use in to validate CSRF tokens.
	 * 
	 * @var	\Rozyn\Session\SessionHandler
	 */
	protected $session;
	
	/**
	 * A RouteFactory that we can use to instantiate routes on the fly.
	 * 
	 * @var	\Rozyn\Composition\RouteFactory
	 */
	protected $routeFactory;
	
	/**
	 * A ControllerFactory that we can use to instantiate controllers after we
	 * match a request to a given route.
	 * 
	 * @var	\Rozyn\Composition\ControllerFactory
	 */
	protected $controllerFactory;
	
	/**
	 * A ResponseFactory that we can use to instantiate responses after we match 
	 * a request to a given route.
	 * 
	 * @var	\Rozyn\Composition\ResponseFactory
	 */
	protected $responseFactory;
	
	/**
	 * Holds default values for route that are added within the current scope.
	 * The values in this variable can be used to prefix, suffix or serve as 
	 * fallbacks for any (omitted) route variables.
	 * 
	 * @var	array
	 */
	protected $scope;
	
	/**
	 * Holds an array of filter function which can be referenced by a Route. 
	 * These filter functions, when assigned to a Route, are called after a
	 * such a Route has been matched to a Request. You can use a filter function
	 * for last minute checks, or to modify the matched variables from the URL
	 * before they are sent to the Controller method.
	 * 
	 * @var	\Closure[]
	 */
	protected $filters;
	
	/**
	 * Creates our object.
	 * 
	 * @param	\Rozyn\Config\Cache						$cache
	 * @param	\Rozyn\Config\Config					$config
	 * @param	\Rozyn\Session\SessionHandler			$session
	 * @param	\Rozyn\Composition\ControllerFactory	$controllerFactory
	 * $param	\Rozyn\Composition\ResponseFactory		$responseFactory
	 * @param	\Rozyn\Composition\RouteFactory			$routeFactory
	 */
	public function __construct(Cache $cache,
								Config $config,
								SessionHandler $session,
								ControllerFactory $controllerFactory, 
								ResponseFactory $responseFactory,
								RouteFactory $routeFactory) {
		
		$this->cache = $cache;
		$this->config = $config;
		$this->session = $session;
		$this->controllerFactory = $controllerFactory;
		$this->responseFactory = $responseFactory;
		$this->routeFactory = $routeFactory;
	}
	
	/**
	 * Reads a given input file and executes it.
	 * 
	 * @param	string	$file
	 */
	public function import($file) {
		// Declare a $router variable, which is a reference to this object. This
		// way, the included file can use $router without having to instantiate 
		// this Router object itself.
		$router = $this;
		
		// Execute the required file.
		require $file;
	}

	/**
	 * Takes an IncomingRequest as a parameter and returns the Route that 
	 * matches its URL. If no matching Route is found, a boolean value of false 
	 * is returned instead.
	 * 
	 * @param	\Rozyn\Request\IncomingRequest $request
	 * @return	\Rozyn\Routing\Route|false
	 * @throws	\Rozyn\Routing\PageNotFoundException
	 */
	public function match(IncomingRequest $request) {
		foreach ($this->getRouteNames() as $name) {
			try {
				$route = $this->getRoute($name);
				$matches = $route->match($request);
				
				// If the route was a match, we perform some additional tests to
				// make sure the we are allowed to access this route.
				if ($matches !== false) {
					// Check if the route is CSRF protected. If it is, and a
					// CSRF token mismatch is detected, an exception is thrown
					// and thus execution of the script is halted.
					if ($route->isCsrfProtected() && !$this->session->verifyCsrfToken($request->getCsrfToken())) {
						throw new PotentialHijackException("CSRF tokens did not match.");
					}
					
					// Check if the route passes all the filters. If it does not,
					// we will simply move on to the next route instead of 
					// throwing an exception, because it might be that our
					// routing depends on the use of filters to find the correct
					// route by filtering out "false positive matches".
					if ($this->filter($route) !== false) {
						return $route;
					}
				}
			} catch (InvalidRouteException $e) {
				continue;
			}
		}
		
		throw new PageNotFoundException($request->getUrl());
	}
	
	/**
	 * Dispatches a route.
	 * 
	 * @param	\Rozyn\Routing\Route|string	$routeOrName
	 * @param	array|null					$params
	 * @return	\Rozyn\Response\Response
	 */
	public function dispatch($routeOrName, array $params = null) {
		// Parse the given parameter and turn it into a valid Route object.
		$route = ($routeOrName instanceof Route) ? $routeOrName : $this->getRoute($routeOrName);
		
		// Set the site's language to the Route's language.
		site_language($route->getLang());
		
		// Create the Response object for our Controller.
		$response = $this->responseFactory->build($route->getType(), [$route->getFile()]);
		
		// Instantiate the Controller and Response objects.
		$controller = $this->controllerFactory->build($route->getController(), [$response]);
		
		// Call the Controller method associated with this route.
		$controller->__callMethod($route->getMethod(), $params ?: $route->getArgs());
		
		// Return our Response.
		return $controller->getResponse();
	}
	
	/**
	 * Adds a new route to the Router based on the given arguments.
	 * 
	 * @param	array	$routeData
	 */
	public function connect(array $routeData) {
		// Run our $routeData through our parser before doing anything else so
		// that we're sure that the data is actually correct.
		$data = $this->parseRouteData($routeData);
		
		// If we're enforcing SSL, make sure the route is labeled as secure.
		if ($this->config->read('force_ssl')) {
			$data['secure'] = true;
		}
			
		// If a name was specified for the route, add the route under that name.
		if (isset($data['name'])) {
			$this->proxies[$data['name']] = $data;
		}
		
		// If not, simply append the route to the routes array.
		else {
			$this->proxies[] = $data;
		}
	}
	
	/**
	 * Adds a new Route object to the Router that is only accessible via a GET
	 * request.
	 * 
	 * @param	array	$routeData
	 */
	public function get(array $routeData) {
		// Overwrite the request method.
		$routeData['requestMethod'] = 'GET';
		
		// Add the route.
		$this->connect($routeData);
	}
	
	/**
	 * Adds a new Route object to the Router that is only accessible via a POST
	 * request.
	 * 
	 * @param	array	$routeData
	 */
	public function post(array $routeData) {
		// Overwrite the request method.
		$routeData['requestMethod'] = 'POST';
		
		// Add the route.
		$this->connect($routeData);
	}
	
	/**
	 * Group a bunch of routes together by providing new scope data for all
	 * routes that are declared in the callback function.
	 * 
	 * @param	array		$scope
	 * @param	\Closure	$closure
	 */
	public function group(array $scope, Closure $closure) {
		// Store the current scope so that we can reset it after executing our
		// callback function.
		$currentScope = $this->scope;
		
		// Update our scope with the newly specified scope values.
		$this->updateScope($scope);
		
		// Execute our callback function using the specified scope.
		$closure($this);
		
		// Reset our scope to what it was before we altered it.
		$this->setScope($currentScope);
	}
	
	/**
	 * Copies an existing Route object so that it can be easily reused for
	 * multiple URLs.
	 *
	 * @param	string	$name
	 * @param	string	$existing
	 * @param	string	$format
	 */
	public function alias($name, $existing, $format) {
		if (null !== ($original = $this->getRoute($existing))) {
			$route = clone $original;
			
			$data = $this->parseRouteData(array(
				'name'		=> $name,
				'format'	=> $format,
			));
			
			$route->setRouteData($data);
			
			$this->routes[$data['name']] = $route;
		}
	}
	
	/**
	 * Redirects any traffic from one route to another.
	 * 
	 * @param	string	$name
	 * @param	string	$existing
	 * @param	string	$format
	 */
	public function redirect($name, $existing, $format) {
		return $this->alias($name, $existing, $format);
	}
	
	/**
	 * Run the filters that are associated with the given Route and return their
	 * result.
	 * 
	 * @param	\Rozyn\Routing\Route	$route
	 * @return	boolean
	 */
	public function filter(Route $route) {
		// Retrieve the Route's filters.
		$filters = $route->getFilters();
		
		// If the route does not have any filters, return true immediately.
		if (empty($filters)) {
			return true;
		}
		
		// Retrieve any potential matches made by this Route so that we can pass
		// them to our filter functions (possibly by reference).
		$matches = $route->getMatches();
		
		// Loop through all of the Route's filters and execute their associated
		// functions one by one.
		foreach ($filters as $filter) {
			// Extract the actual callback.
			$closure = $this->getFilter($filter);
			
			// Run the filter function and check if it returns False. If so, we
			// can immediately exit this function since as long as 1 filter
			// function returns False, the route's filters have failed.
			if ($closure($route) === false) {
				return false;
			}
		}
		
		// Overwrite the existing matches with the potentially modified matches
		// that were passed to each filter function.
		$route->setMatches($matches);
		
		// Since all filter functions were passed successfully, return true.
		return true;
	}
	
	/**
	 * Add a number of filters to this Router.
	 * 
	 * @param	\Closure[]	$filters
	 */
	public function addFilters(array $filters) {
		$this->filters = array_merge($this->filters, $filters);
	}
	
	/**
	 * Add a filter to this Router.
	 * 
	 * @param	string		$name
	 * @param	\Closure	$filter
	 */
	public function addFilter($name, Closure $filter) {
		$this->filters[$name] = $filter;
	}
	
	/**
	 * Returns a specific filter function.
	 * 
	 * @param	string	$name
	 * @return	\Closure
	 * @throws	\Rozyn\Routing\UnknownFilterException
	 */
	public function getFilter($name) {
		if (!isset($this->filters[$name])) {
			throw new UnknownFilterException('No filter was found with name ' . $name);
		}
		
		return $this->filters[$name];
	}
	
	/**
	 * Set the current scope variable.
	 * 
	 * @param	array	$scope
	 */
	protected function setScope(array $scope = null) {
		$this->scope = $scope;
	}
	
	/**
	 * Update the current scope with new values.
	 * 
	 * @param	array	$scope
	 */
	protected function updateScope(array $scope) {
		// Treat the scope array as it if was route data and run it through our
		// route data parser. This will return new route data that we can use
		// as our next scope.
		$this->scope = $this->parseRouteData($scope);
	}
	
	/**
	 * Returns the entire scope array, or a single scope entry if $key is
	 * provided.
	 * 
	 * @param	string	$key
	 * @return	mixed
	 */
	protected function getScope($key = null) {
		return ($key === null) ? $this->scope : $this->scope[$key];
	}
	
	/**
	 * Parses raw route data by running it against the current scope, altering
	 * the data where necessary.
	 * 
	 * @param	string	$routeData
	 * @return	array
	 */
	protected function parseRouteData(array $routeData) {
		// If no scope has been defined yet, return the route data as is.
		if (null === $this->scope) {
			return $routeData;
		}
		
		// Parse the format.
		if (isset($routeData['format'])) {
			if (isset($this->scope['format']) && !empty($this->scope['format'])) {
				$routeData['format'] = $this->scope['format'] . URI_SEPARATOR . ltrim($routeData['format'], URI_SEPARATOR);
			}
			
			$routeData['format'] = trim($routeData['format'],URI_SEPARATOR);
		}
		
		// Parse the name.
		if (isset($routeData['name']) && isset($this->scope['name'])) {
			$routeData['name'] = $this->scope['name'] . $routeData['name'];
		}
				
		// Define the properties that hold arrays that need to be merged with
		// their respective existing scope properties.
		foreach (['args', 'filters'] as $property) {
			if (isset($routeData[$property]) && isset($this->scope[$property])) {
				$routeData[$property] = array_merge($this->scope[$property], $routeData[$property]);
			}
		}
		
		// Merge the new route variables with our scope variable, overwriting
		// any old values. Only do this after we've made sure that any values
		// that need to prefixed or suffixed have been parsed, otherwise they
		// too will be overwritten instead of prefixed or suffixed, repsectively.
		return array_merge($this->scope, $routeData);
	}
	
	/**
	 * Returns whether or not a route with the given name exists.
	 * 
	 * @param	string	$name
	 * @return	boolean
	 */
	public function hasRoute($name) {
		return isset($this->routes[$name]) || isset($this->proxies[$name]);
	}
	
	/**
	 * Sets our initial routes array.
	 * 
	 * @param	array	$routes
	 */
	public function setRoutes(array $routes = []) {
		$this->routes = $routes;
	}
	
	/**
	 * Adds a set of routes to this router.
	 * 
	 * @param	\Rozyn\Routing\Route[]	$routes
	 */
	public function addRoutes(array $routes = []) {
		$this->routes = array_merge($this->routes, $routes);
	}
	
	/**
	 * Returns all routes stored inside this Router.
	 * 
	 * @return	\Rozyn\Routing\Route[]
	 */
	public function getRoutes() {
		foreach (array_keys($this->proxies) as $name) {
			$this->initRoute($name);
		}
		
		return $this->routes;
	}
	
	/**
	 * Returns the names of all registered routes without instantiating any
	 * proxies.
	 * 
	 * @return	string[]
	 */
	public function getRouteNames() {
		return array_merge(array_keys($this->routes), array_keys($this->proxies));
	}
	
	/**
	 * Returns the Route matching the given name.
	 * 
	 * @return	\Rozyn\Routing\Route
	 */
	public function getRoute($name) {
		if (isset($this->routes[$name])) {
			return $this->routes[$name];
		}
		
		if (isset($this->proxies[$name])) {
			return $this->initRoute($name);
		}
		
		return null;
	}
	
	/**
	 * Initialize a route proxy.
	 * 
	 * @param	string	$name
	 * @return	\Rozyn\Routing\Route
	 */
	protected function initRoute($name) {
		$this->routes[$name] = $this->routeFactory->build($this->proxies[$name]);
		
		unset($this->proxies[$name]);
		
		return $this->routes[$name];
	}
}