<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\I18n;

use Rozyn\Data\Collection;

class Translator {
	/**
	 * The locale being used by this Translator.
	 * 
	 * @var	\Rozyn\I18n\Locale
	 */
	protected $locale;
	
	/**
	 * Holds all the translations for this Translator.
	 * 
	 * @var	array
	 */
	protected $translations = [];
	
	/**
	 * Instantiates the object.
	 * 
	 * @param	\Rozyn\I18n\Locale	$locale
	 * @param	array				$translations
	 */
	public function __construct(Locale $locale, array $translations = []) {
		$this->setLocale($locale);
		$this->addTranslations($translations);
	}
	
	/**
	 * Set the locale for this Translator.
	 * 
	 * @param	\Rozyn\I18n\Locale	$locale
	 */
	public function setLocale(Locale $locale) {
		$this->locale = $locale;
	}
	
	/**
	 * Returns the locale for this translator.
	 * 
	 * @return	\Rozyn\I18n\Locale
	 */
	public function getLocale() {
		return $this->locale;
	}
	
	/**
	 * Returns the language code associated with this Translator.
	 * 
	 * @return	string
	 */
	public function getLanguage() {
		return $this->locale->getLanguage();
	}
	
	/**
	 * Checks whether or not a certain key has a translation.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function hasTranslation($key) {
		return isset($this->translations[$key]);
	}
	
	/**
	 * Adds a single translation to the Translator.
	 * 
	 * @param	string	$key
	 * @param	string	$translation
	 */
	public function addTranslation($key, $translation) {
		$this->translations[$key] = $translation;
	}
	
	/**
	 * Adds multiple translations to the Translator.
	 * 
	 * @param	array $translations
	 */
	public function addTranslations(array $translations = []) {
		$this->translations = array_merge($this->translations, $translations);
	}
	
	/**
	 * Returns a translation for a key without inserting any of its parameters.
	 * If no translation matching the given key is found, null is returned.
	 * 
	 * @param	string	$key
	 * @return	string | null
	 */
	public function getTranslation($key) {
		return ($this->hasTranslation($key)) ? $this->translations[$key] : null;
	}
	
	/**
	 * Returns all translations registered under this Translator.
	 * 
	 * @return	array
	 */
	public function getTranslations() {
		return $this->translations;
	}
	
	/**
	 * Returns the translation for a given key. The optional second argument
	 * is an array of variables that have to be inserted into the translation.
	 * If no translation is found, the original key is returned instead.
	 * 
	 * @param	string	$key
	 * @param	array	$params
	 * @return	string
	 */
	public function translate($key, array $params = []) {
		if ($this->hasTranslation($key)) {
			return str_format($this->translations[$key], $params);
		}
		
		return str_format($key, $params);
	}
}