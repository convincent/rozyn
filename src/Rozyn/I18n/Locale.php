<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\I18n;

class Locale {
	/**
	 * Holds the language code for this Locale.
	 * 
	 * @var	string
	 */
	protected $language;
	
	/**
	 * Holds the region code for this Locale.
	 * 
	 * @var	string
	 */
	protected $region;
	
	/**
	 * Holds any variant information for this Locale.
	 * 
	 * @var	string
	 */
	protected $variant;
	
	/**
	 * Instantiate the object. If the language argument contains underscores, 
	 * that argument is treated as an entire locale string, meaning it's run 
	 * through our parse() method. In all other cases, the arguments are simply
	 * set directly using their respective set() methods. 
	 * 
	 * @param	string	$language
	 * @param	string	$region
	 * @param	string	$variant
	 */
	public function __construct($language = null, $region = null, $variant = null) {
		if (strpos($language, '_') !== false) {
			$this->parse($language);
		} 
		
		else {
			$this->setLanguage($language);
			$this->setRegion($region);
			$this->setVariant($variant);
		}
	}
	
	/**
	 * Takes a single string as an argument and extracts all relevant Locale
	 * data from it. The string should be in the following format:
	 *		<lang>_<region>_<variant>
	 * 
	 * @param	string $localeString
	 */
	public function parse($localeString) {
		$components = array_pad(explode('_', $localeString), 3, null);
		
		$this->setLanguage($components[0]);
		$this->setRegion($components[1]);
		$this->setVariant(implode(' ', array_slice($components, 2)));
	}
	
	/**
	 * Formats the Locale and returns it as a string.
	 * 
	 * @return	string
	 */
	public function format() {
		$components = [$this->getLanguage()];
		
		if ($this->hasRegion()) {
			$components[] = $this->getRegion();
		
			if ($this->hasVariant()) {
				$components[] = $this->getVariant();
			}
		}
		
		return implode('_', $components);
	}
	
	/**
	 * Sets the language code of this Locale. If no language is provided, the
	 * site's default language is used.
	 * 
	 * @param	string $language
	 */
	public function setLanguage($language = null) {
		$this->language = strtolower($language ?: site_language());
	}
	
	/**
	 * Sets the region for this Locale.
	 * 
	 * @param	string $region
	 */
	public function setRegion($region) {
		$this->region = strtoupper($region);
	}
	
	/**
	 * Sets the variant information for this Locale.
	 * 
	 * @param	string $variant
	 */
	public function setVariant($variant) {
		$this->variant = snake2camel($variant, ' ');
	}
	
	/**
	 * Returns the language code for this Locale.
	 * 
	 * @return	string
	 */
	public function getLanguage() {
		return $this->language;
	}
	
	/**
	 * Returns the region for this Locale.
	 * 
	 * @return	string
	 */
	public function getRegion() {
		return $this->region;
	}
	
	/**
	 * Returns the variant information for this Locale.
	 * 
	 * @return	string
	 */
	public function getVariant() {
		return $this->variant;
	}
	
	/**
	 * Check whether this Locale has a language associated with it.
	 * 
	 * @return	boolean
	 */
	public function hasLanguage() {
		return !!$this->language;
	}
	
	/**
	 * Check whether this Locale has a region associated with it.
	 * 
	 * @return	boolean
	 */
	public function hasRegion() {
		return !!$this->region;
	}
	
	/**
	 * Check whether this Locale has variant information associated with it.
	 * 
	 * @return	boolean
	 */
	public function hasVariant() {
		return !!$this->variant;
	}
	
	/**
	 * Returns a string representation of this Locale.
	 * 
	 * @return	string
	 */
	public function __toString() {
		return $this->format();
	}
}