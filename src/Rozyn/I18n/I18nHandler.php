<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\I18n;

use Rozyn\Filesystem\File;
use Rozyn\Filesystem\FileNotFoundException;
use Rozyn\Filesystem\DirectoryNotFoundException;
use Rozyn\Cache\Cache;

class I18nHandler {
	/**
	 * Identifies the substring used to separate different locale aspects from
	 * each other (country, region, dialect, etc). So for example, if set to _,
	 * a proper locale string would be 'en_US'
	 */
	const LOCALE_SEPARATOR = '_';
	
	/**
	 * Mark the class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds the cache for this handler.
	 * 
	 * @var	\Rozyn\Cache\Cache
	 */
	protected $cache;
	
	/**
	 * An array of all translations loaded for the App.
	 * 
	 * @var	string[]
	 */
	protected $translations = [];
	
	/**
	 * Constructs the object.
	 * 
	 * @param	\Rozyn\Cache\Cache	$cache
	 */
	public function __construct(Cache $cache) {
		$this->cache = $cache;
	}
	
	/**
	 * Checks whether a Translator is registered under the given Locale. If so,
	 * use that Translator to translate the key. An optional third argument
	 * containing parameters that should be inserted in the Translation can also
	 * be passed along. If no Locale is specified, the site's default language
	 * is tried instead.
	 * 
	 * @param	string	$key
	 * @param	string	$locale
	 * @param	array	$params
	 * @return	string
	 */
	public function translate($key, $locale, array $params = []) {
		return (isset($this->translations[$locale][$key])) ?
					str_format($this->translations[$locale][$key], $params) :
					$key;
	}
	
	/**
	 * Adds an array of translations to the Translator matching the given 
	 * Locale. If no such Translator is found, it will be created.
	 * 
	 * @param	array						$translations
	 * @param	\Rozyn\I18n\Locale | string	$locale
	 */
	public function addTranslations(array $translations, $locale) {
		if (!isset($this->translations[$locale])) {
			$this->translations[$locale] = [];
		}
		
		$this->translations[$locale] = array_merge($this->translations[$locale], $translations);
	}
	
	/**
	 * Returns all translations stored in this object.
	 * 
	 * @return	string[]
	 */
	public function getTranslations() {
		return $this->translations;
	}
	
	/**
	 * A generic import() method that passes the arguments to either the
	 * importFile() method or the importDirectory() method, depending on 
	 * whether the provided path leads to a file or a directory.
	 * 
	 * @param	string	$path
	 * @param	string	$locale
	 * @return	string[]
	 */
	public function import($path, $locale = null) {
		// Check if the path leads to a directory.
		if (is_dir($path)) {
			// If so, import the entire directory.
			return $this->importDirectory($path, $locale);
		}
		
		// If the path does not lead to a directory, it must lead to a file 
		// instead, so try to import it.
		$this->importFile($path, $locale);
	}
	
	/**
	 * Reads a directory and imports all files contained therein recursively.
	 * Any subdirectories are assumed to be part of the locale under which
	 * each translation is added. The $locale argument is the base locale string
	 * under which any translations will be added; it will be expanded with each
	 * subdirectory.
	 * 
	 * @param	string	$path
	 * @param	string	$locale
	 * @return	string[]
	 */
	public function importDirectory($path, $locale = null) {
		// First, check if we have cached the result.
		if ($this->cache->has($key = $this->formatCacheKey($path)) && !$this->cache->outdated($key, dirmtime($path))) {
			// If so, retrieve it from the cache.
			$res = $this->cache->read($key);
			
			// Loop through all the cached translations and add them one by one
			// per locale.
			foreach ($res as $locale => $translations) {
				$this->addTranslations($translations, $locale);
			}
			
			// Return the cache entry.
			return $res;
		}
		
		// If the specified path is not a directory, throw an exception.
		if (!is_dir($path)) {
			throw new DirectoryNotFoundException('Could not import translations from directory ' . $path . ' because the directory could not be found.');
		}
		
		// Initialize the array that's going to be holding all translations.
		$translations = [];
		
		// If a locale was specified, create an entry for that locale in the
		// translations array.
		if ($locale !== null) {
			$translations[$locale] = [];
		}
		
		// Loop through the specified directory and recursively call this method
		// on any subdirectories. All files we encounter will be imported and
		// read into our translations library.
		dir_map($path, function($file, $dir, $path) use ($locale, &$translations) {
			if (is_dir($path)) {
				$translations = array_merge($translations, $this->importDirectory($path, ($locale === null) ? $file : $locale . static::LOCALE_SEPARATOR . $file));
			} 
			
			elseif (is_file($path) && $locale !== null) {
				$translations[$locale] = array_merge($translations[$locale], $this->importFile($path, $locale));
			}
		}, false);
		
		// After we're done scanning the entire directory, cache our result.
		$this->cache->write($key, $translations);
		
		// Return it.
		return $translations;
	}
	
	/**
	 * Reads a file and adds all the translations defined therein to the 
	 * Translator matching the given Locale. If no such Translator is found, it 
	 * will be created.
	 * 
	 * @param	string	$path
	 * @param	string	$locale
	 * @return	string[]
	 */
	public function importFile($path, $locale) {
		// First, check if we've cached this file's translations.
		if ($this->cache->has($key = $this->formatCacheKey($path)) && !$this->cache->outdated($key, filemtime($path))) {
			// If so, read them from the cache...
			$translations = $this->cache->read($key);
			
			// ..., add them to our library...
			$this->addTranslations($translations, $locale);
			
			// ..., and return them!
			return $translations;
		}
		
		// If the file could not be found, throw an Exception.
		if (!is_file($path)) {
			throw new FileNotFoundException('Could not import translations from file ' . $path . ' because the file could not be found.');
		}
		
		// Parse the file based on the file extension.
		$ext = (new File($path))->ext();
		switch ($ext) {
			case 'json':
				$translations = $this->parseJsonFile($path);
				break;
			
			case 'txt':
				$translations = $this->parseTxtFile($path);
				break;
			
			default:
				$translations = $this->parsePhpFile($path);
				break;
		}
		
		// Add the translations under the right locale.
		$this->addTranslations($translations, $locale);
		
		// Also write them to our cache.
		$this->cache->write($key, $translations);
		
		// And finally, return them.
		return $translations;
	}
	
	/**
	 * Takes a PHP language file path as input and returns an array of all 
	 * translations it contains.
	 * 
	 * @param	string	$path
	 * @return	string[]
	 */
	protected function parsePhpFile($path) {
		$translations = include $path;
		
		return (is_array($translations)) ? $translations : [];
	}
	
	/**
	 * Takes a TXT language file path as input and returns an array of all 
	 * translations it contains.
	 * 
	 * @param	string	$path
	 * @return	string[]
	 */
	protected function parseTxtFile($path) {
		$translations = [];
		
		$handle = fopen($path, 'r');
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				$pos = strpos($line, ' ');
				$translations[substr($line, 0, $pos)] = trim(substr($line, $pos));
			}

			fclose($handle);
		} else {
			throw new I18nException("Could not read .txt language file.");
		}
		
		return $translations;
	}
	
	/**
	 * Takes a JSON language file path as input and returns an array of all 
	 * translations it contains.
	 * 
	 * @param	string	$path
	 * @return	string[]
	 */
	protected function parseJsonFile($path) {
		return json_decode(file_get_contents($path));
	}
	
	/**
	 * Takes a path to a file or directory and converts it to the cache key that
	 * should be used to cache the translations found at that location.
	 * 
	 * @param	string	$path
	 * @return	string
	 */
	protected function formatCacheKey($path) {
		return prefix($path, 'i18n.import.');
	}
}

