<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Helper;

use Closure;

class HtmlHelper extends Helper {
	/**
	 * Mark our class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton  =  true;
	
	/**
	 * Holds an array of all tags that are self-closed. For performance issues,
	 * we also use keys for this purpose, so that we can simply call isset()
	 * to determine if a certain tag is self-closed instead of having to use
	 * in_array(), which is slower.
	 * 
	 * @var	string[]
	 */
	protected $selfClosed = array(
		'area'		=> 'area',
		'base'		=> 'base',
		'br'		=> 'br',
		'col'		=> 'col',
		'embed'		=> 'embed',
		'hr'		=> 'hr',
		'img'		=> 'img',
		'input'		=> 'input',
		'keygen'	=> 'keygen',
		'link'		=> 'link',
		'menuitem'	=> 'menuitem',
		'meta'		=> 'meta',
		'param'		=> 'param',
		'source'	=> 'source',
		'track'		=> 'track',
		'wbr'		=> 'wbr',
	);
	
	/**
	 * Returns a valid HTML tag.
	 * 
	 * @param	string					$tag
	 * @param	\Closure|string|array	$contentOrAttributes
	 * @param	array					$attributes
	 * @return	string
	 */
	public function tag($tag, $contentOrAttributes = null, array $attributes = []) {
		// Initialize the string that will hold all the HTML attributes.
		$attr = '';
		
		// Check if the tag we want to create is self-closed.
		$selfClosed = isset($this->selfClosed[$tag]);
		
		// If the tag is not self-closed, compute the actual content of the tag.
		if ($selfClosed === false) {
			$content = ($contentOrAttributes instanceof Closure) ?
							$contentOrAttributes($this) :
							$contentOrAttributes;
		}
		
		// If the tag we want to create is self-closed, there is no need for the
		// $content variable. Instead, treat the second parameter as the 
		// $arguments to be used for this tag.
		elseif (func_num_args() < 3) {
			$attributes = $contentOrAttributes ?: [];
		}
		
		// Parse the attributes for this tag one by one.
		foreach ($attributes as $attribute => $value) {
			// If no property was specified (ie: it's numeric), ignore it.
			$attr .= ' ' . $this->formatAttributeAndValue($attribute, $value);
		}
		
		return ($selfClosed) ? 
					"<{$tag}{$attr}/>" : 
					"<{$tag}{$attr}>{$content}</{$tag}>";
	}
	
	/**
	 * Returns a valid HTML <a> tag.
	 * 
	 * @param	string	$label
	 * @param	string	$dest
	 * @param	array	$options
	 * @return	string
	 */
	public function a($label, $dest = '#', array $options = []) {
		return $this->tag('a', $label, array_merge(['href' => $dest], $options));
	}
	
	/**
	 * Returns a valid HTML <script> tag.
	 * 
	 * @param	string	$file
	 * @param	array	$options
	 * @return	string
	 */
	public function js($file, $options = []) {
		return $this->tag('script', '', array_merge(['type' => 'text/javascript', 'src' => $file], $options));	
	}
	
	/**
	 * Returns a valid HTML <link /> tag.
	 * 
	 * @param	string	$file
	 * @param	array	$options
	 * @return	string
	 */
	public function css($file, $options = []) {
		return $this->tag('link', '', array_merge(['type' => 'text/css', 'rel' => 'stylesheet', 'href' => $file], $options));	
	}
	
	/**
	 * Returns a valid HTML <img /> tag.
	 * 
	 * @param	string	$file
	 * @param	array	$options
	 * @return	string
	 */
	public function img($file, $options = []) {
		$defaults = ['alt' => '', 'src' => $file];
		
		try {
			$info = getimagesize(prefix($defaults['src'], __DOC_ROOT__ . DS));
			
			$defaults['width']  = $info[0];
			$defaults['height'] = $info[1];
		} catch (\Exception $e) {}
		
		return $this->tag('img', '', array_merge($defaults, $options));	
	}
	
	/**
	 * If an unrecognized method is called, assume that the method name is 
	 * actually the tag name that should be created, so we pass along these
	 * method calls to our internal tag() method.
	 * 
	 * @param	string	$name
	 * @param	array	$args
	 * @return	string
	 */
	public function __call($name, $args) {
		return call_user_func_array([$this, 'tag'], array_merge([$name], $args));
	}
	
	/**
	 * Takes an attribute and its corresponding value as arguments and formats
	 * them into a valid HTML attribute/value pair.
	 * 
	 * @param	string	$attribute
	 * @param	mixed	$value
	 * @return	string
	 */
	protected function formatAttributeAndValue($attribute, $value) {
		// If no value is specified or if it is set to false, ignore it.
		if ($value === null || $value === false) {
			return '';
		}

		// If the value is set to true, use the property name for the value.
		// For example: disabled="disabled"
		elseif ($value === true) {
			$value = $attribute;
		}

		// Handle all different array cases.
		elseif (is_array($value)) {
			switch ($attribute) {
				// Handle data attributes
				case 'data':
					$res = '';
					foreach ($value as $dataAttr => $dataVal) {
						$res .= ' data-' . $dataAttr . '="' . $dataVal . '"';
					}

					// Since we've already formatted the result string exactly
					// the way it's supposed to, we can return it immediately.
					return $res;
					
				// Handle the style (or our custom css) property differently to 
				// allow for flexible inputs.
				case 'css':
				case 'style':
					$styleStr = '';

					// Whatever property name was used, make sure it's reset to the 
					// correct "style" property.
					$attribute = 'style';

					// Loop through all the elements in our style array.
					foreach ($value as $styleAttr => $styleVal) {					
						// Allow for camelcased attribute names.
						$styleStr .= camel2snake($styleAttr, '-') . ':' . $styleVal . ';';
					}

					// Set the actual, finalized value of the "style" attribute to 
					// the result of our parsing logic.
					$value = $styleStr;
					break;

				// If after all our preprocessing, the attribute value is still an 
				// array, implode it using a space as the glue.
				default:
					$value = implode(' ', $value);
					break;
			}
		}
		
		// Return the fully formatted string. If the attribute is an integer,
		// we treat the value as an attribute that requires no specified value.
		return ($attribute === (int)$attribute) ? 
					$value : 
					$attribute . '="' . $value . '"';
	}
}