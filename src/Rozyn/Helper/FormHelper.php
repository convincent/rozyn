<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Helper;

use Rozyn\Request\IncomingRequest;
use Rozyn\Session\FlashHandler;
use Rozyn\Session\SessionHandler;

class FormHelper extends Helper {
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton  =  true;
	
	/**
	 * The HTML Helper that will be used to construct our form tags.
	 * 
	 * @var	\Rozyn\Helper\HtmlHelper
	 */
	protected $html;
	
	/**
	 * Holds a FlashHandler object that is used to check for error messages.
	 * 
	 * @var	\Rozyn\Session\FlashHandler
	 */
	protected $flash;
	
	/**
	 * Holds a SessionHandler object that is used to generate CSRF input fields.
	 * 
	 * @var	\Rozyn\Session\SessionHandler
	 */
	protected $session;
	
	/**
	 * Constructs our Form Helper.
	 * 
	 * @param	\Rozyn\Helper\HtmlHelper		$html
	 * @param	\Rozyn\Session\FlashHandler		$flash
	 * @param	\Rozyn\Session\SessionHandler	$session
	 */
	public function __construct(HtmlHelper $html, FlashHandler $flash, SessionHandler $session) {
		$this->html = $html;
		$this->flash = $flash;
		$this->session = $session;
	}
	
	/**
	 * Renders a generic <input> tag.
	 * 
	 * @param	string	$type
	 * @param	string	$name
	 * @param	mixed	$value
	 * @param	array	$attributes
	 * @return	string
	 */
	public function input($type, $name, $value = null, array $attributes = []) {
		return $this->html->tag('input', array_merge(array(
			'value' => htmlentities($value),
			'type'	=> $type,
			'name'	=> $name,
			'id'	=> $name
		), $attributes));
	}
	
	/**
	 * Returns an HTML <select> tag containing the specified $options. Every
	 * key in the $options array should represent the value attribute of that 
	 * option and every value should represent the label used for that option.
	 * 
	 * @param	string	$name
	 * @param	array	$options
	 * @param	string	$selected
	 * @param	array	$attributes
	 * @return	string
	 */
	public function select($name, array $options = [], $selected = null, array $attributes = []) {
		$optionTags = [];
		
		if (isset($attributes['empty'])) {
			list($value, $label) = (is_array($attributes['empty'])) ?
										[key($attributes['empty']), current($attributes['empty'])] :
										['', $attributes['empty']];
			
			$options = [$value => $label] + $options;
			
			unset($attributes['empty']);
		}
		
		foreach ($options as $value => $label) {
			$optionTags[] = $this->html->tag('option', $label, ['value' => $value, 'selected' => $selected == $value || $selected == $label]);
		}
		
		return $this->html->tag('select', implode(PHP_EOL, $optionTags), array_merge(array(
			'id'	=> $name,
			'name'	=> $name,
		), $attributes));
	}
	
	/**
	 * Returns an HTML <input> tag with its type set to "checkbox".
	 *
	 * @param	string	$name
	 * @param	mixed	$value
	 * @param	boolean	$checked
	 * @param	array	$attributes
	 * @return	string
	 */
	public function checkbox($name, $value = null, $checked = false, array $attributes = []) {
		return $this->input('checkbox', $name, $value, array_merge($attributes, ['checked' => $checked]));
	}
	
	/**
	 * Renders an HTML <textarea> tag.
	 * 
	 * @param	string	$name
	 * @param	mixed	$value
	 * @param	array	$attributes
	 * @return	string
	 */
	public function textarea($name, $value = null, array $attributes = []) {
		return $this->html->tag('textarea', $value, array_merge(array(
			'id' => $name,
			'name' => $name,
		), $attributes));
	}
	
	/**
	 * Returns an HTML <input> tag with its type set to "file".
	 * 
	 * @param	string	$name
	 * @param	mixed	$value
	 * @param	array	$attributes
	 * @return	string
	 */
	public function file($name, array $attributes = []) {
		return $this->input('file', $name, null, $attributes);
	}
	
	/**
	 * Returns proper HTML code for an entire form block, which consists of a
	 * wrapper <div> containing a <label> element and the specified form element
	 * which can generally be an <input>, <select> or <textarea> tag.
	 * 
	 * The first argument specifies what kind of form element should be rendered.
	 * The second argument holds the name and id of that form element.
	 * The third argument holds the label text for this form element.
	 * 
	 * Any further arguments that are passed to this method are passed along to
	 * the method that is responsible for rendering the form element.
	 * 
	 * @param	string	$type
	 * @param	string	$name
	 * @param	string	$label
	 * @param	mixed	...
	 * @return	string
	 */
	public function block($type, $name, $label = null /*, ... */) {
		// Create our <label> tag if a label text was specified.
		if ($label !== null) {
			$label = $this->html->tag('label', $label, ['for' => $name]);
		}
		
		// Create our form element tag.
		$element = ((func_num_args() <= 3) ?
						$this->{$type}($name) :
						call_user_func_array([$this, $type], array_merge([$name], array_slice(func_get_args(), 3))));
		
		// Check if any error messages exist for this field.
		$error = ($this->flash->has("error.{$name}")) ? $this->flash->error("error.{$name}") : '';
							
		// Concatenate the label and element tags based on which kind of form
		// element should be rendered. In case of a checkbox or a radio element,
		// the label tag should come after the form element tag.
		$html = ($type === 'checkbox' || $type === 'radio') ?
					$element . $error . $label :
					$label	 . $error . $element;
		
		return $this->html->tag('div', $html, ['class' => 'form-block']);
	}
	
	/**
	 * Returns an HTML string representing a hidden input containing the 
	 * session's CSRF token value and makes sure the input has the correct name.
	 * 
	 * @return	string
	 */
	public function csrf() {
		return $this->input('hidden', IncomingRequest::CSRF_INPUT_NAME, $this->session->getCsrfToken(), ['id' => IncomingRequest::CSRF_INPUT_NAME]);
	}
	
	/**
	 * If any submethod is called that is not defined, it is assumed that the
	 * desired result is an <input> tag whose type matches the called submethod.
	 * 
	 * @param	string	$type
	 * @param	array	$args
	 * @return	string
	 */
	public function __call($type, $args) {		
		return call_user_func_array([$this, 'input'], array_merge([$type], $args));
	}
}