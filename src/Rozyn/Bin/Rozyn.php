<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Bin;

use Rozyn\Facade\Cache;
use Rozyn\App;
use Rozyn\Filesystem\File;
use Rozyn\Filesystem\FileIterator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Rozyn\Security\Hash;
use Rozyn\Security\Randomizer;
use Leafo\ScssPhp\Compiler as ScssCompiler;
use Exception;

class Rozyn {
	/**
	 * Mark this class as a singleton.
	 * 
	 * @var	boolean
	 */
	protected $singleton = true;
	
	/**
	 * Holds an App instance.
	 * 
	 * @var	\Rozyn\App
	 */
	protected $app;
	
	/**
	 * Holds a Hash instance.
	 * 
	 * @var	\Rozyn\Security\Hash
	 */
	protected $hash;
	
	/**
	 * Holds a Randomizer instance.
	 * 
	 * @var	\Rozyn\Security\Randomizer
	 */
	protected $random;
	
	/**
	 * Constructs our object.
	 * @param	\Rozyn\App					$app
	 * @param	\Rozyn\Security\Hash		$hash
	 * @param	\Rozyn\Security\Randomizer	$random
	 */
	public function __construct(App $app, Hash $hash, Randomizer $random) {
		$this->app		= $app;
		$this->hash		= $hash;
		$this->random	= $random;
	}
	
	/**
	 * Hashes a string using this project's hashing algorithm.
	 * 
	 * @param	string	$str
	 * @return	string
	 */
	public function hash($str) {
		return $this->write($this->hash->hash($str));
	}
	
	/**
	 * Execute all integration tests for this project.
	 * 
	 * @param	string	$filter
	 * @return	int
	 */
	public function runIntegrationTests() {
		
	}
	
	/**
	 * Execute all tests for this project.
	 * 
	 * @param	string	$filter
	 * @return	int
	 */
	public function test($filter = null) {
		// Notify the user which project we're going to test.
		$this->write('Executing tests for project ' . $this->getProjectName());

		// Run the tests
		$command = 'cd ' . __SITES__ . DS . $this->getProjectName() . ' && vendor/bin/phpunit';
		if ($filter !== null) {
			$command .= ' --filter "' . addslashes($filter) . '"';
		}
		
		return $this->exec($command);
	}
	
	/**
	 * Creates a model file. The $fields argument can be used to specify the
	 * definition of the table that is to be associated with this model. Each 
	 * separate column name should be separated by a comma. You can use pipe
	 * characters to specify the type of a column.
	 * 
	 * Example: id|int,is_deleted|bool
	 * 
	 * @param	string	$model
	 * @param	string	$fields
	 */
	public function makeModel($model, $fields = null) {
		// Make sure our model directory is writeable.
		if (!is_writeable(__MODELS__)) {
			$this->writeError(__MODELS__ . ' directory is not writeable.');
		}

		// Make sure our model does not exist yet.
		$path = __MODELS__ . DS . $model . '.php';
		if (file_exists($path)) {
			$this->writeError("Model {$model} already exists.");
		}

		// Populate the $fields array if it was specified, otherwise make it 
		// empty.
		if ($fields !== null) {
			$fields = explode(',', $fields);

			$str = "\n";

			foreach ($fields as $field) {
				if (false !== strpos($field, ':')) {
					list($field, $type) = explode(':', $field);

					$str .= "\t\t'{$field}' => '{$this->formatSqlDataType($type)}',\n";
				}

				else {
					$str .= "\t\t'{$field}',\n";
				}
			}

			$fields = $str . "\t";
		}

		// Update our content variable with the computed fields.
		$content = str_format(file_get_contents(__BIN_TEMPLATES__ . DS . 'model.txt'), array(
								'model' => $model,
								'fields' => $fields));

		// Write to the file.
		file_put_contents($path, $content);

		// If no errors occured, output a success message.
		$this->writeSuccess("Model {$model} was successfully created.");
	}

	/**
	 * Runs a migration file.
	 * 
	 * @param	string			$path
	 * @param	\Monolog\Logger	$log
	 * @param	boolean			$suppressErrors
	 * @return	boolean
	 */
	public function runMigration($path, Logger $log = null, $suppressErrors = false) {
		// Create a log channel if none was provided.
		if ($log === null) {
			$log = new Logger('migrations');
			$log->pushHandler(new StreamHandler(__LOGS__ . DS . 'migrations.log'));
		}
		
		// Make sure the finished directory exists.
		$finishedDir = __MIGRATIONS__ . DS . FINISHED_DIR;
		if (!is_dir($finishedDir)) {
			mkdir($finishedDir);
		}
		
		$path = suffix($path, '.php');
		
		// If the path is relative (doesn't start with a /), prepend the migrations path.
		if (!is_absolute_path($path, DS)) {
			$path = DS . MIGRATIONS_DIR . DS . $path;
		}
		
		// Make sure the path is absolute.
		$path = merge_paths(__ROOT__, $path);
		
		// Use the path to create a file object.
		$file = new File($path);
		
		// Construct the path to the finished migrations directory.
		$finished = $finishedDir . DS . $file->name();

		try {
			// Make sure the migration file exists.
			if (!$file->exists()) {
				throw new MigrationException("Migration {$file->name()} does not exist.");
			}

			// If the migration was already rolled out earlier, ignore it.
			if (file_exists($finished)) {
				throw new MigrationException("Migration {$file->name()} has already been rolled out previously.");
			}

			// Migration passed all our checks, so roll it out.
			switch ($file->ext()) {
				case 'php':
					// Generate the class name of the migration object we want 
					// to run.
					$className = preg_replace('/\.php$/', '', $file->name());

					// Lock the file.
					$this->lockMigrationFile($file);

					// Include the PHP file's content.
					if (!class_exists($className)) {
						include $file->path();
					}

					// If we can't find the class after importing it, throw an exception.
					if (!class_exists($className)) {
						throw new MigrationException("Class {$className} not found.");
					}
					
					// Instantiate our migration class.
					$migration = new $className();
					
					// Roll out the migration.
					$migration->rollOut();

					// Free the file.
					$this->unlockMigrationFile($file);

					// If the migration was successful, create its 
					// "finished" file.
					touch($finished);
			}
			
		} catch (Exception $e) {
			// Free the file.
			$this->unlockMigrationFile($file);
			// If the exception is a MigrationException (those are generally
			// reserved for simple errors like migration files that couldn't be
			// found, etc), ignore the error and return true anyway.
			if ($e instanceof MigrationException) {
				return true;
			}
			
			// We log all other errors to our migration log file.
			$log->addError($file->path() . ': ' . $e->getMessage());
			
			// If we shouldn't suppress errors, write the error.
			if (false === $suppressErrors) {
				$this->writeError($e->getMessage());
			}
			
			// Return false to indicate the migration failed.
			return false;
		}
		
		// Output a message to indicate we've finished running this migration.
		$this->writeSuccess("Migration {$file->name()} has been successfully rolled out.");
		
		return true;
	}

	/**
	 * Loops through all our migration directories and executes each migration 
	 * that hasn't been executed before.
	 */
	public function runMigrations($from = null) {
		$errors = 0;
		
		// Create a log channel
		$log = new Logger('migrations');
		$log->pushHandler(new StreamHandler(__LOGS__ . DS . 'migrations.log'));

		// Loop through all migration directories and roll out all migrations.
		foreach ($this->getMigrationDirectories() as $dir) {
			dir_map($dir, function($file, $dir, $path) use ($log, $from, &$errors) {
				if ($from == null || strcasecmp($file, $from) > 0) {
					if (false === $this->runMigration($path, $log, true)) {
						$errors++;
					}
				}
			});
		}
		
		if ($errors !== 0) {
			$this->writeError("We encountered {$errors} error(s) while running the migrations. Please refer to the migrations log file for more information.");
		}
	}

	/**
	 * Reverts a migration.
	 * 
	 * @param	string	$path
	 */
	public function revertMigration($path) {
		if (false === strpos($path, DS)) {
			$path = suffix($path, '.php');
			foreach ($this->getMigrationDirectories() as $dir) {
				if (file_exists($dir . DS . $path)) {
					$path = $dir . DS . $path;
				}
			}
		}
		
		$file = new File($path);	
		$finished = __MIGRATIONS__ . DS . FINISHED_DIR . DS . $file->name();

		if ($file->exists()) {
			switch ($file->ext()) {
				case 'php':
					// Generate the class name of the migration object we want 
					// to run.
					$className = preg_replace('/\.php$/', '', $file->name());

					// Include the PHP file's content.
					if (!class_exists($className)) {
						include $file->path();
					}

					if (class_exists($className)) {
						(new $className())->rollBack();

						// If the rollback was successful, delete the finished
						// file of the migration.
						if (file_exists($finished)) {
							unlink($finished);
						}
						
						$this->writeSuccess('Migration ' . $file->name() . ' was successfully rolled back.');
					}
			}
		}
	}
	
	/**
	 * Unlocks all migration files.
	 */
	public function unlockMigrations() {
		// Loop through all migration directories and roll out all migrations.
		foreach ($this->getMigrationDirectories() as $dir) {
			if (is_dir($dir)) {
				dir_map($dir, function($file, $dir, $path) {
					if (ends_with($file, '-lock')) {
						$this->unlockMigrationFile(new File($path));
					}
				});
			}
		}
	}
	
	/**
	 * Create a migration file.
	 * 
	 * @param	string	$name
	 */
	public function makeMigration($name = null) {
		// If no migration name is provided, generate one on the fly.
		$name = ($name === null) ? 
					md5($this->random->string()) : 
					strtolower(str_replace(' ', '_', $name));
		
		// Prefix the name with the current timestamp so that it's sure to be 
		// executed after all previously created migrations have been executed.
		// Also prepend an alphabetic character so that the filename also be 
		// used as a valid class name (class names starting with a number are 
		// not allowed).
		$name = 'M_' . time() . '_' . $name;

		// Make sure our migrations directory is writeable.
		$dir = __MIGRATIONS__;
		if (!is_writeable($dir)) {
			$this->writeError($dir . ' directory is not writeable.');
		}
		
		// Make sure the migration file does not exist yet.
		$path = $dir . DS . $name . '.php';
		if (file_exists($path)) {
			$this->writeError("Migration {$name} already exists.");
		}

		// Create the migration file.
		file_put_contents($path, str_format(file_get_contents(__BIN_TEMPLATES__ . DS . 'migration.txt'), ['name' => $name]));

		// If no errors occured, output a success message.
		$this->writeSuccess("Migration {$name} was successfully created.");
	}
	
	/**
	 * Updates an existing rozyn project.
	 * 
	 * @param	string	$project
	 * @param	string	$source
	 */
	public function update($project = null, $source = null) {
		// If no destination project is specified, assume the current project is
		// to be updated.
		if ($project === null) {
			$project = $this->getProjectName();
		}
		
		// If no source project is specified, assume the base rozyn project
		// should be used as a source.
		if ($source === null) {
			$source = 'rozyn';
		}

		// If the project doesn't exist, halt script execution and output an 
		// error.
		if (!is_dir($projectRoot = __SITES__ . DS . $project)) {
			$this->writeError("Project {$project} does not exist.\n");
		}
		
		$this->writeSuccess("Updating project {$project} (base: {$source})");

		// 2016-04-06: Drastically revamped the config structure.
		$this->updateConfig_20160406($project);
		
		// 2016-06-10: Changed the names of the bin templates and the finished
		// migration directories.
		$this->updateConfig_20160610($project);
		
		// Moved the /views/ and /templates/ folders outside the /public/ one
		// and into the root.
		$this->updateConfig_20170209($project);

		// We use a stack to keep track of all directories that should be 
		// traversed. We set an empty string as its one and only element, which 
		// forces the script to create the root project folder.
		$stack	= [''];

		// The directories/files that we'll to forcefully overwrite (ie: any 
		// files that are found in the source directory will overwrite any 
		// possible file of the same name in the destination directory).
		$force	= [ // Some core directories of which the main files absolutely 
					// need to to be maintained from 1 root project, so these 
					// need to be updated ALWAYS, no matter what kind of changes 
					// were made. People have no business editing files in these 
					// directories anyway.
					'bin', 'testing', 'plugins', 'src/Rozyn', 'migrations',
			
					// The rozyn CSS and JS files need to be updated as well.
					// Normally we don't want to overwrite any public files,
					// but the /rozyn/ public files should never be changed, so
					// these are safe to overwrite.
					'public/scss/rozyn', 'public/js/rozyn', 'public/css/rozyn',

					// The index.php file is a special file that handles all 
					// incoming traffic, so nothing should be changed there 
					// manually either.
					'index.php',

					// The PHPUnit XML file should also be updated no matter 
					// what.
					'phpunit.xml',

					// Always overwrite our system's config directory.
					'config/rozyn'
			];


		// The directories/files that should be ignored completely.
		$exclude = ['vendor', 'views', 'templates', 'logs', 'lang', 'cache',
					'public/cache',
					'src/Controller', 'src/Model',
					'migrations/_finished', '.git', 'testing/config/custom', 
					'config/plugins', 'config/env', 'testing/providers/integration',
					'config/_key', 'config/bootstrap.php', 'README.md'];

		// The directories/files that should be deleted.
		$delete  = ['config/custom.php', 'config/rozyn/migrations.php',

					// We deleted our Core directory and replaced it with Rozyn
					'src/Core',

					// We no longer use our public index file, so remove it.
					'public/index.php',

					// Delete the Core directory from the testing folder
					'testing/tests/Core',

					// Directories that previously used a plural name but have since 
					// been updated to use singular names.
					'src/Rozyn/Controllers', 'src/Rozyn/Events', 'src/Rozyn/Facades', 'src/Rozyn/Models',
					'src/Rozyn/Relations/', 'src/Rozyn/Plugins', 'src/Rozyn/Helpers',
					'plugins/rozadmyn/src/Rozadmyn/Controllers', 'plugins/rozadmyn/src/Rozadmyn/Facades',
					'plugins/console/src/Console/Controllers', 'plugins/console/src/Console/Facades'];

		// Update our project files using the settings we've just computed.
		$this->updateProjectFiles($project, $stack, $force, $delete, $exclude, $source);

		// Notify the admin that all the files have been successfully copied.
		$this->writeSuccess('Finished updating file system.');

		// Add any new composer requirements.
		$this->write('Updating composer.json');
		$this->updateRequirements($project);

		// Tell the admin that we're now starting to update any composer dependencies.
		// msg('INFO', 'Updating composer dependencies.');

		// Change the directory to where the composer.json is located and call the
		// composer update command to update all of its dependencies.
		// shell_exec('cd ' . __SITES__ . DS . $project . ' && composer update');

		// Set all the proper file permissions.
		$this->setFilePermissions($project);
	}
	
	/**
	 * Installs a new rozyn project.
	 * 
	 * @param	string	$project
	 */
	public function install($project) {
		// If the project already exists, halt script execution and output an error.
		if (file_exists(__SITES__ . DS . $project)) {
			$this->writeError("Project {$project} already exists.");
		}

		// We use a stack to keep track of all directories that should be traversed.
		// We set an empty string as its one and only element, which forces the script
		// to create the root project folder.
		$stack = [''];

		// The paths to any directories that shouldn't be copied from our source project.
		$exclude = ['vendor', '.git', 'migrations' . DS . '_finished', 'public' . DS . 'cache'];

		// While we have directories in our stack, execute the code in this loop, which
		// takes care of creating the directory and all files in it.
		while (!empty($stack)) {
			// The relative path to the directory that is going to be copied.
			$rel_dir = array_shift($stack);

			// The source directory
			$src_dir = __SITES__ . DS . 'rozyn'  . DS . $rel_dir;

			// The destionation directory
			$dest_dir = __SITES__ . DS . $project . DS . $rel_dir;

			// If the source directory exists, make that same directory in the project.
			if (is_dir($src_dir)) {
				mkdir($dest_dir);

				$this->writeSuccess("Created directory {$rel_dir}.");
			}

			// Loop through all the files within the directory
			dir_map($src_dir, function($file, $src_dir) use ($rel_dir, $dest_dir, $exclude, &$stack) {
				// Compute the full path to the source file.
				$src_path = $src_dir . DS . $file;

				// Also compute the relative path to the destination file.
				$rel_path = (empty($rel_dir)) ? $file : $rel_dir . DS . $file;

				// If the source file is actually a directory, add this directory to the
				// stack so that we will recursively traverse that directory at a later 
				// point. Before you add the directory to the stack though, make sure
				// the directory is not in our $exclude array.
				if (is_dir($src_path) && !in_array($rel_path, $exclude)) {
					$stack[] = $rel_path;
				}

				// If the file is indeed just a file and it is not in our EXCLUDE array,
				// copy that source file into our destination folder.
				elseif (file_exists($src_path) && !in_array($rel_path, $exclude)) {
					copy($src_path, implode(DS, [$dest_dir, $file]));

					$this->writeSuccess("Copied file {$src_dir}/{$file}.");
				}	
			});
		}

		// Notify the admin that all the files have been successfully copied.
		$this->writeSuccess('Finished copying file system.');
		
		// Update database config files.
		$dbConfigFileLocal	= $this->getProjectRootPath($project) . DS . CONFIG_DIR . DS . 'env' . DS . ENV_LOCAL . DS . 'database.php';
		$dbConfigFileCli	= str_replace('env' . DS . ENV_LOCAL, 'env' . DS . ENV_CLI, $dbConfigFileLocal);
		file_put_contents($dbConfigFileLocal,	str_replace('rozyn', $project, file_get_contents($dbConfigFileLocal)));
		file_put_contents($dbConfigFileCli,		str_replace('rozyn', $project, file_get_contents($dbConfigFileCli)));
		
		// Clear log files.
		dir_map($this->getProjectRootPath($project) . DS . 'logs', function($file, $dir, $path) {
			file_put_contents($path, '');
		});
		
		// Notify the admin that we're done updating the config files.
		$this->writeSuccess("Finished updating database config files.");

		// Tell the admin that we're now starting to install any composer dependencies.
		$this->writeSuccess('Installing composer dependencies.');

		// Change the directory to where the composer.json is located and call the
		// composer install command to install all of its dependencies.
		$this->exec('cd ' . __SITES__ . DS . $project . ' && composer install');

		// Set all the proper file permissions.
		$this->setFilePermissions($project);		
	}
	
	/**
	 * Clears all cached data for this project.
	 */
	public function clearCache() {
		Cache::clear();
	}
	
	/**
	 * Updates the project's requirements with those of the base rozyn project.
	 * 
	 * @param	string	$project
	 */
	public function updateRequirements($project = null) {
		if ($project === null) {
			$project = $this->getProjectName();
		}
		
		$requirements = [];
		
		// Load the default requirements first.
		$rozynIterator = new FileIterator(__SITES__ . DS . $project . DS . 'config' . DS . 'rozyn' . DS . 'requirements.txt');
		foreach ($rozynIterator as $line) {
			$requirements[] = $line;
		}
		
		// Afterwards, add the project-specific requirements.
		if (file_exists($file = __SITES__ . DS . $project . DS . 'config' . DS . 'requirements.txt')) {
			$projectIterator = new FileIterator($file);
			foreach ($projectIterator as $line) {
				$requirements[] = $line;
			}
		}
		
		if (!empty($requirements)) {
			$this->exec('cd ' . __SITES__ . DS . $project . ' && composer require ' . implode(' ', $requirements));
		}
	}
	
	/**
	 * Compiles SCSS files. If $src is a directory, all .scss files inside that
	 * directory will be compiled (if $recurse is set to TRUE, all .scss files
	 * in subdirectories will be compiled as well).
	 * 
	 * @param	string	$src
	 * @param	string	$dest
	 * @param	boolean	$recurse
	 * @param	
	 * @return	boolean
	 */
	public function compileScss($src = null, $dest = null, $recurse = true, ScssCompiler $compiler = null) {		
		// If no source was specified, assume we should compile the .scss files
		// in the public/scss directory.
		if ($src === null) {
			$src = __PUBLIC__ . DS . 'scss';
		}
		
		// If no destination is specified, take the source and replace any 
		// occurence of "scss" with just "css". That should be a reasonable
		// assumption for the intended destination.
		if ($dest === null) {
			$dest = preg_replace('/(?<=' . PREG_DS . '|\.)scss/', 'css', $src);
		}
		
		// Create our compiler.
		if ($compiler === null) {
			$compiler = new ScssCompiler();
			
			// If the current src is a directory, we add it to the import path
			// so that @import statements in the scss files are handled properly.
			if (is_dir($src)) {
				$compiler->addImportPath($src);
			}
		}
		
		// Check if we need to compile an entire directory.
		if (is_dir($src)) {
			// Of so, make sure the the $dest directory exists as well.
			if (!is_dir($dest)) {
				mkdir($dest, 0777, true);
			}
			
			// Define a callback function that we execute on each file and
			// directory in the $src directory.
			$cb = function($file, $dir, $path) use ($dest, $recurse, $compiler) {
				// If we're currently iterating over a .scss file or a directory
				// with $recurse set to true, recursively call compileScss().
				if (
					is_file($path) && ends_with($file, '.scss') || 
					is_dir($path) && $recurse
				) {
					$this->compileScss($path, $dest . DS . preg_replace('/(?<=' . PREG_DS . '|\.)scss/', 'css', $file), $recurse, $compiler);
				}
			};
			
			// Run through the $src directory and fire the defined callback on
			// each item. We force $recurse to false here, because we handle
			// recursion separately inside the callback.
			dir_map($src, $cb, false);
			
			return true;
		}
		
		// If we're just dealing with a regular file, compile it if it is not a 
		// partial.
		if (file_exists($src) && !preg_match('/(^|' . PREG_DS . ')_[^\/]+\.scss$/', $src)) {
			file_put_contents($dest, $compiler->compile(file_get_contents($src)));
			
			$this->writeSuccess("Successfully compiled SCSS file " . $src);
		
			// If all went well, return true.
			return true;
		}
		
		return false;
	}
	
	/**
	 * Executes a command via the shell.
	 * 
	 * @param	string	$command
	 * @return	int
	 * @throws	\Rozyn\Bin\UnauthorizedExecCallException
	 */
	protected function exec($command) {
		// Make sure we're running from a CLI environment, otherwise we should
		// not allow access to this method.
		if (ENV !== ENV_CLI) {
			throw new UnauthorizedExecCallException();
		}
		
		$res = null;
		passthru($command, $res);
		
		return $res;
	}
	
	/**
	 * Writes the given $str, formatted as an error message. If $shellOnly is 
	 * set to TRUE, it will only write the $str if the current script was called 
	 * via the Terminal or a shell environment.
	 * 
	 * @param	string	$str
	 * @param	boolean	$shellOnly
	 * @param	boolean	$callExit
	 */
	protected function writeError($str, $shellOnly = true, $callExit = true) {
		$this->write($str, $shellOnly);
		
		// Check if the script should be terminated.
		if ($callExit === true) {
			$this->callExit();
		}
	}
	
	/**
	 * Writes the given $str, formatted as a success message. If $shellOnly is 
	 * set to TRUE, it will only write the $str if the current script was called 
	 * via the Terminal or a shell environment.
	 * 
	 * @param	string	$str
	 * @param	boolean	$shellOnly
	 * @return	string
	 */
	protected function writeSuccess($str, $shellOnly = true) {
		return $this->write($str, $shellOnly);
	}
	
	/**
	 * Writes the given $str. If $shellOnly is set to TRUE, it will only write
	 * the $str if the current script was called via the Terminal or a shell-
	 * environment.
	 * 
	 * @param	string	$str
	 * @param	boolean	$shellOnly
	 * @return	string
	 */
	protected function write($str, $shellOnly = true) {
		if ($shellOnly === false || $shellOnly === true && ENV === ENV_CLI) {
			echo $str . "\n";
		}
		
		return $str;
	}

	/**
	 * Sets all the file permissions in the specified $project folder to their 
	 * intended values.
	 * 
	 * @param	string	$project
	 */
	protected function setFilePermissions($project) {
		// Set the phpunit executable rights
		$this->exec('cd ' . __SITES__ . DS . $project . ' && chmod +x vendor/bin/phpunit && chmod +x bin/rozyn');

		// Define which directories need to be writeable.
		$writeable = array(
			CACHE_DIR, 
			LOGS_DIR, 
			MIGRATIONS_DIR, 
			PLUGINS_DIR, 
			PUBLIC_DIR . DS . CACHE_DIR
		);

		$cmds = [];
		$cmds[] = 'cd ' . __SITES__ . DS . $project;

		foreach ($writeable as $dir) {
			$cmds[] = 'chmod -Rf 777 ' . $dir;
		}

		$this->exec(implode(' && ', $cmds));
	}
	
	/**
	 * Locks a migration file so that subsequent requests won't execute this 
	 * migration anymore.
	 * 
	 * @param	\Rozyn\Filesystem\File	$file
	 */
	protected function lockMigrationFile(File $file) {
		$file->rename(suffix($file->name(), '-lock'));
	}

	/**
	 * Unlocks a migration file so that subsequent requests will execute this 
	 * migration again.
	 * 
	 * @param	\Rozyn\Filesystem\File	$file
	 */
	protected function unlockMigrationFile(File $file) {
		$file->rename(preg_replace('/-lock$/', '', $file->name()));
	}
	
	/**
	 * Returns the paths to all directories that can contain migration files.
	 * 
	 * @return	string[]
	 */
	protected function getMigrationDirectories() {
		$baseDirs = [__MIGRATIONS__];
		
		foreach ($this->app->getPlugins() as $plugin) {
			$baseDirs[] = __PLUGINS__ . DS . strtolower($plugin->getDirName()) . DS . MIGRATIONS_DIR;
		}
		
		$migrationDirs = $baseDirs;
		
		foreach ($baseDirs as $baseDir) {
			dir_map($baseDir, function($file, $dir, $path) use (&$migrationDirs) {
				if (is_dir($path) && $file !== FINISHED_DIR) {
					$migrationDirs[] = $path;
				}
			});
		}
		
		return $migrationDirs;
	}
	
	/**
	 * Converts an incomplete or badly formatted SQL datatype into a valid one.
	 * 
	 * @param	string	$type
	 * @return	string
	 */
	protected function formatSqlDataType($type) {
		switch (strtoupper($type)) {
			case 'BOOL':
				return 'TINYINT(1)';
				
			case 'VARCHAR':
			case 'STRING':
				return 'VARCHAR(255)';
				
			case 'INT':
				return 'INT(11)';
				
			default:
				return $type;
		}
	}
	
	/**
	 * Returns the path to the specified project's root directory.
	 * 
	 * @param	string	$project
	 * @return	string
	 */
	protected function getProjectRootPath($project = null) {
		if ($project === null) {
			$project = $this->getProjectName();
		}
		
		return __SITES__ . DS . $project;
	}
	
	/**
	 * 
	 * @param	string	$project
	 * @param	array	$stack
	 * @param	array	$force
	 * @param	array	$delete
	 * @param	array	$exclude
	 * @param	string	$source
	 */
	protected function updateProjectFiles($project, $stack, $force, $delete, $exclude, $source = null) {
		$force = array_map(function($path) {
			return str_replace(URI_SEPARATOR, DS, $path);
		}, $force);
		
		$delete = array_map(function($path) {
			return str_replace(URI_SEPARATOR, DS, $path);
		}, $delete);
		
		$exclude = array_map(function($path) {
			return str_replace(URI_SEPARATOR, DS, $path);
		}, $exclude);
		
		// Default back to rozyn as our source.
		if ($source === null) {
			$source = 'rozyn';
		}

		// Delete any files and directories that should be deleted before we 
		// start writing files.
		foreach ($delete as $rel_path) {
			// The full path to the destination file that should be deleted.
			$dest_path = __SITES__ . DS . $project . DS . $rel_path;

			if (is_file($dest_path)) {
				unlink($dest_path);

				$this->writeSuccess("Deleted file {$dest_path}");
			}

			elseif (is_dir($dest_path)) {
				unlink_dir($dest_path);

				$this->writeSuccess("Deleted directory {$dest_path}");
			}
		}

		// While we have directories in our stack, execute the code in this 
		// loop, which takes care of updating the directory and all files in it. 
		// If a file exists both in the source directory and the destination 
		// directory, it is NOT replaced unless the directory containing said 
		// file is in our $force array.
		while (!empty($stack)) {
			// The relative path to the directory that is going to be copied.
			$rel_dir = array_shift($stack);

			// The source directory
			$src_dir = __SITES__ . DS . $source  . DS . $rel_dir;

			// The destination directory
			$dest_dir = __SITES__ . DS . $project . DS . $rel_dir;

			// If the source directory exists, make that same directory in the 
			// project.
			if (is_dir($src_dir) && !is_dir($dest_dir)) {
				mkdir($dest_dir);

				$this->writeSuccess("Created directory {$rel_dir}...");
			}

			// Loop through all the files within the directory
			dir_map($src_dir, function($file, $src_dir) use ($rel_dir, $dest_dir, $exclude, $force, $delete, &$stack) {
				// Compute the relative path to the destination file.
				$rel_path  = (empty($rel_dir)) ? $file : $rel_dir . DS . $file;

				// Compute the full path to the source file.
				$src_path  = $src_dir . DS . $file;

				// Compute the full path to the destination file.
				$dest_path = $dest_dir . DS . $file;

				// If the source file is actually a directory, add it to the 
				// stack so that we will recursively traverse that directory at 
				// a later time. Before you add the directory to the stack 
				// though, make sure the directory is not in our EXCLUE array.
				if (is_dir($src_path) && !in_array($rel_path, $exclude)) {
					$stack[] = $rel_path;
				}

				// If the file is indeed just a file and it is not in our 
				// EXCLUDE array, copy that source file into our destination 
				// folder if it doesn't already exist in our destination folder. 
				// If it does exist, do not overwrite it, unless the file is in 
				// our $force array, which holds all the files that have to be 
				// overwritten regardless of whether they already exist or not.
				elseif (!in_array($rel_path, $exclude) 
						&& file_exists($src_path) 
						&& (!file_exists($dest_path) 
						|| (starts_with($rel_path, $force)
							&& md5_file($src_path) !== md5_file($dest_path)))) {

					copy($src_path, $dest_path);

					$this->writeSuccess("Updated file {$dest_dir}/{$file}");
				}
			});
		}

		// Fix the rights for our project.
		$this->exec('cd ' . __SITES__ . ' && chmod -Rf 777 ' . $project);
	}
	
	/**
	 * Reads a file and executes it as SQL code.
	 * 
	 * @param	string	$file
	 * @return	int
	 */
	protected function queryFile($file) {
		return $this->exec(sprintf('%s -u %s --password=%s %s < "%s"', MYSQL_BIN, DB_USER, DB_PASS, DB_NAME, $file));
	}
	
	/**
	 * Returns the name of the project from which this class is called.
	 * 
	 * @return	string
	 */
	private function getProjectName() {
		return PROJECT_DIR;
	}
	
	/**
	 * On 2016-04-06 I made some big changes to the config structure of rozyn
	 * projects. This method is meant to update old projects with the old config
	 * structure to the new structure.
	 * 
	 * @param	string	$project
	 */
	private function updateConfig_20160406($project) {
		$projectConfigRoot = __SITES__ . DS . $project . DS . 'config';
		if (!is_dir($projectConfigRoot . DS . 'env')) {
			$delete = array(
				// Old config/bootstrap files.
				'config.php', 'aliases.php', 'autoload.php',
				'debug.php', 'functions.php', 'migrations.php',
				'paths.php', 'validation.php',
			);

			foreach ($delete as $file) {
				if (file_exists($projectConfigRoot . DS . $file)) {
					unlink($projectConfigRoot . DS . $file);
				}
			}

			mkdir($projectConfigRoot . DS . 'env');

			if (!is_dir($projectConfigRoot . DS . 'plugins')) {
				mkdir($projectConfigRoot . DS . 'plugins');
			}

			$envs = ['cli', 'local', 'development', 'production', 'testing'];
			foreach ($envs as $env) {
				if (is_dir($projectConfigRoot . DS . $env)) {
					rename($projectConfigRoot . DS . $env, $projectConfigRoot . DS . 'env' . DS . $env);
				}
			}

			if (is_dir($custom = $projectConfigRoot . DS . 'custom')) {
				dir_map($custom, function($file, $dir, $path) use ($projectConfigRoot) {
					if (is_file($path)) {
						if ($file === 'config.php') {
							$file = 'bootstrap.php';
						}

						rename($path, $projectConfigRoot . DS . $file);
					}

					if (is_dir($path)) {
						rename($path, $projectConfigRoot . DS . 'plugins' . DS . $file);
					}
				}, false);

				unlink_dir($custom);
			}
		}
	}
	
	/**
	 * Changed the names of the bin templates and the finished migrations 
	 * directories.
	 * 
	 * @param	string	$project
	 */
	private function updateConfig_20160610($project) {
		$pathToProject = $this->getProjectRootPath($project);
		$changes = array(
			$pathToProject . DS . 'bin' . DS . '.templates'			=> $pathToProject . DS . 'bin' . DS . '_templates',
			$pathToProject . DS . 'migrations' . DS . 'finished'	=> $pathToProject . DS . 'migrations' . DS . '_finished'
		);
		
		foreach ($changes as $old => $new) {
			if (is_dir($old)) {
				rename($old, $new);
			}
		}
	}
	
	/**
	 * Remove the /views/ and /templates/ directories in the /public/ directory
	 * of plugins, and move the /views/ and /templates/ directories from the 
	 * main project to the root.
	 * 
	 * @param	string	$project
	 */
	private function updateConfig_20170209($project) {
		$pathToProject = $this->getProjectRootPath($project);
		
		$changeDirs = [VIEWS_DIR, TEMPLATES_DIR];
		foreach ($changeDirs as $changeDir) {
			$dir = $pathToProject . DS . PUBLIC_DIR . DS . $changeDir;
			if (is_dir($dir)) {
				rename($dir, $pathToProject . DS . $changeDir);
			}
		}
		
		$pluginDirs = [];
		foreach ($this->app->getPlugins() as $plugin) {
			$pluginDirs[] = $pathToProject . DS . PLUGINS_DIR . DS . strtolower($plugin->getDirName());
		}
		
		foreach ($pluginDirs as $pluginDir) {
			foreach ($changeDirs as $changeDir) {
				$dir = $pluginDir . DS . PUBLIC_DIR . DS . $changeDir;
				if (is_dir($dir)) {
					unlink_dir($dir);
				}
			}
		}
	}
	
	/**
	 * Calls PHP's exit function. This method exists because we want to be able
	 * to mock it in unit tests.
	 */
	final protected function callExit() {
		exit;
	}
}