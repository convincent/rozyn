<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Data;

use Rozyn\Error\PHPRuntimeErrorException;

class Collection implements \Iterator, \ArrayAccess, \JsonSerializable {

	/**
	 * Holds all the elements of the collection.
	 * 
	 * @var	array
	 */
	protected $items;

	/**
	 * Construct a new Collection instance.
	 * 
	 * @param	array	$items
	 */
	public function __construct(array $items = []) {
		$this->items = $items;
	}

	/**
	 * Returns the amount of items in this Collection.
	 * 
	 * @return	int
	 */
	public function size() {
		return count($this->items);
	}

	/**
	 * Returns whether or not this Collection is empty.
	 * 
	 * @return	type
	 */
	public function isEmpty() {
		return $this->get() === null || $this->size() === 0;
	}

	/**
	 * Removes all items from this Collection.
	 * 
	 * @return	\Rozyn\Data\Collection
	 */
	public function clear() {
		$this->items = [];

		return $this;
	}

	/**
	 * Adds an item to the Collection under the given key.
	 * 
	 * @param	mixed	$key
	 * @param	mixed	$item
	 * @return	\Rozyn\Data\Collection
	 */
	public function set($key, $item) {
		$this->items[$key] = $item;

		return $this;
	}

	/**
	 * Adds one or more items to the Collection. If no second argument is
	 * provided, the first argument is assumed to hold the item(s) to be added.
	 * If only 1 item is passed this way, the item is stored under a numerical
	 * key, just as if you would add an element to an array without specifying
	 * a key.
	 * 
	 * @param	mixed	$key
	 * @param	mixed	$item
	 * @return	\Rozyn\Data\Collection
	 */
	public function add($key, $item = null) {
		if (is_array($key) && func_num_args() === 1) {
			foreach ($key as $k => $v) {
				$this->items[$k] = $v;
			}
		} elseif (is_a($key, get_class($this))) {
			$this->merge($key);
		} elseif (func_num_args() === 1) {
			$this->items[] = $key;
		} elseif ($key === null && $item !== null) {
			$this->items[] = $item;
		} else {
			$this->items[$key] = $item;
		}

		return $this;
	}

	/**
	 * Checks whether or not an item with the given key exists in this 
	 * Collection. 
	 * 
	 * @param	mixed	$key
	 * @return	boolean
	 */
	public function has($key) {
		return isset($this->items[$key]);
	}

	/**
	 * Return an item matching the given key. If no key is specified, all items
	 * are returned as an array instead. If no matching item is found, a default
	 * value is returned.
	 * 
	 * @param	mixed	$key
	 * @return	mixed
	 */
	public function get($key = null, $default = null) {
		if ($key !== null && $this->has($key)) {
			return $this->items[$key];
		}

		if ($key === null) {
			return $this->items();
		}

		return $default;
	}

	/**
	 * Remove an item from the Collection.
	 * 
	 * @param	mixed	$key
	 * @return	\Rozyn\Data\Collection
	 */
	public function del($key) {
		if ($this->has($key)) {
			unset($this->items[$key]);
		}

		return $this;
	}

	/**
	 * Merges another Collection with this one.
	 * 
	 * @param	\Rozyn\Data\Collection	$collection
	 * @return	\Rozyn\Data\Collection
	 */
	public function merge(Collection $collection) {
		$this->add($collection->items());

		return $this;
	}

	/**
	 * Iterate through the entire Collection and apply the given callback 
	 * function to each item. The item is passed to the callback function as its
	 * first argument and its corresponding key is passed as the second argument.
	 * 
	 * @param	callable	$cb
	 * @return	\Rozyn\Data\Collection
	 */
	public function map(callable $cb) {
		$res = [];

		foreach ($this->items as $key => $item) {
			$res[$key] = call_user_func($cb, $item, $key);
		}

		return new static($res);
	}
	
	/**
	 * Sorts all items in the Collection based on a callback function. The 
	 * second argument specifies if we want to order in ascending or descending
	 * order.
	 * 
	 * @param	callable	$cb
	 * @return	\Rozyn\Data\Collection
	 */
	public function sort(callable $cb, $order = ORDER_ASCENDING) {
		$array = $this->toArray();
		
		// Earlier PHP versions have a bug where if you throw and catch an
		// exception inside a user callback of a u(a)sort call, PHP will output
		// a warning because it thinks the original array was modified by the
		// user function. This is a false error, so we just catch it and do 
		// nothing.
		try {
			uasort($array, $cb);
		} catch (PHPRuntimeErrorException $e) {}
		
		$res = new static($array);
	
		return ($order === ORDER_ASCENDING) ? $res : $res->reverse();
	}

	/**
	 * Reverses all items in the Collection.
	 * 
	 * @return	\Rozyn\Data\Collection
	 */
	public function reverse() {
		return new static(array_reverse($this->items, true));
	}

	/**
	 * Filters the Collection so that only those items are returned that made 
	 * the given callback function return a boolean value of TRUE.
	 * 
	 * @param	callable	$cb
	 * @return	\Rozyn\Data\Collection
	 */
	public function filter(callable $cb) {
		$res = [];

		foreach ($this->items as $key => $item) {
			if (call_user_func($cb, $item, $key)) {
				$res[$key] = $item;
			}
		}

		return new static($res);
	}

	/**
	 * Similar to array_slice(), but this way you can call it directly on the
	 * Collection.
	 * 
	 * @param	int		$offset
	 * @param	int		$length
	 * @param	boolean	$preserve_keys
	 * @return	\Rozyn\Data\Collection
	 */
	public function slice($offset, $length = null, $preserve_keys = true) {
		return new static(array_slice($this->items, 
									  $offset, 
									  $length, 
									  $preserve_keys));
	}

	/**
	 * Returns a random item from the Collection.
	 * 
	 * @return	mixed
	 */
	public function random() {
		return $this->items[array_rand($this->items)];
	}

	/**
	 * Returns the first item in the Collection.
	 * 
	 * @return	mixed
	 */
	public function first() {
		$tmp = array_slice($this->items, 0, 1);

		return array_pop($tmp);
	}

	/**
	 * Pushes an item on the Collection.
	 * 
	 * @param	mixed	$item
	 */
	public function push($item) {
		$this->items[] = $item;
	}

	/**
	 * Pops an item from the Collection.
	 * 
	 * @return	mixed
	 */
	public function pop() {
		return array_pop($this->items);
	}

	/**
	 * Shifts an item from the Collection.
	 * 
	 * @return	mixed
	 */
	public function shift() {
		return array_shift($this->items);
	}

	/**
	 * Returns all the keys in the Collection.
	 * 
	 * @return	array
	 */
	public function keys() {
		return array_keys($this->items);
	}

	/**
	 * Returns all items in the Collection as an array.
	 * 
	 * @return	array
	 */
	public function items() {
		return $this->items;
	}

	/**
	 * Converts the Collection to an array.
	 * 
	 * @return	array
	 */
	public function toArray() {
		return $this->items();
	}

	/**
	 * Resets the internal pointer of the Collection.
	 */
	public function rewind() {
		reset($this->items);
	}

	/**
	 * Returns the item that is currently being pointed to by the Collection's
	 * internal pointer.
	 * 
	 * @return	mixed
	 */
	public function current() {
		return current($this->items);
	}

	/**
	 * Returns the key that matches the item that is currently being pointed to
	 * by the Collection's internal pointer.
	 * 
	 * @return	mixed
	 */
	public function key() {
		return key($this->items);
	}

	/**
	 * Moves the internal pointer of the Collection one item further and returns
	 * that item.
	 * 
	 * @return	mixed
	 */
	public function next() {
		return next($this->items);
	}

	/**
	 * Returns a boolean value based on the current key of the Collection.
	 * 
	 * @return	boolean
	 */
	public function valid() {
		$key = key($this->items);
		return $key !== null && $key !== FALSE;
	}
	
	/**
	 * Specifies how to encode this object as JSON.
	 * 
	 * @return	array
	 */
	public function jsonSerialize() {
		return $this->items();
	}
	
	public function offsetSet($offset, $value) {
		if (is_null($offset)) {
			$this->items[] = $value;
		} else {
			$this->items[$offset] = $value;
		}
	}

	public function offsetExists($offset) {
		return isset($this->items[$offset]);
	}

	public function offsetUnset($offset) {
		unset($this->items[$offset]);
	}

	public function offsetGet($offset) {
		return isset($this->items[$offset]) ? $this->items[$offset] : null;
	}
}
