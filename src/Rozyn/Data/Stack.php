<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Data;

Class Stack {
	protected $stack;
	protected $size;

	public function __construct() {
		$this->size  = func_num_args();
		$this->stack = ($this->size === 0) ? [] : func_get_args();
	}

	public function peek() {
		return (!$this->isEmpty()) ? $this->stack[$this->size - 1] : null;
	}

	public function pop() {
		if ($this->size === 0)
			throw new EmptyStackException();
		
		$this->size--;
		return array_pop($this->stack);
	}

	public function push($v) {
		$this->stack[] = $v;
		$this->size++;
	}

	public function size() {
		return $this->size;
	}

	public function isEmpty() {
		return $this->size === 0;
	}
}