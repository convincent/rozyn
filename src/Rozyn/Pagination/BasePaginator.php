<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Pagination;

use Rozyn\Facade\Router;
use Rozyn\Routing\Route;
use Rozyn\Facade\Html;

class BasePaginator {
	/**
	 * Holds the total amount of items in the result set.
	 * 
	 * @var	int
	 */
	protected $total;
	
	/**
	 * Holds the amount of items to show per page.
	 * 
	 * @var	int
	 */
	protected $perPage;
	
	/**
	 * Holds the url for this paginator.
	 * 
	 * @var	string
	 */
	protected $url;
	
	/**
	 * Holds the "Previous" label for this paginator's navigation.
	 * 
	 * @var	string
	 */
	protected $previous;
	
	/**
	 * Holds the "Next" label for this paginator's navigation.
	 * 
	 * @var	string
	 */
	protected $next;
	
	/**
	 * Returns the numbers that should be used in any navigation.
	 * 
	 * @param	int		$page
	 * @param	int		$max
	 * @return	array
	 */
	public function getNavNumbers($page = 1, $max = 9) {
		$count = $this->getPageCount() ?: 1;

		$start	= max(1,		$page - floor($max / 2));
		$end	= min($count,	$start + $max);

		if ($max <= 6 || $count <= 8) {
			$res = range($start, $end);
		}
		
		elseif ($page <= ($cutoff = ceil($max / 2))) {
			$res = array_merge(range(1, $cutoff + 1), [null], range($count - ($max - ($cutoff + 3)), $count));
		}
		
		elseif ($page >= ($cutoff = $count - $cutoff + 1)) {
			$res = array_merge(range(1, $max - ($count - $cutoff + 3)), [null], range($cutoff - 1, $count));
		}
		
		else {
			$res = array_merge([1, null], range($start, $end), [null, $count]);
		}
		
		return array_map(function($v) {
			return (is_numeric($v)) ? intval($v) : null;
		}, $res);
	}
	
	/**
	 * Returns a valid HTML string that represents the navigation for this
	 * Paginator.
	 * 
	 * @param	int		$page
	 * @param	int		$max
	 * @param	string	$previous
	 * @param	string	$next
	 * @return	string
	 */
	public function getNav($page = 1, $max = 9, $params = [], $previous = null, $next = null) {
		$numbers = $this->getNavNumbers($page, $max);
		
		$res = '';
		if (count($numbers) > 0) {
			$res .= Html::tag('li', 
							  ($page == 1) ?
								  Html::tag('span', $this->getPrevious()) : 
								  Html::tag('a',	$this->getPrevious(), ['href' => $this->getUrl($page - 1, $params)]),
							  ['class' => 'previous']);
			
			foreach ($numbers as $number) {
				$res .= Html::tag('li', 
								  ($number === $page || null === $number) ? 
										Html::tag('span', $number ?: '...') : 
										Html::tag('a', $number, ['href' => $this->getUrl($number, $params)]),
								  ($number === $page) ? ['class' => 'active'] : []);
			}

			$res .= Html::tag('li', 
							  ($page == count($numbers)) ? 
								  Html::tag('span', $this->getNext()) : 
								  Html::tag('a',	$this->getNext(), ['href' => $this->getUrl($page + 1, $params)]),
							  ['class' => 'next']);
			}
		
		return Html::tag('ul', $res, ['class' => 'pagination']);
	}
	
	/**
	 * Sets the amount of items to be shown per page.
	 * 
	 * @param	int	$perPage
	 */
	public function setPerPage($perPage) {
		$this->perPage = $perPage;
	}
	
	/**
	 * Returns the amount of items to be shown per page.
	 * 
	 * @return	int
	 */
	public function getPerPage() {
		return $this->perPage;
	}
	
	/**
	 * Sets the total amount of items in the result set.
	 * 
	 * @param	int	$total
	 */
	public function setTotal($total) {
		$this->total = $total;
	}
	
	/**
	 * Returns the total amount of items in the result set.
	 * 
	 * @return	int
	 */
	public function getTotal() {
		return $this->total;
	}
	
	/**
	 * Sets the url for this Paginator.
	 * 
	 * @param	string	$url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}
	
	/**
	 * Returns the unparsed URL for this paginator.
	 * 
	 * @return	string
	 */
	public function getRawUrl(array $params = []) {
		return $this->getUrl(null, $params);
	}
	
	/**
	 * Returns the URL for a particular page in the paginator.
	 * 
	 * @param	int		$page
	 * @param	array	$params
	 * @return	string
	 */
	public function getUrl($page = null, array $params = []) {
		if ($this->url === null) {
			return '#';
		}
		
		return ($page === null) ? $this->url : str_format($this->url, array_merge(['page' => $page], $params));
	}
	
	/**
	 * Returns the amount of pages in this Paginator.
	 * 
	 * @return	int
	 */
	public function getPageCount() {
		return ceil($this->getTotal() / $this->getPerPage());
	}
	
	/**
	 * Define a string that is to be used as a label for the "Previous" link in
	 * the pagination's navigation.
	 * 
	 * @param	string	$label
	 */
	public function setPrevious($label) {
		$this->previous = $label;
	}
	
	/**
	 * Define a string that is to be used as a label for the "Next" link in the 
	 * pagination's navigation.
	 * 
	 * @param	string	$label
	 */
	public function setNext($label) {
		$this->next = $label;
	}
	
	/**
	 * Returns the label that is to be used for the "Previous" link in the 
	 * pagination's navigation.
	 * 
	 * @return	string
	 */
	public function getPrevious() {
		return $this->previous ?: t('previous');
	}
	
	/**
	 * Returns the label that is to be used for the "Next" link in the 
	 * pagination's navigation.
	 * 
	 * @return	string
	 */
	public function getNext() {
		return $this->next ?: t('next');
	}
}