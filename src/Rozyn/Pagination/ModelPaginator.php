<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Pagination;

use Rozyn\Database\ModelQuery;

class ModelPaginator extends BasePaginator {
	/**
	 * Holds the ModelQuery object that is used to generate the pagination results.
	 * 
	 * @var	\Rozyn\Database\ModelQuery
	 */
	protected $query;
	
	/**
	 * Constructs the object.
	 * 
	 * @param	\Rozyn\Database\ModelQuery	$query
	 * @param	int							$perPage
	 * @param	string						$url
	 */
	public function __construct(ModelQuery $query = null, $perPage = null, $url = null) {
		$this->setQuery($query);
		$this->setPerPage($perPage);
		$this->setUrl($url);
	}
	
	/**
	 * Returns the Model object associated with this Paginator.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function getModel() {
		return $this->query->getModel();
	}
	
	/**
	 * Sets the ModelQuery object for this Paginator.
	 * 
	 * @param	\Rozyn\Database\ModelQuery $query
	 */
	public function setQuery(ModelQuery $query) {
		if (!$query->hasStatement()) {
			$query->select();
		}
		
		$this->query = $query;
	}
	
	/**
	 * Returns the ModelQuery object for this Paginator.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function getQuery() {
		return $this->query;
	}
	
	/**
	 * Execute the query and get the results in plain array form.
	 * 
	 * @param	int		$page
	 * @return	array
	 */
	public function fetch($page) {
		$statement = clone $this->getQuery()->getStatement();
		
		$res = $this->getQuery()
					->offset(max(0, $page - 1) * $this->getPerPage())
					->limit($this->getPerPage())
					->fetchModels();
		
		$this->getQuery()->setStatement($statement);
		
		return $res;
	}
	
	/**
	 * Returns the total amount of items in this Paginator.
	 * 
	 * @return	int
	 */
	public function getTotal() {
		if ($this->total === null) {
			$this->total = $this->query->count();
		}
		
		return $this->total;
	}
	
	/**
	 * Pass any unknown method calls along to the Query associated with this
	 * Paginator.
	 * 
	 * @param	string	$name
	 * @param	array	$args
	 * @return	mixed
	 */
	public function __call($name, array $args = []) {
		$res = call_user_func_array([$this->getQuery(), $name], $args);
		
		return ($res instanceof ModelQuery) ? $this : $res;
	}
}