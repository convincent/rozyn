<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Pagination;

use Rozyn\Database\Query;

class QueryPaginator extends BasePaginator {
	/**
	 * Holds the Query object that is used to generate the pagination results.
	 * 
	 * @var	\Rozyn\Database\Query
	 */
	protected $query;
	
	/**
	 * Constructs the object.
	 * 
	 * @param	\Rozyn\Database\Query	$query
	 * @param	int						$perPage
	 * @param	string					$url
	 */
	public function __construct(Query $query, $perPage = null, $url = null) {
		$this->setQuery($query);
		$this->setPerPage($perPage);
		$this->setUrl($url);
	}
	
	/**
	 * Sets the Query object for this Paginator.
	 * 
	 * @param	\Rozyn\Database\Query $query
	 */
	public function setQuery(Query $query) {
		$this->query = $query;
	}
	
	/**
	 * Returns the Query object for this Paginator.
	 * 
	 * @return	\Rozyn\Database\Query
	 */
	public function getQuery() {
		return $this->query;
	}
	
	/**
	 * Execute the query to get the results belonging to a certain page.
	 * 
	 * @param	int						$page
	 * @return	\Rozyn\Data\Collection
	 */
	public function fetch($page) {
		$statement = clone $this->getQuery()->getStatement();
		
		$res = $this->getQuery()
					->offset(max(0, $page - 1) * $this->getPerPage())
					->limit($this->getPerPage())
					->fetchAll();
		
		$this->getQuery()->setStatement($statement);
		
		return $res;
	}
	
	/**
	 * Returns the total amount of items in this Paginator.
	 * 
	 * @return	int
	 */
	public function getTotal() {
		if ($this->total === null) {
			$this->total = $this->query->count();
		}
		
		return parent::getTotal();
	}
}