<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

use Rozyn\Model\Model;
use Rozyn\Database\ModelQuery;

abstract class SingleRecordRelation extends Relation {
	/**
	 * Returns whether or not this relation is a single record relation.
	 * 
	 * @return	boolean
	 */
	public function isSingleRecord() {
		return true;
	}
	
	/**
	 * Returns whether or not this relation is a multi record relation.
	 * 
	 * @return	boolean
	 */
	public function isMultiRecord() {
		return false;
	}
	
	/**
	 * Returns a representation of the related model in case there is none.
	 * 
	 * @return	null
	 */
	public function newEmpty() {
		return null;
	}
	
	/**
	 * Prepares an existing ModelQuery to include the table associated with this
	 * relation using JOINs.
	 * 
	 * @param	\Rozyn\Database\ModelQuery
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function prepareQueryJoins(ModelQuery $query) {
		$query->getStatement()->leftJoin($this->getRelated()->getAliasedTable(), array_merge($this->getConditions(), [$this->getAliasedLocalKey() . '=' . $this->getAliasedForeignKey()]));
		$query->bind($this->getParams());
		
		return $query;
	}
	
	/**
	 * Prepares an existing ModelQuery to include the related model's fields.
	 * 
	 * @param	\Rozyn\Database\ModelQuery
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function prepareQueryFields(ModelQuery $query) {
		$query->getStatement()->fields($this->getRelated()->getAliasedFields(), true);
		
		return $query;
	}
	
	/**
	 * Removes all of this relation's related data for the parent model before
	 * adding the $related model.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	public function sync(Model $related = null) {
		if ($related === null) {
			$this->clear();
		}
		
		else {
			$this->save($related);
		}
	}
	
	/**
	 * Syncs the parent instance with related instances matching the given id.
	 * 
	 * @param	int		$id
	 */
	public function syncId($id = null) {
		if ($id === null) {
			$this->clear();
		}
		
		else {
			// Retrieve the related instance based on the ID.
			$related = $this->getRelated()
							->newQuery()
							->select()
							->whereEquals($this->getRelated()->getAliasedPrimaryKey(), $id)
							->fetchModel();
			
			// Sync it.
			$this->sync($related);
		}
	}
	
	/**
	 * Set the given related instance on the parent model of this relation.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	public function set(Model $related) {
		$this->getParent()->set($this->getName(), $related);
	}
}