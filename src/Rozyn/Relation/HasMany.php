<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

use Rozyn\Model\Model;
use Rozyn\Database\ModelQuery;
use Rozyn\Model\Collection as ModelCollection;

class HasMany extends MultiRecordRelation  {
	use HasOneOrMany;
	
	/**
	 * Saves the related model and stores a relation between it and the parent
	 * model.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	public function save(Model $related) {
		$parent = $this->getParent();
		
		if (!$parent->exists()) {
			throw new ParentDoesNotExistException();
		}
		
		$related->set($this->getForeignKey(), $parent->id())->save();

		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		if (!$parent->get($this->getName())) {
			$parent->set($this->getName(), $related->newCollection());
		}
		
		$parent->get($this->getName())->add($related->id(), $related);
	}
	
	/**
	 * Saves all Models in a ModelCollection in relation to this relation's
	 * parent Model.
	 * 
	 * @param	ModelCollection $related
	 */
	public function saveAll(ModelCollection $related) {
		if (!$related->isEmpty()) {
			foreach ($related as $model) {
				// Establish the relation by setting the parent's foreignKey
				// column in the related model.
				$model->set($this->getForeignKey(), $this->getParent()->id());
			}
			
			// Store the data in the database.
			$related->save();

			// Make sure the actual parent model is immediately synchronized with 
			// the new data as well, so that subsequent save() calls on the model
			// don't undo what we did here.
			$this->getParent()->getRelationData($this->getName())->merge($related);
		}
	}
	
	/**
	 * Clears all relations between this model and its related data, but doens't
	 * actually delete any of the related models themselves.
	 * 
	 * @return	\Rozyn\Relation\HasMany
	 */
	public function clear() {
		$this->getRelated()
			 ->newQuery()
			 ->update()
			 ->whereEquals($this->getForeignKey(), $this->getParent()->id())
			 ->where($this->getConditionsWithoutAlias())
			 ->bind($this->getParams())
			 ->set($this->getForeignKey(), 0)
			 ->execute();
		
		// Update the data inside each model in the related collection.
		foreach ($this->getParent()->getRelationData($this->getName()) as $related) {
			$related->set($this->getForeignKey(), 0);
		}
		
		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		$this->getParent()->forceSet($this->getName(), $this->getRelated()->newCollection());
		
		return $this;
	}
	
	/**
	 * Removes all of this relation's related data for the parent model before
	 * adding the models contained in the $related ModelCollection.
	 * 
	 * @param	\Rozyn\Model\Collection	$related
	 */
	public function sync(ModelCollection $related) {
		// First remove all existing relations.
		$this->clear();
		
		// Then just save all related data.
		$this->saveAll($related);
	}
	
	/**
	 * Retrieves all related instances for any number of instances of the 
	 * relation's parent Model. The $ids array should contain the ids of the
	 * parent models whose related models should be loaded. The $with argument 
	 * should be in a format that is accepted by the ModelQuery::with() method.
	 * A $models array can also be passed along by reference, which will prompt
	 * this method to save any related records that are retrieved in this array
	 * as well.
	 * 
	 * @param	array	$ids
	 * @param	array	$with
	 * @param	array	$models
	 * @return	array
	 */
	public function loadBatchArray(array $ids, array $with = [], array &$models = null) {
		$foreignKey	= $this->getAliasedForeignKey();
		
		$result = [];
		if (!empty($ids)) {
			$query		= $this->getRelated()
							   ->newQuery()
							   ->select()
							   ->lazy()
							   ->with($with)
							   ->whereIn($foreignKey, $ids)
							   ->apply([$this, 'prepareQuery'])
							   // Set the ORDER BY clause explicitly,
							   // otherwise it doesn't work.
							   ->orderBy($this->getRelated()->getAliasedPrimaryKey(), 'ASC')
							   // We manually reset the limit after applying the
							   // prepareQuery method because limit only applies
							   // when we're retrieving related rows for a
							   // single instance, not a batch of them.
							   ->limit(null);

			$result = $query->fetchArrays();
		}
		
		if (!empty($models)) {
			// Set up some variables that we need in our foreach loops ahead.
			$name		= $this->getName();
			$foreignKey = $this->getForeignKey();
			
			foreach ($models as &$model) {
				$model[$name] = [];
			}
			
			foreach ($result as $id => $row) {
				$models[$row[$foreignKey]][$name][$id] = $row;
			}
			
			// Since we disregarded the limit clause in the original query,
			// we take it into account here so that we never add more
			// related rows to the parent model than the limit would allow.
			if (null !== ($limit = $this->getLimit())) {
				foreach ($models as &$model) {
					$model[$name] = array_slice($model[$name], 0, $limit, true);
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * Returns a ModelQuery object that's been prepared to query the subset of 
	 * related models in the related model's table.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function newQuery() {
		return $this->getRelated()
					->newQuery()
					->select()
					->whereEquals($this->getAliasedForeignKey(), $this->getParent()->id())
					->apply([$this, 'prepareQuery']);
	}
	
	/**
	 * Prepares an existing ModelQuery to include the table associated with this
	 * relation using JOINs.
	 * 
	 * @param	\Rozyn\Database\ModelQuery
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function prepareQueryJoins(ModelQuery $query) {
		$query->leftJoin($this->getRelated()->getAliasedTable(), $this->getAliasedForeignKey() . '=' . $this->getParent()->id())
			  ->bind($this->getParams());
		
		return $query;
	}
	
	/**
	 * Returns a subquery that can be used in an EXISTS clause.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function newExistsSubQuery() {
		return $this->getRelated()->newQuery()->select()->whereEquals($this->getParent()->getAliasedPrimaryKey(), $this->getAliasedForeignKey(), false);
	}
}