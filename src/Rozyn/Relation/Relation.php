<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

use Rozyn\Model\Model;
use Rozyn\Database\ModelQuery;

abstract class Relation {
	/**
	 * The parent model instance.
	 * 
	 * @var	\Rozyn\Model
	 */
	protected $parent;
	
	/**
	 * The related model instance.
	 * 
	 * @var	\Rozyn\Model 
	 */
	protected $related;
	
	/**
	 * The foreign key.
	 * 
	 * @var	string
	 */
	protected $foreignKey;
	
	/**
	 * The local key.
	 * 
	 * @var	string
	 */
	protected $localKey;
	
	/**
	 * Holds any WHERE conditions that should be applied to select related rows
	 * 
	 * @var	array
	 */
	protected $conditions = [];
	
	/**
	 * Holds an optional ORDER BY clause for the relation.
	 * 
	 * @var	string|array
	 */
	protected $orderBy;
	
	/**
	 * Holds an optional LIMIT clause for the relation.
	 * 
	 * @var	int
	 */
	protected $limit;
	
	/**
	 * Holds any query parameters that need to be bound to relation queries.
	 * 
	 * @var	array
	 */
	protected $params = [];
	
	/**
	 * Creates a new instance of a Relation.
	 * 
	 * @param	string				$name
	 * @param	\Rozyn\Model\Model	$parent
	 * @param	\Rozyn\Model\Model	$related
	 * @param	array				$options
	 */
	public function __construct($name = '', 
								Model $parent = null, 
								Model $related = null, 
								array $options = []) {
		
		$this->setName($name);
		$this->setParent($parent);
		$this->setRelated($related);
		
		foreach ($options as $option => $value) {
			$this->{'set' . snake2camel($option)}($value);
		}
	}
	
	/**
	 * Sets an alias on the related model that will be used in queries to the
	 * database.
	 * 
	 * @param	string	$alias
	 */
	public function setAlias($alias) {
		$this->getRelated()->setAlias($alias);
	}
	
	/**
	 * Sets the foreign key for this relation.
	 * 
	 * @param	string	$foreignKey
	 */
	public function setForeignKey($foreignKey) {
		$this->foreignKey = $foreignKey;
	}
 	
	/**
	 * Returns the key in the related model's table that identifies the parent 
	 * model.
	 * 
	 * @return	string
	 */
	public function getForeignKey() {
		if (null === $this->foreignKey) {
			$this->foreignKey = $this->inferForeignKey();
		}
		
		return $this->foreignKey;
	}
	
	/**
	 * Sets the local key for this relation.
	 * 
	 * @param	string	$localKey
	 */
	public function setLocalKey($localKey) {
		$this->localKey = $localKey;
	}
	
	/**
	 * Returns the key in the parent model's table that identifies the related 
	 * model.
	 * 
	 * @return	string
	 */
	public function getLocalKey() {
		if (null === $this->localKey) {
			$this->localKey = $this->inferLocalKey();
		}
		
		return $this->localKey;
	}
		
	/**
	 * Set the parent model.
	 * 
	 * @param	\Rozyn\Model\Model	$parent
	 */
	public function setParent(Model $parent) {
		$this->parent = $parent;
	}
	
	/**
	 * Set the related model.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	public function setRelated(Model $related) {
		$this->related = $related;
	}
		
	/**
	 * Returns the stored instance of the parent model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function getParent() {
		return $this->parent;
	}
	
	/**
	 * Returns the stored instance of the related model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function getRelated() {
		return $this->related;
	}
	
	/**
	 * Returns the name (identifier) of this relation. This name is used in the
	 * parent model to store the related data.
	 * 
	 * @return	string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Set the name (identifier) for this relation.
	 * 
	 * @param	string	$name
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	/**
	 * Adds a condition to relation.
	 * 
	 * @param	string|array	$condition
	 */
	public function addCondition($condition) {
		$this->conditions[] = (is_string($condition)) ? 
								[$condition] : 
								 $condition;
	}
	
	/**
	 * Set the WHERE clause for this relation.
	 * 
	 * @param	string|array	$conditions
	 */
	public function setConditions($conditions) {
		$this->conditions = (is_string($conditions)) ? 
								[$conditions] : 
								 $conditions;
	}
	
	/**
	 * Get the WHERE clause for this relation.
	 * 
	 * @return	array
	 */
	public function getConditions() {
		$conditions = $this->conditions;
		
		if ($this->getRelated()->isSoft()) {
			$conditions[] = "({$this->getRelated()->getAliasedSoftDeleteField()} = 0 OR {$this->getRelated()->getAliasedSoftDeleteField()} IS NULL)";
		}
		
		return $conditions;
	}
	
	/**
	 * Get the WHERE clause for this relation without aliases so that it can be
	 * used in UPDATE and DELETE queries.
	 * 
	 * @return	array
	 */
	public function getConditionsWithoutAlias() {
		$conditions = $this->getConditions();
		$aliases = [$this->getParent()->getAlias(), $this->getRelated()->getAlias()];
		
		foreach ($conditions as &$condition) {
			foreach ($aliases as $alias) {
				$condition = preg_replace('/\b' . preg_quote($alias, '/') . '\./', '', $condition);
			}
		}
		
		return $conditions;
	}
	
	/**
	 * Drop the WHERE clause for this relation.
	 * 
	 * @return	array
	 */
	public function dropConditions() {
		$this->conditions = [];
	}
	
	/**
	 * Set the LIMIT clause for this relation.
	 * 
	 * @param	int
	 */
	public function setLimit($limit) {
		$this->limit = $limit;
	}
	
	/**
	 * Get the LIMIT clause for this relation.
	 * 
	 * @return	int
	 */
	public function getLimit() {
		return $this->limit;
	}
	
	/**
	 * Set the ORDER BY clause for this relation.
	 * 
	 * @param	string|array
	 */
	public function setOrderBy($orderBy) {
		$this->orderBy = $orderBy;
	}
	
	/**
	 * Get the ORDER BY clause for this relation.
	 * 
	 * @return	string|array
	 */
	public function getOrderBy() {
		return $this->orderBy;
	}
	
	/**
	 * Add a parameter to the relation's query.
	 * 
	 * @param	string	$key
	 * @param	mixed	$value
	 */
	public function addParam($key, $value) {
		$this->params[$key] = $value;
	}
	
	/**
	 * Sets the parameters for this relation's query. If $merge is set to true,
	 * the new parameters are merged with the old parameters, otherwise the old
	 * parameters are thrown away.
	 * 
	 * @param	array	$vars
	 * @param	boolean	$merge
	 */
	public function setParams(array $vars, $merge = false) {
		$this->params = ($merge) ? array_merge($this->params, $vars) : $vars;
	}
	
	/**
	 * Get the parameters bound to this relation's query.
	 * 
	 * @return	array
	 */
	public function getParams() {
		return $this->params;
	}
	
	/**
	 * Get a specific parameter bound to this relation's query.
	 * 
	 * @param	string	$param
	 * @param	mixed	$default
	 * @return	mixed
	 */
	public function getParam($param, $default = null) {
		return (isset($this->params[$param])) ? $this->params[$param] : $default;
	}
	
	/**
	 * Takes a Query that retrieves related data as a parameter and prepares it
	 * for actual use. This means that it sets things like conditions, limits,
	 * order by clauses, etc. on the query.
	 *
	 * @param	\Rozyn\Database\ModelQuery	$query
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function prepareQuery(ModelQuery $query) {
		$query->where($this->getConditions());
		
		if ($this->getOrderBy() !== null) {
			$query->orderBy($this->getOrderBy());
		}
		
		if ($this->getLimit() !== null) {
			$query->limit($this->getLimit());
		}
		
		$query->bind($this->getParams());
		
		return $query;
	}
	
	/**
	 * Retrieves related model data from the database for an array of IDs of 
	 * models of the same type as the parent model. Returns the retrieved data 
	 * as a Model Collection.
	 * 
	 * @param	array					$ids
	 * @return	\Rozyn\Model\Model|Rozyn\Model\Collection|null
	 */
	public function loadBatch(array $ids) {
		return $this->getRelated()
					->newQuery()
					->objectify($this->loadBatchArray($ids));
	}
	
	/**
	 * Saves the related instance matching the given id.
	 * 
	 * @param	int		$id
	 */
	public function saveId($id) {
		// Retrieve the related instance based on the ID.
		$related = $this->getRelated()
						->newQuery()
						->select()
						->whereEquals($this->getRelated()->getAliasedPrimaryKey(), $id)
						->fetchModel();
		
		$this->save($related);
	}
	
	/**
	 * Syncs the parent instance with related instances matching the given id.
	 * 
	 * @param	int		$id
	 */
	abstract public function syncId($id = null);
	
	/**
	 * Returns whether or not this relation is a single record relation.
	 * 
	 * @return	boolean
	 */
	abstract public function isSingleRecord();
	
	/**
	 * Returns whether or not this relation is a multi record relation.
	 * 
	 * @return	boolean
	 */
	abstract public function isMultiRecord();

	/**
	 * Return a new empty ModelCollection based on the related model if this 
	 * Relation symbolizes a multi record relationship. If it symbolizes a 
	 * single record relationship, null is returned.
	 * 
	 * @return	\Rozyn\Model\Collection|null
	 */
	abstract public function newEmpty();
	
	/**
	 * Returns the foreign key prepended with an alias for a query.
	 * 
	 * @return	string
	 */
	abstract public function getAliasedForeignKey();
	
	/**
	 * Returns the local key prepended with an alias for a query.
	 * 
	 * @return	string
	 */
	abstract public function getAliasedLocalKey();
	/**
	 * Guesses the appropriate foreign key for this relation.
	 * 
	 * @return	string
	 */
	abstract public function inferForeignKey();
	
	/**
	 * Guesses the appropriate local key for this relation.
	 * 
	 * @return	string
	 */
	abstract public function inferLocalKey();
	
	/**
	 * Clears all relations between this model and its related data, but doens't
	 * actually delete any of the related models themselves.
	 * 
	 * @return	\Rozyn\Relation\Relation
	 */
	abstract public function clear();
	
	/**
	 * Retrieves the related model data from the database and stores it in the
	 * parent model.
	 * 
	 * @return	\Rozyn\Model\Model|\Rozyn\Model\Collection
	 */
	abstract public function load();
	
	/**
	 * Retrieves all related instances for any number of instances of the 
	 * relation's parent Model. The $ids array should contain the ids of the
	 * parent models whose related models should be loaded. The $with argument 
	 * should be in a format that is accepted by the ModelQuery::with() method.
	 * A $models array can also be passed along by reference, which will prompt
	 * this method to save any related records that are retrieved in this array
	 * as well.
	 * 
	 * @param	array	$ids
	 * @param	array	$with
	 * @param	array	$models
	 * @return	array
	 */
	abstract public function loadBatchArray(array $ids, 
											array $with = [], 
											array &$models = null);
	
	/**
	 * Saves the related model and stores a relation between it and the parent
	 * model.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	abstract public function save(Model $related);
	
	/**
	 * Prepares an existing ModelQuery to include the table associated with this
	 * relation using JOINs.
	 * 
	 * @param	\Rozyn\Database\ModelQuery
	 * @return	\Rozyn\Database\ModelQuery
	 */
	abstract public function prepareQueryJoins(ModelQuery $query);
}