<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

use Rozyn\Model\Collection as ModelCollection;

abstract class MultiRecordRelation extends Relation {
	/**
	 * Returns whether or not this relation is a single record relation.
	 * 
	 * @return	boolean
	 */
	public function isSingleRecord() {
		return false;
	}
	
	/**
	 * Returns whether or not this relation is a multi record relation.
	 * 
	 * @return	boolean
	 */
	public function isMultiRecord() {
		return true;
	}
	
	/**
	 * Syncs the parent instance with related instances matching the given id.
	 * 
	 * @param	int		$id
	 */
	public function syncId($id = null) {
		if ($id === null) {
			$this->clear();
		}
		
		else {
			$this->syncIds([$id]);
		}
	}
	
	/**
	 * Syncs the parent instance with related instances matching the given set
	 * of ids.
	 * 
	 * @param	array	$ids
	 */
	public function syncIds(array $ids = []) {
		if (empty($ids)) {
			$this->clear();
		}
		
		else {
			// Retrieve the related models.
			$related = $this->getRelated()
							->newQuery()
							->select()
							->whereIn($this->getRelated()->getAliasedPrimaryKey(), $ids)
							->fetchModels();
			
			// Sync them.
			$this->sync($related);
		}
	}
	
	/**
	 * Saves related instances matching the given set of ids.
	 * 
	 * @param	array	$ids
	 */
	public function saveIds(array $ids = []) {
		// Retrieve the related models.
		$related = $this->getRelated()
						->newQuery()
						->select()
						->whereIn($this->getRelated()->getAliasedPrimaryKey(), $ids)
						->fetchModels();

		// Sync them.
		$this->saveAll($related);
	}
	
	/**
	 * Returns a subquery that can be used in an EXISTS clause.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function newExistsQuery() {
		return $this->newExistsSubQuery();
	}
	
	/**
	 * Returns a new, empty ModelCollection based on the related model.
	 * 
	 * @return	\Rozyn\Model\Collection
	 */
	public function newEmpty() {
		return $this->getRelated()->newCollection();
	}
	
	/**
	 * Set the given related instances on the parent model of this relation.
	 * 
	 * @param	\Rozyn\Model\Collection	$related
	 */
	public function set(ModelCollection $related) {
		$this->getParent()->set($this->getName(), $related);
	}
	
	/**
	 * Load all related data for the parent model and return it.
	 * 
	 * @return	\Rozyn\Model\Collection
	 */
	public function load() {
		$localId = $this->getParent()->id();
		$models = [$localId => []];
		$this->loadBatchArray([$this->getParent()->id()], [], $models);
		
		$result = $this	->getRelated()
						->newQuery()
						->objectify($models[$localId][$this->getName()],
									$this->getRelated(),
									$this);
		
		$this->getParent()->set($this->getName(), $result);
		
		return $result;
	}
	
	/**
	 * Removes all of this relation's related data for the parent model before
	 * adding the models contained in the $related ModelCollection.
	 * 
	 * @param	\Rozyn\Model\Collection	$related
	 */
	abstract public function sync(ModelCollection $related);
	
	/**
	 * Saves the related data in the database and makes sure everything is set
	 * properly to ensure the relation between the parent model and the related 
	 * models stays intact.
	 * 
	 * @param	\Rozyn\Model\Collection	$related
	 */
	abstract public function saveAll(ModelCollection $related);
	
	/**
	 * Returns a ModelQuery object that's been prepared to perform a SELECT 
	 * query on the subset of related models in the related model's table.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	abstract public function newQuery();
	
	/**
	 * Returns a subquery that can be used in an EXISTS clause.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	abstract public function newExistsSubQuery();
}