<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

use Rozyn\Model\Model;

class HasOne extends SingleRecordRelation {
	use HasOneOrMany;
	
	/**
	 * Saves the related model and stores a relation between it and the parent
	 * model.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	public function save(Model $related) {
		$parent = $this->getParent();
		
		if ($related->get($parent->getForeignKey()) !== $parent->id()) {
			// Since this is a one-to-one relation, there can't be any other row 
			// in the related table with the same foreign key value, so make 
			// sure that's true before we add our new row.
			$this->clear();

			// Store the ID of the parent model in the related model.
			$related->set($parent->getForeignKey(), $parent->id());
		}
		
		// Update the related model.
		$related->save();

		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		$this->getParent()->set($this->getName(), $related);
	}
	
	/**
	 * Clears all relations between this model and its related data, but doesn't
	 * actually delete any of the related models themselves.
	 * 
	 * @return	\Rozyn\Relation\HasOne
	 */
	public function clear() {
		$this->getRelated()
			 ->newQuery()
			 ->update()
			 ->set($this->getForeignKey(), 0)
			 ->whereEquals($this->getForeignKey(), $this->getParent()->id())
			 ->where($this->getConditionsWithoutAlias())
			 ->bind($this->getParams())
			 ->execute();
		
		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		$this->getParent()->set($this->getName(), null);
		
		return $this;
	}
	
	/**
	 * Retrieves the related model data from the database and stores it in the
	 * parent model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function load() {
		$model = $this->getRelated()
					  ->newQuery()
					  ->select()
					  ->whereEquals($this->getAliasedForeignKey(), 
									$this->getParent()->id())
					  ->apply([$this, 'prepareQuery'])
					  ->fetchModel();
		
		$this->getParent()->set($this->getName(), $model);
		
		return $model;
	}
	
	/**
	 * Retrieves all related instances for any number of instances of the 
	 * relation's parent Model. The $ids array should contain the ids of the
	 * parent models whose related models should be loaded. The $with argument 
	 * should be in a format that is accepted by the ModelQuery::with() method.
	 * A $models array can also be passed along by reference, which will prompt
	 * this method to save any related records that are retrieved in this array
	 * as well.
	 * 
	 * @param	array	$ids
	 * @param	array	$with
	 * @param	array	$models
	 * @return	array
	 */
	public function loadBatchArray(	array $ids, 
									array $with = [], 
									array &$models = null) {
		
		$foreignKey	= $this->getAliasedForeignKey();
		
		$result = [];
		if (!empty($ids)) {
			$query		= $this->getRelated()
							   ->newQuery()
							   ->select()
							   ->lazy()
							   ->with($with)
							   ->whereIn($foreignKey, $ids)
							   ->apply([$this, 'prepareQuery']);

			$result = $query->fetchArrays();
		}
		
		if (!empty($models)) {
			foreach ($models as &$model) {
				$model[$this->getName()] = null;
			}
			
			foreach ($result as $row) {
				$models[$row[$this->getForeignKey()]][$this->getName()] = $row;
			}
		}
		
		return $result;
	}
}