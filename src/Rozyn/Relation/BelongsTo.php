<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

use Rozyn\Model\Model;

class BelongsTo extends SingleRecordRelation  {
	/**
	 * Saves the related model and stores a relation between it and the parent
	 * model.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	public function save(Model $related) {
		// Save the related model first
		$related->save();
		
		// Then update the parent model with the (new) ID of the related model.
		$this->getParent()->set($this->getForeignKey(), $related->id())->save();

		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		$this->getParent()->set($this->getName(), $related);
	}
	
	/**
	 * Returns the foreign key prepended with an alias for a query.
	 * 
	 * @return	string
	 */
	public function getAliasedForeignKey() {
		return $this->getParent()->getAliasedField($this->getForeignKey());
	}
	
	/**
	 * Returns the local key prepended with an alias for a query.
	 * 
	 * @return	string
	 */
	public function getAliasedLocalKey() {
		return $this->getRelated()->getAliasedField($this->getLocalKey());
	}
	
	/**
	 * Guesses the appropriate foreign key for this relation.
	 * 
	 * @return	string
	 */
	public function inferForeignKey() {
		return $this->related->getForeignKey();
	}
	
	/**
	 * Guesses the appropriate local key for this relation.
	 * 
	 * @return	string
	 */
	public function inferLocalKey() {
		return $this->related->getPrimaryKey();
	}
	
	/**
	 * Clears all relations between this model and its related data, but doens't
	 * actually delete any of the related models themselves.
	 * 
	 * @return	\Rozyn\Relation\BelongsTo
	 */
	public function clear() {
		$this->getParent()->set($this->getForeignKey(), 0)->save();
		
		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		$this->getParent()->set($this->getName(), null);
		
		return $this;
	}
	
	/**
	 * Retrieves the related model data from the database and stores it in the
	 * parent model.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	public function load() {
		$model = $this->getRelated()
					  ->newQuery()
					  ->select()
					  ->whereEquals($this->getAliasedLocalKey(), 
									$this->getParent()->get($this->getForeignKey()))
					  ->apply([$this, 'prepareQuery'])
					  ->fetchModel();
		
		$this->getParent()->set($this->getName(), $model);
		
		return $model;
	}
	
	/**
	 * Retrieves all related instances for any number of instances of the 
	 * relation's parent Model. The $ids array should contain the ids of the
	 * parent models whose related models should be loaded. The $with argument 
	 * should be in a format that is accepted by the ModelQuery::with() method.
	 * A $models array can also be passed along by reference, which will prompt
	 * this method to save any related records that are retrieved in this array
	 * as well.
	 * 
	 * @param	array	$ids
	 * @param	array	$with
	 * @param	array	$models
	 * @return	array
	 */
	public function loadBatchArray(	array $ids, 
									array $with = [], 
									array &$models = null) {
		
		// Retrieve the parent model.
		$parent	= $this->getParent();
		$result = [];
		
		if (!empty($ids)) {
			$query		= $this->getRelated()
							   ->newQuery()
							   ->select()
							   ->lazy()
							   ->with($with)
							   ->leftJoin($parent->getAliasedTable(), $this->getAliasedForeignKey() . '=' .  $this->getAliasedLocalKey())
							   ->whereIn($parent->getAliasedPrimaryKey(), $ids)
							   ->apply([$this, 'prepareQuery']);

			$result = $query->fetchArrays();
		}
		
		if (null !== $models) {
			$name		= $this->getName();
			$foreignKey = $this->getForeignKey();
			
			foreach ($models as &$model) {
				$model[$name] = (isset($result[$model[$foreignKey]])) ? 
									$result[$model[$foreignKey]] : 
									null;
			}
		}
		
		return $result;
	}
}