<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

use PDOException;
use Rozyn\Model\Model;
use Rozyn\Model\JoinModel;
use Rozyn\Database\ModelQuery;
use Rozyn\Model\Collection as ModelCollection;

class HasAndBelongsToMany extends MultiRecordRelation {
	/**
	 * The name of the table used to join related records together.
	 * 
	 * @var	string
	 */
	protected $joinTable = null;
	
	/**
	 * An optional alias for the join table to prevent table name collisions.
	 * 
	 * @var	string
	 */
	protected $joinTableAlias = null;
	
	/**
	 * Holds a JoinModel for the join table.
	 * 
	 * @var	\Rozyn\Model\JoinModel
	 */
	protected $joinModel = null;
	
	/**
	 * Saves the related model and stores a relation between it and the parent
	 * model.
	 * 
	 * @param	\Rozyn\Model\Model	$related
	 */
	public function save(Model $related) {
		// If the parent doesn't exist, abort.
		if (!$this->getParent()->exists()) {
			throw new ParentDoesNotExistException();
		}
		
		// First, save the related model.
		$related->save();
		
		// Remove a previous relation link if it existed.
		$related->newPlainQuery()
				->delete()
				->from($this->getJoinTable())
				->whereEquals($this->getLocalKey(), $this->getParent()->id())
				->whereEquals($this->getForeignKey(), $related->id())
				->execute();
		
		// Set up the query to update the join table.
		$related->newPlainQuery()
				->insert()
				->into($this->getJoinTable())
				->fields($this->getJoinTableFields())
				->values($this->getJoinTableValues($related))
				->execute();

		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		if (!$this->getParent()->get($this->getName())) {
			$this->getParent()->set($this->getName(), $this->getRelated()->newCollection());
		}
		
		$this->getParent()->get($this->getName())->add($related->id(), $related);
	}
	
	/**
	 * Saves all Models in a ModelCollection in relation to this relation's
	 * parent Model.
	 * 
	 * @param	\Rozyn\Model\Collection	$related
	 */
	public function saveAll(ModelCollection $related) {
		if (!$related->isEmpty()) {
			// Check which relations between the parent and related models 
			// already exist.
			$check = $this	->newQuery()
							->fields([])
							->fetchList($this->getAliasedForeignKey());
			
			// Loop through each model in the collection and add the values of 
			// each model to the query we prepared earlier.
			$this->getRelated()->transact(function() use ($related) {
				foreach ($related as $model) {
					$model->save();
				}
			});
			
			// Set up the query to update the join table.
			$joinTableQuery = $related->newPlainQuery()
									  ->insert()
									  ->into($this->getJoinTable())
									  ->fields($this->getJoinTableFields());
			
			// Loop through each model in the collection and add the values of 
			// each model to the query we prepared earlier.
			foreach ($related as $model) {
				if (!in_array($model->id(), $check)) {
					// If no relation between the model and the parent model
					// exists yet, add a record to the join table.
					$joinTableQuery->values($this->getJoinTableValues($model));
				}
			}
			
			if (count($joinTableQuery->values()) > 0) {
				$joinTableQuery->execute();

				// Make sure the actual parent model is immediately synchronized 
				// with the new data as well, so that subsequent save() calls on 
				// the model don't undo what we did here.
				$this->getParent()->getRelationData($this->getName())->merge($related);
			}
		}
	}
	
	/**
	 * Clears all relations between this model and its related data, but doens't
	 * actually delete any of the related models themselves.
	 * 
	 * @return	\Rozyn\Relation\HasAndBelongsToMany
	 */
	public function clear() {
		$this->getRelated()
			 ->newPlainQuery()
			 ->delete()
			 ->from($this->getJoinTable())
			 ->whereEquals($this->getLocalKey(), $this->getParent()->id())
			 ->execute();
		
		// Make sure the actual parent model is immediately synchronized with 
		// the new data as well, so that subsequent save() calls on the model
		// don't undo what we did here.
		$this->getParent()->set($this->getName(), $this->getRelated()->newCollection());
		
		return $this;
	}
	
	/**
	 * Removes all of this relation's related data for the parent model before
	 * adding the models contained in the $related ModelCollection.
	 * 
	 * @param	\Rozyn\Model\Collection	$related
	 */
	public function sync(ModelCollection $related) {
		// Clear all data from the join table that has to do with this record 
		// before we (re-)add the new relations.
		$this->clear();
		
		// Save all related data.
		$this->saveAll($related);
	}
	
	/**
	 * Retrieves all related instances for any number of instances of the 
	 * relation's parent Model. The $ids array should contain the ids of the
	 * parent models whose related models should be loaded. The $with argument 
	 * should be in a format that is accepted by the ModelQuery::with() method.
	 * A $models array can also be passed along by reference, which will prompt
	 * this method to save any related records that are retrieved in this array
	 * as well.
	 * 
	 * @param	int[]	$ids
	 * @param	array	$with
	 * @param	array	$models
	 * @return	array
	 */
	public function loadBatchArray(array $ids, array $with = [], array &$models = null) {
		$related = $this->getRelated();
		$rows = [];
		
		if (!empty($ids)) {
			$query = $related->newQuery()
							 ->select()
							 ->lazy()
							 ->with($with)
							 ->leftJoin($this->getAliasedJoinTable(), $related->getAliasedPrimaryKey() . ' = ' . $this->getAliasedForeignKey())
							 ->whereIn($this->getAliasedLocalKey(), $ids)
							 ->apply([$this, 'prepareQuery'])
							   // We manually reset the limit after applying the
							   // prepareQuery method because limit only applies
							   // when we're retrieving related rows for a
							   // single instance, not a batch of them.
							 ->limit(null);

			$rows = $query->fetchArrays();
			
			// Set up some variables that we may need in loops later on.
			$name		= $this->getName();
			$foreignKey = $this->getForeignKey();
			$localKey	= $this->getLocalKey();
			
			// Retrieve the relevant rows from the join table. We can use these 
			// to match each related row to one or more of the models.
			$joinTableQuery = $related->newPlainQuery()
									 ->select($this->getJoinTableFields())
									 ->from($this->getJoinTable())
									 ->whereIn($localKey, $ids)
									 // Set the ORDER BY clause explicitly,
									 // otherwise it doesn't work.
									 ->orderBy('id', 'ASC');
			
			// Since not all join tables necessarily have an ID attribute, we
			// have to put the query in a try/catch statement. If it fails, we 
			// simply execute the query again, but without the ORDER BY clause.
			try {
				$joinTableRows = $joinTableQuery->fetchAll();
				
				// Order the originally retrieved rows based on the order of IDs 
				// in the joinTableRows.
				$ordered = [];
				foreach ($joinTableRows as $i => $joinTableRow) {
					if (isset($rows[$joinTableRow[$foreignKey]])) {
						$ordered[$joinTableRow[$foreignKey]] = $rows[$joinTableRow[$foreignKey]];
					}
				}

				// Overwrite the original rows array by the ordered one.
				$rows = &$ordered;
			} catch (PDOException $e) {
				$joinTableRows = $joinTableQuery->removeOrderBy('id')
												->fetchAll();
			}
			
			if ($this->hasJoinModel()) {
				foreach ($joinTableRows as $row) {
					$foreignId = $row[$foreignKey];
					if (array_key_exists($foreignId, $rows)) {
						$rows[$foreignId][JoinModel::JOIN_MODEL_KEY] = $row;
					}
				}
			}
				
			if (!empty($models)) {
				foreach ($models as &$model) {
					$model[$name] = [];
				}
				
				foreach ($joinTableRows as $row) {
					$foreignId = $row[$foreignKey];
					
					if (array_key_exists($foreignId, $rows)) {
						$models[$row[$localKey]][$name][$foreignId] = $rows[$foreignId];
					}
				}

				// Since we disregarded the limit clause in the original query,
				// we take it into account here so that we never add more
				// related rows to the parent model than the limit would allow.
				if (null !== ($limit = $this->getLimit())) {
					foreach ($models as &$model) {
						$model[$name] = array_slice($model[$name], 0, $limit, true);
					}
				}
			}
		}
		
		return $rows;
	}
	
	/**
	 * Returns a ModelQuery object that's been prepared to query the subset of 
	 * related models in the related model's table.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function newQuery() {
		$related = $this->getRelated();
		
		return  $related->newQuery()
						->select()
						->lazy()
						->table($related->getAliasedTable())
						->leftJoin($this->getAliasedJoinTable(), $related->getAliasedPrimaryKey() . ' = ' . $this->getAliasedForeignKey())
						->apply([$this, 'prepareQuery'])
						->whereEquals($this->getAliasedLocalKey(), $this->getParent()->id());
	}
	
	/**
	 * Returns a subquery that can be used in an EXISTS clause.
	 * 
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function newExistsSubQuery() {
		$related = $this->getRelated();
		
		return $related->newQuery()
					   ->select()
					   ->leftJoin($this->getAliasedJoinTable(), $related->getAliasedPrimaryKey() . ' = ' . $this->getAliasedForeignKey())
					   ->whereEquals($this->getParent()->getAliasedPrimaryKey(), $this->getAliasedLocalKey(), false);
	}
	
	/**
	 * Returns whether or not this relation has a JoinModel.
	 * 
	 * @return	boolean
	 */
	public function hasJoinModel() {
		return $this->joinModel !== null;
	}
	
	/**
	 * Returns the JoinModel for this relation.
	 * 
	 * @return	\Rozyn\Model\JoinModel
	 */
	public function getJoinModel() {
		return $this->joinModel;
	}
	
	/**
	 * Set the JoinModel for this relation. The table for the JoinModel is also
	 * used as the join table.
	 * 
	 * @param	\Rozyn\Model\JoinModel|string	$model
	 */
	public function setJoinModel($model) {
		// If the specified $model is actually a string, treat it as the class
		// name for the join model. We can then create a new instance of this
		// class.
		if (is_string($model)) {
			$model = new $model();
		}
		
		$this->joinModel = $model;
		
		// Use the table defined in the JoinModel as the join table for this
		// relation, unless one has already been defined.
		if ($this->joinTable === null) {
			$this->setJoinTable($model->getTable());
		}
	}
	
	/**
	 * Set the name of the join table.
	 * 
	 * @param	string	$joinTable
	 */
	public function setJoinTable($joinTable) {
		$this->joinTable = $joinTable;
	}
	
	/**
	 * Returns the name of the join table or throws a RelationException if no
	 * join table is specified.
	 * 
	 * @return	string
	 * @throws	\Rozyn\Relation\RelationException
	 */
	public function getJoinTable() {
		// If no join table was explicitly defined, try to infer it.
		if ($this->joinTable === null) {
			$this->joinTable = $this->inferJoinTable();
		}
		
		return $this->joinTable;
	}
	
	/**
	 * Tries to guess the name of the join table.
	 * 
	 * @return	string
	 */
	public function inferJoinTable() {
		// Retrieve the names of the two model tables involved.
		$parent = $this->getParent()->getTable();
		$related = $this->getRelated()->getTable();
		
		// Concatenate them in alphabetical order, so the result is the same
		// when this method would be called on the inverse relation.
		return (strcasecmp($parent, $related) < 0) ?
					$parent	 . '_' . $related :
					$related . '_' . $parent;
	}
	
	/**
	 * Returns all the fields for the join table.
	 * 
	 * @return	string[]
	 */
	public function getJoinTableFields() {
		// If this relation has a join model, simply return the fields defined
		// there.
		if ($this->hasJoinModel()) {
			return $this->getJoinModel()->getFields();
		}
		
		// If not, we we assume that the join table only uses the local key and
		// the foreign key as its columns.
		return [$this->getLocalKey(), $this->getForeignKey()];
	}
	
	/**
	 * Returns the values to be inserted in the join table for a given related
	 * model.
	 * 
	 * @return	array
	 */
	public function getJoinTableValues(Model $related) {
		// Check if the related model has a join model, in which case we can
		// simply take the values stored in there.
		if ($related->hasJoinModel()) {
			return $related->getJoinModel()->getDataForQuery();
		}
		
		// If not, we have to build an array of values of our own. By default
		// we're only interested in the parent ID and the related ID.
		$res = [$this->getParent()->id(), $related->id()];
		
		// If this relation does support join models, but the related model
		// simply doesn't have one yet, we need to pad our values array with
		// null values so that the number of values matches the expected number
		// of fields as defined in this relation's join model.
		if ($this->hasJoinModel()) {
			return array_merge($res, array_fill(0, max(0, count($this->getJoinModel()->getFields()) - 2), null));
		}
		
		return $res;
	}
	
	/**
	 * Specify any additional fields of the join table.
	 * 
	 * @param	string[]	$fields
	 */
	public function setJoinTableFields(array $fields) {
		$this->joinTableFields = $fields;
	}
	
	/**
	 * Specify an alias for the join table.
	 * 
	 * @param	string	$alias
	 */
	public function setJoinTableAlias($alias) {
		$this->joinTableAlias = $alias;
	}
	
	/**
	 * Returns the alias for the join table.
	 * 
	 * @return	string
	 */
	public function getJoinTableAlias() {
		return $this->joinTableAlias ?: $this->joinTableAlias = snake2camel($this->joinTable);
	}
	
	/**
	 * Returns the aliased join table ready for use in queries.
	 * 
	 * @return	string
	 */
	public function getAliasedJoinTable() {
		return $this->getJoinTable() . ' AS ' . $this->getJoinTableAlias();
	}
	
	/**
	 * Guess the local key for this relation.
	 * 
	 * @return	string
	 */
	public function inferLocalKey() {
		return $this->getParent()->getForeignKey();
	}
	
	/**
	 * Guess the foreign key for this relation.
	 * 
	 * @return	string
	 */
	public function inferForeignKey() {
		return $this->getRelated()->getForeignKey();
	}
	
	/**
	 * Returns the aliased local key ready for use in queries.
	 * 
	 * @return	string
	 */
	public function getAliasedLocalKey() {
		return $this->getJoinTableAlias() . '.' . $this->getLocalKey();
	}
	
	/**
	 * Returns the aliased foreign key ready for use in queries.
	 * 
	 * @return	string
	 */
	public function getAliasedForeignKey() {
		return $this->getJoinTableAlias() . '.' . $this->getForeignKey();
	}
	
	/**
	 * Prepares an existing ModelQuery to include the table associated with this
	 * relation using JOINs.
	 * 
	 * @param	\Rozyn\Database\ModelQuery
	 * @return	\Rozyn\Database\ModelQuery
	 */
	public function prepareQueryJoins(ModelQuery $query) {
		$related = $this->getRelated();
		
		$query->leftJoin($this->getAliasedJoinTable(), $this->getParent()->getAliasedPrimaryKey() . ' = ' . $this->getAliasedLocalKey())
			  ->leftJoin($related->getAliasedTable(), array_merge($this->getConditions(), [$related->getAliasedPrimaryKey() . ' = ' . $this->getAliasedForeignKey()]))
			  ->bind($this->getParams());
		
		return $query;
	}
}