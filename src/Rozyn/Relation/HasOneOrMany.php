<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozyn\Relation;

trait HasOneOrMany {
	/**
	 * Returns the foreign key prepended with an alias for a query.
	 * 
	 * @return	string
	 */
	public function getAliasedForeignKey() {
		return $this->getRelated()->getAliasedField($this->getForeignKey());
	}
	
	/**
	 * Returns the local key prepended with an alias for a query.
	 * 
	 * @return	string
	 */
	public function getAliasedLocalKey() {
		return $this->getParent()->getAliasedField($this->getLocalKey());
	}
	/**
	 * Guesses the appropriate foreign key for this relation.
	 * 
	 * @return	string
	 */
	public function inferForeignKey() {
		return $this->parent->getForeignKey();
	}
	
	/**
	 * Guesses the appropriate local key for this relation.
	 * 
	 * @return	string
	 */
	public function inferLocalKey() {
		return $this->parent->getPrimaryKey();
	}
}