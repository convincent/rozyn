<?php $this->style('reset', 'main', 'login', 'form'); ?>

<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->getStyleSheetTags(); ?>
		
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

		<title><?php echo t('Admin Panel - Login'); ?></title>
	</head>

	<body>
		<div id="wrapper">
			<div id="content">
				<?php echo $this->getContent(); ?>
			</div>
		</div>

		<?php echo $this->getScriptTags(); ?>
	</body>
</html>