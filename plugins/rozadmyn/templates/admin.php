<?php
	use Rozyn\Facade\Auth;
	use Rozyn\Facade\Request;

	$this->script('jquery', 'class', 'overlay', '*', 'dialogs/dialog', 'dialogs/*');
	$this->style('reset', '*');
	
	$client = Auth::client();
	$host = Request::host();
?>


<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->getStyleSheetTags(); ?>

		<title><?php echo t('Admin Panel'); ?></title>
		
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		
		<script type="text/javascript">
			var __ROOT__ = '<?php echo purify(__WWW_ROOT__, PURIFY_JS); ?>';
			var CKEDITOR_BASEPATH = __ROOT__ + '/plugins/rozadmyn/public/js/ckeditor/';
			var LANG = '<?php echo site_language(); ?>';
			
			var Rozadmyn = {};
			
			<?php if (file_exists($langJsFile = merge_paths(__ROOT__, str_replace(URI_SEPARATOR, DS, passet('js/lang/' . site_language() . '.js', 'rozadmyn'))))) : ?>
				<?php include $langJsFile; ?>
			<?php endif; ?>
		</script>
	</head>

	<body>
		<div id="wrapper">
			<div id="top-menu">
				<?php echo str_format(__('rozadmyn.cms_title'), ['host' => __($this->config->read('site_title', $host))]); ?>
			</div>
			
			<aside id="sidebar">
				<div id="user-menu">
					<?php echo $this->requestView('/plugins/rozadmyn/views/user/thumbnail', ['avatar' => $client->avatar, 'gender' => 'male', 'info' => [['text' => $client->firstname, 'class' => 'user-name', 'tag' => 'span'], ['text' => t('rozadmyn.logout'), 'href' => route('rozadmyn.logout'), 'tag' => 'a']]]); ?>
				</div>				
				
				<?php echo $this->requestAction('\Rozadmyn\Controller\Admin\NavController', 'main', ['_controller' => $_controller, '_method' => $_method]); ?>
			</aside>
			
			<div id="content" class="cf">
				<?php echo $this->getContent(); ?>
			</div>
		</div>

		<?php echo $this->getScriptTags(); ?>
	</body>
</html>