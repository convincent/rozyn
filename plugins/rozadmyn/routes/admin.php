<?php

use Rozyn\Routing\Router;

// The index page of the admin panel.
$router->connect(array(
		'name' => 'rozadmyn.index', 
		'format' => '/', 
		'controller' => '\Rozadmyn\Controller\Admin\DashboardController', 
		'method' => 'index'));

// All other routes should be grouped together so that they can all use the
// default Rozadmyn filter which transforms controller URL parameters into valid
// Controller class names.
$router->group(['filters' => ['rozadmyn.default']], function(Router $router) {
	
	// The default overview route for all registered controllers.
	$router->get(array(
		'name' => 'rozadmyn.overview.default', 
		'format' => '/:controller', 
		'method' => 'index'));

	// Set up our default GET route.
	$router->get(array(
		'name' => 'rozadmyn.default', 
		'format' => '/:controller/:method/*',
		'filters' => ['rozadmyn.default.get']));

	// The delete route for all controllers.
	$router->post(array(
		'name' => 'rozadmyn.post.delete', 
		'format' => '/:controller/delete/*', 
		'method' => 'post_delete',
		'type' => 'json'));

	// The batch delete route for all controllers.
	$router->post(array(
		'name' => 'rozadmyn.post.batch.delete', 
		'format' => '/:controller/delete_batch/*', 
		'method' => 'post_delete_batch',
		'type' => 'json'));

	// Set up our default POST route.
	$router->post(array(
		'name' => 'rozadmyn.post.default', 
		'format' => '/:controller/:method/*',
		'filters' => ['rozadmyn.default.post']));
});