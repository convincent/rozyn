<?php

use Rozyn\Routing\Router;

$router->group(['controller' => '\Rozadmyn\Controller\PublicController'], function(Router $router) {
	$router->get(array(
		'name' => 'rozadmyn.login',
		'format' => '/admin/login',
		'method' => 'login',
		'file' => '/plugins/rozadmyn/templates/clean.php'));
	
	$router->connect(array(
		'name' => 'rozadmyn.logout',
		'format' => '/admin/logout',
		'method' => 'logout'));
	
	$router->post(array(
		'name' => 'rozadmyn.process_login',
		'format' => '/admin/login',
		'method' => 'processLogin'));
});