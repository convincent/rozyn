<?php

use Rozyn\Facade\App;
use Rozyn\Model\Collection;

/**
 * The labels for the table headers.
 * 
 * @var	array
 */
$headers = (isset($headers)) ? $headers : [];

/**
 * The names of the fields of each model instance that should be loaded in each
 * of the table's cells.
 * 
 * @var	array
 */
$fields = (isset($fields)) ? $fields : array_fill(0, count($headers), null);

/**
 * The actions that are available for mass-selected rows. The key is the 
 * technical identifier for that action and is used as a class name for the 
 * action to allow for custom styling and custom javascript code, etc. The value 
 * is the label for the action.
 * 
 * @var	array
 */
$batchActions;

$tmp			= (isset($batchActions)) ? $batchActions : [];
$batchActions	= [];
foreach ($tmp as $action => $label) {
	$batchActions[suffix($action, '_batch')] = $label;
}

/**
 * The actions that are available for each row. The key is the technical 
 * identifier for that action and is used as a class name for the action to 
 * allow for custom styling and custom javascript code, etc. The value is the 
 * label for the action.
 * 
 * @var	array
 */
$actions = (isset($actions)) ? $actions : [];

/**
 * The name of the route that will be used as a default for all actions.
 * 
 * @var	string
 */
$route = (isset($route)) ? $route : 'rozadmyn.default';

/**
 * A more detailed routes array that allows you specify different routes for 
 * each action. If no route is found for an action, the default route is used
 * instead.
 * 
 * @var	array
 */
$routes = (isset($routes)) ? $routes : [];
$routes = array_merge(array_fill_keys(array_keys(array_merge($actions, $batchActions)), $route), ['edit' => $route], $routes);

/**
 * Specifies whether or not search functionality should be included in this
 * overview.
 * 
 * @var	string|boolean
 */

$search;
if (!isset($search)) {
	$search = (array_key_exists(0, App::getRoute()->getArgs())) ? App::getRoute()->getArgs()[0] : true;
}

/**
 * Any parameters that need to be passed on to the route.
 * 
 * @var	array
 */
$params = (isset($params)) ? $params : [];

/**
 * The collection of models that should be loaded in the table.
 * 
 * @var	\Rozyn\Model\Collection
 */
$collection = (isset($collection)) ? $collection : new Collection();

/**
 * The page number that is currently being viewed.
 * 
 * @var	int
 */
$page = (isset($page)) ? intval($page) : 1;

/**
 * The column that is used in the order by clause.
 * 
 * @var	string
 */
$sort = (isset($sort)) ? $sort : null;

/**
 * The order by direction.
 * 
 * @var	string
 */
$dir = (isset($dir)) ? $dir : 'DESC';

/**
 * The columns that should not be sortable.
 * 
 * @var	string[]
 */
$nosort = (isset($nosort)) ? $nosort : [];

/**
 * A paginator object.
 * 
 * @var	\Rozyn\Pagination\ModelPaginator
 */
$paginator = (isset($paginator)) ? $paginator : null;


$this->style('overview');
$this->script('overview');


// Try and extract any translations the models may have.
$languages = [];

foreach ($collection as $model) {
	$tmp = [];

	foreach ($model->translations ?: [] as $translation) {
		$tmp[] = $translation->language;
	}
	
	if (!empty($tmp)) {
		$languages[$model->id()] = $tmp;
	}
}

$translatable = is_multilingual() && ($collection->getInstance() instanceof Rozyn\Model\TranslatableModel || !empty($languages));

?>

<form class="overview" action="<?php echo route($route, array_merge($params, ['method' => 'search'])); ?>" method="GET">
	<input type="hidden" name="sort" id="overview-sort" value="<?php echo $sort; ?>" />
	<input type="hidden" name="dir" id="overview-dir" value="<?php echo $dir; ?>" />
	<input type="hidden" name="page" id="overview-page" value="<?php echo $page; ?>" />
	
	<div class="overview-top cf">
		<?php if ($search !== false) : ?>
			<div class="search-wrapper">
				<input class="search" value="<?php echo (is_string($search)) ? $search : ''; ?>" type="text" placeholder="<?php echo t('rozadmyn.search_placeholder'); ?>" name="search" id="overview-search" autocomplete="off" />
			</div>
		<?php endif; ?>

		<?php if (!empty($batchActions)) : ?>
			<div class="batch-actions-wrapper"><?php echo t('rozadmyn.with_selected'); ?>:
				<ul class="actions batch">
					<?php foreach ($batchActions as $action => $label) {
							echo $this->Html->tag('li',
												  $this->Html->tag('a', $label, ['href' => route($routes[$action], array_merge($params, ['method' => $action])), 'class' => $action]), 
												  ['class' => $action]);
					} ?>
				</ul>
			</div>
		<?php endif; ?>
		
		<?php if ($paginator) : ?>
			<?php echo $paginator->getNav($page); ?>
		<?php endif; ?>
	</div>
	
	<table>
		<thead>
			<tr>
				<th>
					<input type="checkbox" id="master" name="master" />
					<label for="master"></label>
				</th>
				
				<?php 
				foreach ($headers as $i => $header) {
					if (in_array($fields[$i], $nosort)) {
						echo $this->Html->tag('th', $header);
					} else {
						echo $this->Html->tag('th', $this->Html->a($header, '#'), array(
									'class' => ($fields[$i] === $sort) ? ['sorted', strtolower($dir)] : '', 
									'data' => ['field' => $fields[$i]]));
					}
				}
				?>

				<?php if ($translatable) : ?>
					<th><?php echo t('rozadmyn.i18n'); ?></th>
				<?php endif; ?>
			</tr>
		</thead>

		<tbody>
			<?php foreach ($collection as $i => $model) : ?>
			
				<tr data-id="<?php echo $model->id(); ?>">
					<td>
						<?php echo $this->Html->tag('input', ['type' => 'checkbox', 'value' => $model->id(), 'name' => 'selected[]', 'id' => "selected-{$i}"]); ?>
						<?php echo $this->Html->tag('label', '', ['for' => "selected-{$i}"]); ?>
					</td>

					<?php foreach ($fields as $i => $field) : ?>
						<td><span class="data"><?php echo $model->get($field, (method_exists($model, $method = 'get' . snake2camel($field))) ? $model->$method() : null); ?></span>
							<?php if ($i === 0 && !empty($actions))  : ?>
								<ul class="actions">
									<?php foreach ($actions as $action => $label) {
											echo $this->Html->tag('li', 
																  $this->Html->tag('a', $label, ['href' => route($routes[$action], array_merge($params, $model->getData(), ['method' => $action, 'model' => camel2snake(get_class_name($model)), $model->id()])), 'class' => $action]), 
																  ['class' => $action]);
									} ?>
								</ul>
							<?php endif; ?>
						</td>
					<?php endforeach; ?>

					<?php if ($translatable) : ?>
						<td><?php 
							foreach (site_languages() as $language) {
								$classes = ['flag', $language];
								if (isset($languages[$model->id()]) && !in_array($language, $languages[$model->id()])) {
									$classes[] = 'inactive';
								}

								echo $this->Html->a($language, route($routes['edit'], array_merge($params, $model->getData(), ['method' => 'edit', 'model' => camel2snake(get_class_name($model)), $model->id(), $language])), ['class' => $classes, 'title' => $language]);
							}
						?></td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</form>