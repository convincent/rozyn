<?php 

use Rozadmyn\Facade\Rozadmyn;

if (!isset($route)) {
	$route = 'rozadmyn.default';
}

$current = (isset($language)) ? $language : lang(); ?>

<ul class="languages cf"><?php 
	foreach (site_languages() as $language) {
		$classes = ['flag', $language];
		
		if ($current !== $language) {
			$classes[] = 'inactive';
		}
		
		echo $this->Html->tag('li', $this->Html->a($language, route($route, ['controller' => Rozadmyn::getControllerAlias($_controller), 'method' => $_method, $language]), ['class' => $classes, 'title' => $language]));
	}
?></ul>