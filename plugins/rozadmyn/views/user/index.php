<?php $this->script('/plugins/rozadmyn/public/js/main/overview.js'); ?>

<h2><?php echo __('Users'); ?></h2>

<?php echo $this->requestView('/plugins/rozadmyn/views/overview', array(
			'collection'	=> $models,
			'headers'		=> [__('rozadmyn.full_name'), __('rozadmyn.email')],
			'fields'		=> ['full_name', 'email'],
			'batchActions'	=> ['delete' => __('delete')],
			'actions'		=> ['edit' => __('edit'), 'delete' => __('delete')],
			'params'		=> ['controller' => 'rozadmyn.user'],
		));