<?php $this->script('/plugins/rozadmyn/public/js/admin/user.js'); ?>

<?php 
	foreach ($groupToResourceMapping as &$mapping) {
		$mapping = array_map(function($resource) {
			return str_replace('.', '_', $resource);
		}, $mapping);
	}
?>

<label for="username"><?php echo __('rozadmyn.username'); ?></label>
<?php echo $this->Form->text('username', $model->username); ?>

<label for="firstname"><?php echo __('rozadmyn.firstname'); ?></label>
<?php echo $this->Form->text('firstname', $model->firstname); ?>

<label for="lastname"><?php echo __('rozadmyn.lastname'); ?></label>
<?php echo $this->Form->text('lastname', $model->lastname); ?>

<label for="email"><?php echo __('rozadmyn.email'); ?></label>
<?php echo $this->Form->email('email', $model->email); ?>

<label for="password"><?php echo __('rozadmyn.password'); ?></label>
<?php echo $this->Form->password('password'); ?>

<label><?php echo __('rozadmyn.groups'); ?></label>
<ul id="auth_groups">
	<?php foreach ($groups as $id => $group) : ?>
		<li>
			<?php echo $this->Form->checkbox('groups[]', $id, $model->groups->has($id), ['id' => 'group_' . $id]); ?>
			<?php echo $this->Html->tag('label', $group, ['for' => 'group_' . $id]); ?>
		</li>
	<?php endforeach; ?>
</ul>

<label><?php echo __('rozadmyn.permissions'); ?></label>
<ul id="auth_resources">
	<?php foreach ($resources as $resource) : ?>
		<li>
			<?php echo $this->Form->checkbox('resources[]', $resource, isset($model->getPermissions()[$resource]), ['id' => 'resource_' . str_replace('.', '_', $resource)]); ?>
			<?php echo $this->Html->tag('label', $resource, ['for' => 'resource_' . str_replace('.', '_', $resource)]); ?>
		</li>
	<?php endforeach; ?>
</ul>

<input type="submit" value="<?php echo __('rozadmyn.save'); ?>" />

<script type="text/javascript">
	var mapping = <?php echo json_encode($groupToResourceMapping); ?>;
</script>