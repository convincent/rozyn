<div class="user-thumbnail-wrapper cf"><?php

	use Rozyn\Facade\Config;

	$src = prefix($avatar, __WWW_ROOT__ . '/');
	$default = Config::read("rozadmyn.default_avatar_{$gender}", '');
	try {
		if ($src === URI_SEPARATOR || !is_file($src) || !is_readable($src)) {
			$src = $default;
		}
	} catch (\Exception $e) {
		$src = $default;
	}

	echo $this->Html->tag('div', '', ['class' => 'user-thumbnail', 'css' => ['background-image' => "url({$src})"]]);
?>
	
	
	<div class="user-info">
		<?php if (isset($info)) : ?>
		
			<div class="user-info">
				<?php $self = $this; ?>
				
				<?php echo implode('<br />', array_map(function($info) use ($self) {
						$tag  = (isset($info['tag']))  ? $info['tag']  : 'span';
						$text = (isset($info['text'])) ? $info['text'] : '';

						unset($info['tag'], $info['text']);

						return $self->Html->tag($tag, $text, $info);
					}, $info)); ?>
			</div>
		
		<?php endif; ?>
	</div>
</div>