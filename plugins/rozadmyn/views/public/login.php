<?php use Rozyn\Facade\Flash; ?>
<?php use Rozyn\Facade\Config; ?>

<?php $this->style('form'); ?>

<style type="text/css">
	#wrapper { background-image: url(<?php echo $this->config->read('rozadmyn.login_background'); ?>); }
</style>

<form id="login-form" method="post">
	<?php echo Flash::error('error'); ?>
	
	<div class="form-header">
		<h2><?php echo t('rozadmyn.admin_login'); ?></h2>
	</div>

	<div class="form-block">
		<label for="email"><?php echo t('rozadmyn.email'); ?></label>
		<input type="email" id="email" name="email" />
	</div>

	<div class="form-block">
		<label for="password"><?php echo t('rozadmyn.password'); ?></label>
		<input type="password" id="password" name="password" />
	</div>
	
	<div class="form-block">
		<input type="checkbox" id="remember" name="remember" value="1" />
		<label for="remember"><?php echo t('rozadmyn.remember_me'); ?></label>
	</div>

	<div class="form-block">
		<input type="submit" name="submit-login" id="submit-login" class="action" value="<?php echo t('rozadmyn.login'); ?>" />
	</div>
</form>