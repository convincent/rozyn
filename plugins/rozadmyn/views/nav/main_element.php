<?php

use Rozyn\Facade\Config;

if ($controller) : ?>
	

<li class="<?php if ($_args['_controller'] === $controller) echo 'active'; ?>">
	<?php echo $this->Html->tag('a', t(Config::read('rozadmyn.lang_prefix') . $alias), ['href' => '#']); ?>

	<ul>
		<?php
			foreach ($actions as $action) {
				echo $this->Html->tag('li', 
						$this->Html->tag('a', t(Config::read('rozadmyn.lang_prefix') . $action['label']), ['href' => '#']),
						['class' => ($_args['_controller'] === $controller && $_args['_method'] === $action['method']) ? 'active' : null]);
			}
		?>
	</ul>
</li>


<?php endif;