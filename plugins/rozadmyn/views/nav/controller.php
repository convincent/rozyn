<?php

use Rozyn\Facade\Config;
use Rozyn\Facade\Request;

if ($controller) {	
	$subMenu = '';
	$hasActive = preg_match('/^\/' . preg_quote(Config::read('rozadmyn.site_area', 'admin')) . '\/' . preg_quote($alias) . '\//', Request::getQuery());
	
	foreach ($actions as $action) {
		$href = $action['url'] ?: route('rozadmyn.default', ['controller' => $alias, 'method' => $action['method']]);
		$active    = ends_with(Request::getUrl(), $href);

		$subMenu .= $this->Html->tag('li', 
						$this->Html->tag('a', __($this->config->read('rozadmyn.lang_prefix') . $action['label']), ['href' => $href]),
						['class' => ($active) ? 'active' : null]);
	}
	
	$menu = $this->Html->tag('a', __($this->config->read('rozadmyn.lang_prefix') . $alias), ['href' => '#']) . 
			$this->Html->tag('ul', $subMenu);
	
	echo $this->Html->tag('li', $menu, ['class' => ($hasActive) ? 'active' : null]);
}