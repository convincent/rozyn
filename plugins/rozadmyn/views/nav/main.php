<?php use Rozyn\Controller\ControllerMethodAuthException; ?>

<nav id="main-nav">
	<ul><?php
		foreach ($controllers as $alias => $controller) {
			try {
				echo $this->requestAction($controller, 'adminMenu', $_args);
			} catch(ControllerMethodAuthException $e) {}
		}
	?></ul>
</nav>