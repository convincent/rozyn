var Overlay;

(function($) {
	Overlay = Base.extend({
		init: function(content, settings) {
			var defaults = {
				
			};
			
			this.element = null;
			this.content = null;
			
			this.settings = $.extend({}, defaults, settings);
			
			this.setContent(content);

			this.setId('id_' + (Math.random() + '').replace('0.', ''));

			this.fadeDuration  = 300;
		},

		respond: function() {
			var self = this;

			this.element.on('click', function(e) {
				self.die();
			});
			
			var handler = function(e) {
				if (e.keyCode === 27) {
					self.die();
				}
			};

			$(document).bind('keydown', handler);

			this.on('die', function() {
				$(document).unbind('keydown', handler);
			});
			
			this.respondContent();
		},
		
		respondContent: function() {
			this.getContentElement().children().on('click', function(e) {
				e.stopImmediatePropagation();
			});
		},

		show: function(duration, callback) {
			if (this.isActive()) {
				return;
			}

			var self = this;

			if (typeof(duration) === 'undefined') {
				var duration = this.fadeDuration;
			}

			if (this.isAnyActive()) {
				var active = this.getInstanceFromBody();

				active.hideContent(duration, function() {
					active.hide(0, function() {
						self.show(0);
						self.hideContent(0);
						self.showContent(self.fadeDuration);
					});
				});
			}

			else {
				this.setInstanceOnBody();
				this.setElement($('<div>').attr('id', 'overlay-wrapper').append(
									$('<div>').attr('id', 'overlay-content').append(
										this.getContent()
									)
								).css('display', 'none'));

				// Calculate the top offset.
				var offset = $(document).scrollTop();

				// Mark the HTML tag as containing an overlay.
				$('html').addClass('overlay');

				// Do the same for the BODY tag and set the previously calculated
				// top offset on it. This is necessary bceause the overlay class
				// forces a fixed positining on the BODY element, which means it
				// needs an appropriate top offset to still show the same section
				// that was shown before the overlay was initialized.
				$('body').addClass('overlay').css('top', -offset).append(this.getElement());

				this.fire('beforeshow', [this]);

				this.getElement().fadeIn(duration, function() {
					if (typeof(callback) !== 'undefined') {
						callback(self);
					}

					// Fire events.
					self.fire('aftershow', [self]);
					self.fire('show', [self]);

					self.respond();
				});
			}
		},

		hide: function(duration, callback) {
			if (!this.isActive())
				return;

			if (typeof(duration) === 'undefined')
				var duration = this.fadeDuration;

			var self = this;

			// Fire events.
			this.fire('hide', [this]);
			this.fire('beforehide', [this]);

			this.getElement().fadeOut(duration, function() {
				self.getElement().remove();
				self.removeInstanceFromBody();

				var offset = -parseInt($('body').css('top').replace(/px$/, ''));

				// Remove the overlay class from the HTML and BODY tags.
				$('body, html').removeClass('overlay');

				// We don't need the top offset on the body anymore, so reset that.
				$('body').css('top', '');

				// Scroll back to where the user was on the page before they opened
				// the overlay.
				$(document).scrollTop(offset);

				if (typeof(callback) !== 'undefined') {
					callback(self);
				}

				self.fire('afterhide', [self]);
			});
		},

		showContent: function(duration, callback) {
			var self = this;

			this.fire('beforeshowcontent', [this]);

			if (typeof(duration) === 'undefined') {
				var duration = this.fadeDuration;
			}

			this.getContentElement().fadeIn(duration, function() {
				if (typeof(callback) !== 'undefined') {
					callback(self);
				}

				self.fire('aftershowcontent', [self]);
			});
		},

		hideContent: function(duration, callback) {
			var self = this;

			this.fire('beforehidecontent', [this]);

			if (typeof(duration) === 'undefined') {
				var duration = this.fadeDuration;
			}

			this.getContentElement().fadeOut(duration, function() {
				if (typeof(callback) !== 'undefined') {
					callback(self);
				}

				self.fire('afterhidecontent', [self]);
			});
		},

		die: function() {
			this.fire('die', [this]);
			
			this.hide(this.fadeDuration);
		},

		find: function(q) {
			return (this.content) ? this.getContentElement().find(q) : this.getElement().find(q);
		},

		setElement: function(element) {
			this.element = element;
		},

		getElement: function() {
			return this.element;
		},
		
		getContentElement: function() {
			return this.getElement().children('#overlay-content').first();
		},

		setContent: function(content) {
			this.content = content || null;
			
			var self = this;
			
			if (this.isActive()) {
				this.hideContent(0, function() {
					self.getContentElement().html(self.getContent());
					self.respondContent();
					self.showContent(0);
				});
			}
		},

		getContent: function() {
			return this.content;
		},

		isActive: function() {
			return (this.isAnyActive() && this.equals(this.getInstanceFromBody()));
		},

		isAnyActive: function() {
			return $('body').hasClass('overlay');
		},

		getInstanceFromBody: function() {
			return $('body').data('_overlay');
		},

		setInstanceOnBody: function() {
			$('body').data('_overlay', this);
		},

		removeInstanceFromBody: function() {
			$('body').data('_overlay', null);
		},

		equals: function(otherOverlay) {
			return this.getId() === otherOverlay.getId();
		},

		setId: function(id) {
			this._id = id;
		},

		getId: function() {
			return this._id;
		}
	});
})(jQuery);