/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

var AutoCompleter;
 
(function($) {
	AutoCompleter = Base.extend({
		/**
		 * The constructor. 
		 * 
		 * @param	jQuery		element
		 * @param	dict		settings
		 * @return	void
		 */
		init: function(element, settings) {
			var defaults = {
				source: null,
				method: 'GET',
				url: window.location.href,
				minLength: 2,
				
				error: function() {},
				
				success: function(data, self) {
					if (data.constructor === Array && data.length > 0) {
						var res = '<ul>';
						
						for (i in data) {
							res += '<li><a href="#">' + data[i] + '</a></li>';
						}
						
						res += '</ul>';
						
						self.setList(res);
						
						return res;
					}
					
					self.clear();
				},
				
				confirm: function(item, self) {
					self.element.val(item.html());
					self.clear();
				},
			};
			
			this.settings = $.extend({}, defaults, settings);
			
			this.list		= null;
			this.query		= null;
			this.element	= element;
			
			this.bindHandlers();
			
			this.respondInput();
		},
		
		/**
		 * Makes the input responsive.
		 */
		respondInput: function() {
			var input	= this.element,
				self	= this;
		
			input.attr({
				autocomplete: 'off',
			});
			
			input.on('keydown', function(e) {
				var list = self.getList();
				if (list && [38, 40, 13].indexOf(e.keyCode) !== -1) {
					e.preventDefault();
					
					var active	= list.find('.active');

					// Enter
					if (e.keyCode === 13 && active.size()) {
						active.find('a').trigger('click');
					}

					// Up
					if (e.keyCode === 38 && active.size()) {
						var newActive = active.prev();

						if (newActive.size()) {
							active.removeClass('active');
							newActive.addClass('active');
							
							list.scrollTop(Math.max(0, newActive.prevAll().size() -2) * newActive.outerHeight());
						}
					}

					// Down
					if (e.keyCode === 40) {
						var newActive;
						
						if (active.size()) {
							newActive = active.next();
							active.removeClass('active');
						} else {
							newActive = list.find('li').eq(0);
						}
						
						if (newActive.size()) {
							list.scrollTop(Math.max(0, newActive.prevAll().size() -2) * newActive.outerHeight());
							newActive.addClass('active');
						}
					}
				}
			});
			
			this.element.on('change focus keyup', function(e) {
				var value = $(this).val();
				if (self.getList() && self.getQuery() === value) {
					return false;
				}
				
				self.setQuery(value);
				
				if (value.trim().length > self.getMinLength()) {
					self.fire('', [self]);
					
					$.ajax(self.getUrl(), {
						dataType: 'json',
						method: self.getMethod(),
						data: {
							query: value
						},
						
						success: function(data) {
							self.fire('success', [data, self]);
						},
						
						error: function(jqXHR, textStatus, errorThrown) {
							self.fire('load', [textStatus, self]);
						}
					});
				}
			});
		},
		
		/**
		 * Binds all the necessary handlers.
		 */
		bindHandlers: function() {
			this.on('success', this.settings.success);
			this.on('error', this.settings.error);
			this.on('confirm', this.settings.confirm);

			var self = this;
			this.listClickHandler = function(e) {
				if (!$.contains(self.element[0], e.target) &&
					!$(e.target).is(self.element[0])) {
				
						self.clearList();
				}
			};
			
			this.on('clearList', function() {
				$(document).unbind('click', self.listClickHandler);
			});
		},
		
		/**
		 * Returns the list with suggestions for this autocompleter.
		 * 
		 * @returns {jQuery}
		 */
		getList: function() {
			return this.list;	
		},
		
		/**
		 * Takes an HTML list as a parameter and sets it as the suggestions list
		 * for the input.
		 * 
		 * @param	{String}	list
		 */
		setList: function(list) {
			this.clearList();
			
			this.list = $(list);
			
			var self = this;
			
			this.list.find('li').each(function() {
				$(this).on('mouseenter', function(e) {
					$(this).addClass('active').siblings('.active').removeClass('active');
				});
			});
				
			this.list.find('li a').on('click', function(e) {
				e.preventDefault();
				
				self.fire('confirm', [$(this), self]);
			});
			
			$(document).bind('click', self.listClickHandler);
			
			this.list.addClass('rozadmyn-autocomplete-list');
			
			this.element.after(this.list);
		},
		
		/**
		 * Clears both the list and the input.
		 */
		clear: function() {
			this.fire('clear', [this]);
			
			this.clearList();
			
			this.element.val('');
		},
		
		/**
		 * Clears and closes the list with suggestions.
		 */
		clearList: function() {
			this.fire('clearlist', [this]);
			
			if (this.list) {
				this.list.remove();
				
				this.list = null;
			}
		},
		
		/**
		 * Returns the query typed by the user.
		 * 
		 * @returns {string}
		 */
		getQuery: function() {
			return this.query;
		},
		
		/**
		 * Sets the query typed by the user.
		 * 
		 * @param	{string}	query
		 */
		setQuery: function(query) {
			this.query = query;
		},
		
		/**
		 * Returns the method by which the AJAX request will be sent.
		 * 
		 * @returns {String}
		 */
		getMethod: function() {
			return this.settings.method;
		},
		
		/**
		 * Returns the minimum length that the query string needs to have before
		 * we start making suggestions.
		 * 
		 * @returns	{integer}
		 */
		getMinLength: function() {
			return this.settings.minLength;
		},
		
		/**
		 * Returns the URL that is used to retrieve suggestions from.
		 * 
		 * @returns	{string}
		 */
		getUrl: function() {
			return this.settings.url;
		}
	});
})(jQuery);