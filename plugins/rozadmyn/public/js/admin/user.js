if (typeof(mapping) === 'undefined') {
	var mapping = [];
}

var disableGroupResources = function() {
	var groupCheckboxes = $('#auth_groups input[type="checkbox"]');
	
	groupCheckboxes.each(function() {
		if ($(this).is(':checked')) {
			for (var i in mapping[$(this).val()]) {
				console.log(mapping[$(this).val()][i]);
				$('#resource_' + mapping[$(this).val()][i]).prop('checked', true).prop('disabled', true);
			}
		}
	});
};

$(document).ready(function() {
	// Initialize the resource checkboxes by disabling all the checkboxes that
	// correspond to one of the groups the user belongs to. This way, you can't
	// "turn off" permissions to resources that the user would automatically 
	// have access to through the groups they belong to.
	disableGroupResources();
	
	// Bind a handler to the group checkboxes.
	var groupCheckboxes = $('#auth_groups input[type="checkbox"]');
	
	groupCheckboxes.each(function() {
		// Whenever a group checkbox is changed, recompute which resources 
		// should be (de)selected).
		$(this).on('change', function() {
			// Retrieve all resources that are associated with the group that 
			// was just (de)selected.
			var groupResources = mapping[$(this).val()];
			
			if ($(this).is(':checked')) {
				// Check all the boxes that correspond to resources associated 
				// with the group whose checkbox was just checked.
				disableGroupResources();
			} else {
				// Compute which resources correspond to the groups that are 
				// still selected.
				var remainingResources = [];
				groupCheckboxes.each(function() {
					if ($(this).is(':checked')) {
						for (var i in mapping[$(this).val()]) {
							remainingResources[remainingResources.length] = mapping[$(this).val()][i];
						}
					}
				});
				
				// Uncheck every resource checkbox that belonged to the now
				// unchecked group checkbox as long as that resource isn't also
				// covered by one of the remaining selected groups.
				for (var i in groupResources) {
					if (remainingResources.indexOf(groupResources[i]) === -1) {
						$('#resource_' + groupResources[i]).prop('checked', false).prop('disabled', false);
					}
				}
			}
		});
	});
});