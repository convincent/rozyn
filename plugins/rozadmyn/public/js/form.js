var Form;
 
(function($) {
	Form = Base.extend({
		/**
		 * The Form constructor. 
		 * 
		 * @param	jQuery	element
		 * @param	dict	settings
		 * @return	void
		 */
		init: function(element, settings, validator) {
			var defaults = {
				ajax: false,
				initial: 0,
				ordered: true,
				coachMarks: true,
				validation: true,
			};
			
			this.disabled = false;
			this.element  = element;
			this.settings = $.extend({}, defaults, settings);
			
			this.validator = (typeof(validator) === 'undefined') ? new Validator() : validator;
			
			/**
			 * Holds the reference to the active coachmark in this form.
			 * 
			 * @var	{Popout}
			 */
			this.coachmark;
			
			/**
			 * Holds all the references to the active datetime popout in this form.
			 * 
			 * @var	{Popout}
			 */
			this.datetime;
					
			// Make the Form responsive.
			this.respond();
		},
		
		/**
		 * Looks up a child element of this form.
		 * 
		 * @return	s {jQuery}
		 */
		find: function(s) {
			return this.element.find(s);
		},
		
		/**
		 * Our master respond() method which calls all other "submethods" to
		 * make each part of our form responsive.
		 * 
		 * @return	void
		 */
		respond: function() {
			this.respondForm();
			this.respondNavigation();
			this.respondTimeline();
			this.respondInputs();
			this.respondCoachMarks();
			this.respondDateTimes();
			
			if (this.isAjax()) {
				this.respondAjax();
			}
		},
		
		/**
		 * Generic responsiveness for the form as a whole.
		 * 
		 * @return	s	{void}
		 */
		respondForm: function() {
			var frm = this;
			
			this.step(this.getInitialStep(), true);
			
			this.find('.button.submit').on('click', function(e) {
				e.preventDefault();
				
				frm.getElement().submit();
			});
			
			this.element.on('submit', function(e) {
				frm.fire('beforesubmit', [this]);
				
				if (!frm.canSubmit()) {
					e.preventDefault();
					return false;
				}
				
				frm.fire('submit', [this]);
			});
		},
		
		/**
		 * Adds additional coach mark functionality to the form blocks.
		 * 
		 * @return	s	{void}
		 */
		respondCoachMarks: function() {
			if (this.hasCoachMarks()) {
				var block, coachmark, self = this;
				
				// Whenever an element is focused, check if a coachmark is 
				// currently active. If so, close it.
				this.getInputs().add(this.getElement().find('a')).on('focus', function() {
					if (self.coachmark) {
						self.coachmark.close();
					}
				});

				this.element.find('.form-block').each(function() {
					block = $(this);

					$(this).find('input, select, textarea').each(function() {
						coachmark = block.children('.coachmark');

						if (coachmark.size()) {
							var p = new Popout(coachmark.html(), $(this), {
								id: 'form-coachmark',
								offset: 20,
								className: 'coachmark',
								positions: ['rightTop']
							});
							
							p.on('open', function() {
								// Register this coachmark with the form so that
								// it knows that it's open.
								self.coachmark = p;
								
								// Check if the click event was triggered by
								// an element that was a child of either the
								// popout element or its trigger element, or
								// by one of those elements themselves. If 
								// so, we should do nothing, because that 
								// means the user is trying to interact with
								// our popout. If the click originated from
								// anywhere else, close the popout.
								var clickHandler = function(e) {
									if (!$.contains(p.getElement()[0], e.target) &&
										!$.contains(p.getTriggerElement()[0], e.target) &&
										!$(e.target).is(p.getElement()) &&
										!$(e.target).is(p.getTriggerElement())) {
									
											p.close();
									}
								};

								// Bind the handler to the document so that all
								// click events are run through it.
								$(document).bind('click', clickHandler);
								
								// Once the popout has been closed, the above
								// handlers are no longer necessary, so remove
								// them from the DOM to prevent unnecessary 
								// clogging of event handlers.
								p.on('close', function() {
									$(document).unbind('click', clickHandler);
									
									// When the popout closes, this coachmark is
									// no longer active, so instruct the form
									// that it can clear its coachmark data.
									self.coachmark = null;
								});
							});

							// When an input element is focused, open any
							// coachmarks that are associated with it.
							$(this).on('focus', function() {
								p.open();
							});
						}
						
						coachmark.hide();
					});
				});
			}
		},
		
		/**
		 * Adds functionality to date and time-based inputs.
		 *
		 * @return	void
		 */
		respondDateTimes: function() {
			var self = this;
			
			this.find('input.date, input.time, input.datetime').each(function() {
				var input = $(this);
				var element = $('<div>');
				
				var format = [],
					isDate = false,
					isTime = false;
					
				if ($(this).hasClass('date') || $(this).hasClass('datetime')) {
					isDate = true;
					format.push('Y-m-d');
				}
				
				if ($(this).hasClass('time') || $(this).hasClass('datetime')) {
					isTime = true;
					format.push('H:i:s');
				}
				
				// Convert the initial value to a Date object.
				var datetime =  $(this).val(),
					date = datetime.split(' ')[0],
					time = datetime.split(' ')[1],
					initial = new Date(),
					info = [];
					
				if (date) {
					info = date.split('-');
					
					initial.setFullYear(info[0]);
					initial.setMonth(parseInt(info[1] - 1));
					initial.setDate(info[2]);
				}
				
				if (time) {
					info = time.split(':');
					
					initial.setHours(info[0]);
					initial.setMinutes(info[1]);
					initial.setSeconds(info[2]);
				}
				
				var dt = new DatePicker(element, {
					time: isTime,
					date: isDate,
					format: format.join(' '),
					initial: initial,
				});
				
				dt.on('change', function(self) {
					input.val(self.format());
				});

				var p = new Popout(dt.getWrapper(), $(this), {
					id: 'form-datetime',
					offset: 20,
					className: 'datetime',
					positions: ['rightBottom', 'rightTop']
				});

				p.on('open', function() {
					// Register this coachmark with the form so that
					// it knows that it's open.
					self.datetime = p;

					// Check if the click event was triggered by
					// an element that was a child of either the
					// popout element or its trigger element, or
					// by one of those elements themselves. If 
					// so, we should do nothing, because that 
					// means the user is trying to interact with
					// our popout. If the click originated from
					// anywhere else, close the popout.
					var clickHandler = function(e) {
						if (!$.contains(p.getElement()[0], e.target) &&
							!$.contains(p.getTriggerElement()[0], e.target) &&
							!$(e.target).is(p.getElement()) &&
							!$(e.target).is(p.getTriggerElement())) {

								p.close();
						}
					};

					// Bind the handler to the document so that all
					// click events are run through it.
					$(document).bind('click', clickHandler);

					// Once the popout has been closed, the above
					// handlers are no longer necessary, so remove
					// them from the DOM to prevent unnecessary 
					// clogging of event handlers.
					p.on('close', function() {
						$(document).unbind('click', clickHandler);

						// When the popout closes, this coachmark is
						// no longer active, so instruct the form
						// that it can clear its coachmark data.
						self.datetime = null;
					});
				});

				// When an input element is focused, open any
				// coachmarks that are associated with it.
				$(this).on('focus', function() {
					p.open();
				});
			});
		},
		
		/**
		 * Takes care of making all the different inputs in the form responsive.
		 * 
		 * @return	void
		 */
		respondInputs: function() {
			var self = this;
			
			this.getInputs().each(function() {
				if (self.isInputObligatory($(this))) {
					self.getInputLabel($(this)).after($('<span>').addClass('obligatory'));
				}
				
				$(this).on('focus', function(e) {
					self.clearError($(this));
				});
			});
			
			this.respondPublishFields();
			this.respondFileUploads();
			this.respondRichTextAreas();
		},
		
		/**
		 * Makes the Form navigation responsive.
		 * 
		 * @return	void
		 */
		respondNavigation: function() {
			var nav	 = this.element.find('.form-navigation'),
				self = this;
			
			if (nav.size()) {
				nav.find('.prev').on('click', function(e) {
					e.preventDefault();
					self.prev();
				});
				
				nav.find('.next').on('click', function(e) {
					e.preventDefault();
					self.next();
				});
				
				self.on('step', function() {
					self.updateNavigation();
				});
			}
			
			// If no step has been marked as active, do so for the first step.
			if (this.getStepIndex() === -1) {
				this.step(0);
			}
			
			// Call the updateNavigation() method once to update the navigation
			// to the Form's current state.
			this.updateNavigation();
		},
		
		/**
		 * Makes the form timeline responsive.
		 * 
		 * @return	s {void}
		 */
		respondTimeline: function() {
			var timeline = this.getTimeline(),
				self	 = this;
		
			if (timeline.size()) {
				timeline.find('ul li a').on('click', function(e) {
					var index = $(this).parent().prevAll().size();

					if (!self.isOrdered() || self.isStepVisited(index)) {
						self.step(index);
					}

					e.preventDefault();
				});
				
				self.on('step', function() {
					// Update the timeline based on the new active step.
					self.updateTimeline();
				});
			}
		},
		
		/**
		 * Makes all the inputs that deal with publishing the item responsive.
		 * 
		 * @return	void
		 */
		respondPublishFields: function() {
			var self = this;
			
			this.find('#is_published').on('change', function(e) {
				var published = $(this).prop('checked');
				self.find('#publish_from, #publish_until').each(function() {
					$(this).parent('.form-block').toggle(published);
					
					$(this).toggle(published);
					self.getInputLabel($(this)).toggle(published);
				});
			}).trigger('change');
		},
		
		/**
		 * Makes all the file upload fields responsive. This method first
		 * performs some generic logic that should be applied to every single
		 * file input. Afterwards, submethods are called to handle specific
		 * types of file inputs (for example: image file uploads, excel file
		 * uploads, etc.)
		 * 
		 * @return	void
		 */
		respondFileUploads: function() {
			this.element.find('input[type=file]').each(function() {
				// Replace each file input with a placeholder that can be styled
				// more easily. We bind the newly created placeholder element to
				// a variable only after we've wrapped it around our file input,
				// because if you do it before that, there are some bugs if you
				// try to call functions on that element later on. This way, we
				// circumvent all those potential bugs.
				$(this).wrap($('<div>').addClass('form-element-wrapper'));
			   
				var name		= $(this).attr('name');
				var wrapper		= $(this).parent();
				var placeholder = $('<div>').addClass('form-element')
											.addClass($(this).attr('type'))
											.addClass($(this).attr('class'));
					
				if ($(this).attr('multiple')) {
					if ($(this).slice(-2) !== "[]") {
						name += "[]";
						$(this).attr('name', name);
					}
				}
									
				if ($(this).data('value')) {
					placeholder.html($(this).data('value'));
				}
									
				for (var x in $(this).data()) {
					placeholder.attr("data-" + x, $(this).data(x));
				}
				
				placeholder.insertBefore($(this));
				
				// Bind a focus handler to the input so that we can add a 
				// "focus" class to the placeholder when the actual input is
				// being focused. This allows us to still apply focus styles,
				// even though the placeholder itself isn't actually being
				// focused.
				$(this).on('focus', function(e) {
					placeholder.addClass('focus');
				});
				
				// Remove focus when the input itself loses focus.
				$(this).on('focusout', function(e) {
					placeholder.removeClass('focus');
				});
				
				// Bind a change handler that checks whether or not the input
				// has a file selected. This allows us to toggle a class on the
				// placeholder which indicates if the input has a file selected.
				$(this).on('change', function(e) {
					placeholder.toggleClass('empty', $(this)[0].files.length === 0);
					
					// We also want to update the file upload text field based
					// on the names of the uploaded files, so we have to keep
					// track of them.
					var names = [];

					// Check if a valid file has been specified and if so, loop 
					// through them all.
					for (i = 0; i < $(this)[0].files.length; i++) {
						// Add the file name to our names array.
						names.push($(this)[0].files[i].name);
					}
					
					// Add the names to the placeholder's inner HTML.
					placeholder.text(names.join(', '));
				});
				
				placeholder.toggleClass('empty', !placeholder.html());
			});
			
			// Call several submethods to handle different kinds of file-type 
			// uploads.
			this.respondImageFileUploads();
		},
		
		/**
		 * Makes the image file upload fields responsive. This is a submethod
		 * that should only be called after respondFileUploads() has been called
		 * previously.
		 * 
		 * @return	void
		 */
		respondImageFileUploads: function() {
			// Handle some image-specific logic.
			this.element.find('input[type=file].image').each(function() {
				// The preview variable holds the wrapper element for any 
				// preview images of the uploaded files.
				var preview;
				
				// The element that wraps around the file input.
				var wrapper = $(this).parent();
				
				// The placeholder for the input
				var placeholder = wrapper.find('.form-element').first();

				// If the input has a data-preview attribute, parse its value as
				// a jQuery selector and use the matching element as the preview
				//  wrapper.
				if ($(this).data('preview')) {
					preview = $($(this).data('preview'));
				} 

				// If the input doesn't contain a data-preview attribute, we
				// create a new <div> element on the fly to serve as our preview
				// wrapper. We prepend this <div> to our file input placeholder 
				// element.
				else {
					preview = $('<div>').addClass('preview');
					preview.insertBefore(wrapper);
				}
				
				// Show any images that were previously stored.
				if ($(this).data('src')) {
					var images = $(this).data('src').split('|');
					
					for (var i = images.length - 1; i >= 0; i--) {
						preview.append($('<img />').addClass('old').attr({
							alt: '',
							src: images[i]
						}));
					}
				}

				// We want to update the previews each time the file input's
				// value is changed, so we specify a callback for that.
				$(this).on('change', function(e) {
					// If this image input supports multiple images, only remove
					// the images that were added on this page, leaving all the
					// images that were stored earlier intact.
					if ($(this).attr('multiple')) {
						preview.find('img.new').remove();
					} 
					
					// If this is a single image input, replace the old image
					// with the new one regardless of when the old image was
					// added.
					else {
						preview.find('img').remove();
					}
					
					// Check if a valid file has been specified and if so, loop 
					// through them all, since each of them.
					for (i = 0; i < $(this)[0].files.length; i++) {
						// Extract the current file.
						var file = $(this)[0].files[i];
						
						// Create the image tag that will hold the preview of 
						// this particular file. Add a "new" class to distinguish
						// it from other images that were already stored and are
						// simply being displayed to give the client the option
						// of deleting them manually.
						var img	 = $('<img />').addClass('new');

						// Add the <img> tag to our previously established
						// preview wrapper and set its file to the file we
						// are currently processing.
						preview.append(img);
						img[0].file = file;

						// We need to specify an onload callback for this
						// file which makes it so that the "src" attribute 
						// of the <img> tag is updated to load the image file.
						var reader = new FileReader();
						reader.onload = (function(img) {
							return function(e) {
								img.src = e.target.result;
							};
						})(img[0]);

						reader.readAsDataURL(file);
					}
				});
			});
		},
		
		/**
		 * Makes any rich textareas responsive. Partly inspired by
		 * https://www.youtube.com/watch?v=-aY5qm8KCGU
		 * 
		 * @return	s {void}
		 */
		respondRichTextAreas: function() {
			var self = this;
			
			// Initialize the rich text areas.
			this.element.find('textarea.rich').each(function() {
				var el = $(this),
					origId = el.attr('id') || el.attr('name');
					
				// Apparently, the CKEDITOR cannot be resized when the ID == "text", so we
				// temporarily set it to something else, then create the CKEDITOR instance
				// and then change it back again. Perhaps "text" is a special key in the 
				// CKEDITOR.instances dictionary?
				var tmpId = (origId === 'text') ? origId + '_ckeditor_fix' : origId;
					
				if (tmpId) {
					// Temporarily change the ID of the form element as explained above.
					el.attr('id', tmpId);
					
					CKEDITOR.replace(tmpId, {
						language: LANG,
						toolbarGroups: [
							{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
							{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
							{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
							{ name: 'forms', groups: [ 'forms' ] },
							{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
							{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
							{ name: 'links', groups: [ 'links' ] },
							{ name: 'insert', groups: [ 'insert' ] },
							{ name: 'styles', groups: [ 'styles' ] },
							{ name: 'colors', groups: [ 'colors' ] },
							{ name: 'tools', groups: [ 'tools' ] },
							{ name: 'others', groups: [ 'others' ] },
							{ name: 'about', groups: [ 'about' ] }
						],
						
						format_tags: 'p;h1;h2;h3;h4;h5;h6',
						contentsCss:	CKEDITOR_BASEPATH + 'styles/main.css',
						removeButtons:	'Source,Save,Templates,Cut,Find,SelectAll,Scayt,Form,Strike,RemoveFormat,Outdent,Indent,CreateDiv,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Anchor,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Styles,Font,Replace,Copy,Paste,PasteText,PasteFromWord,Print,Preview,NewPage,FontSize,Radio,Checkbox,TextField,Textarea,Select,Button,ImageButton,HiddenField,TextColor,ShowBlocks,Maximize,About,BGColor',
					});
					
					// Set the ID of the form element back to its original value.
					el.attr('id', origId);
					
					CKEDITOR.on('instanceReady', function(e) {
						var doc = el.next('.cke_chrome').find('iframe')[0].contentDocument;
						
						$(doc.body).css({
							color: el.css('color'),
							fontSize: el.css('fontSize'),
							lineHeight: el.css('lineHeight'),
							fontFamily: el.css('fontFamily'),
							margin: el.css('padding')
						});
					});
				}
				
				self.on('beforesubmit', function() {
					// Set allowfullscreen on all iframes
					data = CKEDITOR.instances[tmpId].getData();
					
					data = data.replace(new RegExp('(<iframe[^>]*?)>(?:</iframe>)', 'g'), function(match, m1) {
						return m1 + ' allowfullscreen></iframe>';
					});
					
					CKEDITOR.instances[tmpId].destroy();
					
					el.val(data);
				});
			});
		},
		
		/**
		 * Makes the form submit its data through an AJAX request.
		 * 
		 * @return	s {void}
		 */
		respondAjax: function() {
			// Store a reference to this object so that we can use it in our 
			// callback functions below.
			var frm = this;
			
			this.element.on('submit', function(e) {
				e.preventDefault();
				
				// Make sure the form can in fact be submitted.
				if (!frm.canSubmit()) {
					return false;
				}
				
				var formData = new FormData(frm.element.get(0));
				
				// Take care of file inputs.
				$(this).find('input[type=file]').each(function() {
					// Extract the input name and the associated files first.
					var name  = $(this).attr('name'),
						files = $(this).get(0).files;
					
					// If we're dealing with multiple files on a single input,
					// make sure that the name ends with "[]".
					if (files.length > 1 && !(new RegExp("\[\]$").test(name))) {
						name += '[]';
					}
					
					// Loop through all the files for this input and add them to
					// our formData object.
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						
						formData.append(name, file, file.name);
					}
				});
				
				// Inspired by http://stackoverflow.com/questions/2320069/jquery-ajax-file-upload/10811427#10811427
				$.ajax({
					url:  frm.element.attr('action') || window.location,
					type: frm.element.attr('method').toUpperCase() || 'POST',
					
					// Custom xhr callback to allow for progress updates.
					xhr: function() {
						myXhr = $.ajaxSettings.xhr();
						// First make sure the upload property actually exists.
						if (myXhr.upload) {
							myXhr.upload.addEventListener('progress', function(e) {
								frm.fire('progress', [e.loaded, e.total, e.timeStamp, e]);
							}, false);
						}
						
						return myXhr;
					},
					
					// Ajax events
					success: function(data, textStatus, jqXHR) {
						frm.fire('success', [data, textStatus, jqXHR]);
					},
					
					error: function(jqXHR, textStatus, errorThrown) {
						frm.fire('error', [textStatus, jqXHR, errorThrown]);
					},
					
					// Form data
					data: formData,
					
					// Options to tell JQuery not to process data or worry about 
					// content-type
					cache: false,
					contentType: false,
					processData: false
				}, 'json');
				
				frm.disable();
			});
			
			frm.on('success error', function() {
				frm.enable();
			});
		},
		
		/**
		 * Updates all the elements in the navigation based on the new state of 
		 * the Form.
		 * 
		 * @return	void
		 */
		updateNavigation: function() {
			if (this.hasNavigation()) {
				// Disable the "previous" button in the Form navigation if we're on
				// the first step.
				this.getNavigation().find('.prev').toggleClass('disabled', this.getStepIndex() === 0);

				// Similarly, disable the "next" button if we're on the last step.				
				this.getNavigation().find('.next').toggleClass('disabled', this.getStepIndex() === this.getStepCount() - 1);

				// Disable or enable any step-specific buttons in the navigation.
				// If a certain navigation button should only be visible within
				// a step that has a class "final", you can add a class of
				// "step-final" to the button and it will be automatically hidden
				// on all other form pages.
				var active	= this.getStep(),
					matches	= null,
					pattern	= /[\s^]step-([a-zA-Z_-]+)/g,
					show;
			
				this.getNavigation().children().each(function() {
					show = true;
					
					while (matches = pattern.exec($(this).attr('class'))) {
						show = active.hasClass(matches[1]);
						
						if (show) {
							break;
						}
					}
					
					$(this).toggle(show);
				});
				
				this.getNavigation().find('');
			}
		},
		
		/**
		 * Updates the form timeline.
		 * 
		 * @return	s {void}
		 */
		updateTimeline: function() {
			if (this.hasTimeline()) {
				var timeline = this.getTimeline(),
					preview  = timeline.find('li.active').first(),
					current	 = timeline.find('li').eq(this.getStepIndex());
				
				current.addClass('active visited');
				current.siblings().removeClass('active completed');
				current.prevAll('li').addClass('completed');
			}
		},
		
		/**
		 * Returns whether or not the form may be submitted.
		 * 
		 * @return	s {Boolean}
		 */
		canSubmit: function() {
			return this.isEnabled() && (!this.hasValidation() || this.validate(true));
		},
		
		/**
		 * Disables the form.
		 */
		disable: function() {
			this.disabled = true;
			
			this.element.find('input[type=submit]').addClass('loading').attr('disabled', 'disabled');
		},
		
		/**
		 * Enables the form.
		 */
		enable: function() {
			this.disabled = false;
			
			this.element.find('input[type=submit]').removeClass('loading').removeAttr('disabled');
		},
		
		/**
		 * Returns true if the form is disabled.
		 * 
		 * @return	s {Boolean}
		 */
		isDisabled: function() {
			return !!this.disabled;
		},
		
		/**
		 * Returns true if the form is enabled.
		 * 
		 * @return	s {Boolean}
		 */
		isEnabled: function() {
			return !this.disabled;
		},
		
		/**
		 * Validates the entire form and returns true if all inputs pass their
		 * validation rules. If the mark argument is set to TRUE, inputs that
		 * don't pass their validation rules are marked as faulty. Otherwise no
		 * visual representation of errors is given.
		 * 
		 * @param	{Boolean}	mark
		 * @return	s {Boolean}
		 */
		validate: function(mark) {
			var self = this,
				res  = true;
			
			this.getInputs().each(function() {
				res = self.validateInput($(this), mark) && res;
			});
			
			if (mark) {
				this.jumpToFirstError();
			}
			
			return res;
		},
		
		/**
		 * Validates the step matching the given index.
		 * 
		 * @param	{int}		i
		 * @param	{Boolean}	mark
		 * @return	s {Boolean}
		 */
		validateStep: function(i, mark) {
			var self = this,
				res  = true;
			
			this.getStep(i).find('input, select, textarea').each(function() {
				res = self.validateInput($(this), mark) && res;
			});
			
			return res;
		},
		
		/**
		 * Validates a single input
		 * 
		 * @param	{jQuery}	input
		 * @param	{Boolean}	mark
		 * @return	s {Boolean}
		 */
		validateInput: function(input, mark) {
			var self = this,
				res	 = true;
			
			if (!this.getValidator().checkInput(input)) {
				res = false;

				if (mark) {
					this.markError(input);
				}
			}
			
			return res;
		},
		
		/**
		 * Marks the given input as faulty.
		 * 
		 * @param	{jQuery}	input
		 * @return	s {void}
		 */
		markError: function(input) {
			input.addClass('error');
			input.siblings('.form-element').addClass('error');
			input.closest('.form-block').addClass('error');
		},
		
		/**
		 * Clears the input of any error indicators.
		 * 
		 * @return	s {void}
		 */
		clearError: function(input) {
			input.removeClass('error');
			input.siblings('.form-element').removeClass('error');
			input.closest('.form-block').removeClass('error');
		},
		
		/**
		 * Scrolls to the first error in the form.
		 * 
		 * @return	s {void}
		 */
		jumpToFirstError: function() {
			var element = this.find('.error').first();
			
			if (element.size()) {
				if (this.hasSteps()) {
					this.jumpToFirstStepError(this.getStepIndex(element.closest('.form-step')));
				}

				else {
					$('body, html').animate({
						scrollTop: element.offset().top,
					}, 300);
				}
			}
		},
		
		/**
		 * Scrolls to the first error element inside a given step.
		 * 
		 * @param	{int} i
		 * @return	s {void}
		 */
		jumpToFirstStepError: function(i) {
			var element = this.getStep(i).find('.error').first();
			
			if (element.size()) {
				this.step(i, true);
				
				$('body, html').animate({
					scrollTop: element.offset().top,
				}, 300);
			}
		},
		
		/**
		 * Moves the Form to the previous step if possible.
		 * 
		 * @return	void
		 */
		prev: function() {
			this.step(this.getStepIndex() - 1);
		},
		
		/**
		 * Moves the Form to the next step if possible.
		 * 
		 * @return	void
		 */
		next: function() {
			this.step(this.getStepIndex() + 1);
		},
		
		/**
		 * Moves the Form to the designated step. If force is set to TRUE, no
		 * checks are performed on the validity of the form inputs.
		 * 
		 * @param	{int}		step
		 * @param	{Boolean}	force
		 * @return	{void}
		 */
		step: function(i, force) {
			if (i >= 0 && this.getSteps().size() > i) {
				// Validate the current step to make sure that it has been 
				// finished properly.
				if (force || !this.hasValidation() || this.validateStep(this.getStepIndex(), true)) {
					var prev = this.getStep();
					
					this.getStep().removeClass('active');
					this.getStep(i).addClass('active visited');
					
					this.fire('step', [prev, this.getStep(), this]);
				}
				
				else {
					this.jumpToFirstStepError(this.getStepIndex());
				}
			}
		},
		
		/**
		 * Returns the index of the specified step. If no step is provided, the
		 * index of the current step will be returned. If no step is currently
		 * active, a value of -1 is returned.
		 * 
		 * @param	{jQuery}	step
		 * @return	s	{int}
		 */
		getStepIndex: function(step) {
			if (typeof(step) === 'undefined') {
				step = this.getSteps().filter('.active');
			}
			
			return (step.size()) ? step.prevAll('.form-step').size() : -1;
		},
		
		/**
		 * Returns a jQuery object representing the ith step. If no argument is
		 * provided, a jQuery object representing the current step is returned
		 * instead.
		 * 
		 * @param	int		i
		 * @return	jQuery
		 */
		getStep: function(i) {
			return this.getSteps().eq((typeof(i) === 'undefined') ? this.getStepIndex() : i);
		},
		
		/**
		 * Returns the amount of steps in the Form.
		 * 
		 * @return	int
		 */
		getStepCount: function() {
			return this.getSteps().size();
		},
		
		/**
		 * Returns a jQuery object representing all the steps in the Form.
		 * 
		 * @return	jQuery
		 */
		getSteps: function() {
			return this.element.find('.form-step');
		},
		
		/**
		 * Returns a jQuery object representing the Form navigation.
		 * 
		 * @return	jQuery
		 */
		getNavigation: function() {
			return this.element.find('.form-navigation');
		},
		
		/**
		 * Returns a jQuery object representing the Form timeline.
		 * 
		 * @return	jQuery
		 */
		getTimeline: function() {
			return this.element.find('.form-timeline');
		},
		
		/**
		 * Returns the Validator object used to validate inputs in this form.
		 * 
		 * @return	s {Validator}
		 */
		getValidator: function() {
			return this.validator;
		},
		
		/**
		 * Returns the index of the step that should be set as the active step 
		 * when the form is first loaded.
		 * 
		 * @return	s	{int}
		 */
		getInitialStep: function() {
			return this.settings.initial;
		},
		
		/**
		 * Returns whether the specified step has been visited previously.
		 * 
		 * @return	s {Boolean}
		 */
		isStepVisited: function(i) {
			return this.getStep(i).hasClass('visited');
		},
		
		/**
		 * Returns whether or not the specified input is obligatory.
		 * 
		 * @param	{jQuery}	input
		 * @return	s {Boolean}
		 */
		isInputObligatory: function(input) {
			return input.data('validation') && input.data('validation').split('|').indexOf('obligatory') > -1;
		},
		
		/**
		 * Returns whether or not the form is ordered, meaning that it has to be
		 * filled in step by step.
		 * 
		 * @return	s {Boolean}
		 */
		isOrdered: function() {
			return !!this.settings.ordered;
		},
		
		/**
		 * Returns whether or not the form is meant to be sent through an AJAX
		 * request.
		 * 
		 * @return	s	{Boolean}
		 */
		isAjax: function() {
			return !!this.settings.ajax;
		},
		
		/**
		 * Returns whether or not the form supports coachmarks.
		 * 
		 * @return	s {Boolean}
		 */
		hasCoachMarks: function() {
			return !!this.settings.coachMarks;
		},
		
		/**
		 * Returns whether or not this form has a navigation.
		 * 
		 * @return	s {Boolean}
		 */
		hasNavigation: function() {
			return !!this.getNavigation().size();
		},
		
		/**
		 * Returns whether or not this form has a timeline..
		 * 
		 * @return	s {Boolean}
		 */
		hasTimeline: function() {
			return !!this.getTimeline().size();
		},
		
		/**
		 * Returns whether or not the form contains steps.
		 * 
		 * @return	s {Boolean}
		 */
		hasSteps: function() {
			return !!this.getSteps().size();
		},
		
		/**
		 * Returns whether or not validation is enabled for this form.
		 * 
		 * @return	s {Boolean}
		 */
		hasValidation: function() {
			return this.settings.validation;
		},
		
		/**
		 * Returns a jQuery collection that represents all form elements in this
		 * form.
		 * 
		 * @return	s {jQuery}
		 */
		getInputs: function() {
			return this.getElement().find('input, textarea, select');
		},
		
		/**
		 * Returns a jQuery representation of the label that corresponds to the
		 * specified input element.
		 * 
		 * @param	{jQuery}		input
		 * @return	s {jQuery}
		 */
		getInputLabel: function(input) {
			// Check to see if the input contains a sibling that is a label.
			var siblings = input.siblings('label'),
				label;
			siblings.each(function() {
				if ($(this).attr('for') === input.attr('id')) {
					label = $(this);
				}
			});
			
			if (label) {
				return label;
			}
			
			// If no label has been found up until now, just check all the
			// labels in the same form block.
			var block	= input.closest('.form-block'),
				res		= null;
		
			block.find('label').each(function() {
				if ($(this).attr('for') === input.attr('id')) {
					res = $(this);
				}
				
				return false;
			});
			
			// If a result was found, return that. If not, default back to the
			// previously found sibling element.
			return res || siblings.first();
		},
		
		/**
		 * Returns a jQuery object that represents this form.
		 * 
		 * @return	s	{jQuery}
		 */
		getElement: function() {
			return this.element;
		}
	});
})(jQuery);