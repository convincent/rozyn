var Popout = Base.extend({
	/**
	 * Initializes the popout.
	 * 
	 * @param	{String}	content
	 * @param	{jQuery}	triggerElement
	 * @param	{dict}	settings
	 */
	init: function(content, triggerElement, settings) {
		var defaults = {
			// Give an ID to the popout.
			id: '',
			
			// Give additional classes to the popout.
			className: 'standard',
			
			// Define the element to which the popout should be appended.
			parent: $('body'),
			
			// Specify the order of popout positions in descending priority (the
			// first position will be used if all positions fail to display the 
			// popout fully).
			positions: ['top', 'right', 'bottom', 'left'],
			
			// Specify the offset of the popout relative to the trigger element 
			// (this will be turned into padding later).
			offset: 15,
			
			// Specify a margin that can be used to further specify the exact 
			// position of the popout.
			margin: 0,
			
			// Specify the duration of the show/hide animations.
			animationDuration: 200
		};
		
		this.triggerElement = triggerElement;
		
		
		this.setContent(content);

		//Override the default settings with the specified settings.
		this.settings = $.extend(defaults, settings);
	},
	
	/**
	 * Returns whether or not this popout is currently active.
	 * 
	 * @return	s {Boolean}
	 */
	isActive: function() {
		return this.getTriggerElement().hasClass('popout-active') && this.getWrapper() && this.getWrapper().size();
	},
	
	/**
	 * Returns the content of the popout element.
	 * 
	 * @return	s {String}
	 */
	getContent: function() {
		return this.content;
	},
	
	/**
	 * Sets the content for the popout element. If the popout is currently
	 * active, the live content is updated as well. If the popout is inactive,
	 * the wrapper and element variables are unset so that they have to be
	 * recreated the next time the popout is opened. This forces variables like
	 * width, height and positioning to be updated accordingly.
	 * 
	 * @param	{String}	content
	 */
	setContent: function(content) {
		this.content = content;
		
		if (this.isActive()) {
			this.element.html(content);
		}
		
		else {
			this.wrapper = this.element = null;
		}
	},
	
	/**
	 * Returns the parent element of the popout wrapper.
	 * 
	 * @return	s {jQuery
	 */
	getParent: function() {
		return this.settings.parent;
	},
	
	/**
	 * Returns the popout wrapper element.
	 * 
	 * @return	s {jQuery}
	 */
	getWrapper: function() {
		return this.wrapper;
	},
	
	/**
	 * Returns the popout element.
	 * 
	 * @return	s {jQuery}
	 */
	getElement: function() {
		return this.element;
	},
	
	/**
	 * Returns the element that triggers the popout.
	 * 
	 * @return	s {unresolved}
	 */
	getTriggerElement: function() {
		return this.triggerElement;
	},
	
	/**
	 * Returns whether or not the popout wrapper element is entirely visible on
	 * the client's screen.
	 * 
	 * Inspired by: http://upshots.org/javascript/jquery-test-if-element-is-in-viewport-visible-on-screen
	 * 
	 * @return	s	{Boolean}
	 */
	isOnScreen: function() {
		var win = $(window);
	
		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.getWrapper().offset();
		
		bounds.right  = bounds.left + this.getWrapper().outerWidth();
		bounds.bottom = bounds.top  + this.getWrapper().outerHeight();
		
		return viewport.right > bounds.right && viewport.left < bounds.left && viewport.bottom > bounds.bottom && viewport.top < bounds.top;
	},
	
	/**
	 * Opens the popout. When the popout is opened successfully, an "open" event
	 * is triggered on the object.
	 * 
	 * @return	s {Boolean}
	 */
	open: function() {
		// If this popout is already active, don't do anything.
		if (this.isActive()) {
			return false;
		}
		
		// Mark the trigger element with a class that tells it that a popout is
		// currently active for it.
		this.getTriggerElement().addClass('popout-active');

		// Create the popout elements if they haven't been created before.
		if (!this.getWrapper() || !this.getElement()) {
			this.wrapper = $('<div>').addClass('popout-wrapper');			
			this.element = $('<div>').appendTo(this.getWrapper()).addClass('popout').append(this.getContent());
		}
		
		// Check if the popout wrapper is in the DOM. If not, add it.
		if (!this.getWrapper().parent().size()) {
			this.getWrapper().appendTo(this.getParent())
		}

		// Add the popout to the DOM so that we can get access to its styles. 
		// Set its visibility to hidden for now.
		this.getWrapper().addClass(this.settings.className).attr('id', this.settings.id).css('visibility', 'hidden');

		// Add the top priority position to the end of the positions array, so 
		// that it's also the last one. This means that once all other options
		// have been tested and no suitable match has been found, the popout
		// will default to the top position, since it would have been the last
		// to be tried and thus any settings that were set on the element during
		// that tryout will remain intact.
		this.settings.positions.push(this.settings.positions[0]);

		// The position variable keeps track of the current position in the 
		// positions array while we're iterating through it.
		var position;
		// The styles object holds all the styles that should be applied to the 
		// popout, based on the current position.
		
		var styles;
		
		// Determine the width of the popout.
		var width  = this.settings.width  || this.getElement().outerWidth() + 1;
		
		// The relative offset of the element triggering the popout to its 
		// wrapper element.
		var offset = this.getTriggerElement().offset();

		// Calculate the offset if the parent element is not the <body> tag.
		if (this.getParent().get(0).tagName.toLowerCase() !== 'body') {
			var pOffset = this.getParent().offset();

			for (var c in offset) {
				offset[c] -= pOffset[c];
			}
		}

		// Set the width explicitly
		this.getWrapper().width(width);

		// Get the height of the popout's content element
		var height = this.settings.height || this.getElement().outerHeight();

		for (var i in this.settings.positions) {
			// Clear the styles from the previous iteration.
			this.getWrapper().attr('style', '');

			// Update the position.
			position = this.settings.positions[i];

			// Define standard styles that are applied to the popout, regardless
			// of position.
			styles = {
				visibility: 'hidden',
				position: 'absolute'
			};

			// Before we start styling based on position, we set the dimensions 
			// of the popout wrapper based on the position.
			if (position === 'top' || position === 'bottom' || position.indexOf('Top') > 0 || position.indexOf('Bottom') > 0) {
				this.getWrapper().width(width).height(height + this.settings.offset);
			} else {
				this.getWrapper().width(width + this.settings.offset).height(height);
			}

			// Calculate the styles based on the current position.
			if (position === 'top') {
				styles = $.extend(styles, {
					top: offset.top - this.getWrapper().outerHeight(),
					left: offset.left + (this.getTriggerElement().outerWidth() - this.getWrapper().outerWidth()) / 2,
					paddingBottom: this.settings.offset,
					marginTop: -this.settings.margin,
				});
			} else if (position === 'right') {
				styles = $.extend(styles, {
					top: offset.top + (this.getTriggerElement().outerHeight() - this.getWrapper().outerHeight()) / 2,
					left: offset.left + this.getTriggerElement().outerWidth(),
					paddingLeft: this.settings.offset,
					marginLeft:  this.settings.margin,
				});
			} else if (position === 'bottom') {
				styles = $.extend(styles, {
					top: offset.top + this.getTriggerElement().outerHeight(),
					left: offset.left + (this.getTriggerElement().outerWidth() - this.getWrapper().outerWidth()) / 2,
					paddingTop: this.settings.offset,
					marginTop: this.settings.margin,
				});
			} else if (position === 'left') {
				styles = $.extend(styles, {
					top: offset.top + (this.getTriggerElement().outerHeight() - this.getWrapper().outerHeight()) / 2,
					left: offset.left - this.getWrapper().outerWidth(),
					paddingRight: this.settings.offset,
					marginLeft: -this.settings.margin,
				});
			} else if (position === 'topLeft') {
				styles = $.extend(styles, {
					top: offset.top,
					left: offset.left - this.getWrapper().outerWidth(),
					paddingRight: this.settings.offset,
					marginLeft: -this.settings.margin,
				});
			} else if (position === 'topRight') {
				styles = $.extend(styles, {
					top: offset.top,
					left: offset.left + this.getTriggerElement().outerWidth(),
					paddingLeft: this.settings.offset,
					marginLeft:  this.settings.margin,
				});
			} else if (position === 'leftTop') {
				styles = $.extend(styles, {
					top: offset.top - this.getWrapper().outerHeight(),
					left: offset.left,
					paddingBottom: this.settings.offset,
					marginTop: -this.settings.margin,
				});
			} else if (position === 'rightTop') {
				styles = $.extend(styles, {
					top: offset.top - this.getWrapper().outerHeight(),
					left: offset.left + (this.getTriggerElement().outerWidth() - this.getWrapper().outerWidth()),
					paddingBottom: this.settings.offset,
					marginTop: -this.settings.margin,
				});
			} else if (position === 'leftBottom') {
				styles = $.extend(styles, {
					top: offset.top + this.getTriggerElement().outerHeight(),
					left: offset.left,
					paddingTop: this.settings.offset,
					marginTop: this.settings.margin,
				});
			} else if (position === 'rightBottom') {
				styles = $.extend(styles, {
					top: offset.top + this.getTriggerElement().outerHeight(),
					left: offset.left + (this.getTriggerElement().outerWidth() - this.getWrapper().outerWidth()),
					paddingTop: this.settings.offset,
					marginTop: this.settings.margin,
				});
			}

			// Override the previous styles with the new ones.
			this.getWrapper().css(styles);

			// Check if popout can be displayed fully. If so, we can stop the 
			// loop and display the popout.
			if (this.isOnScreen()) {
				break;
			}
		}

		// Show the popout once the loop is completed, because by then, it will 
		// have been set to its final position.
		this.getWrapper().addClass('active').css('visibility', 'visible');

		//Trigger the "open" event
		this.fire('open', [this]);
		
		return true;
	},
	
	/**
	 * Closes the popout and triggers a "close" event on the popout.
	 */
	close: function() {
		this.getWrapper().css('visibility', 'hidden').removeClass('active');
		
		this.getTriggerElement().removeClass('popout-active');
		
		this.fire('close', [this]);
	}
});