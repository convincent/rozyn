var DatePicker;
 
(function($) {
	DatePicker = Base.extend({
		/**
		 * The constructor. 
		 * 
		 * @param	jQuery	element
		 * @param	dict	settings
		 * @return	void
		 */
		init: function(element, settings) {
			var defaults = {
				date: true,
				time: true,
				initial: '2015-12-16 23:12:56',
				format: 'Y-m-d H:i:s',
				days: Rozadmyn.lang.dtDays,
				months: Rozadmyn.lang.dtMonths,
			};
			
			this.settings = $.extend({}, defaults, settings);
			
			this.element = element;
			
			// Fill the element with the correct HTML.
			this.initHtml();
			
			// Set the date to the initial date.
			this.setDateTime(this.getInitial());
			
			// Update the clock and the digital clock.
			this.markClock();
			this.markDigitalClock();
		},
		
		/**
		 * Fills the wrapper element with the proper HTML. 
		 */
		initHtml: function() {
			// Create our wrapper element.
			this.wrapper	= $('<div>').addClass('dt-wrapper');
			
			if (this.isDate()) {
				this.wrapper.addClass('date');
			}
			
			if (this.isTime()) {
				this.wrapper.addClass('time');
			}
			
			// First, take care of all calendar related stuff.
			this.calendar	= $('<table>').addClass('dt-calendar');
			
			// Create the calendar structure
			this.calendarHeader = $('<thead>').addClass('dt-calendar-header');
			this.calendarBody	= $('<tbody>').addClass('dt-calendar-body');
			
			// Create the calendar title.
			this.calendarTitle	= $('<th>').attr('colspan', 7).addClass('dt-calendar-title');
			this.calendarTitle.append($('<span>').addClass('month'));
			this.calendarTitle.append(' ');
			this.calendarTitle.append($('<span>').addClass('year'));
			
			// Create the calendar navigation
			this.calendarNavigation = $('<div>').addClass('dt-calendar-nav');
			this.calendarNavigation.append($('<a>').attr('href', '#').addClass('prev').html('&lt;'));
			this.calendarNavigation.append($('<a>').attr('href', '#').addClass('next').html('&gt;'));
			
			// Append the calendar navigation to the calendar title.
			this.calendarTitle.append(this.calendarNavigation);
			
			// Append the calendar title to the calendar header.
			this.calendarHeader.append($('<tr>').append(this.calendarTitle));
			
			// Fill the calendar header.
			var tr	 = $('<tr>'),
				days = this.getDays();
			
			this.calendarHeader.append(tr);
			for (var i in days) {
				tr.append($('<th>').html(days[i]));
			}
			
			this.calendar.append(this.calendarHeader).append(this.calendarBody);
			this.wrapper.append(this.calendar);
			
			
			// Afterwards, create our clock.
			this.clockWrapper = $('<div>').addClass('dt-clock-wrapper');
			
			this.clock = $('<div>').addClass('dt-clock');
			for (var j = 0; j < 6; j++) {
				this.clock.append($('<div>').addClass('dt-clock-marker').addClass('dt-clock-marker-' + (j + 1)));
			}
			
			// Add the clock face.
			this.clockFace = $('<div>').addClass('dt-clock-face');
			this.clock.append(this.clockFace);
			
			// Add the clock hands.
			this.clockFace.append($('<div>').addClass('dt-clock-hand').addClass('dt-clock-hour-hand'));
			this.clockFace.append($('<div>').addClass('dt-clock-hand').addClass('dt-clock-minute-hand'));
			this.clockFace.append($('<div>').addClass('dt-clock-hand').addClass('dt-clock-second-hand'));
			
			// Add the digital, editable time fields
			this.digitalClock = $('<div>').addClass('dt-digital-clock');
			this.digitalClock.append($('<input/>').attr({
				type: 'text',
				maxlength: '2'
			}).addClass('dt-clock-hour').addClass('dt-clock-input').data('max', 23));
			
			this.digitalClock.append(':');
			this.digitalClock.append($('<input/>').attr({
				type: 'text',
				maxlength: '2'
			}).addClass('dt-clock-minute').addClass('dt-clock-input').data('max', 59));
			
			this.digitalClock.append(':');
			this.digitalClock.append($('<input/>').attr({
				type: 'text',
				maxlength: '2'
			}).addClass('dt-clock-second').addClass('dt-clock-input').data('max', 59));
			
			this.clockWrapper.append(this.clock).append(this.digitalClock);
			this.wrapper.append(this.clockWrapper);
			
			
			// Once we've finished both the calendar and the clock, we can add
			// the elements to the actual DOM and get everything loaded up.
			this.element.append(this.wrapper);
			
			var initial = this.getInitial();
			this.fillCalendar(initial.getFullYear(), initial.getMonth());
			
			
			// Make everything responsive.
			this.respond();
		},
		
		/**
		 * Our master respond() method which calls all other "submethods" to
		 * make each part of our element responsive.
		 * 
		 * @return	void
		 */
		respond: function() {
			this.respondCalendarNavigation();
			this.respondClock();
		},
		
		/**
		 * Make the calendar body responsive. 
		 */
		respondCalendarBody: function() {
			var self = this;
			
			// Make the calendar days responsive.
			this.getCalendarBody().find('td').on('click', function(e) {
				if ($(this).hasClass('active')) {
					self.setDate(null);
				} 
				
				else if ($(this).html().length) {
					self.setDate(self.getCalendarYear(), self.getCalendarMonth(), parseInt($(this).html()));
				}
			});	
		},
		
		/**
		 * Make the calendar navigation responsive.
		 */
		respondCalendarNavigation: function() {
			var self = this;
			
			// Make the prev button responsive.
			this.getCalendarNavigation().find('.prev').on('click', function(e) {
				e.preventDefault();
				
				var month	= self.getCalendarMonth() - 1,
					year	= self.getCalendarYear();
				
				if (month === -1) {
					month = 11;
					year--;
				}
				
				self.fillCalendar(year, month);
			});
			
			// Make the next button responsive.
			this.getCalendarNavigation().find('.next').on('click', function(e) {
				e.preventDefault();
				
				var month	= self.getCalendarMonth() + 1,
					year	= self.getCalendarYear();
				
				if (month === 12) {
					month = 0;
					year++;
				}
				
				self.fillCalendar(year, month);
			});
		},
		
		/**
		 * Makes the clock responsive.
		 */
		respondClock: function() {
			var clock = this.getClock();
			
			var self = this;
			clock.find('.dt-clock-input').each(function() {
				$(this).on('focusout', function(e) {
					$(this).val(self.pad($(this).val(), 2));
				});
				
				$(this).on('keyup', function(e) {
					var hours	= $(this).parent().children('.dt-clock-hour').val(),
						minutes	= $(this).parent().children('.dt-clock-minute').val(),
						seconds	= $(this).parent().children('.dt-clock-second').val();
					
					self.setTime(hours, minutes, seconds);
				});
				
				$(this).on('keydown', function(e) {
					if (e.keyCode === 38 || e.keyCode === 40) { // UP or DOWN respectively
						e.preventDefault();
						
						var pos = $(this)[0].selectionStart;
											
						// Allow up and down keys to change the time.
						if (e.keyCode === 38 || e.keyCode === 40) { // UP or DOWN respectively
							e.preventDefault();

							// Convert the current value to an integer.
							var val = (isNaN($(this).val()) || $(this).val().trim().length === 0) ? 0 : parseInt($(this).val());

							// Compute the new value.
							var newVal = (e.keyCode === 38) ? Math.min(val + 1, parseInt($(this).data('max'))) : Math.max(1, val - 1);

							// Set the new value.
							$(this).val(self.pad(newVal, 2));
						}
						     
						$(this)[0].selectionStart = pos; 
						$(this)[0].selectionEnd = pos;
					}
				});
			});
		},
		
		/**
		 * Fills the calendar for a given year and month.
		 */
		fillCalendar: function(year, month, day) {
			var days	= this.getDaysInMonth(year, month);
			var weekday = this.getWeekDayFromDate(year, month, 1);
			var rows	= Math.ceil((days + weekday) / 7);
			var cells	= rows * 7;
			
			// Clear the current calendar.
			this.getCalendarBody().html('');
			
			// Create the first row manually by skipping all days until we reach
			// the day that corresponds to the first day of the month.
			var tr = $('<tr>');
			for (var i = 0; i < weekday; i++) {
				tr.append($('<td>'));
			}
			
			// Loop through the rest of our cells.
			for (; i < cells; i++) {
				// Every time we reach the 8th, 15th, etc cell, we need to 
				// start a new row before adding this cell since all our rows
				// hold 7 columns.
				if (i % 7 === 0) {
					this.getCalendarBody().append(tr);
					tr = $('<tr>');
				}
				
				// Create our empty cell element.
				td = $('<td>');
				// If our iteration variable corresponds to an actual day within
				// this month, set the content of the cell to the numeric 
				// representation of that day within this month.
				if (i >= weekday && i < weekday + days) {
					td.html(i - weekday + 1);
				}
				
				// Append our cell to the current row.
				tr.append(td);
			}
			
			// Since the last row won't be commited to our calendar within the
			// for loop, we have to add it manually afterwards.
			this.getCalendarBody().append(tr);
			
			// Change the calendar title.
			this.getCalendarTitle().find('.month').html(this.getMonthLabel(month));
			this.getCalendarTitle().find('.year').html(year);
			
			// Make the new calendar body responsive.
			this.respondCalendarBody();
			
			// Mark the currently active date or a specified date on the calendar.
			this.markCalendar();
		},
		
		/**
		 * Marks a date as active on the calendar.
		 * 
		 * @return	s	void
		 */
		markCalendar: function() {
			if (this.hasCalendar()) {
				var self	= this,
					tds		= this.getCalendarBody().find('td');

				tds.removeClass('active');

				if (this.getCalendarYear() === this.getYear() && this.getCalendarMonth() === this.getMonth()) {
					tds.each(function() {
						if (parseInt($(this).html()) === self.getDay()) {
							$(this).addClass('active');
						}
					});
				}
			}
		},
		
		/**
		 * Synchronizes the clock to the current time.
		 * 
		 * @return	s	void
		 */
		markClock: function() {
			if (this.hasClock()) {
				var hDegrees = this.getHours() * 30 + this.getMinutes() / 2 + this.getSeconds() / 120,
					mDegrees = this.getMinutes() * 6 + this.getSeconds() / 10,
					sDegrees = this.getSeconds() * 6;
			
				this.getHourHand().css({
					'transform': 'rotate(' + hDegrees + 'deg)',
				});
			
				this.getMinuteHand().css({
					'transform': 'rotate(' + mDegrees + 'deg)',
				});
			
				this.getSecondHand().css({
					'transform': 'rotate(' + sDegrees + 'deg)',
				});
			}
		},
		
		/**
		 * Synchronizes the digital clock to the current time.
		 * 
		 * @return	s {void}
		 */
		markDigitalClock: function() {
			if (this.hasClock()) {
				this.getClock().find('.dt-clock-hour').val(this.pad(this.getHours(), 2));
				this.getClock().find('.dt-clock-minute').val(this.pad(this.getMinutes(), 2));
				this.getClock().find('.dt-clock-second').val(this.pad(this.getSeconds(), 2));
			}
		},
		
		/**
		 * Returns whether or not this picker has a calendar.
		 * 
		 * @return	{boolean}
		 */
		hasCalendar: function() {
			return !!this.calendar;
		},
		
		/**
		 * Returns whether or not this picker has a clock.
		 * 
		 * @return	{boolean}
		 */
		hasClock: function() {
			return !!this.clock;
		},
		
		/**
		 * Looks up a child element.
		 * 
		 * @return	s {jQuery}
		 */
		find: function(s) {
			return this.element.find(s);
		},
		
		/**
		 * Returns the days array loaded for this date picker.
		 * 
		 * @return	{Array}
		 */
		getDays: function() {
			return this.settings.days;
		},
		
		/**
		 * Returns the month array loaded for this date picker.
		 * 
		 * @return	{Array}
		 */
		getMonths: function() {
			return this.settings.months;
		},
		
		/**
		 * Returns the amount of days for a given month and year
		 * 
		 * @param	{int}			year
		 * @param	{int|string}	month
		 * @return	{int}
		 */
		getDaysInMonth: function(year, month) {
			month = (typeof(month) === 'string') ? this.getMonths().indexOf(month) : parseInt(month);
			
			if (month === 1) {
				return (year % 4 === 0) ? 29 : 28;
			}
			
			else if (month === 0 || month === 2 || month === 4 || month === 6 || month === 7 || month === 9 || month === 11) {
				return 31;
			}
			
			return 30;
		},
		
		/**
		 * Returns the amount of days for a given year
		 * 
		 * @param	{int}			year
		 * @return	{int}
		 */
		getDaysInYear: function(year) {
			return (year % 4 === 0) ? 366 : 365;
		},
		
		/**
		 * Returns the initial value for this object.
		 * 
		 * @return	{Date}
		 */
		getInitial: function() {
			if (this.settings.initial === null) {
				return new Date();
			}
			
			return (typeof(this.settings.initial) === 'object') ? this.settings.initial : new Date(this.settings.initial);
		},
		
		/**
		 * Returns the selected value of this object as a formatted string.
		 * 
		 * @param	{string}	format
		 * @return	{string}
		 */
		format: function(format) {
			if (this.getDateTime() === null) {
				return null;
			}
			
			if (typeof(format) === 'undefined') {
				format = this.getFormat();
			}
						
			var day		= this.getDay(),
				month	= (this.getMonth() !== null) ? this.getMonth() + 1 : 0,
				year	= this.getYear(),
				seconds	= this.getSeconds(),
				minutes	= this.getMinutes(),
				hours	= this.getHours();
			
			var vars = {
				d: this.pad(day, 2),
				m: this.pad(month, 2),
				Y: this.pad(year, 4),
				j: day,
				n: month,
				y: this.pad(year % 100, 2),
				H: this.pad(hours, 2),
				i: this.pad(minutes, 2),
				s: this.pad(seconds, 2)
			};
			
			for (var x in vars) {
				format = format.replace(new RegExp(x), vars[x]);
			}
			
			return format;
		},
		
		/**
		 * Returns the date and time of this object as a Date object.
		 * 
		 * @return	s {Date}
		 */
		getDateTime: function() {
			return (!this.getDate() && !this.getTime()) ? null : new Date(this.getYear(), this.getMonth(), this.getDay(), this.getHours(), this.getMinutes(), this.getSeconds(), 0);
		},
		
		/**
		 * Returns the selected date of this object as a Date object.
		 * 
		 * @return	{Date}
		 */
		getDate: function() {
			return this.date;
		},
		
		/**
		 * Returns the selected time of this object as a Date object.
		 * 
		 * @return	{Date}
		 */
		getTime: function() {
			return this.time;
		},
		
		/**
		 * Sets a given date.
		 * 
		 * @param	{int|Date} year
		 * @param	{int} month
		 * @param	{int} day
		 * @return	s {void}
		 */
		setDate: function(year, month, day) {
			if (typeof(year) === 'object' || year === null) {
				this.date = year;
			} else {
				this.date = new Date(year, month, day);
			}
			
			this.fire('change', [this]);
			
			this.markCalendar();
		},
		
		/**
		 * Sets a given time.
		 * 
		 * @param	{int|Date} hours
		 * @param	{int} minutes
		 * @param	{int} seconds
		 * @return	s {void}
		 */
		setTime: function(hours, minutes, seconds) {
			if (typeof(hours) === 'object' || hours === null) {
				this.time = hours;
			} else {
				this.time = new Date(null, null, null, hours, minutes, seconds);
			}
			
			this.fire('change', [this]);
			
			this.markClock();
		},
		
		/**
		 * Sets a given date and time.
		 * 
		 * @param	{int|Date} year
		 * @param	{int} month
		 * @param	{int} day
		 * @param	{int} hours
		 * @param	{int} minutes
		 * @param	{int} seconds
		 * @return	s {void}
		 */
		setDateTime: function(year, month, day, hours, minutes, seconds) {
			if (typeof(year) === 'object' || year === null) {
				this.setDate(year);
				this.setTime(year);
			} else {
				this.date = new Date(year, month, day, null, null, null, null);
				this.time = new Date(null, null, null, hours, minutes, seconds, null);
			}
			
			// If this picker doesn't allow modification of the time, make it so
			// that the time is always set to 00:00:00.
			if (!this.isTime()) {
				this.time = new Date(null, null, null, null, null, null, null);
			}
			
			this.fire('change', [this]);
			
			this.markCalendar();
			this.markClock();
		},
		
		/**
		 * Returns the month (0-11) that is currently active on the calendar.
		 * 
		 * @return	s {int}
		 */
		getCalendarMonth: function() {
			return (this.hasCalendar()) ? parseInt(this.getMonths().indexOf(this.getCalendarTitle().find('.month').html())) : null;
		},
		
		/**
		 * Returns the year that is currently active on the calendar.
		 *
		 * @return	{int}
		 */
		getCalendarYear: function() {
			return (this.hasCalendar()) ? parseInt(this.getCalendarTitle().find('.year').html()) : null;
		},
		
		/**
		 * Returns the year (yyyy) of the date that is currently active or null
		 * if no date is active.
		 * 
		 * @return	s	{int}
		 */
		getYear: function() {
			return (this.date) ? this.date.getFullYear() : null;
		},
		
		/**
		 * Returns the month (0-11) of the date that is currently active or null
		 * if no date is active.
		 * 
		 * @return	s	{int}
		 */
		getMonth: function() {
			return (this.date) ? this.date.getMonth() : null;
		},
		
		/**
		 * Returns the day (1-31) of the date that is currently active or null 
		 * if no date is active.
		 * 
		 * @return	s	{int}
		 */
		getDay: function() {
			return (this.date) ? this.date.getDate() : null;
		},
		
		/**
		 * Returns the hours of the time that is currently active or null if no 
		 * time is active.
		 * 
		 * @return	s	{int}
		 */
		getHours: function() {
			return (this.time) ? this.time.getHours() : null;
		},
		
		/**
		 * Returns the minutes of the time that is currently active or null if 
		 * no time is active.
		 * 
		 * @return	s	{int}
		 */
		getMinutes: function() {
			return (this.time) ? this.time.getMinutes() : null;
		},
		
		/**
		 * Returns the seconds of the time that is currently active or null if 
		 * no time is active.
		 * 
		 * @return	s	{int}
		 */
		getSeconds: function() {
			return (this.time) ? this.time.getSeconds() : null;
		},
		
		/**
		 * Returns a given month label
		 * 
		 * @param	{int}	i
		 * @return	{string}
		 */
		getMonthLabel: function(i) {
			var months = this.getMonths();
			return (i >= 0 && i < months.length) ? months[i] : null;
		},
		
		/**
		 * Returns a given day label.
		 * 
		 * @param	{int}	i
		 * @return	{string}
		 */
		getDayLabel: function(i) {
			var days = this.getDays();
			return (i >= 0 && i < days.length) ? days[i] : null;
		},
		
		/**
		 * Returns the index of the weekday that corresponds to the given date.
		 * 
		 * We use the following formula:
		 * https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week#Useful_concepts
		 * 
		 * @param	{int|Date} year
		 * @param	{int} month
		 * @param	{int} day
		 * @return	s {int}
		 */
		getWeekDayFromDate: function(year, month, day) {
			if (typeof(year) === 'object') {
				day		= year.getDate();
				month	= year.getMonth();
				year	= year.getFullYear();
			}
			
			// Is it a leap year?
			var leap = year % 4 === 0;
			
			// Determine the mapping to a valid month number in the months table.
			// https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week#Months_table
			var mMapping = (leap) ? [6, 2, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5] : [0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5];
			
			// Determine the mapping for the c variable.
			var cMapping = [0, 5, 3, 1];
			
			// Convert our variables to those used in the wiki formula.
			var d = day,
				m = mMapping[month],
				y = year % 100,
				c = cMapping[Math.floor(year / 100) % 4];
		
			// Determine the result for our formula.
			var res = (d + m + y + Math.floor(y / 4) + c) % 7;
			
			// Our formula's result counts Saturday as day 0 while we assume that
			// Monday is day 0. Hence we need to perform a slight correction to
			// convert this result to one that our methods will interpret 
			// correctly.
			return (res + 5) % 7;
		},
		
		/**
		 * Alias for getSelected() if no arguments are provided. If arguments 
		 * are provided, the date that corresponds to those arguments is
		 * selected.
		 * 
		 * @param	{int|Date}	year
		 * @param	{int}	month
		 * @param	{int}	day
		 * @return	{Date}
		 */
		val: function(year, month, day) {
			if (typeof(year) === 'undefined') {
				return this.getDate();
			}
			
			else {
				return this.setDate(year, month, day);
			}
		},
		
		/**
		 * Returns the wrapper element for this object.
		 * 
		 * @return	{jQuery}
		 */
		getWrapper: function() {
			return this.wrapper;
		},
		
		/**
		 * Returns the header element for this object.
		 * 
		 * @return	{jQuery}
		 */
		getHeader: function() {
			return this.header;
		},
		
		/**
		 * Returns the calendar element for this object.
		 * 
		 * @return	{jQuery}
		 */
		getCalendar: function() {
			return this.calendar;
		},
		
		/**
		 * Returns the calendar header element for this object.
		 * 
		 * @return	{jQuery}
		 */
		getCalendarHeader: function() {
			return this.calendarHeader;
		},
		
		/**
		 * Returns the calendar title element for this object.
		 * 
		 * @return	{jQuery}
		 */
		getCalendarTitle: function() {
			return this.calendarTitle;
		},
		
		/**
		 * Returns the calendar body element for this object.
		 * 
		 * @return	{jQuery}
		 */
		getCalendarBody: function() {
			return this.calendarBody;
		},
		
		/**
		 * Returns the calendar body element for this object.
		 * 
		 * @return	{jQuery}
		 */
		getCalendarNavigation: function() {
			return this.calendarNavigation;
		},
		
		/**
		 * Returns the clock element for this object. 
		 *
		 * @return	{jQuery}
		 */
		getClock: function() {
			return this.clockWrapper;
		},
		
		/**
		 * Returns the hour hand element for the clock inside this object.
		 * 
		 * @return	{jQuery}
		 */
		getHourHand: function() {
			return this.clock.find('.dt-clock-hour-hand');
		},
		
		/**
		 * Returns the minute hand element for the clock inside this object.
		 * 
		 * @return	{jQuery}
		 */
		getMinuteHand: function() {
			return this.clock.find('.dt-clock-minute-hand');
		},
		
		/**
		 * Returns the second hand element for the clock inside this object.
		 * 
		 * @return	{jQuery}
		 */
		getSecondHand: function() {
			return this.clock.find('.dt-clock-second-hand');
		},
		
		/**
		 * Returns the default format for our date.
		 * 
		 * @return	{string}
		 */
		getFormat: function() {
			return this.settings.format;
		},
		
		/**
		 * Returns whether or not this DateTime picker allows modification of
		 * the date.
		 * 
		 * @return	{boolean}
		 */
		isDate: function() {
			return this.settings.date;
		},
		
		/**
		 * Returns whether or not this DateTime picker allows modification of
		 * the time.
		 * 
		 * @return	{boolean}
		 */
		isTime: function() {
			return this.settings.time;
		},
		
		/**
		 * Returns whether or not this DateTime picker allows modification of
		 * both the date and the time.
		 * 
		 * @return	{boolean}
		 */
		isDateTime: function() {
			return this.isDate() && this.isTime();
		},
		
		/**
		 * Pads a date element with leading zeroes.
		 * 
		 * @param	{string|int}	num
		 * @param	{int}			length
		 * @return	{string}
		 */
		pad: function(num, length) {
			if (num === null) {
				num = '';
			}
			
			if (typeof(length) === 'undefined') {
				var length = 2;
			}
			
			while ((""+num).length < length) {
				num = "0" + num;
			}
			
			return num;
		}
	});
})(jQuery);
