var Dialog;

(function($) {
	Dialog = Overlay.extend({
		init: function(element, settings) {
			var defaults = {
				className: 'default'
			};
			
			this._super(element, $.extend({}, defaults, settings));
		},
		
		setContent: function(content) {
			this._super($('<div>').addClass('dialog').addClass(this.settings.className).append(content));
		}
	});
})(jQuery);