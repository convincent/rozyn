var Confirmation;

(function($) {
	Confirmation = Dialog.extend({
		init: function(element, settings) {
			var defaults = {
				className: 'confirmation'
			};
			
			this._super(element, $.extend({}, defaults, settings));
		},
		
		respond: function() {
			this._super();

			var self = this;
			
			this.element.find('.cancel').on('click', function(e) {
				e.preventDefault();
				self.fire('cancel', [self]);
			});
			
			this.element.find('.confirm').on('click', function(e) {
				e.preventDefault();
				self.fire('confirm', [self]);
			});
			
			this.bindEventHandlers();
		},
		
		bindEventHandlers: function() {
			var self = this;
			
			this.on('cancel', function() {
				self.die();
			});
			
			this.on('confirm', function() {
				self.die();
			});
			
			var handler = function(e) {
				if (e.keyCode === 13) {
					self.getConfirmButton().trigger('focus');
					
					self.fire('confirm', [self]);
				}

				if (e.keyCode === 27) {
					self.getCancelButton().trigger('focus');
				}
			};

			$(document).bind('keydown', handler);

			this.on('die', function() {
				$(document).unbind('keydown', handler);
			});
		},
		
		setContent: function(content) {
			var wrapper = $('<div>').addClass('container'),
				message = $('<p>').html(content),
				buttons = $('<div>').addClass('buttons'),
				confirm = $('<a>').addClass('button confirm').html('{{rozadmyn.continue}}').attr('href', '#'),
				cancel	= $('<a>').addClass('cancel').html('{{rozadmyn.cancel}}').attr('href', '#');
		
			buttons.append(confirm).append(cancel);
		
			this._super(wrapper.append(message).append(buttons));
		},
		
		getConfirmButton: function() {
			return this.getContentElement().find('.button.confirm');
		},
		
		getCancelButton: function() {
			return this.getContentElement().find('.cancel');
		},
	});
})(jQuery);