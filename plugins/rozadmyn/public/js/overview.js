var Overview;

(function($) {
	Overview = Base.extend({
		/**
		 * Initializes the Overview.
		 * 
		 * @param	jQuery	element
		 * @param	dict	settings
		 * @return	s {void}
		 */
		init: function(element, settings) {
			var defaults = {
				
			};
			
			this.element  = element;
			this.settings = $.extend({}, defaults, settings);
			
			// Make the Overview responsive.
			this.respond();
		},
		
		/**
		 * Returns a jQuery representation of the specified row.
		 * 
		 * @param	{jQuery|int}	row
		 * @return	s {jQuery}
		 */
		getRow: function(row) {
			if (!isNaN(row)) {
				row = this.getBody().children().eq(row);
			}
			
			return row;
		},
		
		/**
		 * Returns the id of the model that is being represented by the specified
		 * row. 
		 * 
		 * @param	{jQuery|int}	row
		 * @return	s {int}
		 */
		getRowId: function(row) {
			row = this.getRow(row);
			
			return row.data('id');
		},
		
		/**
		 * Returns a string by which a client should be able to identify a
		 * particular row in the overview. Usually this will be the value of the
		 * first non-checkbox column.
		 * 
		 * @param	{jQuery|int}	row
		 * @return	s {string}
		 */
		getRowName: function(row) {
			row = this.getRow(row);
			
			if (row.size()) {
				return row.find('span.data').first().html();
			}
			
			return null;
		},
		
		/**
		 * Hides a row in the overview table.
		 * 
		 * @param	{jQuery|int}	row
		 */
		hideRow: function(row) {
			row = this.getRow(row);
			
			row.find('td:first-child input[type=checkbox]').prop('checked', false);
			
			row.hide();
		},
		
		/**
		 * Shows a row in the overview table.
		 * 
		 * @param	{jQuery|int}	row
		 */
		showRow: function(row) {
			row = this.getRow(row);
			
			row.show();
		},
		
		/**
		 * Shows or hides the specified row based on the second argument. If no
		 * second argument is provided, the row is toggled based on its current
		 * state. 
		 *
		 * @param	{jQuery|int}	row
		 * @param	{Boolean}		show
		 */
		toggleRow: function(row, show) {
			row = this.getRow(row);
			
			if (typeof(show) === 'undefined') {
				show = !row.is(':visible');
			}
			
			if (show) {
				this.showRow(row);
			}
			
			else {
				 this.hideRow(row);
			 }
		},
		
		/**
		 * Returns a jQuery collection representing the selected rows.
		 * 
		 * @return	s {Array(jQuery)}
		 */
		getSelectedRows: function() {
			var rows = [];
			
			this.element.find('tr td:first-child input[type=checkbox]').each(function() {
				if ($(this).is(':checked')) {
					rows[rows.length] = $(this).parent().parent();
				}
			});
			
			return rows;
		},
		
		/**
		 * Returns the IDs of the selected rows.
		 * 
		 * @return	s {Array(int)}
		 */
		getSelectedIds: function() {
			var ids = [];
			
			this.element.find('tr td:first-child input[type=checkbox]').each(function() {
				if ($(this).is(':checked')) {
					ids[ids.length] = $(this).parent().parent().data('id');
				}
			});
			
			return ids;
		},
		
		/**
		 * Returns the amount of selected rows.
		 * 
		 * @return	s {int}
		 */
		getSelectedCount: function() {
			return this.getSelectedIds().length;
		},
		
		/**
		 * Returns the overview element.
		 * 
		 * @return	s {jQuery}
		 */
		getElement: function() {
			return this.element;
		},
		
		/**
		 * Returns the table as a jQuery object.
		 * 
		 * @return	s {jQuery}
		 */
		getTable: function() {
			return this.element.find('table').first();
		},
		
		/**
		 * Returns the <tbody> element of the table as a jQuery object. 
		 *
		 * @return	s {jQuery}
		 */
		getBody: function() {
			return this.element.find('tbody').first();
		},
		
		/**
		 * Returns the pagination element.
		 * 
		 * @return	s {jQuery}
		 */
		getPagination: function() {
			return this.element.find('ul.pagination');
		},
		
		/**
		 * Searches the overview table for rows matching the given terms.
		 */
		search: function() {
			var sort	= this.element.find('#overview-sort').val(),
				dir		= this.element.find('#overview-dir').val(),
				search	= this.element.find('#overview-search').val();
		
			this.load(search, 1, sort, dir);
		},
		
		/**
		 * Sorts the overview table.
		 * 
		 * @return	s {undefined}
		 */
		sort: function(sort, dir) {
			var search = this.element.find('#overview-search').val();
			
			this.load(search, 1, sort, dir);
		},
		
		/**
		 * Loads new data from the server.
		 * 
		 * @param	{string} search
		 * @param	{int} page
		 * @param	{string} sort
		 * @param	{string} dir
		 */
		load: function(search, page, sort, dir) {
			var self	= this,
				url		= this.element.attr('action') + [encodeURIComponent(search), page, sort, dir].join('/');
		
			// For now we have disabled AJAX. Remove the next two lines to 
			// reenable AJAX.
			window.location = url;
			return;
			
			$.ajax({
				url: url,
				method: this.element.attr('method'),
				success: function(data) {
					self.getTable().html($(data).find('form.overview table').html());
					self.getPagination().html($(data).find('ul.pagination').html());
					
					self.respondTable();
					self.respondPagination();
				}
			});
		},
		
		/**
		 * Makes the entire overview responsive.
		 * 
		 * @return	s {void}
		 */
		respond: function() {
			this.respondBatchActions();
			// this.respondSearch();
			this.respondForm();
			this.respondTable();
			
			this.respondFixedHeaders();
		},
		
		/**
		 * Makes the overview table responsive.
		 */
		respondTable: function() {
			this.respondHeaders();
			this.respondCheckboxes();
			this.respondActions();
			this.respondPagination();
		},
		
		/**
		 * Makes the overview form responsive.
		 */
		respondForm: function() {
			var self	= this;
			
			this.element.on('submit', function(e) {
				var sort	= self.element.find('#overview-sort').val(),
					page	= 1,
					dir		= self.element.find('#overview-dir').val(),
					search	= self.element.find('#overview-search').val();
				
				e.preventDefault();
				self.load(search, page, sort, dir);
			});
		},
		
		/**
		 * Makes the pagination responsive.
		 */
		respondPagination: function() {
			var self = this,
				pagination = this.getPagination();
			
			pagination.find('li a').each(function() {
				var parent = $(this).parent();
				if (!parent.hasClass('previous') && !parent.hasClass('next')) {
					$(this).on('click', function(e) {
						e.preventDefault();
						var sort	= self.element.find('#overview-sort').val(),
							dir		= self.element.find('#overview-dir').val(),
							search	= self.element.find('#overview-search').val();
				
						self.load(search, $(this).html(), sort, dir);
					});
				}
			});
			
			pagination.find('.next a').on('click', function(e) {
				e.preventDefault();
				var current = parseInt(pagination.find('.active').text()),
					sort	= self.element.find('#overview-sort').val(),
					dir		= self.element.find('#overview-dir').val(),
					search	= self.element.find('#overview-search').val();
				
				self.load(search, current + 1, sort, dir);
			});
			
			pagination.find('.previous a').on('click', function(e) {
				e.preventDefault();
				var current = parseInt(pagination.find('.active').text()),
					sort	= self.element.find('#overview-sort').val(),
					dir		= self.element.find('#overview-dir').val(),
					search	= self.element.find('#overview-search').val();
				
				self.load(search, current - 1, sort, dir);
			});
		},
		
		/**
		 * Makes the headers of the overview table responsive.
		 */
		respondHeaders: function() {
			var self = this;
			var thead = this.element.find('thead');
			
			thead.find('th a').on('click', function(e) {
				e.preventDefault();

				self.sort($(this).parent().data('field'), ($(this).parent().hasClass('asc')) ? 'DESC' : 'ASC');
			});
		},
		
		/**
		 * Makes the top section of the overview (pagination, search, etc.) and headers of
		 * the table fixed when you scroll past them.
		 */
		respondFixedHeaders: function() {
			var overview = this.element;
			var topSection = $('.overview-top');
			var thead = this.element.find('thead');
			var breakpoint = topSection.offset().top;
			
			var fixHeaders = function() {
				var numColumns = thead.find('tr').children().size();
				for (i = 1; i <= numColumns; i++) {
					var th = thead.find('th:nth-child(' + i + ')');
					var cells = overview.find('table td:nth-child(' + i + ')');
					
					width = th.outerWidth();
					
					th.css('width', width + 'px');
					cells.css('width', width + 'px');
				}
				
				
				topSection.width(topSection.width());
				thead.css('top', topSection.outerHeight());
				
				overview.addClass('fixed');
			};
			
			var unfixHeaders = function() {
				overview.removeClass('fixed');
				topSection.css('width', '');
				overview.find('th, td').css('width', '');
			};
			
			$(window).on('scroll', function() {
				if ($(window).scrollTop() > breakpoint && !overview.hasClass('fixed')) {
					fixHeaders();
				} 
				
				if ($(window).scrollTop() <= breakpoint && overview.hasClass('fixed')) {
					unfixHeaders();
				}
			});
		},
		
		/**
		 * Makes the search input responsive.
		 */
		respondSearch: function() {
			this.searchTimeout;
			
			var input	= this.element.find('input.search'),
				self	= this;
			
			if (input.size()) {
				input.on('keyup', function(e) {
					clearTimeout(self.searchTimeout);
					
					var input = $(this);
					self.searchTimeout = setTimeout(function() {
						self.search(input.val().split(' '));
					}, 200);
				});
			}
		},
		
		/**
		 * Makes the checkboxes responsive.
		 */
		respondCheckboxes: function() {
			var self = this;
			
			// Make it so that the <th> checkbox (un)checks all other checkboxes.
			this.element.find('thead th:first-child input[type=checkbox]').on('change', function(e) {
				var master = $(this);
				
				self.element.find('tbody tr').each(function() {
					if ($(this).is(':visible')) {
						$(this).find('input[type=checkbox]').prop('checked', master.is(':checked'));
					}
				});
			});
		},
		
		/**
		 * Makes the actions for each row responsive.
		 */
		respondActions: function() {
			var self = this;
			
			this.element.find('tr').not('.responsive').each(function() {
				var tr = $(this).addClass('responsive');
						
				tr.find('.actions a').on('click', function(e) {
					self.fire($(this).attr('class'), [e, $(this), tr, self]);
				});
			});
			
			this.bindActionHandlers();
		},
		
		/**
		 * Makes the mass actions responsive.
		 */
		respondBatchActions: function() {
			var self = this;
			
			this.element.find('.actions.batch a').on('click', function(e) {
				self.fire($(this).attr('class'), [e, $(this), self]);
			});
			
			this.bindBatchActionHandlers();
		},
		
		/**
		 * Binds all the default action handlers.
		 */
		bindActionHandlers: function() {
			var self = this;
			
			// Delete
			this.on('delete', function(e, action, row) { 
				var confirm = new Confirmation("{{rozadmyn.confirm_delete}}".replace(/\{name\}/, row.find('span.data').html()));

				confirm.show();

				confirm.on('confirm', function(e) {
					$.ajax({
						url:  action.attr('href'),
						type: 'POST',
						data: { id: row.data('id') },
						
						success: function(data, textStatus, jqXHR) {
							row.remove();
						}
					});
				});

				e.preventDefault();
			});
		},
		
		/**
		 * Binds all the batch action handlers. 
		 */
		bindBatchActionHandlers: function() {
			var self = this;
			
			// Delete
			this.on('delete_batch', function(e, action) {
				var ids		= self.getSelectedIds();
				var rows	= self.getSelectedRows();
				var confirm = new Confirmation("{{rozadmyn.confirm_delete_batch}}".replace(/\{count\}/, ids.length));
				
				confirm.show();

				confirm.on('confirm', function(e) {
					$.ajax({
						url:  action.attr('href'),
						type: 'POST',
						data: { ids: ids },
						success: function(data) {
							for (var i = 0; i < rows.length; i++) {
								rows[i].remove();
							}
						}
					});
				});

				e.preventDefault();
			});
		},
	});
})(jQuery);