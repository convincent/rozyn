<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1463243636_rozadmyn_resources extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		$table = Table::instance('auth_resource');
		
		if ($table->hasColumn('name')) {
			$table->renameColumn('name', 'id');
			$table->push();
		}
		
		$query = new Query();
		$query	->replace()
				->into('auth_resource')
				->fields(['id'])
				->values(['id' => 'rozadmyn'])
				->values(['id' => 'rozadmyn.log.auth.attempts'])
				->values(['id' => 'rozadmyn.log.error'])
				->values(['id' => 'rozadmyn.user'])
				->execute();
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {
		$query = new Query();
		$query	->delete()
				->from('auth_resource')
				->whereIn('name', ['rozadmyn', 'rozadmyn.log.auth.attempts', 'rozadmyn.log.error', 'rozadmyn.user'])
				->execute();
	}
}