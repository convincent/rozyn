<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1463311174_assign_rozadmyn_resources_to_superadmin_user_group extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		// Make sure the resource_id column supports strings.
		$table = Table::instance('auth_groups_resources');
		$table->string('resource_id');
		
		$query = new Query();
		$query	->insert()
				->into('auth_groups_resources')
				->fields(['group_id', 'resource_id'])
				->values([5, 'rozadmyn'])
				->values([5, 'rozadmyn.log.auth_attempts'])
				->values([5, 'rozadmyn.log.error'])
				->values([5, 'rozadmyn.user'])
				->execute();
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {
		$query = new Query();
		
		$query	->delete()
				->from('auth_groups_resources')
				->whereEquals('group_id', 5)
				->whereIn('resource_id', ['rozadmyn', 'rozadmyn.log.auth_attempts', 'rozadmyn.log.error', 'rozadmyn.user'])
				->execute();
	}
}