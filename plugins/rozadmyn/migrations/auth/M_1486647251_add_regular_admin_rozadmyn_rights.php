<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1486647251_add_regular_admin_rozadmyn_rights extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		$query = new Query();
		$query	->insert()
				->into('auth_groups_resources')
				->fields(['group_id', 'resource_id'])
				->values([4, 'rozadmyn'])
				->execute();
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {

	}
}