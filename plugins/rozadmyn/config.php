<?php

return array(
	/**
	 * The site area used for admin pages.
	 * 
	 * @var string
	 */
	'rozadmyn.site_area' => 'admin',

	/**
	 * The default admin template file used for admin pages.
	 * 
	 * @var string
	 */
	'rozadmyn.admin_template' => '/plugins/rozadmyn/templates/admin.php',

	/**
	 * The prefix used for all automatically generated labels for our Translators.
	 * 
	 * @var string
	 */
	'rozadmyn.lang_prefix' => 'rozadmyn.',

	/**
	 *  The prefix prepended to all method names used to distinguish between 
	 * POST and GET handlers.
	 * 
	 * @var string
	 */
	'rozadmyn.post_prefix' => 'post_',

	/**
	 * Make Rozadmyn secure by default (https).
	 * 
	 * @var boolean
	 */
	'rozadmyn.secure' => true,

	/**
	 * The default male administrator avatar that will be used if the user 
	 * hasn't uploaded an image of their own.
	 * 
	 * @var string
	 */
	'rozadmyn.default_avatar_male' => __WWW_ROOT__ . '/plugins/rozadmyn/public/img/default-male.png',

	/**
	 * The default female administrator avatar that will be used if the user 
	 * hasn't uploaded an image of their own.
	 * 
	 * @var string
	 */
	'rozadmyn.default_avatar_female' => __WWW_ROOT__ . '/plugins/rozadmyn/public/img/default-female.png',

	/**
	 * The background image shown on the login page.
	 * 
	 * @var string
	 */
	'rozadmyn.login_background' => __WWW_ROOT__ . '/plugins/rozadmyn/public/img/login-bg.jpg',

	/**
	 * An array where each key is a directory containing AdminController files 
	 * and each value is the namespace of each AdminController class defined in 
	 * those files (including a trailing backslash!).
	 * 
	 * Additional AdminController can always be loaded manually using the 
	 * Rozadmyn::registerController() method. The only reason we specify these
	 * directories is so that we don't have to manually load all of our main 
	 * app's AdminControllers.
	 * 
	 * @var	string[]
	 */
	'rozadmyn.controller_directories' => array(
		__CONTROLLERS__ . DS . 'Admin' => NS_CONTROLLERS . 'Admin\\',
	),
);