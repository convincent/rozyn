<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn;

use Rozyn\Plugin\Plugin;
use Rozyn\Routing\Router;

class Rozadmyn extends Plugin {
	/**
	 * The site area that corresponds to Rozadmyn's admin panel.
	 * 
	 * @var string
	 */
	const SITE_AREA = 'admin';
	
	/**
	 * Holds the names of all AdminControllers for this admin panel.
	 * 
	 * @var	array
	 */
	protected $controllers = [];
	
	/**
	 * Holds the names of all aliases used to identify AdminControllers.
	 * 
	 * @var	array
	 */
	protected $aliases = [];
	
	/**
	 * Holds the alias of the currently active Controller.
	 * 
	 * @var	string
	 */
	protected $activeAlias;
	
	/**
	 * Holds the name of the currently active Controller.
	 * 
	 * @var	string
	 */
	protected $activeController;
	
	/**
	 * Returns whether or not this plugin is required.
	 * 
	 * @return	boolean
	 */
	public function isRequired() {
		return preg_match('/^' . PREG_URI_SEPARATOR . '?' . preg_quote(self::SITE_AREA, '/') . '/', $this->app()->request()->getQuery());
	}
	
	/**
	 * Returns whether or not the client has access to rozadmyn.
	 * 
	 * @return	boolean
	 */
	public function isClientAuthorized() {
		return $this->app()->auth()->clearance('rozadmyn');
	}
	
	/**
	 * Registers a Controller as an AdminController for this admin panel. The
	 * alias is used to identify the controller from URL parameters, but can
	 * be omitted to force this method to generate its own alias.
	 * 
	 * @param	string	$controller
	 * @param	string	$alias
	 */
	public function registerController($controller, $alias = null) {
		if ($alias === null) {
			$alias = camel2snake(preg_replace('/Controller$/', '', get_class_name($controller)));
		}
		
		$this->controllers[$alias] = $controller;
		$this->aliases[$controller] = $alias;
	}
	
	/**
	 * Returns the name of a registered AdminController matching the given alias.
	 * 
	 * @param	string	$alias
	 * @return	string
	 */
	public function getController($alias) {
		if ($this->hasControllerAlias($alias)) {
			return $this->controllers[$alias];
		}
		
		throw new \Exception('No controller found for alias ' . $alias);
	}
	
	/**
	 * Returns the names of all registered AdminControllers.
	 * 
	 * @return	array
	 */
	public function getControllers() {
		return $this->controllers;
	}
	
	/**
	 * Returns whether or not a given alias matches a registerd AdminController.
	 * 
	 * @param	string	$alias
	 * @return	boolean
	 */
	public function hasControllerAlias($alias) {
		return isset($this->controllers[$alias]);
	}
	
	/**
	 * Returns whether or not a given controller is registered.
	 * 
	 * @param	string	$controller
	 * @return	boolean
	 */
	public function hasController($controller) {
		return isset($this->aliases[$controller]);
	}
	
	/**
	 * Returns an AdminController's alias based on the given controller name.
	 * 
	 * @param	string	$controller
	 * @return	string
	 */
	public function getControllerAlias($controller) {
		if ($this->hasController($controller)) {
			return $this->aliases[$controller];
		}
		
		// If at first the Controller can't be found, try prefixing the name
		// with a backslash.
		if (!starts_with($controller, '\\')) {
			return $this->getControllerAlias('\\' . $controller);
		}
		
		throw new \Exception('No alias found for controller ' . $controller);
	}
	
	/**
	 * Returns the alias of the currently active Controller.
	 * 
	 * @return	string
	 */
	public function getActiveControllerAlias() {
		if (!$this->activeAlias) {
			$matches = [];
			if (preg_match('/' . preg_quote($this->app()->config()->read('rozadmyn.site_area'), '/')  . PREG_URI_SEPARATOR . '([^' . PREG_URI_SEPARATOR . ']+)/', $this->app()->request()->getQuery(), $matches) === 1) {
				$this->activeAlias = $matches[1];
			}
		}

		return $this->activeAlias;
	}
	
	/**
	 * Returns the name of the currently active Controller.
	 * 
	 * @return	string
	 */
	public function getActiveController() {
		if (!$this->activeController && ($alias = $this->getActiveControllerAlias())) {
			$this->activeController = $this->getController($alias);
		}
		
		return $this->activeController;
	}
	
	/**
	 * Registers a routes file and applies all the default filters, security
	 * settings, etc to all the routes in it.
	 * 
	 * @param	string	$file
	 */
	public function registerRoutes($file) {
		// Create a new scope in which we'll add the routes in the file.
		$this->app()
			 ->router()
			 ->group(array('format' => $this->app()->config()->read('rozadmyn.site_area'),
						   'file'   => $this->app()->config()->read('rozadmyn.admin_template')),
				 
					 function(Router $router) use ($file) {
						// Include our routes file after all the filters and other
						// settings have been applied to the current scope.
						$router->import($file);
					 });
	}
}