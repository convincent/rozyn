<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn\Controller\Admin;

use Rozyn\Facade\Redirect;
use Rozyn\Facade\Url;
use Rozyn\Facade\Config;
use Rozyn\Controller\Controller;
use Rozadmyn\Facade\Rozadmyn;
use ReflectionException;
use ReflectionMethod;
use ReflectionClass;

abstract class AdminController extends Controller {
	/**
	 * Our magic method that is executed before every method call in our 
	 * controller, as long as that call is routed through __callMethod().
	 * 
	 * @param	string	$name
	 * @param	array	$args
	 */
	protected function __beforeMethod($name, array $args) {
		// Make sure that the user has admin rights.
		if (!Rozadmyn::isClientAuthorized()) {
			Redirect::route('rozadmyn.login');
		}
		
		$this->set('_alias', camel2snake($this->getName()));
		
		parent::__beforeMethod($name, $args);
	}
	
	/**
	 * Renders the main navigation menu for the admin panel.
	 */
	final protected function adminMenu() {
		$this->renders('/plugins/rozadmyn/views/nav/controller.php');
		
		// The actions that should be shown in the menu for this controller.
		$actions	= $this->getAdminMenuActions();
		
		$controller = (!empty($actions)) ? $actions[0]['controller'] : null;
		$alias		= (!empty($actions)) ? $actions[0]['alias']		 : null;
		
		$this->set(compact('controller', 'alias', 'actions'));
	}
	
	/**
	 * Returns all the necessary data regarding the admin actions that should
	 * be shown in the admin menu.
	 * 
	 * @return	array
	 */
	final protected function getAdminMenuActions() {
		// The array which will hold all public actions for this controller.
		$actions	= [];
		
		// The full name of this controller.
		$controller	= get_class($this);
		
		// The alias under which this controller is registered.
		$alias		= Rozadmyn::getControllerAlias($controller);
		
		foreach ($this->getPublicMethods() as $method) {
			// If the method starts with an underscore, we shouldn't show it.
			if (starts_with($method->getName(), '_')) {
				continue;
			}
			
			// If the method has a @hide annotation, don't show it.
			if (get_annotation($method, 'hide')) {
				continue;
			}
			
			// If the client does not have access to this method, don't show it.
			if (!$this->isAuthorizedForMethod($method->getName())) {
				continue;
			}
			
			// If the method requires parameters, we can't include the action
			// in our menu since we have no way of knowing which parameters to
			// pass on. Unless a default value has been specified of course,
			// but we'll check for that separately.
			$show = true;
			
			// We loop through all the method's parameters to make sure that 
			// all of them are optional. If one of them isn't, we don't show
			// this method in our navigation menu.
			if (count($method->getParameters()) > 0) {
				foreach ($method->getParameters() as $param) {
					try {
						$param->getDefaultValue();
					} catch (ReflectionException $e) {
						// If the parameter has no default value, break out of
						// the current for loop.
						$show = false;
						break;
					}
				}
				
				// If there was a parameter without a default value, we can't
				// show the action of the current iteration, so continue on to
				// the next.
				if (!$show) {
					continue;
				}
			}
			
			// First we check if any Doc Comments are present. It's possible to
			// define your own custom label using the @label annotation in the
			// Doc Comment section of your method.
			$label = get_annotation($method, 'label') ?: $method->getName();
			
			// Determine if a custom URL should be used for this action
			$url = ($a = get_annotation($method, 'url')) ? 
						Url::format(merge_paths(Config::read('rozadmyn.site_area') . '/', $a)) :
						null;
			
			$actions[] = array(
				'controller'	=> $controller,
				'method'		=> $method->getName(),
				'label'			=> $label,
				'alias'			=> $alias,
				'url'			=> $url,
			);
		}
		
		return $actions;
	}
	
	/**
	 * Returns an array of all public methods defined in a child class. This 
	 * can be used to automatically generated admin navigation menus, etc.
	 * 
	 * @return	\ReflectionMethod[]
	 */
	final protected function getPublicMethods() {
		$res		= [];
		$reflection = new ReflectionClass($this);
		
		foreach ($reflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
			if (!starts_with($method->getDeclaringClass()->getName(), 'Rozyn\Controller\\')) {
				$res[] = $method;
			}
		}
		
		return $res;
	}
	
	/**
	 * Forbid the necessary methods in this Controller.
	 */
	protected function setup() {
		parent::setup();
		
		$this->forbid(get_class_methods(__CLASS__));
		
		$this->allow('adminMenu');
	}
}