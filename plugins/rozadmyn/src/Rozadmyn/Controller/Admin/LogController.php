<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn\Controller\Admin;

use Rozyn\Model\Auth\Attempt;
use Rozyn\Filesystem\FileIterator;

class LogController extends AdminController {
	/**
	 * The directory containing this controller's view files.
	 * 
	 * @var string
	 */
	protected $views = 'log';
	
	/**
	 * Inspect the error log.
	 * 
	 * @auth	rozadmyn.log.error
	 */
	public function error() {
		$lines = new FileIterator(__LOGS__ . DS . 'error.log');
		
		$this->set(compact('lines'));
	}
	
	/**
	 * Inspect the login attempts log.
	 * 
	 * @auth	rozadmyn.log.auth.attempts
	 */
	public function auth_attempts() {
		$attempts = Attempt::query()->fetchArrays();
		
		$this->set(compact('attempts'));
	}
}