<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn\Controller\Admin;

class DashboardController extends AdminController {
	protected $views = '/plugins/rozadmyn/views/dashboard';
	
	/**
	 * @label	home
	 */
	public function index() {
		
	}
}