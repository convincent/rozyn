<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn\Controller\Admin;

use Rozadmyn\Facade\Rozadmyn;

/**
 * @hide
 */
class NavController extends AdminController {
	protected $views = '/plugins/rozadmyn/views/nav';
	
	protected function main() {
		$this->set('controllers', Rozadmyn::getControllers());
	}
}