<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn\Controller\Admin;

use Rozyn\Facade\Hash;
use Rozyn\Model\Auth\Resource;
use Rozyn\Model\Auth\Group;

/**
 * @auth	rozadmyn.user
 */
class UserController extends CrudController {
	/**
	 * The directory containing this controller's view files.
	 * 
	 * @var string
	 */
	protected $views = 'user';
	
	/**
	 * The full class name of the model associated with this controller.
	 * 
	 * @var string
	 */
	protected $model = 'Rozyn\Model\Auth\User';
	
	/**
	 * Returns a User model filled with the request data and the arguments 
	 * passed to this method.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 * @return	\Rozyn\Model\Auth\User
	 */
	protected function getRequestModel($id = null, $language = null) {
		$data	= $this->getRequestModelData();
		$model	= $this->getRequestLazyModel($id, $language, $data);
		
		if (!$data->isEmpty()) {
			$model->groups()->set(Group::query()->whereIn('UserGroup.id', $data->get('groups', []))->fetchModels());
			$model->resources()->set(Resource::query()->whereIn('Resource.id', $data->get('resources', []))->fetchModels());
			
			if ($data->get('password', null)) {
				$model->set('password', Hash::hash($data->get('password')));
			}
		}
		
		return $model;
	}

	/**
	 * Sets all necessary variables for the form view.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 */
	protected function prepareForm($id = null, $language = null) {
		$model = $this->getRequestModel($id, $language);
		
		// Retrieve all possible resources that the user can be given access to.
		$resources = Resource::query()->fetchList('Resource.id');
		
		// Retrieve a list of all auth groups.
		$groups = Group::query()->fetchList('UserGroup.name');
		
		// Retrieve all groups that the user can be given access to and their
		// associated resources.
		$groupToResourceMapping = Group::query()->with('resources')->fetchArrays();
		
		foreach ($groupToResourceMapping as &$mapping) {
			$mapping = array_keys($mapping['resources']);
		}
		
		$this->set(compact('model', 'groups', 'resources', 'groupToResourceMapping'));
	}
}