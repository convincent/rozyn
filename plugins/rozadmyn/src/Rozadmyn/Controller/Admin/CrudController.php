<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn\Controller\Admin;

use Rozadmyn\Facade\Rozadmyn;
use Rozyn\Facade\Redirect;
use Rozyn\Facade\Request;
use Rozyn\Facade\Flash;
use Rozyn\Data\Collection;
use Rozyn\Model\TranslatableModel;

abstract class CrudController extends AdminController {
	/**
	 * Renders the overview page for the Model associated with this Controller.
	 * 
	 * @param	int		$page
	 * @param	string	$sort
	 * @param	string	$dir
	 */
	public function index($page = 1, $sort = null, $dir = null) {
		$this->search(null, $page, $sort, $dir);
	}
	/**
	 * Renders the overview page for the Model associated with this Controller
	 * using a specified search query.
	 * 
	 * @param	string		$search
	 * @param	int			$page
	 * @param	string		$sort
	 * @param	string		$dir
	 */
	protected function search($search = null, $page = 1, $sort = null, $dir = null) {
		$this->renders('index');
		
		$model	= $this->getModel();
		if ($search !== null) {
			$search = urldecode($search);
		}
		
		// IF YOU WANT TO SORT BY A DIFFERENT LANGUAGE, MAYBE DETECT THE LANGUAGE IN THE FIELD
		// AND THEN RUN THE ACTUAL QUERY THROUGH THIS LANGUAGE SO THAT THAT LANGUAGE IS USED 
		// IN THE PRIMARY QUERY.
		
		$orderColumn = $model->getAliasedPrimaryKey();
		if ($model->hasField($sort)) {
			$orderColumn = $model->getAliasedField($sort);
		} elseif ($model instanceof TranslatableModel && $model->getTranslation()->hasField($sort)) {
			$orderColumn = $model->getTranslation()->getAliasedField($sort);
		}
		
		$orderDirection = (strtoupper($dir) === 'ASC') ? 'ASC' : 'DESC';
		
		$paginator = $model->newPaginator(100)
						   ->orderBy($orderColumn, $orderDirection);
		
		if ($model instanceof TranslatableModel) {
			$paginator->with(SINGLE_TRANSLATION_RELATION)
					  ->with(MULTIPLE_TRANSLATION_RELATION);
		}
		
		$paginator->setPrevious(__('rozadmyn.previous'));
		$paginator->setNext(__('rozadmyn.next'));
		
		if ($search) {
			$paginator->where(function($query) use ($search, $model) {
							$fields = $model->getAliasedFields();
							if ($model instanceof TranslatableModel) {
								$fields = array_merge($fields, $model->getTranslation()->getAliasedFields());
							}

							$terms = explode(' ', $search);

							foreach ($terms as $term) {
								$query->where(function($query) use ($fields, $term) {
									foreach ($fields as $field) {
										$query->orWhereContains($field, $term);
									}
								});
							}
					   });
		}
		
		$conditions = $this->getConditions();
		if (!empty($conditions)) {
			$paginator->where($conditions);
		}
		
		$models = $paginator->fetch($page);
		
		$this->set(compact('models', 'search', 'page', 'sort', 'dir', 'paginator'));
	}
	
	/**
	 * Renders the CREATE form for the Model associated with this Controller.
	 */
	public function add() {
		$this->prepareForm();
	}
	
	/**
	 * Processes the data sent to the server from the CREATE form.
	 */
	protected function post_add() {
		$model = $this->process();
		
		if (!$model->hasErrors()) {
			Flash::write('success', t('rozadmyn.form_success'));
			Redirect::referer();
		}
		
		else {
			Flash::write('error', t('rozadmyn.form_error'));
			Flash::write(camel2snake($this->getName()), Request::post());
			Redirect::route('rozadmyn.default', array(
							'controller' => camel2snake(Rozadmyn::getControllerAlias(get_class($this))),
							'method' => 'index'));
		}
	}
	
	/**
	 * Renders the UPDATE form for the Model associated with this Controller.
		*/
	protected function edit($id, $language = null) {
		$this->prepareForm($id, $language);
	}
	
	/**
	 * Processes the data sent to the server from the UPDATE form.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 */
	protected function post_edit($id, $language = null) {
		$model = $this->process($id, $language);
		
		if (!$model->hasErrors()) {
			Flash::write('success', t('rozadmyn.form_success'));
		}
		
		else {
			Flash::write('error', t('rozadmyn.form_error'));
			Flash::write(camel2snake($this->getName()), Request::post());
		}
		
		Redirect::referer();
	}
	
	/**
	 * Deletes an instance of the Model associated with this Controller.
	 */
	protected function post_delete() {
		$id = (int)Request::post('id');
		
		if ($id) {
			$model = $this->callModelStatic('read', [$id]);
			
			if ($model) {
				$model->delete();
				
				return $this->set(array(
					'success'	=> true,
					'code'		=> 1,
				));
			}
			
			return $this->set(array(
				'success'	=> false,
				'code'		=> -1,
			));
		}
		
		return $this->set(array(
			'success'	=> false,
			'code'		=> -2,
		));
	}
	
	/**
	 * Deletes all specified instances of the Model associated with this 
	 * Controller.
	 */
	protected function post_delete_batch() {
		$ids = Request::post('ids');
		
		$models = $this->callModelStatic('read', [$ids]);
		
		$models->delete();
	}
	
	/**
	 * Processes an add OR edit request and returns the resulting project. It
	 * saves the project and all its related data in the database and sets the
	 * proper flash messsages.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 * @return	\Rozyn\Model\Model
	 */
	protected function process($id = null, $language = null) {
		$model = run_language(function() use ($id, $language) {
			$model = $this->getRequestModel($id, $language);
			
			if ($model->validates()) {
				$model->saveAll();
			}
			
			return $model;
		}, $language);
		
		return $model;
	}
	
	/**
	 * Returns all submitted request data that has to do with the Model at hand.
	 * This method looks at both flash data and regular request data.
	 * 
	 * @return	\Rozyn\Data\Collection
	 */
	protected function getRequestModelData() {
		$data = new Collection(Flash::read(camel2snake($this->getName()), []));
		$data->add(Request::rawPost());
		
		return $data;
	}
	
	/**
	 * Returns a lazy instance of the Model, filling it with the Request data.
	 * Lazy refers to the model not having any relation data yet.
	 * 
	 * @return	\Rozyn\Model\Model
	 */
	protected function getRequestLazyModel($id, $language, Collection $data = null) {
		if (null === $data) {
			$data = $this->getRequestModelData();
		}
		
		$model = run_language(function() use ($id, $data) {
					$proxy = $this->getNewModelInstance();
					$model = ($id) ? $proxy->newQuery()
										   ->select()
										   ->whereEquals($proxy->getAliasedPrimaryKey(), $id)
										   ->fetchModel() : $this->getNewModelInstance();
					
					$model->fill(purify($data->items(), PURIFY_HTML));
					
					return $model;
				}, $language);
		
		return $model;
	}
	
	/**
	 * Returns an array of conditions for the model associated with this controller.
	 * 
	 * @return	array
	 */
	protected function getConditions() {
		return [];
	}
	
	/**
	 * Creates a new Project instance based on the data that is supplied through
	 * the current request.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 * @return	Model\Project
	 */
	abstract protected function getRequestModel($id = null, $language = null);
	
	/**
	 * Takes care of loading all of the variables that are used in the form.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 */
	abstract protected function prepareForm($id = null, $language = null);
	
	/**
	 * Forbid all helper functions in this Controller.
	 */
	protected function setup() {
		parent::setup();
		
		$this->forbid('process', 'getRequestLazyModel', 'getConditions', 'getRequestModelData', 'getRequestModel', 'prepareForm');
	}
}