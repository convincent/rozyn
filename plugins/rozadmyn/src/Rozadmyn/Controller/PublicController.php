<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Rozadmyn\Controller;

use Rozadmyn\Facade\Rozadmyn;
use Rozyn\Auth\AuthHandler;
use Rozyn\Controller\Controller;
use Rozyn\Facade\Auth;
use Rozyn\Auth\LoginFailedException;
use Rozyn\Auth\LogoutFailedException;
use Rozyn\Auth\ClientNotAuthorizedException;
use Rozyn\Facade\Redirect;
use Rozyn\Facade\Request;

class PublicController extends Controller {
	/**
	 * The folder containing this controller's view files.
	 * 
	 * @var string
	 */
	protected $views = 'public';
	
	/**
	 * Generate the login view for the client. If the client is already logged 
	 * in and authorized to access rozadmyn, redirect them to the rozadmyn 
	 * dashboard instead.
	 */
	public function login() {
		// If the user is already logged in with an admin account, redirect them
		// to the Admin Panel home page.
		if (Rozadmyn::isClientAuthorized()) {
			Redirect::route('rozadmyn.index');
		}
	}
	
	/**
	 * Terminate the client's session. Redirect them to the rozadmyn login page
	 * after successfully logging them out.
	 */
	public function logout() {
		try {
			Auth::logout();
		} catch (LogoutFailedException $e) {}
		
		Redirect::route('rozadmyn.login');
	}
	
	/**
	 * Process the client's request to login. After authenticating the client, 
	 * we also make sure they have access to rozadmyn before redirecting them
	 * to the dashboard. If anything goes wrong, the client is redirected to the
	 * login page with the appropriate error message.
	 */
	public function processLogin() {
		$credentials = array(
			AuthHandler::AUTH_FIELD		=> Request::post(AuthHandler::AUTH_FIELD),
			AuthHandler::PASSWORD_FIELD	=> Request::post(AuthHandler::PASSWORD_FIELD),
		);
		
		try {
			Auth::login($credentials, (bool)Request::post('remember', false));
			
			// After logging in the client, make sure they are permitted to
			// access the rozadmyn panel.
			if (!Rozadmyn::isClientAuthorized()) {
				throw new ClientNotAuthorizedException("Client is not authorized to access Rozadmyn.");
			}
			
			// Redirect the client to rozadmyn's dashboard.
			Redirect::route('rozadmyn.index');
		} catch (LoginFailedException $e) {
			// If authentication failed for whatever reason, redirect the client
			// back to the login page with an error message explaining what went
			// wrong.
			Redirect::flash('error', 'Login failed')->route('rozadmyn.login');
		} catch (ClientNotAuthorizedException $e) {
			// If the client was authenticated, but is not authorized to access
			// rozadmy, also return them to the login page with the appropriate
			// error message.
			Redirect::flash('error', 'Unauthorized!')->route('rozadmyn.login');
		}
	}
}