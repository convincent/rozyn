<?php

/**
 * The Rozadmyn plugin implements a generic CMS for our website. It interacts
 * easily with your application's Controllers and Models so that you are in full
 * control of what sections of your website you want to open up for managing.
 * 
 * It also handles things like Routing automatically, though you can always 
 * specify your own additional routing rules to complement the ones generated
 * automatically by this plugin.
 */

use Rozyn\Routing\Route;

/* @var $plugin \Rozadmyn\Rozadmyn */

// Add a default filter which transforms a controller URL parameter into a valid
// Controller class name.
$plugin->app()->router()->addFilter('rozadmyn.default', function(Route $route) use ($plugin) {
	// Since the parameters that were matched for this Route are passed
	// by reference, we can edit them in this filter callback. This way
	// we can load the actual controller based on the controller name
	// that was specified in the URL.
	try {
		$controller = $plugin->getController($route->getMatch('controller'));
		
		if (class_exists($controller)) {
			$route->setController($controller);
			return true;
		}
		
		return false;
	} catch (\Exception $e) {
		return false;
	}
});

// Define a default filter used by all GET requests to make sure that the method
// that is being requested actually exists.
$plugin->app()->router()->addFilter('rozadmyn.default.get', function(Route $route) {
	// Make sure the specified method exists.
	return method_exists($route->getController(), $route->getMethod());
});

// Define a default filter used by all POST requests to prepend a potential POST
// prefix to method names.
$plugin->app()->router()->addFilter('rozadmyn.default.post', function(Route $route) use ($plugin) {
	// Prepend a unique prefix to the method name so that we can 
	// distinguish between methods that handle GET and POST requests.
	$method = $plugin->app()->config()->read('rozadmyn.post_prefix') . $route->getMethod();
	
	if (method_exists($route->getController(), $method)) {
		$route->setMethod($method);
		return true;
	}

	return false;
});

// Import our default config settings.
$plugin->app()->config()->import(__DIR__ . DS . 'config.php');

// Load our functions.
require __DIR__ . DS . 'functions.php';

// Create a separate directory in our custom config directory for rozadmyn.
if (!is_dir($rozadmynConfigDir = $plugin->getConfigPath())) {
	mkdir($rozadmynConfigDir);
}

// Include an optional custom config file. Create it if it doesn't exist yet.
if (!file_exists($custom = $rozadmynConfigDir . DS . 'config.php')) {
	touch($custom);
}

// Import the custom config file.
$plugin->app()->config()->import($custom);

// Load our native public routes.
$plugin->app()->router()->import(__DIR__ . DS . 'routes' . DS . 'public.php');

// Register our native protected admin routes.
$plugin->registerRoutes(__DIR__ . DS . 'routes' . DS . 'admin.php');

// Register Rozadmyn's built-in controllers.
$plugin->registerController('\Rozadmyn\Controller\Admin\UserController', 'rozadmyn.user');
$plugin->registerController('\Rozadmyn\Controller\Admin\LogController', 'rozadmyn.log');

// Try to load our main App's AdminControllers automatically.
if ($plugin->app()->config()->has('rozadmyn.controller_directories')) {
	foreach ($plugin->app()->config()->read('rozadmyn.controller_directories', []) as $dir => $namespace) {
		if (file_exists($dir) && is_dir($dir)) {
			dir_map($dir, function($file) use ($plugin, $namespace) {
				if (ends_with($name = str_replace('.php', '', $file), 'Controller')) {
					$plugin->registerController($namespace . $name);
				}
			});
		}
	}
}