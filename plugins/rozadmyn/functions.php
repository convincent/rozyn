<?php



/*******************************************************************************
 * I. TRANSLATIONS / LOCALE
 * 
 * The Rozyn core functions already provide a lot of functionality for multi-
 * lingual applications, but this plugin has to perform a lot of multilingual 
 * automatically and so a little more support is needed. The functions below
 * are meant to make working with multiple languages a little bit easier.
 * 
 *******************************************************************************/

/**
 * Returns the full name of the language that corresponds to the given locale
 * string in the current site language. So for example, on an input of "nl" 
 * this function would return "Dutch" if the current site language equals
 * en, but it would return "Nederlands" if the current site language
 * equals nl.
 * 
 * You can specify an optional second argument to override this behavior. You 
 * can specify a language of your own in which the resulting string should be
 * translated. If this argument is omitted, the current site language is used
 * instead, as explained earlier.
 * 
 * @param	string	$locale
 * @param	string	$language
 * @return	string
 */
function language($locale = null, $language = null) {
	return t(prefix($locale ?: site_language(), 'language_'), [], $language);
}

/**
 * Returns the full name of the country that corresponds to the given locale
 * string in the current site language. So for example, on an input of "nl" 
 * this function would return "The Netherlands" if the current site language 
 * equals en, but it would return "Nederland" if the current site language
 * equals nl.
 * 
 * You can specify an optional second argument to override this behavior. You 
 * can specify a language of your own in which the resulting string should be
 * translated. If this argument is omitted, the current site language is used
 * instead, as explained earlier.
 * 
 * @param	string	$locale
 * @param	string	$language
 * @return	string
 */
function country($locale = null, $language = null) {
	return t(prefix($locale ?: site_language(), 'country_'), [], $language);
}