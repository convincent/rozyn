<?php

return array(
	// General
	'add'			=> 'Toevoegen',
	'edit'			=> 'Bewerken',
	'delete'		=> 'Verwijderen',
	'perma_delete'	=> 'Permanent verwijderen',
	
	'rozadmyn.i18n'	=> 'Vertalingen',
	
	'rozadmyn.search_placeholder'	=> 'Zoeken...',
	'rozadmyn.continue'				=> 'Doorgaan',
	'rozadmyn.cancel'				=> 'Annuleren',
	'rozadmyn.you_sure'				=> 'Weet je het zeker?',
	'rozadmyn.with_selected'		=> 'Met geselecteerde',
	
	'rozadmyn.previous'		=> 'Vorige',
	'rozadmyn.next'			=> 'Volgende',
	
	// Dashboard
	'rozadmyn.dashboard'	=> 'Dashboard',
	'rozadmyn.cms_title'	=> 'Dit is het CMS van: <strong>{host}</strong>',
	
	'rozadmyn.admin_login'	=> 'Admin Login',
	'rozadmyn.email'		=> 'E-mailadres',
	'rozadmyn.password'		=> 'Wachtwoord',
	'rozadmyn.remember_me'	=> 'Onthoud mij',
	'rozadmyn.login'		=> 'Inloggen',
	'rozadmyn.logout'		=> 'Uitloggen',
	
	'rozadmyn.username'		=> 'Gebruikersnaam',
	'rozadmyn.firstname'	=> 'Voornaam',
	'rozadmyn.lastname'		=> 'Achternaam',
	'rozadmyn.confirm_password'		=> 'Bevestig wachtwoord',
	
	'rozadmyn.home'			=> 'Home',
	'rozadmyn.add'			=> 'Toevoegen',
	'rozadmyn.index'		=> 'Overzicht',
	
	'rozadmyn.confirm_delete'		=> 'Weet je zeker dat je <strong>{name}</strong> wilt verwijderen?',
	'rozadmyn.confirm_delete_batch'	=> 'Weet je zeker dat je deze <strong>{count}</strong> items wilt verwijderen?',
	
	'rozadmyn.form_success'	=> 'Het item is succesvol opgeslagen!',
	'rozadmyn.form_error'	=> 'Er is een fout opgetreden bij het opslaan van dit item. Probeer het alstublieft opnieuw',
	
	'rozadmyn.language'		=> 'Taal',
	
	'rozadmyn.save'	=> 'Opslaan',
	
	// User management.
	'rozadmyn.rozadmyn.user'	=> 'Gebruikers',
	'rozadmyn.user.add'			=> 'Gebruiker toevoegen',
	'rozadmyn.user.edit'		=> 'Gebruiker bewerken',
	'rozadmyn.full_name'		=> 'Naam',
	'rozadmyn.groups'			=> 'Groepen',
	'rozadmyn.permissions'		=> 'Rechten',
	
	'rozadmyn.rozadmyn.log'		=> 'Logs',
	'rozadmyn.error'			=> 'Error',
);