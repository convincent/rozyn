<?php

return array(
	'language_nl' => 'Nederlands',
	'language_en' => 'Engels',
	'language_de' => 'Duits',
	'language_fr' => 'Frans',
);