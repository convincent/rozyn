<?php

use Rozyn\Routing\Router;

$router->group(['format' => '/mediadmyn/file', 'controller' => '\Mediadmyn\Controller\Admin\FileController'], function(Router $router) {		
	// The route that loads the AJAX file browser.
	$router->connect(array(
		'name' => 'mediadmyn.browse',
		'format' => '/browse/:page/:search',
		'method' => 'browse',
		'args' => ['page' => 1, 'search' => ''],
		'type' => 'ajax'));

	$router->alias('mediadmyn.browse_no_page', 'mediadmyn.browse', 'browse');
	$router->alias('mediadmyn.browse_no_search', 'mediadmyn.browse', 'browse/:page');

	
	
	/***************
	 * JSON ROUTES *
	 ***************/
	$router->group(['type' => 'json'], function(Router $router) {
		// The global delete route that is used to delete a single file.
		$router->post(array(
			'name' => 'mediadmyn.post.delete', 
			'format' => 'delete/*', 
			'method' => 'post_delete'));

		// The global delete route that is used to delete any number of files
		$router->post(array(
			'name' => 'mediadmyn.post.delete_batch', 
			'format' => 'delete_batch', 
			'method' => 'post_delete_batch'));
	});

	// Default GET route.
	$router->get(array(
		'name' => 'mediadmyn.default', 
		'format' => ':method/*'));

	// Default POST route.
	$router->post(array(
		'name' => 'mediadmyn.post.default',
		'filters' => ['rozadmyn.default.post'],
		'format' => ':method/*'));
});