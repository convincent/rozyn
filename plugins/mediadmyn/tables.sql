--
-- Table structure for table `mediadmyn_file`
--

CREATE TABLE `mediadmyn_file` (
`id` int(11) NOT NULL,
  `file_type_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `original_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mime` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mediadmyn_file_translation`
--

CREATE TABLE `mediadmyn_file_translation` (
`id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `file_id` int(11) NOT NULL,
  `alt` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mediadmyn_file_type`
--

CREATE TABLE `mediadmyn_file_type` (
`id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mediadmyn_file`
--
ALTER TABLE `mediadmyn_file`
 ADD PRIMARY KEY (`id`), ADD KEY `name` (`name`), ADD KEY `original_name` (`original_name`), ADD KEY `width` (`width`,`height`,`size`), ADD KEY `mime` (`mime`), ADD KEY `file_type_id` (`file_type_id`);

--
-- Indexes for table `mediadmyn_file_translation`
--
ALTER TABLE `mediadmyn_file_translation`
 ADD PRIMARY KEY (`id`), ADD KEY `language` (`language`), ADD KEY `file_id` (`file_id`);

--
-- Indexes for table `mediadmyn_file_type`
--
ALTER TABLE `mediadmyn_file_type`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `key` (`key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mediadmyn_file`
--
ALTER TABLE `mediadmyn_file`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mediadmyn_file_translation`
--
ALTER TABLE `mediadmyn_file_translation`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mediadmyn_file_type`
--
ALTER TABLE `mediadmyn_file_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `mediadmyn_file_type` (`id`, `key`) VALUES
(1, 'image'),
(2, 'pdf');