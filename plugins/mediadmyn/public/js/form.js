var MediadmynForm;

(function($) {
	MediadmynForm = Form.extend({		
		/**
		 * Makes all the file upload fields responsive. This method first
		 * performs some generic logic that should be applied to every single
		 * file input. Afterwards, submethods are called to handle specific
		 * types of file inputs (for example: image file uploads, excel file
		 * uploads, etc.)
		 * 
		 * @return	void
		 */
		respondFileUploads: function() {
			this._super();
			
			var form = this;
			
			this.element.find('input[type=file]').each(function() {
				var input	= $(this),
					name	= $(this).attr('name'),
					element = $(this).siblings('.form-element').first();
					
				if (input.attr('multiple')) {
					element.addClass('multiple');
				}
				
				// If the input has a data-id attribute, paste that value to the
				// data-ids attribute since that's what we'll be using here.
				if (input.data('id') && !input.data('ids')) {
					input.data('ids', input.data('id'));
				}
					
				// If the input has a data-ids attribute, use those IDs to
				// generate hidden inputs so that existing file relations aren't
				// destroyed when the form is submitted.
				if (input.data('ids')) {
					var ids = ("" + input.data('ids')).split(',');
					
					for (var i in ids) {
						form.getElement().append($('<input>').attr({
							type: 'hidden',
							name: name
						}).val(parseInt(ids[i])));
					}
				}
				
				var loadBrowser = function(e) {
					var settings = {
						input: input,
						types: (element.data('type')) ? [element.data('type')] : null,
						multiple: (element.hasClass('multiple')) ? 1 : 0,
						extensions: (element.data('extensions')) ? element.data('extensions').split(',') : null,
					};
					
					var browser = new MediadmynBrowser(settings);
				};
				
				// Unbind the change event handler that was added in the parent
				// method, because this input doesn't serve a functional purpose
				// anymore.
				$(this).unbind('change');
				
				// Change the input from a file input into a text input so that
				// no default behaviour related to file input is fired. We also
				// turn off autocompletion so that we don't get any unexpected
				// behaviour when the web browser tries to pull some smart
				// logic for this input.
				$(this).attr({
					type: 'text',
					autocomplete: 'off'
				});
				
				// If the input had a data-value attribute, set the value of the
				// input to that attribute's value.
				if ($(this).data('value')) {
					$(this).val($(this).data('value'));
				}
				
				// Store the name attribute in a data field so that we can
				// safely remove it, but still be able to access it.
				$(this).data('name', $(this).attr('name'));
				
				// Remove the name attribute form the input so that its data is
				// not sent to the server when the form is submitted.
				$(this).removeAttr('name');
				
				// Add a keydown handler that loads the browser when the ENTER
				// or SPACEBAR is pressed. This is how normal file inputs work,
				// so let's recreate that behaviour here.
				$(this).on('keydown', function(e) {
					if (e.keyCode === 32 || e.keyCode === 13) {
						loadBrowser(e);
						
						e.preventDefault();
						return false;
					}
				});
				
				// We also need to load the browser on a regular click event.
				$(this).on('click', loadBrowser);
			});
		},
		
		/**
		 * Makes the image file upload fields responsive. This is a submethod
		 * that should only be called after respondFileUploads() has been called
		 * previously.
		 * 
		 * @return	void
		 */
		respondImageFileUploads: function() {
			this.element.find('input[type=file].image').each(function() {
				$(this).siblings('.form-element').data('type', 'image');
			});
		},
		
		/**
		 * Makes any rich textareas responsive. Partly inspired by
		 * https://www.youtube.com/watch?v=-aY5qm8KCGU
		 * 
		 * @return	s {void}
		 */
		respondRichTextAreas: function() {
			this._super();
			
			var self = this;
			
			// Initialize the rich text areas.
			this.element.find('textarea.rich').each(function() {
				var el = $(this),
					id = el.attr('id') || el.attr('name'),
					insertImageButton = $('<a>');
					
				insertImageButton.addClass('mediadmyn-insert-image-button button').on('click', function(e) {
					e.preventDefault();
					
					var browser = new MediadmynBrowser({
						types: ['image'],
						multiple: true
					});
					
					browser.on('confirm', function(self) {
						var thumbnails = self.getSelectedThumbnails();
						
						thumbnails.each(function() {
							CKEDITOR.instances[id].insertHtml('<img src="' + self.getThumbnailImage($(this)) + '" alt="" />');
						});
						
						self.close();
					});
				});
				
				insertImageButton.html('{{mediadmyn.insert_media}}');
				
				el.before(insertImageButton);
			});
		},
	});
	
})(jQuery);