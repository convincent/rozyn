$(document).ready(function() {
	var frm = new Form($('#mediadmyn-add-files-form'), {
		ajax: true
	});
	
	var overview = new Overview($('form.overview'));
	
	frm.on('success', function(data) {
		html = $(data);
		
		var rows = html.find('tbody tr');
		
		if (rows.size()) {
			overview.getBody().prepend(rows);
			overview.getElement().show();
			overview.respondActions();
			
			rows.find('a').attr('target', '_blank');
		}
	});
});