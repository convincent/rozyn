var MediadmynBrowser;

(function($) {
	MediadmynBrowser = Base.extend({
		/**
		 * Initializes the browser.
		 * 
		 * @param	{string}	name
		 * @param	{jQuery}	input
		 * @param	{dict}		settings
		 * @return	s {void}
		 */
		init: function(settings) {
			var self = this;
			
			this.element	= null;
			this.overlay	= null;
			this.ids		= [];
			
			var defaults = {
				name: null,
				input: null,
				form: null,
				types: [],
				extensions: [],
				multiple: false,
			};
			
			this.settings = $.extend({}, defaults, settings);
			
			this.on('load', function() {
				self.respond();
			});
			
			this.load();
		},
		
		/**
		 * A pass-along method for jQuery's find() method.
		 * 
		 * @param	string		s
		 * @return	s {jQuery}
		 */
		find: function(s) {
			return this.getElement().find(s);
		},
		
		/**
		 * Takes care of the initial load of the browser.
		 * 
		 * @return	s {void}
		 */
		load: function() {
			var overlay = new Overlay();
			
			overlay.setContent('');
			overlay.show();
			
			this.setOverlay(overlay);
						
			var self	= this;
			$.ajax({
				url: __ROOT__ + '/admin/mediadmyn/file/browse',
				type: 'GET',

				data: {
					extensions: self.getExtensions(),
					multiple: self.isMultiple(),
					types: self.getTypes(),
					selected: self.getInitialIds()
				},

				success: function(response, textStatus, jqXHR) {
					var overlay = self.getOverlay();
					
					overlay.setContent(response);

					if (overlay.find('#mediadmyn-browser').size()) {
						self.setElement(overlay.find('#mediadmyn-browser'));
						self.fire('load', [self]);
						overlay.show();
					}
				}
			});
		},
		
		/**
		 * Closes the browser.
		 */
		close: function() {
			if (this.getOverlay()) {
				this.fire('close', [this]);

				this.getOverlay().die();
			}
		},
		
		/**
		 * Makes the entire browser responsive.
		 * 
		 * @return	s {void}
		 */
		respond: function() {
			this.ids = this.getInitialIds();
			
			this.bindEventHandlers();
			this.respondButtons();
			this.respondThumbnails();
			this.respondPagination();
			this.respondSearch();
			this.respondForm();
		},
		
		/**
		 * Binds default action handlers to the browser.
		 * 
		 * @return	s {void}
		 */
		bindEventHandlers: function() {
			var self	= this;
			
			if (this.hasForm() && this.hasInput()) {
				var form	= this.getForm(),
					input	= this.getInput();
			
				// Bind a confirm action that is fired whenever the user confirms
				// their selection.
				this.on('confirm', function() {
					var element	= input.siblings('.form-element').first();
					
					// Reset the original input's value so that
					// if no files have been selected, it won't
					// pass validation if it has been marked as
					// obligatory.
					input.val('');

					// Close the overlay.
					self.close();

					// Display the file names in the form element.
					element.html(self.getSelectedNames().join(', '));

					// Extract the input name that should be 
					// used for the inputs that are going to
					// be holding the IDs of the selected files.
					var name = self.getName();

					// Remove all inputs that have that name.
					form.find('input[name="' + name + '"]').remove();

					// Retrieve the selected thumbnails and
					// create a new hidden input for each one so
					// that its ID is sent to the server when
					// the form is submitted.
					if (self.getSelectedThumbnails().size()) {
						var ids = self.getSelectedIds();
						for (i = 0; i < ids.length; i++) {
							thumbnail = self.getSelectedThumbnail(ids[i]);
							
							form.append($('<input>').attr({
								name: name,
								type: 'hidden'
							}).val(thumbnail.data('id')));

							// Also update the original input so that it
							// can pass its validation rules.
							input.val(thumbnail.data('id'));
						}
					} 

					// If not a single thumbnail was selected, clear the value.
					else {
						form.append($('<input>').attr({
							name: name,
							type: 'hidden'
						}).val(0));

						// Also update the original input.
						input.val(0);
					}
				});
			}
		},
		
		/**
		 * Makes the action buttons in the browser responsive.
		 * 
		 * @return	s {void}
		 */
		respondButtons: function() {
			var self = this;
			this.getElement().find('.confirm-selection').on('click', function(e) {
				e.preventDefault();
				
				self.fire('confirm', [self]);
			});
		},
		
		/**
		 * Makes the thumbnails in the browser responsive.
		 * 
		 * @return	s {void}
		 */
		respondThumbnails: function() {
			var self = this;
			
			// Pre-select any previously selected thumbnails.
			var first = this.getThumbnails().first(),
				ids   = this.getSelectedIds();
		
			for (var idx = ids.length - 1; idx > -1; idx--) {
				var id = ids[idx];
				
				this.getThumbnails().each(function() {
					if ($(this).data('id') === id) {
						$(this).addClass('selected').insertAfter(self.getPagination());
						return false;
					}
				});
			}
			
			this.updateSelectedIndices();
			
			this.getElement().find('.mediadmyn-file-thumbnail').on('click', function(e) {
				if ($(this).hasClass('selected')) {
					self.deselect($(this));
				} else {
					self.select($(this));
				}
				
				if (!self.isMultiple()) {
					$(this).siblings('.selected').each(function() {
						self.deselect($(this));
					});
				}
			});
		},
		
		/**
		 * Makes the search fields in the browser responsive.
		 * 
		 * @return	s {void}
		 */
		respondSearch: function() {
			this.searchTimeout;
			
			var input	= this.getElement().find('input.search'),
				self	= this;
			
			if (input.size()) {
				input.on('keyup', function(e) {
					clearTimeout(self.searchTimeout);
					
					self.searchTimeout = setTimeout(function() {
						self.search(input.val());
					}, 200);
				});
			}
		},
		
		/**
		 * Makes the form in the browser responsive.
		 * 
		 * @return	s	{void}
		 */
		respondForm: function() {
			this.getElement().find('form').on('submit', function(e) {
				e.preventDefault();
			});
		},
		
		/**
		 * Makes the pagination responsive.
		 * 
		 * @return	s	{void}
		 */
		respondPagination: function() {
			var self = this,
				pagination = this.getPagination();
			
			pagination.find('li a').each(function() {
				var parent = $(this).parent();
				if (!parent.hasClass('previous') && !parent.hasClass('next')) {
					$(this).on('click', function(e) {
						e.preventDefault();

						self.loadThumbnails($(this).html(), self.getSearchInput().val());
					});
				}
			});
			
			pagination.find('.next a').on('click', function(e) {
				e.preventDefault();
				var current = parseInt(pagination.find('.active').text());
				
				self.loadThumbnails(current + 1, self.getSearchInput().val());
			});
			
			pagination.find('.previous a').on('click', function(e) {
				e.preventDefault();
				var current = parseInt(pagination.find('.active').text());
				
				self.loadThumbnails(current - 1, self.getSearchInput().val());
			});
		},
		
		/**
		 * Returns the selected thumbnails.
		 * 
		 * @return	s {jQuery}
		 */
		getSelectedThumbnails: function() {
			return this.getElement().find('#mediadmyn-browser-selected').children();
		},
		
		/**
		 * Returns the original selected thumbnails (not the clones).
		 * 
		 * @return	s {jQuery}
		 */
		getOriginalSelectedThumbnails: function() {
			return this.getElement().find('#mediadmyn-browser-files .mediadmyn-file-thumbnail.selected');
		},
		
		/**
		 * Returns a specific selected thumbnail.
		 * 
		 * @return	s {jQuery}
		 */
		getSelectedThumbnail: function(id) {
			var res;
			
			this.getElement().find('#mediadmyn-browser-selected').children().each(function() {
				if ($(this).data('id') === id) {
					res = $(this);
					return false;
				}
			});
			
			return res;
		},
		
		/**
		 * Returns all thumbnails.
		 * 
		 * @return	s {jQuery}
		 */
		getThumbnails: function() {
			return this.getElement().find('#mediadmyn-browser-files .mediadmyn-file-thumbnail');
		},
		
		/**
		 * Returns the file names of the selected thumbnails.
		 * 
		 * @return	s {jQuery}
		 */
		getSelectedNames: function() {
			var names = [];
			
			this.getSelectedThumbnails().each(function() {
				names.push($(this).find('.details .file-name').html());
			});
			
			return names;
		},
		
		/**
		 * Returns the file IDs of the selected thumbnails.
		 * 
		 * @return	s {int}
		 */
		getSelectedIds: function() {
			return this.ids;
		},
		
		/**
		 * Synchronizes the selected ids array with the ids of the selected
		 * thumbnails.
		 */
		synchronizeIds: function() {
			this.ids = [];
			
			var self = this;
			
			this.getSelectedThumbnails().each(function() {
				self.ids.push($(this).data('id'));
			});
		},
		
		/**
		 * Add a thumbnail to the list of selected thumbnails..
		 * 
		 * @param	{jQuery}	thumbnail
		 */
		select: function(thumbnail) {
			var id		= thumbnail.data('id'),
				clone	= thumbnail.clone();
		
			thumbnail.addClass('selected');
			
			this.ids.push(id);
			
			this.getElement().find('#mediadmyn-browser-selected').append(clone);
			
			this.fillDetails(clone);
			
			this.updateSelectedIndices();
		},
		
		/**
		 * Remove a thumbnail from the list of selected thumbnails. Returns
		 * whether or not a thumbnail was removed from the selection.
		 * 
		 * @param	{jQuery}	thumbnail
		 * @returns	{Boolean}
		 */
		deselect: function(thumbnail) {
			var id		= thumbnail.data('id'),
				index	= this.ids.indexOf(id),
				self	= this;
		
			thumbnail.removeClass('selected');
			thumbnail.attr('data-index', null);

			if (index > -1) {
				this.ids.splice(index, 1);
			}
			
			this.updateSelectedIndices();
			
			this.getSelectedThumbnails().each(function() {
				if ($(this).data('id') === id) {
					$(this).remove();
					
					var lastSelected = self.getSelectedThumbnails().last();
					
					self.fillDetails((lastSelected.size()) ? lastSelected : null);
					
					return true;
				}
			});
			
			return false;
		},
		
		updateSelectedIndices: function() {
			var ids = this.ids;
			
			console.log(ids);
			
			this.getOriginalSelectedThumbnails().each(function() {
				index = ids.indexOf($(this).data('id'));
				
				console.log(index);
				
				if (index > -1) {
					$(this).attr('data-index', index + 1);
				}
			});
		},
		
		/**
		 * Loads the thumbnails for a given page or search.
		 * 
		 * @param	int		page
		 * @param	string	search
		 */
		loadThumbnails: function(page, search) {
			var self = this,
				filesWrapper = this.find('#mediadmyn-browser-files');
		
			if (filesWrapper.hasClass('loading')) {
				return false;
			}
		
			filesWrapper.addClass('loading');
			
			search	= (typeof(search) === 'undefined') ? null : encodeURIComponent(search);
			page	= (typeof(page)	  === 'undefined') ? 1	  : parseInt(page);
			
			$.ajax({
				url: __ROOT__ + '/admin/mediadmyn/file/browse',
				type: 'GET',

				data: {
					extensions: this.getExtensions(),
					multiple: this.isMultiple(),
					types: this.getTypes(),
					search: search,
					page: page
				},

				success: function(response, textStatus, jqXHR) {
					var data		 = $(response),
						pagination	 = self.getPagination();
				
					var newPagination = data.find('ul.pagination'),
						newThumbnails = newPagination.nextAll();
				
					// Set a fixed height for the files wrapper while we load
					// our new thumbnails.
					filesWrapper.css('height', filesWrapper.outerHeight() + 'px');
				
					// Refill the pagination.
					pagination.html(newPagination.children());
					
					// Clear the thumbnail section.
					pagination.nextAll().remove();
					
					// If the first page is loaded and no search query has been
					// provided, the browser was essentially reset to its
					// initial state, so we also have to prepend any selected
					// thumbnails to the newly loaded thumbnails so that the
					// user can easily deselect them if they so desire.
					if (page === 1 && (search === null || search.length === 0)) {
						var selected = self.getSelectedThumbnails().clone();
						
						selected.each(function() {
							if (!newThumbnails.filter('[data-id=' + $(this).data('id') + ']').size()) {
								filesWrapper.append($(this));
							}
						});
					}
					
					// Refill the thumbnail section with the new thumbnails.
					filesWrapper.append(newThumbnails);
					
					// Now that our new thumbnails are loaded, remove the
					// loading class from our files wrapper.
					filesWrapper.removeClass('loading');
					
					// Reset the height of the file wrapper to have it adjust to
					// its new contents.
					filesWrapper.css('height', 'auto');
					
					self.respondPagination();
					self.respondThumbnails();
				}
			});
		},
		
		/**
		 * Filters the thumbnails based on a the given search string.
		 * 
		 * @param	{string}	string
		 * @return	s {void}
		 */
		search: function(search) {
			this.loadThumbnails(1, search);
		},
		
		/**
		 * Returns the src attribute belonging to the thumbnail image.
		 * 
		 * @returns {string}
		 */
		getThumbnailImage: function(thumbnail) {
			var src = thumbnail.find('.preview').css('backgroundImage').trim();
			
			src = src.replace(new RegExp('\\/mediadmyn_thumbnail_([^\\/]+)$'), '/$1');
			src = src.replace(new RegExp('^url\\("'),	'');
			src = src.replace(new RegExp('"\\)$'),		'');
			
			return src;
		},
		
		/**
		 * Fills the details section of the browser with the data for the 
		 * specified thumbnail. Returns whether or not the details are filled.
		 * 
		 * @param	{jQuery}	thumbnail
		 * @returns {Boolean}
		 */
		fillDetails: function(thumbnail) {
			var details	= this.getDetailsElement();
			
			if (thumbnail === null) {
				details.removeClass('filled');
				details.find('.preview').html('');
				details.find('.details').html('');
				
				return false;
			}
			
			details.find('.preview').html($('<img>').attr({
				alt: '',
				src: this.getThumbnailImage(thumbnail),
			}));
			
			details.find('.details').html(thumbnail.find('.details').html());
			
			details.addClass('filled');
			
			return true;
		},
		
		/**
		 * Empties the details section of the browser.
		 * 
		 * @return	s {void}
		 */
		emptyDetails: function() {
			this.getDetailsElement().removeClass('filled');
		},
		
		/**
		 * Returns the name of the form element that will be used to store the
		 * IDs of the files selected within this browser.
		 * 
		 * @return	s {string}
		 */
		getName: function() {
			if (!this.hasName()) {
				this.settings.name = (this.hasInput()) ? this.getInput().data('name') : '';
			}
			
			if (this.isMultiple() && this.settings.name.slice(-2) !== '[]') {		
				this.settings.name += '[]';
			}
			
			return this.settings.name;
		},
		
		/**
		 * Returns whether or not this browser has a name.
		 * 
		 * @returns	{boolean}
		 */
		hasName: function() {
			return typeof(this.settings.name) !== 'undefined' && this.settings.name !== null;
		},
		
		/**
		 * Returns the <form> element that contains the input which triggered
		 * this browser.
		 * 
		 * @return	s {jQuery}
		 */
		getForm: function() {
			if (!this.settings.form && this.hasInput()) {
				this.settings.form = this.getInput().closest('form');
			}
			
			return this.settings.form;
		},
		
		/**
		 * Returns whether or not this browser has a <form> element associated
		 * with it.
		 * 
		 * @returns {boolean}
		 */
		hasForm: function() {
			return this.getForm() !== null;
		},
		
		/**
		 * Returns a jQuery object that represents the browser element.
		 * 
		 * @return	s {jQuery}
		 */
		getElement: function() {
			return this.element;
		},
		
		/**
		 * Returns a jQuery object that represents the browser details element.
		 * 
		 * @return	s {jQuery}
		 */
		getDetailsElement: function() {
			return this.getElement().find('#mediadmyn-browser-details');
		},
		
		/**
		 * Returns whether or not this browser supports mass selection.
		 * 
		 * @return	s {Boolean}
		 */
		isMultiple: function() {
			return this.settings.multiple;
		},
		
		/**
		 * Returns the types associated with this browser.
		 * 
		 * @return	s {Array} 
		 */
		getTypes: function() {
			return this.settings.types;
		},
		
		/**
		 * Returns the extensions associated with this browser.
		 * 
		 * @return	s	{Array}
		 */
		getExtensions: function() {
			return this.settings.extensions;
		},
		
		/**
		 * Returns whether or not this browser has an input element associated
		 * with it.
		 * 
		 * @returns	{boolean}
		 */
		hasInput: function() {
			return this.settings.input !== null;
		},
		
		/**
		 * Returns the input element that triggered the browser.
		 * 
		 * @returns	{jQuery}
		 */
		getInput: function() {
			return this.settings.input;
		},
		
		/**
		 * Returns the search input element that allows the user to search 
		 * through the Mediadmyn library.
		 * 
		 * @return	s	{jQuery}
		 */
		getSearchInput: function() {
			return this.getElement().find('input.search');
		},
		
		/**
		 * Returns the pagination element of the browser.
		 * 
		 * @return	s {jQuery}
		 */
		getPagination: function() {
			return this.getElement().find('ul.pagination');
		},
		
		/**
		 * Returns an array of all IDs that were already loaded for the input
		 * associated with this browser.
		 * 
		 * @returns {Array}
		 */
		getInitialIds: function() {
			if (this.hasInput()) {
				// Get all the inputs that represent a file for the input associated
				// with this browser.
				var inputs = this.getForm().find('input[name="' + this.getName() + '"]');

				// If there are no such inputs or if the input value has been set to
				// 0 (ie: no file selected), refer to the data-ids attribute of the
				// original input instead, for it will hold all the IDs that are 
				// bound to it in a comma-separated list.
				if (this.getInput() && (!inputs.size() || inputs.first().val() === "0")) {
					return (this.getInput().data('ids')) ? ("" + this.getInput().data('ids')).split(',') : [];
				}

				// In all other cases we need to refer to the hidden inputs instead,
				// so we create a list and add the value of each hidden input to it
				// as an element.
				var ids = [];

				inputs.each(function() {
					ids.push(parseInt($(this).val()));
				});

				return ids;
			}
			
			return [];
		},
		
		/**
		 * Returns the overlay object that contains this browser.
		 * 
		 * @return	s	{Overlay}
		 */
		getOverlay: function() {
			return this.overlay;
		},
		
		/**
		 * Sets the element.
		 * 
		 * @param	{jQuery}
		 */
		setElement: function(element) {
			this.element = element;
		},
		
		/**
		 * Sets the element.
		 * 
		 * @param	{jQuery}
		 */
		setOverlay: function(overlay) {
			this.overlay = overlay;
		}
	});
})(jQuery);


