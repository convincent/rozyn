<?php

return array(
	// General
	'mediadmyn.add_files'		=> 'Bestanden uploaden',
	'mediadmyn.files'			=> 'Bestanden',
	'mediadmyn.images'			=> 'Afbeeldingen',
	'mediadmyn.edit_file'		=> 'Bestandsgegevens wijzigen',
	
	'mediadmyn.file_name'		=> 'Bestandsnaam',
	'mediadmyn.file_url'		=> 'URL',
	'mediadmyn.file_size'		=> 'Grootte',
	'mediadmyn.file_type'		=> 'Type',
	'mediadmyn.file_title'		=> 'Titel',
	'mediadmyn.file_description'=> 'Omschrijving',
	
	'mediadmyn.insert_media'	=> 'Afbeeldingen invoegen',
	
	'mediadmyn.browser_header'			=> 'Media browser',
	'mediadmyn.browser_details_empty'	=> 'Zodra je een bestand hebt geselecteerd, wordt hier de uitgebreide bestandsinformatie getoond.',
	'mediadmyn.browser_intro'			=> 'Kies 1 van de onderstaande bestanden. Als je het juiste bestand geselecteerd hebt, klik dan op "Bevestigen" (je kunt je selectie altijd later nog aanpassen). Mocht je het bestand dat je zoekt niet onmiddellijk kunnen vinden, filter de bestanden dan door de zoekbalk te gebruiken.',
	'mediadmyn.browser_intro_multiple'	=> 'Kies 1 of meer van de onderstaande bestanden. Als je de juiste bestanden geselecteerd hebt, klik dan op "Bevestigen" (je kunt je selectie altijd later nog aanpassen). Mocht je de bestanden die je zoekt niet onmiddellijk kunnen vinden, filter de bestanden dan door de zoekbalk te gebruiken.',
	'mediadmyn.clear_selection'			=> 'Selectie verwijderen',
	'mediadmyn.confirm_selection'		=> 'Bevestigen',
	
	'mediadmyn.image_width'		=> 'Breedte',
	'mediadmyn.image_height'	=> 'Hoogte',
	'mediadmyn.image_alt'		=> 'Alt tekst',
	
	'mediadmyn.search'			=> 'Zoeken',
	'mediadmyn.preview'			=> 'Voorvertoning',
	'mediadmyn.resolution'		=> 'Resolutie',
	
	'mediadmyn.files_add_text'	 => 'Via het formulier hieronder kun je bestanden uploaden. Selecteer eerst alle bestanden die je wilt uploaden en klik daarna op de "Opslaan" knop. Afhankelijk van de grootte van je bestanden kan het even duren voordat alle bestanden succesvol zijn opgeslagen.<br /><br />Zodra alle bestanden succesvol ge&uuml;pload zijn, zul je ze hieronder in een overzicht te zien krijgen. Vanaf dat moment kun je ze in het hele systeem gebruiken, maar vergeet ook vooral niet om na het uploaden de meta-data van de bestanden bij te werken om de vindbaarheid van je website verder te verbeteren!',
	'mediadmyn.files_index_text' => 'Hieronder vind je een overzicht van alle bestanden die ge&uuml;pload zijn. Je kunt de meta-data voor ieder bestand afzonderlijk wijzigen om zo de vindbaarheid van je website te optimaliseren.',
	
	// Admin
	'rozadmyn.mediadmyn'		=> 'Mediabeheer',
	'rozadmyn.mediadmyn_index'	=> 'Alle bestanden',
	'rozadmyn.mediadmyn_add'	=> 'Bestanden uploaden',
);