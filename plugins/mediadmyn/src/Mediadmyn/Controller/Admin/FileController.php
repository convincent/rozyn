<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Mediadmyn\Controller\Admin;

use Rozadmyn\Controller\Admin\CrudController;
use Mediadmyn\Facade\Mediadmyn;
use Mediadmyn\Model\File;
use Rozyn\Composition\DI;
use Rozyn\Facade\Redirect;
use Rozyn\Facade\Request;
use Rozyn\Facade\Flash;

class FileController extends CrudController {
	protected $model = 'Mediadmyn\Model\File';
	
	protected $helpers	= array(
		'Html' => 'Rozyn\Helper\HtmlHelper'
	);
	
	/**
	 * @label	mediadmyn_index
	 */
	public function index($page = 1, $sort = null, $dir = null) {
		parent::index($page, $sort, $dir);
	}
	
	/**
	 * @label	mediadmyn_add
	 */
	public function add() {
		
	}
	
	protected function search($search = null, $page = 1, $sort = null, $dir = null) {
		parent::search($search, $page, $sort, $dir);
		
		$models = $this->get('models');
		
		$files = File::collection();
		
		foreach ($models as $model) {
			$image = $model->convertTo($model->getMediadmynKey());
			$image->set('preview', $image->getPreview());
			
			if ($image->width && $image->height) {
				$image->set('resolution', $image->width . ' x ' . $image->height);
			}
			
			$files->add($model->id(), $image);
			
			unset($model);
		}
		
		$this->set('models', $files);
	}
	
	protected function browse() {
		$page		= intval(Request::get('page', 1));
		$search		= Request::get('search', null);
		$types		= Request::get('types', null);
		$extensions	= Request::get('extensions', null);
		$multiple	= (bool)Request::get('multiple', false);
		
		$paginator	= File::paginate(24);
		
		$paginator->setPrevious(t('rozadmyn.previous'));
		$paginator->setNext(t('rozadmyn.next'));
		
		$query		= $paginator->getQuery();
		
		// Retrieve the type ids.
		if (is_array($types)) {
			$query->whereIn('FileType.key', $types);
		}
		
		if (is_array($extensions)) {
			foreach ($extensions as $ext) {
				$query->whereEndsWith('File.name', $ext);
			}
		}
		
		if ($search) {
			$terms = explode(' ', urldecode($search));

			foreach ($terms as $term) {
				$query->where(function($query) use ($term) {
					$fields = array_merge(	$query->getModel()->getAliasedFields(), 
											$query->getModel()->getTranslation()->getAliasedFields());
					
					foreach ($fields as $field) {
						$query->orWhereContains($field, $term);
					}
				});
			}
		}
		
		$query->orderBy($query->getModel()->getAliasedPrimaryKey(), 'DESC');
		
		// Load the preselected files
		$files = (null === ($tmp = Request::get('selected', null))) ? File::collection() : File::read($tmp);
		
		$this->set(compact('paginator', 'page', 'multiple', 'search', 'files'));
	}
	
	protected function post($postMethod, $args = null) {
		return $this->__callMethod('post_' . $postMethod);
	}
	
	protected function post_add() {
		$this->renders('/plugins/rozadmyn/views/overview');
		
		$uploads	= Request::files('files');
		$collection = File::collection();
		
		foreach ($uploads as $upload) {
			if ($upload['error'] === 0) {
				$model = Mediadmyn::identifyModel($upload['name']);

				// First, save the actual file.
				$file = DI::getInstanceOf('Rozyn\Filesystem\File', [$upload['tmp_name']]);
				$file->save(Mediadmyn::path($upload['name']));
				
				// Next, create a model instance that we'll store in the db.
				$instance = DI::getInstanceOf($model, [['name' => $file->name(), 'original_name' => $upload['name']]]);
				$instance->save();
				
				$instance->set('preview', $instance->getPreview());
				
				$collection->add($instance);
			}
		}
		
		// Define the other parameters that we need to pass on to our overview.
		$this->set(array(
			'collection'	=> $collection,
			'fields'		=> ['original_name', 'preview'],
			'actions'		=> ['edit' => t('edit'), 'delete' => t('delete')],
			'route'			=> 'mediadmyn.default'
		));
	}
	
	/**
	 * Creates a new instance based on the data that is supplied through the 
	 * current request.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 * @return	Model\HistoricProject
	 */
	protected function getRequestModel($id = null, $language = null) {
		$data	= $this->getRequestModelData();
		$model	= $this->getRequestLazyModel($id, $language, $data);
		
		return $model;
	}
	
	/**
	 * Takes care of loading all of the variables that are used in the form.
	 * 
	 * @param	int		$id
	 * @param	string	$language
	 */
	protected function prepareForm($id = null, $language = null) {
		$model = $this->getRequestModel($id, $language);
		
		$this->set('file', $model);
		$this->set('language', $language ?: site_language());
	}
	
	public function __beforeMethod($name, array $args = []) {
		ini_set('memory_limit', -1);
		
		return parent::__beforeMethod($name, $args);
	}
}