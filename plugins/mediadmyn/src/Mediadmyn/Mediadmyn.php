<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Mediadmyn;

use Rozadmyn\Rozadmyn;
use Rozyn\Facade\Request;
use Rozyn\Plugin\Plugin;
use Rozyn\Facade\Config;
use Rozyn\Filesystem\File;
use Rozyn\Filesystem\MimeTypes;
use Rozyn\Filesystem\UnsupportedFileExtensionException;

class Mediadmyn extends Plugin {
	
	use MimeTypes;
	
	/**
	 * The directory Mediadmyn uses to upload files to.
	 * 
	 * var string
	 */
	const UPLOAD_DIR = 'uploads';
	
	/**
	 * Holds all the model names that represent files of one of the file groups 
	 * recognized by Mediadmyn. Each key is the respective identifier for the 
	 * file group being represented by the model.
	 * 
	 * @var	array
	 */
	protected $models = [];
	
	/**
	 * Holds all the mime type patterns that are supposed to match mime types of
	 * files belonging to a specific file group. Each key is the identifier for
	 * the file group that is supposed to be matched by the pattern.
	 * 
	 * @var	array
	 */
	protected $patterns = [];
	
	/**
	 * Returns whether or not this plugin is required.
	 * 
	 * @return	boolean
	 */
	public function isRequired() {
		return preg_match('/^\/?' . preg_quote(Rozadmyn::SITE_AREA, '/') . '/', Request::getQuery());
	}
	
	/**
	 * Register a file group with Mediadmyn. This method requires:
	 * - A unique identifier for the file group to be registered
	 * - A string or regular expression which mime types of all the files in 
	 *	 this group should match.
	 * - The name of the model responsible for representing files of this group.
	 * 
	 * @param	string	$key
	 * @param	string	$pattern
	 * @param	string	$model
	 */
	public function registerFileGroup($key, $pattern, $model) {
		$this->models[$key]			= $model;
		$this->patterns[$key]		= $pattern;
	}
	
	/**
	 * Registers a new file group based on a file extension so that all files
	 * with the specified extension are included in this new file group.
	 * 
	 * @param	string	$key
	 * @param	string	$extension
	 * @param	string	$model
	 */
	public function registerFileType($key, $extension, $model) {
		// Make sure the extension is properly formatted.
		$extension = preg_replace('/^\./', '', strtolower($extension));
		
		// Make sure the extension matches a valid mime type.
		if (isset($this->mimeTypes[$extension])) {
			return $this->registerFileGroup($key, $this->mimeTypes[$extension], $model);
		}
		
		throw new UnsupportedFileExtensionException($extension);
	}
	
	/**
	 * Returns the $models array.
	 * 
	 * @return	array
	 */
	public function getModels() {
		return $this->models;
	}
	
	/**
	 * Returns the $patterns array.
	 * 
	 * @return	array
	 */
	public function getPatterns() {
		return $this->patterns;
	}
	
	/**
	 * Returns the name of the model matching the specified key. If no model is
	 * found for the key, null is returned instead.
	 * 
	 * @param	string	$key
	 * @return	string
	 */
	public function getModel($key) {
		return ($this->hasKey($key)) ? $this->models[$key] : 'Mediadmyn\Model\File';
	}
	
	/**
	 * Returns whether or not a given key is recognized by Mediadmyn.
	 * 
	 * @param	string	$key
	 * @return	boolean
	 */
	public function hasKey($key) {
		return array_key_exists($key, $this->getModels());
	}
	
	/**
	 * Tries to identify the file group to which the specified file belongs. If
	 * a match is found, the key that identifies that file group is returned. If
	 * no match is found, the default key for the fallback file group is 
	 * returned instead.
	 * 
	 * @param	string | Rozyn\Filesystem\File	$file
	 * @return	string
	 */
	public function identify($file) {
		// Get the mime type of the file.
		if (is_string($file)) {
			$file = new File($file);
		}
		
		$mime = $file->mime();
		
		foreach ($this->getPatterns() as $key => $pattern) {
			// Check regex patterns.
			if (starts_with($pattern, '/') && ends_with($pattern, '/') && preg_match($pattern, $mime) === 1) {
				return $key;
			}
			
			// Check simple patterns.
			if ($pattern !== null && strpos($mime, $pattern) !== false) {
				return $key;
			}
		}
		
		return Config::read('mediadmyn.default_key');
	}
	
	/**
	 * Tries to identify the file group to which the specified file belongs. If
	 * a match is found, the name of the model that is responsible for handling
	 * files from that file group is returned. If no match is found, the model 
	 * matching the default key is returned instead.
	 * 
	 * @param	string | Rozyn\Filesystem\File	$file
	 * @return	string
	 */
	public function identifyModel($file) {
		return $this->getModel($this->identify($file));
	}
	
	/**
	 * Returns the path to the specified file name.
	 * 
	 * @param	string	$file
	 * @return	string
	 */
	public function path($file) {
		return __PUBLIC__ . DS . self::UPLOAD_DIR . DS . $file;
	}
	
	/**
	 * Returns a relative URL to the specified asset.
	 * 
	 * @param	string	$file
	 * @return	string
	 */
	public function asset($file) {
		return asset(self::UPLOAD_DIR . '/' . $file);
	}
}