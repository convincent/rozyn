<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Mediadmyn\Model;

use Rozyn\Model\TranslationModel;

class FileTranslation extends TranslationModel {
	protected $prefix = 'mediadmyn_';
	
	protected $fields = array(
		'id'			=> 'INT',
		'file_id'		=> 'INT',
		'language'		=> 'VARCHAR',
		'title'			=> 'VARCHAR',
		'alt'			=> 'VARCHAR',
		'description'	=> 'TEXT'
	);
	
	/**
	 * Tries to guess the database table name for this model.
	 * 
	 * @return	string
	 */
	public function inferTable() {
		return 'file_translation';
	}
}