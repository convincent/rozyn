<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Mediadmyn\Model;

use Mediadmyn\Facade\Mediadmyn;
use Rozyn\Model\TranslatableModel;
use Rozyn\Composition\DI;

class File extends TranslatableModel {
	protected $file_type_id = 0;
	
	protected $prefix = 'mediadmyn_';
	
	protected $foreignKey = 'file_id';
	
	protected $fields = array(
		'id'			=> 'INT',
		'file_type_id'	=> 'INT',
		'name'			=> 'VARCHAR',
		'original_name' => 'VARCHAR',
		'width'			=> 'INT',
		'height'		=> 'INT',
		'size'			=> 'INT',
		'mime'			=> 'VARCHAR',
	);
	
	/**
	 * A Rozyn\Filesystem\File object that represents this File model.
	 * 
	 * @var	\Rozyn\Filesystem\File
	 */
	protected $file;
	
	/**
	 * Sets up the model.
	 */
	public function init() {
		$this->addEagerRelations(['type', 'translation']);
	}
	
	/**
	 * Execute initialization code.
	 */
	public function type() {
		return $this->belongsTo('Mediadmyn\Model\FileType');
	}
	
	/**
	 * Tries to guess the database table name for this model.
	 * 
	 * @return	string
	 */
	public function inferTable() {
		return 'file';
	}

	/**
	 * Saves the model in the database. We extend this method here so that we
	 * can forcefully set some of the attributes in this model.
	 * 
	 * @return	boolean
	 */
	public function save() {
		$this->setFileAttributes();
		
		return parent::save();
	}
	
	/**
	 * Returns the size of this image as stored in the database table. If the
	 * $format argument is set to true, an appropriate unit (bytes, kB, MB, etc.
	 * will be included).
	 * 
	 * @param	boolean	$format
	 * @return	int
	 */
	public function size($format = false) {
		$size = intval($this->get('size', 0));
		
		if ($format) {
			$units = ['bytes', 'kB', 'MB', 'GB'];

			foreach ($units as $i => $unit) {
				if ($size < pow(1024, $i + 1)) {
					return round($size / pow(1024, $i), 2) . ' ' . $unit;
				}
			}
		}
		
		return $size;
	}
	
	/**
	 * Returns the mime type of this image as stored in the database table.
	 * 
	 * @return	string
	 */
	public function mime() {
		return $this->get('mime', null);
	}
	
	/**
	 * Returns the height of this image as stored in the database table.
	 * 
	 * @return	int
	 */
	public function height() {
		return $this->get('height', null);
	}
	
	/**
	 * Returns the width of this image as stored in the database table.
	 * 
	 * @return	int
	 */
	public function width() {
		return $this->get('width', null);
	}
	
	/**
	 * Returns the Mediadmyn key that matches this file.
	 * 
	 * @return	string
	 */
	public function getMediadmynKey() {
		if ($this->has('type')) {
			return $this->get('type')->get('key', null);
		}
		
		return Mediadmyn::identify($this->get('name'));
	}
	
	/**
	 * Returns the file as an Rozyn\Filesystem\File object so that it can be 
	 * edited.
	 * 
	 * @return	\Rozyn\Filesystem\File
	 */
	public function getFile() {
		if ($this->file === null || $this->file->path() !== Mediadmyn::path($this->name)) {
			$this->file = static::newFile(Mediadmyn::path($this->name));
		}
		
		return $this->file;
	}
	
	/**
	 * Reads the file to set a couple of file attributes. This way, we don't
	 * have to recompute things like file size and mime type every single time
	 * we need them -- we can just read their values from the database.
	 * 
	 * @return	Mediadmyn\Model\File
	 */
	public function setFileAttributes() {
		if (!$this->has('file_type_id')) {
			$this->set('file_type_id', $this->getFileTypeId());
		}
		
		if (!$this->has('size')) {
			$this->set('size', $this->getFile()->size());
		}
		
		if (!$this->has('mime')) {
			$this->set('mime', $this->getFile()->mime());
		}
		
		return $this;
	}
	
	/**
	 * Returns a relative URL to the file.
	 * 
	 * @return	string
	 */
	public function asset() {
		return Mediadmyn::asset($this->name);
	}
	
	/**
	 * Returns the path to an asset that can be used as a preview for this file.
	 * 
	 * @return	string
	 */
	public function getPreview() {
		return '';
	}
	
	/**
	 * Returns a new Rozyn\Filesystem\File instance that can handle files of the
	 * desired type, which differs per subclass.
	 * 
	 * @param	string					$path
	 * @return	\Rozyn\Filesystem\File
	 */
	public static function newFile($path) {
		return DI::getInstanceOf('Rozyn\Filesystem\File', [$path]);
	}
	
	/**
	 * Returns the name of the model that holds the translations for this model.
	 * 
	 * @return	string
	 */
	public function getTranslationClass() {
		return 'Mediadmyn\Model\FileTranslation';
	}
	
	/**
	 * Returns the file type id associated with files of this type.
	 * 
	 * @return	int
	 */
	public function getFileTypeId() {
		if (isset($this->conditions['file_type_id'])) {
			return $this->conditions['file_type_id'];
		}
		
		return $this->file_type_id;
	}

	/**
	 * Converts an object of this class to one of its subclasses based on the
	 * specified Mediadmyn key.
	 * 
	 * @param	string	$key
	 * @param	boolean	$proxy
	 * @return	Mediadmyn\Model\File
	 */
	public function convertTo($key, $proxy = false) {
		$model = ($proxy) ? DI::getProxyInstanceOf(Mediadmyn::getModel($key)) :
							DI::getInstanceOf(Mediadmyn::getModel($key));
		
		$model->forceFill($this->getData());
		
		foreach ($this->getRelationData() as $name => $data) {
			$model->forceSet($name, $data);
		}
		
		return $model;
	}
	
	/**
	 * Pass along any method calls that can't be processed by this class to the
	 * Rozyn\Filesystem\File class instead.
	 * 
	 * @param	string	$name
	 * @param	array	$args
	 * @return	mixed
	 */
	public function __call($name, $args) {
		$file = $this->getFile();
		
		if (method_exists($file, $name)) {
			return call_user_func_array([$file, $name], $args);
		}
		
		return parent::__call($name, $args);
	}
	
	/**
	 * When a model of this type needs to be converted to a string, return the 
	 * path to the file that is associated with this model instead.
	 * 
	 * @return	string
	 */
	public function __toString() {
		return $this->asset();
	}
}