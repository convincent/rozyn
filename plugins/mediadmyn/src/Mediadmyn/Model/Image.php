<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Mediadmyn\Model;

use Rozyn\Filesystem\Image as ImageFile;
use Rozyn\Facade\Html;
use Rozyn\Composition\DI;
use Mediadmyn\Facade\Mediadmyn;

class Image extends File {
	protected $conditions = array(
		"file_type_id" => 1,
	);
	
	/**
	 * Saves a thumbnail of the image under the specified code (ie: file name
	 * suffix) after resizing and cropping it as to make sure that it matches
	 * the given width and height.
	 * 
	 * You can also specify a callback as the second argument in which case that
	 * callback is used to alter the image in the desired way. Make sure that 
	 * the callback accepts a single parameter, namely a Rozyn\Filesystem\Image
	 * instance, on which it can perform the desired operations.
	 * 
	 * @param	string	$code
	 * @param	int | callable		$widthOrCallback
	 * @param	int					$height
	 * @return	Mediadmyn\Model\Image
	 */
	public function saveThumbnail($code, $widthOrCallback, $height = null) {
		$file = clone $this->getFile();
		
		$callback = (is_callable($widthOrCallback)) ? 
						$widthOrCallback : 
						function(ImageFile $image) use ($widthOrCallback, $height) {
							if (is_int($height)) {
								$image->resizeAndCrop($widthOrCallback, $height);
							}

							else {
								$image->width($widthOrCallback);
							}
						};
		
		$file->apply($callback)->prefixName(suffix($code, '_'))->save();
		
		return $this;
	}
	
	/**
	 * Returns a thumbnail that was saved for this image under the specified
	 * code. If no such thumbnail was found, the original image is used instead.
	 * 
	 * @param	string	$code
	 * @return	string
	 */
	public function getThumbnail($code) {
		$name = $code . '_' . $this->name;
		
		return (file_exists(Mediadmyn::path($name))) ?
					Mediadmyn::asset($name) : 
					$this->asset();
	}

	/**
	 * Saves the model in the database. We extend this method here so that we
	 * can forcefully set some of the attributes in this model.
	 * 
	 * @return	boolean
	 */
	public function save() {
		if (!$this->exists()) {
			$this->saveThumbnail('mediadmyn_preview', function($image) {
				$image->height(48);
			});
			
			$this->saveThumbnail('mediadmyn_thumbnail', 312, 312);
		}
		
		return parent::save();
	}
	
	/**
	 * Reads the file to set a couple of file attributes. This way, we don't
	 * have to recompute things like file size and mime type every single time
	 * we need them -- we can just read their values from the database.
	 * 
	 * @return	Mediadmyn\Model\Image
	 */
	public function setFileAttributes() {
		parent::setFileAttributes();
		
		if (!$this->has('width')) {
			$this->set('width', $this->getFile()->width());
		}
		
		if (!$this->has('height')) {
			$this->set('height', $this->getFile()->height());
		}
		
		return $this;
	}
	
	/**
	 * Returns a new Image file object.
	 * 
	 * @param	string					$path
	 * @return	\Rozyn\Filesystem\Image
	 */
	public static function newFile($path) {
		return DI::getInstanceOf('Rozyn\Filesystem\Image', [$path]);
	}
	
	/**
	 * Returns the path to an asset that can be used as a preview for this file.
	 * 
	 * @return	string
	 */
	public function getPreview() {
		return Html::tag('img', ['src' => $this->getThumbnail('mediadmyn_preview'), 'alt' => '']);
	}
}