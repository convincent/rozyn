<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Mediadmyn\Model;

use Rozyn\Model\Model;

class FileType extends Model {
	protected $prefix = 'mediadmyn_';
	
	protected $soft = false;
	
	protected $fields = array(
		'id'	=> 'INT',
		'key'	=> 'image',
	);
	
	public function files() {
		return $this->hasMany('Mediadmyn\Model\File', ['localKey' => 'file_id', 'foreignKey' => 'file_type_id']);
	}
}