<?php $this->style('files/overview.css'); ?>
<?php $this->script('files/overview.js'); ?>

<h2><?php echo t('mediadmyn.files'); ?></h2>

<p><?php echo t('mediadmyn.files_index_text'); ?></p>

<?php echo $this->requestView('/plugins/rozadmyn/views/overview', array(
			'collection'	=> $models,
			'headers'		=> [t('mediadmyn.file_name'), t('mediadmyn.preview'), t('mediadmyn.resolution')],
			'fields'		=> ['original_name', 'preview', 'resolution'],
			'batchActions'	=> ['delete' => t('delete')],
			'actions'		=> ['edit' => t('edit'), 'delete' => t('delete')],
			'route'			=> 'mediadmyn.default',
			'sort'			=> $sort,
			'search'		=> $search,
			'dir'			=> $dir,
			'page'			=> $page,
			'paginator'		=> $paginator,
			'nosort'		=> ['resolution'],
		));