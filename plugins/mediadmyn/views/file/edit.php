<h2><?php echo t('mediadmyn.edit_file'); ?></h2>

<?php $this->style('files/edit.css'); ?>

<form method="post" id="mediadmyn-edit-file-form">

	<?php use Rozyn\Facade\Flash; ?>
	<?php echo Flash::success('success'); ?>

	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dolor ligula, suscipit eget dapibus nec, semper eu nisi. Sed ut lorem et ligula dictum dictum id nec eros. In porta ac nisi quis tristique. Praesent mollis pulvinar mi, pretium dignissim tellus dignissim ac.</p>

	<?php echo $this->Form->block('text', 'name', t('mediadmyn.file_name'), $file->original_name, ['disabled']); ?>
	<?php echo $this->Form->block('text', 'language', t('rozadmyn.language'), language($language), ['disabled', 'class' => ['flag', $language]]); ?>
	<?php echo $this->Form->block('text', 'title', t('mediadmyn.file_title'), $file->title); ?>
	<?php echo $this->Form->block('textarea', 'description', t('mediadmyn.file_description'), $file->description); ?>

	<input type="submit" value="<?php echo t('submit'); ?>" />
</form>

<div id="mediadmyn-file-info-wrapper">
	<?php echo $this->requestView('/plugins/mediadmyn/views/file/details', ['file' => $file]); ?>
</div>