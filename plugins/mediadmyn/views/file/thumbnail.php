<div class="mediadmyn-file-thumbnail file" data-file-id="<?php echo $file->id(); ?>" data-id="<?php echo $file->id(); ?>">
	<?php echo $this->Html->tag('div', '', ['class' => 'preview', 'css' => ['backgroundImage' => 'url(' . passet('img/file@2x.png', 'mediadmyn') . ')']]); ?>
	<div class="info"><?php echo $file->original_name; ?></div>
	
	<div class="details">
		<?php echo $this->requestView('/plugins/mediadmyn/views/file/details', ['file' => $file]); ?>
	</div>
</div>
