<?php $this->script('files/add.js'); ?>
<?php $this->style('files/add.css', 'files/overview.css'); ?>

<?php use Mediadmyn\Model\File; ?>

<form id="mediadmyn-add-files-form" method="post" enctype="multipart/form-data">
	<div class="form-header">
		<h2><?php echo t('mediadmyn.add_files'); ?></h2>
		
		<p><?php echo t('mediadmyn.files_add_text'); ?></p>
	</div>

	<div class="form-block">
		<label for="files"><?php echo t('mediadmyn.files'); ?></label>
		<?php echo $this->Form->file('files', ['multiple', 'data' => ['label' => t('browse')]]); ?>
	</div>

	<input type="submit" value="<?php echo t('submit'); ?>" />
</form>

<?php echo $this->requestView('/plugins/rozadmyn/views/overview', array(
			'collection'	=> File::collection(),
			'headers'		=> [t('mediadmyn.file_name'), t('mediadmyn.preview')],
			'batchActions'	=> ['delete' => t('delete')],
			'params'		=> ['model' => null],
			'route'			=> 'mediadmyn.default',
		));