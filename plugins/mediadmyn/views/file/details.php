<table>
	<tr>
		<td><?php echo t('mediadmyn.file_name'); ?>:</td>
		<td class="file-name"><?php echo $file->original_name; ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.file_url'); ?>:</td>
		<td class="file-url"><?php echo asset(UPLOADS_DIR . '/' . $file->name); ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.file_size'); ?>:</td>
		<td class="file-size"><?php echo $file->size(true); ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.file_type'); ?>:</td>
		<td class="file-mime"><?php echo $file->mime(); ?></td>
	</tr>
</table>