<?php

use Mediadmyn\Model\File;

$this->style('browse.css');

$classes = ['cf'];
if (isset($multiple) && $multiple === true) {
	$classes[] = 'multiple';
}

if (!isset($files)) {
	$files = File::collection();
}

$initial = $files->items();

$files->add($paginator->fetch($page));

foreach ($files as $id => $model) {
	$files->set($id, $model->convertTo($model->getMediadmynKey()));

	unset($model);
}

?>

<div id="mediadmyn-browser" class="<?php echo implode(' ', $classes); ?>">
	<a id="mediadmyn-browser-close" href="#"></a>
	
	<form id="mediadmyn-browser-form">
		<h2><?php echo t('mediadmyn.browser_header'); ?></h2>
		
		<div id="mediadmyn-browser-files">
			<div id="mediadmyn-browser-top-bar" class="cf">
				<ul id="mediadmyn-browser-actions" class="actions">
					<li><a href="#" class="button confirm-selection"><?php echo t('mediadmyn.confirm_selection'); ?></a></li>
				</ul>
				
				<div id="mediadmyn-browser-search">
					<input class="search" type="text" placeholder="<?php echo t('rozadmyn.search_placeholder'); ?>" name="search" id="search" autocomplete="off" />
				</div>
			</div>
			
			<?php echo $paginator->getNav($page, 7); ?>
			
			<?php echo $this->requestView('/plugins/mediadmyn/views/file/thumbnails', ['files' => $files]); ?>
			
			<?php if ($files->isEmpty()) : ?>
				<p><?php echo t('mediadmyn.browser_no_files'); ?></p>
			<?php endif; ?>
		</div>

		<div id="mediadmyn-browser-details">
			<p id="mediadmyn-browser-details-empty"><?php echo t('mediadmyn.browser_details_empty'); ?></p>

			<div id="mediadmyn-browser-details-filled">
				<div class="preview"></div>
				<div class="details"></div>
			</div>
		</div>
		
		<div id="mediadmyn-browser-selected">
			<?php echo $this->requestView('/plugins/mediadmyn/views/file/thumbnails', ['files' => array_intersect_key($files->items(), $initial)]); ?>
		</div>
	</form>
</div>