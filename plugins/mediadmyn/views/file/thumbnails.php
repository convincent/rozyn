<?php

$baseView = '/plugins/mediadmyn/views/{key}/thumbnail';

foreach ($files as $file) {
	if (!view_exists($view = str_replace('{key}', $file->getMediadmynKey(), $baseView))) {
		$view = str_replace('{key}', 'file', $baseView);
	}

	echo $this->requestView($view, ['file' => $file]);
}