<?php use Rozyn\Facade\Url; ?>

<table>
	<tr>
		<td><?php echo t('mediadmyn.file_name'); ?>:</td>
		<td class="file-name"><?php echo $file->original_name; ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.file_url'); ?>:</td>
		<td class="file-url"><?php echo Url::format(asset(UPLOADS_DIR . '/' . $file->name)); ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.image_width'); ?>:</td>
		<td><?php echo $file->width(); ?> x <?php echo $file->height(); ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.file_size'); ?>:</td>
		<td class="file-size"><?php echo $file->size(true); ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.file_type'); ?>:</td>
		<td class="file-mime"><?php echo $file->mime(); ?></td>
	</tr>
	
	<?php if ($file->translation): ?>
	<tr>
		<td><?php echo t('mediadmyn.title'); ?>:</td>
		<td class="file-title"><?php echo $file->translation->title; ?></td>
	</tr>
	<tr>
		<td><?php echo t('mediadmyn.description'); ?>:</td>
		<td class="file-description"><?php echo $file->translation->description; ?></td>
	</tr>
	<?php endif; ?>
</table>