<?php
	$maxWidth	= 240;
	$maxHeight	= 204;
	
	$css = ['backgroundImage' => 'url(' . $file->getThumbnail('mediadmyn_thumbnail') . ')'];

	if ($file->width() <= $maxWidth && $file->height() <= $maxHeight) {
		$css['backgroundSize'] = "{$file->width()}px {$file->height()}px";
	}

?>

<div class="mediadmyn-file-thumbnail image" data-file-id="<?php echo $file->file_id; ?>" data-id="<?php echo $file->id(); ?>">
	<?php echo $this->Html->tag('div', '', ['class' => 'preview', 'css' => $css]); ?>
	
	<div class="details">
		<?php echo $this->requestView('/plugins/mediadmyn/views/image/details', ['file' => $file]); ?>
	</div>
</div>