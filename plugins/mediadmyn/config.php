<?php

use Rozyn\Facade\Config;


Config::write('mediadmyn.dir',			'uploads');
Config::write('mediadmyn.path',			__PUBLIC__ . DS . Config::read('mediadmyn.dir'));
Config::write('mediadmyn.default_key',	'file');


// Make sure that the Mediadmyn directory exists
if (!is_dir(Config::read('mediadmyn.path'))) {
	mkdir(Config::read('mediadmyn.path'));
}