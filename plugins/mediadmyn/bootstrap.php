<?php

/**
 * The Mediadmyn plugin consists of a generic media browser that administrators
 * can also use to upload their own media. Via this browser, administrators can
 * specify all sorts of SEO-related matters regarding their media to make sure
 * they are indexed optimally. It has been written specifically for the Rozadmyn
 * plugin.
 */

use Rozyn\Facade\App;
use Rozyn\Facade\Config;
use Rozadmyn\Facade\Rozadmyn;
use Mediadmyn\Facade\Mediadmyn;

// Load our config file first.
require __DIR__ . DS . 'config.php';

// Add a default file group to Mediadmyn so that every file has something to 
// fall back on.
Mediadmyn::registerFileGroup(Config::read('mediadmyn.default_key'),	
							 null, 
							 'Mediadmyn\Model\File');

Mediadmyn::registerFileGroup('image', 
							 '/^image\/(jpg|jpeg|png|gif)$/', 
							 'Mediadmyn\Model\Image');

// Plug in Rozadmyn, since we depend on it.
App::plug('Rozadmyn');

// Add our FileController to Rozadmyn.
Rozadmyn::registerController('\Mediadmyn\Controller\Admin\FileController', 'mediadmyn');

// Register our admin routes through Rozadmyn.
Rozadmyn::registerRoutes(__DIR__ . DS . 'routes.php');