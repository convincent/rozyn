<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1520002200_12a6f86ba08b0f327e6aec1fbf08fcc6 extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		Table::instance('mediadmyn_file')->string('hash')->nullable('hash');
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {
		Table::instance('mediadmyn_file')->dropColumn('hash');
	}
}