<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1459970060_b4a822e14d0481f16c6d86dbc640ed98 extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		(new Table('mediadmyn_file'))->nullable(['size', 'width', 'height', 'mime']);
		(new Table('mediadmyn_file_translation'))->nullable(['alt', 'title', 'description']);
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {

	}
}