<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1459445417_2518659e5241b61f67c2097174c5b237 extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		// Create our mediadmyn_file table.
		$table = new Table('mediadmyn_file');
		if (!$table->exists()) {
			$table->primary('id');
			$table->integer('file_type_id');
			$table->string('name');
			$table->string('original_name');
			$table->integer('width');
			$table->integer('height');
			$table->integer('size');
			$table->string('mime');
			$table->soft();
			$table->timestamps();

			$table->index('file_type_id');
			$table->index('name');
			$table->index('original_name');
			$table->index('width');
			$table->index('height');
			$table->index('size');
			$table->index('mime');

			$table->create();
		}
		
		// Create our translation table.
		$table = new Table('mediadmyn_file_translation');
		if (!$table->exists()) {
			$table->primary('id');
			$table->string('language', 10);
			$table->integer('file_id');
			$table->string('alt');
			$table->string('title');
			$table->text('description');
			
			$table->index('language');
			$table->index('file_id');
			$table->index('title');
			
			$table->create();
		}
		
		// Create our file type table.
		$table = new Table('mediadmyn_file_type');
		if (!$table->exists()) {
			$table->primary('id');
			$table->string('key', 255);
			
			$table->index('key');
			
			$table->create();
		}
		$table->truncate();
		
		// Populate our file type table.
		$query = new Query();
		$query->insert()->into('mediadmyn_file_type')->fields('key')->values(array('image'))->values(array('pdf'))->execute();
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {
		
	}
}