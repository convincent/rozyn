<?php

namespace Test;

use Rozyn\Request\Request;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
abstract class HttpSuccessCodeTestCase extends IntegrationTestCase {
	/**
	 * Tests whether a certain url returns a valid response.
	 * 
	 * @param	string	$url
	 * @param	string	$method
	 * @param	array	$data
	 * 
	 * @dataProvider urlProvider
	 */
	final public function testUrlReturnsValidResponse($url, $method = null, $data = array()) {
		if ($method === null) {
			$method = Request::HTTP_GET;
		}
		
		// Make sure the URL that corresponds to the Route returns a valid
		// response.
		$this->assertUrlReturnsValidResponse($url, $method, $data);
	}
	
	/**
	 * Provides all the necessary data to test the specified URLs.
	 * 
	 * @return array
	 */
	abstract public function urlProvider();
}