<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Test;

abstract class DatabaseTestCase extends \PHPUnit_Extensions_Database_TestCase {
    // only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
    private $conn = null;

    final public function getConnection() {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS, array(
										\PDO::ATTR_PERSISTENT	=> true,
										\PDO::ATTR_ERRMODE		=> \PDO::ERRMODE_EXCEPTION,
									));
            }
			
            $this->conn = $this->createDefaultDBConnection(self::$pdo);
        }
		
        return $this->conn;
    }
	
	public function getDataSet() {
        return $this->createMySQLXMLDataSet(_DB_DUMP_XML_);
    }
}