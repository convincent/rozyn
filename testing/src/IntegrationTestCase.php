<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Test;

use Rozyn\Composition\DI;

abstract class IntegrationTestCase extends TestCase {
	/**
	 * A Transporter object that can send URL requests for us and inspect the
	 * response.
	 * 
	 * @var \Rozyn\Request\Transporter
	 */
	protected $transporter;
	
	/**
	 * 
	 * @param	string	$name
	 * @param	array	$data
	 * @param	string	$dataName
	 */
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		
		$this->transporter = DI::getInstanceOf('Rozyn\Request\CurlTransporter');
	}
	
	/**
	 * Assert that the given Url returns a valid response.
	 * 
	 * @param	string	$url
	 * @param	string	$method
	 * @param	array	$data
	 */
	public function assertUrlReturnsValidResponse($url, $method, array $data = []) {
		$this->transporter->setRequestMethod($method);
		$this->transporter->setUrl($url);
		
		$res = $this->transporter->isSuccess();
		
		$this->assertTrue($res);
		
		return $res;
	}
}