<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Test\Model;

use Rozyn\Model\Model;
use Test\Model\PostsTagsJoinModel;

class Tag extends Model {
	protected $soft = false;
	
	protected $fields = ['id', 'label'];
	
	public function posts() {
		return $this->hasAndBelongsToMany('Test\Model\Post', ['joinTable' => 'posts_tags', 'joinModel' => new PostsTagsJoinModel()]);
	}
}