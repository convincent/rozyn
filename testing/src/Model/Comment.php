<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Test\Model;

use Rozyn\Model\Model;

class Comment extends Model {
	protected $soft = false;
	
	protected $fields = ['id', 'author_id', 'post_id', 'content'];
	
	public function post() {
		return $this->belongsTo('Test\Model\Post');
	}
	
	public function author() {
		return $this->belongsTo('Test\Model\Author');
	}
}