<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Test\Model;

use Rozyn\Model\JoinModel;

class PostsTagsJoinModel extends JoinModel {
	protected $table = 'posts_tags';
	protected $fields = ['post_id', 'tag_id', 'created_at'];
}