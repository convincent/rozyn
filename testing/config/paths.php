<?php

// Special directories used for testing purposes.
define('PROVIDER_DIR', 'providers');

// File paths.
define('_AUTOMATED_ROUTER_TEST_DATA_PROVIDER_', __TESTING__ . DS . PROVIDER_DIR . DS . INTEGRATION_DIR . DS . 'automated_router_test.php');