<?php

use Rozyn\Facade\Router;

// Define a constant so that the regular bootstrap.php file knows we're running
// a PHPUnit test suite.
define('PHPUNIT_TEST_SUITE', true);

require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'rozyn' . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Define a number of directories that need to be searched when we're dealing
// with test classes.
$test_autoload_dirs = [TESTS_DIR, SRC_DIR, INTEGRATION_DIR];

// Set up autoloading for our helper classes.
spl_autoload_register(function($className) use ($test_autoload_dirs) {
	if (substr($className, 0, 4) === 'Test') {
		foreach ($test_autoload_dirs as $dir) {
			// Fall back on our default class loader.
			if (false !== ($path = get_class_path(preg_replace('/^Test/', $dir, $className)))) {
				require $path;
				break;
			}
		}
	}
});

// Make sure we are indeed in a TESTING environment.
if (ENV !== ENV_TESTING) {
	die('Trying to run tests from a non-testing environment (' . ENV . ')');
}

// Include test-specific config files.
$configDirs = [__TESTING__ . DS . CONFIG_DIR, __TESTING__ . DS . CONFIG_DIR . DS . CUSTOM_DIR];

foreach ($configDirs as $dir) {
	if (file_exists($dir) && is_dir($dir)) {
		dir_map($dir, function($file, $dir, $path) {
			if (ends_with($path, '.php')) {
				include($path);
			}
		}, false);
	}
}

// Import our application's routes.
Router::import(_ROUTES_);

// Define the location of the test database dump.
define('_DB_DUMP_SQL_', __TESTING__ . DS . 'testdb.sql');
define('_DB_DUMP_XML_', __TESTING__ . DS . 'testdb.xml');

// Define a function that resets the entire database.
function init_test_db() {
	shell_exec(sprintf('%s -u %s --password=%s -e "DROP DATABASE %s"', MYSQL_BIN, DB_USER, DB_PASS, DB_NAME));
	shell_exec(sprintf('%s -u %s --password=%s -e "CREATE DATABASE %s"', MYSQL_BIN, DB_USER, DB_PASS, DB_NAME));
	shell_exec(sprintf('%s -u %s --password=%s %s < "%s"', MYSQL_BIN, DB_USER, DB_PASS, DB_NAME, _DB_DUMP_SQL_));
}

init_test_db();

// register_shutdown_function('init_test_db');