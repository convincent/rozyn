<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

namespace Test;

class HttpSuccessCodeTest extends HttpSuccessCodeTestCase {
	public function urlProvider() {
		return array(
			['http://localhost/rozyn'],
		);
	}
}