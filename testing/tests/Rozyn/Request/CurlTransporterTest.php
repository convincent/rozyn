<?php

namespace Test\Rozyn\Request;

use Test\TestCase;
use Rozyn\Request\CurlTransporter;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class CurlTransporterTest extends TestCase {
	const DEFAULT_URL		= 'http://localhost/rozyn/';
	const DEFAULT_500_URL	= 'http://localhost/rozyn/bad/';
	const DEFAULT_404_URL	= 'http://localhost/rozyn/not-found/';
	const DEFAULT_POST_URL	= 'http://localhost/rozyn/post/';
	const DEFAULT_HOST		= 'localhost';
	const DEFAULT_PORT		= 80;
	
	/**
	 * The socket transporter instance.
	 * 
	 * @var \Rozyn\Request\CurlTransporter
	 */
	protected $transporter;
	
	/**
	 * Sets things up for each test.
	 */
	public function setUp() {
		$this->transporter = new CurlTransporter();
	}
	
	/**
	 * Clean up after ourselves.
	 */
	public function tearDown() {
		$this->transporter->close();
	}
	
	public function testCanInit() {
		$this->transporter->init();
	}
	
	public function testCanInitWithUrl() {
		$this->transporter->init(static::DEFAULT_URL);
		
		$this->assertEquals($this->transporter->getUrl(), static::DEFAULT_URL);
	}
	
	public function testReceivesCorrectResponse() {
		$this->transporter->init(static::DEFAULT_URL);
		
		$this->assertContains('Hello world!', $this->transporter->send());
	}
	
	public function testCanRetrieveHeaders() {
		$this->transporter->init(static::DEFAULT_URL);
		
		$this->transporter->setOption(CURLOPT_HEADER, true);
		$this->transporter->setOption(CURLOPT_NOBODY, true);
		
		$this->assertContains('HTTP/1.1 200 OK', $this->transporter->send());
	}
	
	public function testCanMakeHttpGetRequestWithParams() {
		$this->transporter->init(static::DEFAULT_URL);
		
		$this->transporter->setData(['test' => 'test']);
		
		$this->assertContains('Hello world!', $this->transporter->get());
		
		$this->assertStringEndsWith('?test=test', $this->transporter->getUrl());
	}
	
	public function testCanMakePostRequest() {
		$this->transporter->init(static::DEFAULT_POST_URL);
		$this->transporter->setOption(CURLOPT_HEADER, true);
		
		$this->assertContains('HTTP/1.1 200 OK', $this->transporter->post());
	}
	
	public function testCanMakePostRequestWithParams() {
		$this->transporter->init(static::DEFAULT_POST_URL);
		$this->transporter->setOption(CURLOPT_HEADER, true);
		
		$this->transporter->setData(['test' => 'test']);
		
		$this->assertContains('HTTP/1.1 200 OK', $this->transporter->post());
	}
	
	public function testCanGetValidHttpCode() {
		$this->transporter->init(static::DEFAULT_URL);
		
		$this->assertEquals(200, $this->transporter->getCode());
	}
	
	public function testCanGetNotFoundHttpCode() {
		$this->transporter->init(static::DEFAULT_404_URL);
		
		$this->assertEquals(404, $this->transporter->getCode());
	}
}

