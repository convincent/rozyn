<?php

namespace Test\Rozyn\Request;

use Test\TestCase;
use Rozyn\Request\SocketTransporter;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class SocketTransporterTest extends TestCase {
	const DEFAULT_HOST	= 'localhost';
	const DEFAULT_PORT	= 80;
	
	/**
	 * The socket transporter instance.
	 * 
	 * @var \Rozyn\Request\SocketTransporter
	 */
	protected $transporter;
	
	/**
	 * Sets things up for each test.
	 */
	public function setUp() {
		$this->transporter = new SocketTransporter();
	}
	
	/**
	 * Clean up after ourselves.
	 */
	public function tearDown() {
		$this->transporter->close();
	}
	
	public function testCanOpenConnection() {
		$fp = $this->transporter->open(static::DEFAULT_HOST, static::DEFAULT_PORT);
		
		$this->assertNotEquals($fp, false);
	}
	
	public function testCanCloseConnection() {
		$fp = $this->transporter->open(static::DEFAULT_HOST, static::DEFAULT_PORT);
		
		$this->assertTrue($this->transporter->close());
	}
	
	public function testCanReadLineFromSocketConnection() {
		$this->transporter->open(static::DEFAULT_HOST, static::DEFAULT_PORT);
		
		$out  = "GET /rozyn/ HTTP/1.1\r\n";
		$out .= "Host: localhost\r\n";
		$out .= "Connection: Close\r\n\r\n";
		
		$this->transporter->write($out);
		
		$line = $this->transporter->line();
		
		$this->transporter->close();
		
		$this->assertEquals($line, "HTTP/1.1 200 OK\r\n");
	}
	
	public function testCanSendRequestObject() {
		// Create a stub for the SomeClass class.
        $stub = $this->getMockBuilder('Rozyn\Request\Request')
					 ->setMethods(array(
						 'getRequestMethod',
						 'getUrl',
						 'getHost',
						 'getPort',
						 'getScheme'))
                     ->getMock();
		
		$stub->expects($this->any())
			 ->method('getRequestMethod')
			 ->will($this->returnValue('GET'));
		
		$stub->expects($this->any())
			 ->method('getUrl')
			 ->will($this->returnValue('http://localhost/rozyn/'));
		
		$stub->expects($this->any())
			 ->method('getHost')
			 ->will($this->returnValue('localhost'));
		
		$stub->expects($this->any())
			 ->method('getScheme')
			 ->will($this->returnValue('http://'));
		
		$stub->expects($this->any())
			 ->method('getPort')
			 ->will($this->returnValue(80));

		$st = new SocketTransporter();
		$st->request($stub);
		
		// $this->assertEquals("HTTP/1.1 200 OK\r\n", $st->line());
	}
}

