<?php

namespace Test\Rozyn\Request;

use Test\TestCase;
use Rozyn\Request\Request;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class RequestTest extends TestCase {
	const DEFAULT_URL = 'http://www.example.com';
	const DEFAULT_SECURE_URL = 'https://www.example.com';
	/**
	 * The Request instance.
	 * 
	 * @var \Rozyn\Request\Request
	 */
	protected $request;
	
	/**
	 * Sets things up for each test.
	 */
	public function setUp() {
		$this->request = new Request();
		
		$this->request->setUrl(static::DEFAULT_URL);
	}
	
	public function testCanSetUrlThroughConstructor() {
		$request = new Request(static::DEFAULT_URL);
		
		$this->assertEquals($request->getUrl(), static::DEFAULT_URL);
	}
	
	public function testCanSetCustomUrl() {
		$this->assertEquals($this->request->getUrl(), static::DEFAULT_URL);
	}
	
	public function testCanComputeSchemeFromUrl() {
		$this->assertEquals($this->request->getScheme(), 'http://');
	}
	
	public function testCanComputeHostFromUrl() {
		$this->assertEquals($this->request->getHost(), 'www.example.com');
	}
	
	public function testCanComputeUriFromUrl() {
		$this->request->setUrl(static::DEFAULT_URL . '/test');
		
		$this->assertEquals($this->request->getUri(), '/test');
	}
	
	public function testProperlyDetectsNonSecureUrl() {
		$this->assertFalse($this->request->isSecure());
	}
	
	public function testProperlyDetectsSecureUrl() {
		$this->request->setUrl(static::DEFAULT_SECURE_URL);
		
		$this->assertTrue($this->request->isSecure());
	}
}

