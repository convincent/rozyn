<?php

namespace Test\Rozyn\Pagination;

use Test\TestCase;
use Rozyn\Pagination\QueryPaginator;
use Rozyn\Database\Query;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class QueryPaginatorTest extends TestCase {
	public function setUp() {
		$query = new Query();
		
		$query->select(['id', 'label'])->from('tag');
		
		$this->paginator = new QueryPaginator($query);
	}
	
	public function testGetTotalReturnsCorrectNumber() {
		$this->assertEquals(4, $this->paginator->getTotal());
	}
}