<?php

namespace Test\Rozyn\Pagination;

use Test\TestCase;
use Rozyn\Pagination\ModelPaginator;
use Test\Model\Page;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class ModelPaginatorTest extends TestCase {

	public function setUp() {
		$page = new Page();

		$this->paginator = new ModelPaginator($page->newQuery());
	}

	public function testGetTotalReturnsCorrectNumber() {
		$this->assertEquals($this->paginator->getTotal(), 3);
	}
}