<?php

namespace Test\Rozyn\Relation;

use Test\TestCase;
use Test\Model\Post;
use Test\Model\Author;
use Rozyn\Relation\BelongsTo;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class BelongsToTest extends TestCase {
	protected $relation;
	
	public function setUp() {
		$this->post		= Post::read(1);
		$this->author	= Author::read(1);
		
		$this->relation = new BelongsTo('author', $this->post, $this->author);
	}
	
	public function testLoad() {
		$expected	= 1;
		$result		= $this->relation->load();
		
		$this->assertEquals($expected, $result->id());
	}
	
	public function testInferForeignKey() {
		$expected	= 'author_id';
		$result		= $this->relation->inferForeignKey();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testInferLocalKey() {
		$expected	= 'id';
		$result		= $this->relation->inferLocalKey();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testClear() {
		$previous = 1;
		$this->assertEquals($previous, $this->post->get('author_id'));
		
		$this->relation->clear();
		
		$expected	= 0;
		$result		= $this->post->get('author_id');
		
		$this->assertEquals($expected, $result);
		
		// Reset
		$this->post->set('author_id', $previous)->save();
	}
	
	public function testLoadBatchArray_1() {
		$expected	= 2;
		$result		= $this->relation->loadBatchArray([1,2]);
		
		$this->assertEquals($expected, count($result));
	}
	
	public function testLoadBatchArray_2() {
		$posts = $this->post->newQuery()->select()->fetchArrays();
		
		$this->relation->loadBatchArray([1,2], [], $posts);
		
		$this->assertArrayHasKey('author', $posts[1]);
		$this->assertArrayHasKey('id', $posts[1]['author']);
		$this->assertEquals(1, $posts[1]['author']['id']);
		$this->assertEquals(2, $posts[2]['author']['id']);
		$this->assertEquals(1, $posts[3]['author']['id']);
	}
}

