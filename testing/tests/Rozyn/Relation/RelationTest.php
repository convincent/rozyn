<?php

namespace Test\Rozyn\Relation;

use Test\TestCase;
use Rozyn\Relation\HasAndBelongsToMany;
use Test\Model\Project;
use Test\Model\Tag;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class RelationTest extends TestCase {
	protected $relation;
	
	public function setUp() {
	}
	
	public function tearDown() {
	}
	
	public function testEagerIsSetCorrectly() {
	}
	
	public function testLimitIsSetCorrectly() {
	}
	
	public function testConditionsAreSetCorrectly() {
	}
	
	public function testParamsAreSetCorrectly() {
	}
	
	public function testOrderByIsSetCorrectly() {
		
	}
}

