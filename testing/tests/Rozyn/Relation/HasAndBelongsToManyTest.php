<?php

namespace Test\Rozyn\Relation;

use Test\Model\Post;
use Test\Model\Tag;
use Test\Model\PostsTagsJoinModel;
use Test\TestCase;
use Rozyn\Model\Collection;
use Rozyn\Relation\HasAndBelongsToMany;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class HasAndBelongsToManyTest extends TestCase {
	protected $relation;
	
	public function setUp() {
		$this->post	= Post::read(1);
		$this->tag	= Tag::read(1);
		
		$this->relation = new HasAndBelongsToMany('tags', $this->post, $this->tag, ['joinTable' => 'posts_tags', 'joinModel' => new PostsTagsJoinModel()]);
	}
	
	public function testGetJoinTable_1() {
		$expected	= 'posts_tags';
		$result		= $this->relation->getJoinTable();
		
		$this->assertEquals($expected, $result);
	}
	
	/**
	 * If no join table is defined, throw an Exception
	 */
	public function testInferJoinTable() {
		$this->relation->setJoinTable(null);
		
		$expected = 'post_tag';
		$result = $this->relation->inferJoinTable();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testInferForeignKey() {
		$expected	= 'tag_id';
		$result		= $this->relation->inferForeignKey();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testInferLocalKey() {
		$expected	= 'post_id';
		$result		= $this->relation->inferLocalKey();
		
		$this->assertEquals($expected, $result);
	}
	
	/**
	 * Try loading all the Comments for an Author who has 2.
	 */
	public function testLoad_1() {
		$expected = 3;
		$result = $this->relation->load();
		
		$this->assertEquals($expected, $result->size());
	}
	
	public function testSave_1() {
		$tag = new Tag(['label' => 'testSave_1']);
		
		$this->relation->save($tag);
		
		$this->assertEquals('testSave_1', $this->post->getRelationData('tags')->pop()->get('label'));
	}
	
	/**
	 * Try creating a new Author model and immediately save a related Comment 
	 * for it, without first storing the Author model in the database. This
	 * should throw an Exception.
	 */
	public function testSave_2() {
		$this->setExpectedException('Rozyn\Relation\ParentDoesNotExistException');
		
		$post = new Post(['title' => 'testSave_2', 'content' => 'testSave_2']);
		$relation = new HasAndBelongsToMany('tags', $post, $this->tag, ['joinTable' => 'posts_tags']);
		
		$tag = new Tag(['label' => 'testSave_1']);
		$relation->save($tag);
	}
	
	/**
	 * Try editing and saving multiple Comments of an Author simultaneously.
	 */
	public function testSaveAll_1() {
		$initial = $this->relation->load()->size();
		
		$tag1 = new Tag(['label' => 'testSaveAll_1_1']);
		$tag2 = new Tag(['label' => 'testSaveAll_1_2']);
		$tag3 = new Tag(['label' => 'testSaveAll_1_3']);
		
		$this->relation->saveAll(new Collection([$tag1, $tag2, $tag3], $this->tag));
		
		$after = $this->relation->load()->size();
		
		$this->assertEquals($initial + 3, $after);
	}
	
	/**
	 * Check if clear updates everything correctly locally, ie: without checking
	 * the database.
	 */
	public function testClear_1() {
		$initial = $this->relation->load();
		$this->relation->clear();
		$after = $this->post->getRelationData('tags');
		
		$this->assertGreaterThan(0, $initial->size());
		$this->assertEquals(0, $after->size());
		
		// Reset
		$this->relation->saveAll($initial);
	}
	
	/**
	 * Check if clear updates everything correctly in the database.
	 */
	public function testClear_2() {
		$initial = $this->relation->load();
		
		$this->relation->clear();
		$after = $this->relation->load();
		
		$this->assertGreaterThan(0, $initial->size());
		$this->assertEquals(0, $after->size());
		
		// Reset
		$this->relation->saveAll($initial);
	}
	
	/**
	 * Test if sync works as intended -- clearing the relation before saving the
	 * specified collection.
	 */
	public function testSync() {
		$mock = $this->getMockBuilder('Rozyn\Relation\HasAndBelongsToMany')
					 ->setMethods(['clear', 'saveAll'])
					 ->disableOriginalConstructor()
					 ->getMock();
		
		$mock->expects($this->once())->method('clear');
		$mock->expects($this->once())->method('saveAll');
		
		$mock->sync(new Collection);
	}
	
	public function testLoadBatchArray_1() {
		$expected	= 7;
		$result		= $this->relation->loadBatchArray([1,2,3]);
		
		$this->assertEquals($expected, count($result));
	}
	
	public function testLoadBatchArray_2() {
		$posts = $this->post->newQuery()->select()->fetchArrays();
		
		$this->relation->loadBatchArray([1,2,3], [], $posts);
		
		$this->assertArrayHasKey('tags', $posts[1]);
		$this->assertEquals(7, count($posts[1]['tags']));
		$this->assertEquals(1, count($posts[2]['tags']));
		$this->assertEquals(2, count($posts[3]['tags']));
	}
	
	/**
	 * Test loadBatchArray with a limit on the relation.
	 */
	public function testLoadBatchArray_3() {
		$this->relation->setLimit(3);
		
		$posts = $this->post->newQuery()->select()->fetchArrays();
		
		$this->relation->loadBatchArray([1,2,3], [], $posts);
		
		$this->assertArrayHasKey('tags', $posts[1]);
		$this->assertEquals(3, count($posts[1]['tags']));
	}
	
	/**
	 * Test loadBatchArray with a limit on the relation.
	 */
	public function testLoadBatchArray_4() {
		$tags = $this->relation->loadBatchArray([1,2,3], []);
		
		$this->assertArrayHasKey(\Rozyn\Model\JoinModel::JOIN_MODEL_KEY, $tags[1]);
	}
}