<?php

namespace Test\Rozyn\Relation;

use Test\TestCase;
use Test\Model\Page;
use Test\Model\PageTranslation;
use Rozyn\Relation\HasOne;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class HasOneTest extends TestCase {
	protected $relation;
	
	public function setUp() {
		$this->page			= Page::read(1);
		$this->translation	= PageTranslation::read(1);
		
		$this->relation = new HasOne('translation', $this->page, $this->translation, ['conditions' => 'PageTranslation.language = "nl"']);
	}
	
	public function testLoad() {
		$expected	= 1;
		$result		= $this->relation->load();
		
		$this->assertEquals($expected, $result->id());
	}
	
	public function testInferForeignKey() {
		$expected	= 'page_id';
		$result		= $this->relation->inferForeignKey();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testInferLocalKey() {
		$expected	= 'id';
		$result		= $this->relation->inferLocalKey();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testClear() {
		$previous = 1;
		$this->assertEquals($previous, $this->translation->get('page_id'));
		
		$this->relation->clear();
		
		$expected	= 0;
		$result		= $this->page->translation()->load();
		
		$this->assertEquals($expected, count($result));
		
		// Reset
		$this->translation->set('page_id', $previous)->save();
	}
	
	public function testLoadBatchArray_1() {
		$expected	= 2;
		$result		= $this->relation->loadBatchArray([2,3]);
		
		$this->assertEquals($expected, count($result));
	}
	
	public function testLoadBatchArray_2() {
		$pages = $this->page->newQuery()->select()->fetchArrays();
		
		$this->relation->loadBatchArray([2,3,4], [], $pages);
		
		$this->assertArrayHasKey('translation', $pages[2]);
		$this->assertArrayHasKey('id', $pages[2]['translation']);
		$this->assertEquals(3, $pages[2]['translation']['id']);
		$this->assertEquals(5, $pages[3]['translation']['id']);
		$this->assertEquals(7, $pages[4]['translation']['id']);
	}
}

