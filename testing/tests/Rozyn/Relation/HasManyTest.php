<?php

namespace Test\Rozyn\Relation;

use Rozyn\Model\Collection;
use Test\TestCase;
use Test\Model\Comment;
use Test\Model\Author;
use Rozyn\Relation\HasMany;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class HasManyTest extends TestCase {
	protected $relation;
	
	public function setUp() {
		$this->author	= Author::read(3);
		$this->comment	= Comment::read(2);
		
		$this->relation = new HasMany('comments', $this->author, $this->comment);
	}
	
	/**
	 * Try loading all the Comments for an Author who has 2.
	 */
	public function testLoad_1() {
		$expected = 2;
		$result = $this->relation->load();
		
		$this->assertEquals($expected, $result->size());
	}
	
	/**
	 * Try loading all the Comments for an Author who has none.
	 */
	public function testLoad_2() {
		$relation = new HasMany('comments', Author::read(1), $this->comment);
		
		$result = $relation->load();
		
		$this->assertTrue($result->isEmpty());
	}
	
	public function testSave_1() {
		$comment = new Comment(['post_id' => 1, 'content' => 'testSave_1']);
		
		$this->relation->save($comment);
		
		$this->assertEquals($this->author->id(), $comment->get('author_id'));
	}
	
	/**
	 * Try creating a new Author model and immediately save a related Comment 
	 * for it, without first storing the Author model in the database. This
	 * should throw an Exception.
	 */
	public function testSave_2() {
		$this->setExpectedException('Rozyn\Relation\ParentDoesNotExistException');
		
		$author = new Author(['name' => 'Jantje']);
		$relation = new HasMany('comments', $author, $this->comment);
		
		$comment = new Comment(['post_id' => 1, 'content' => 'testSave_2']);
		$relation->save($comment);
	}
	
	/**
	 * Try saving a new Comment for an Author who has no previous comments to
	 * his name.
	 */
	public function testSave_3() {
		$author = Author::read(1);
		$relation = new HasMany('comments', $author, $this->comment);
		
		$this->assertTrue($relation->load()->isEmpty());
		
		$comment = new Comment(['post_id' => 1, 'content' => 'testSave_3']);
		$relation->save($comment);
		
		$this->assertEquals($author->id(), $comment->get('author_id'));
	}
	
	/**
	 * Try editing and saving multiple Comments of an Author simultaneously.
	 */
	public function testSaveAll_1() {
		$initial = $this->relation->load()->size();
		
		$comment1 = new Comment(['post_id' => 1, 'content' => 'testSaveAll_1_1']);
		$comment2 = new Comment(['post_id' => 1, 'content' => 'testSaveAll_1_2']);
		$comment3 = new Comment(['post_id' => 1, 'content' => 'testSaveAll_1_3']);
		
		$this->relation->saveAll(new Collection([$comment1, $comment2, $comment3], $this->comment));
		
		$after = $this->relation->load()->size();
		
		$this->assertEquals($this->author->id(), $comment1->get('author_id'));
		$this->assertEquals($this->author->id(), $comment2->get('author_id'));
		$this->assertEquals($this->author->id(), $comment3->get('author_id'));
		$this->assertEquals($initial + 3, $after);
	}
	
	public function testInferForeignKey() {
		$expected	= 'author_id';
		$result		= $this->relation->inferForeignKey();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testInferLocalKey() {
		$expected	= 'id';
		$result		= $this->relation->inferLocalKey();
		
		$this->assertEquals($expected, $result);
	}
	
	/**
	 * Check if clear updates everything correctly locally, ie: without checking
	 * the database.
	 */
	public function testClear_1() {
		$initial = $this->relation->load();
		$this->relation->clear();
		$after = $this->author->getRelationData('comments');
		
		$this->assertGreaterThan(0, $initial->size());
		$this->assertEquals(0, $initial->first()->get('author_id'));
		$this->assertEquals(0, $after->size());
		
		// Reset
		$this->relation->saveAll($initial);
	}
	
	/**
	 * Check if clear updates everything correctly in the database.
	 */
	public function testClear_2() {
		$initial = $this->relation->load();
		$this->relation->clear();
		$after = $this->relation->load();
		
		$this->assertGreaterThan(0, $initial->size());
		$this->assertEquals(0, $after->size());
		
		// Reset
		$this->relation->saveAll($initial);
	}
	
	/**
	 * Test if sync works as intended -- clearing the relation before saving the
	 * specified collection.
	 */
	public function testSync() {
		$mock = $this->getMockBuilder('Rozyn\Relation\HasMany')
					 ->setMethods(['clear', 'saveAll'])
					 ->disableOriginalConstructor()
					 ->getMock();
		
		$mock->expects($this->once())->method('clear');
		$mock->expects($this->once())->method('saveAll');
		
		$mock->sync(new Collection);
	}
	
	public function testLoadBatchArray_1() {
		$expected	= 8;
		$result		= $this->relation->loadBatchArray([1,2,3]);
		
		$this->assertEquals($expected, count($result));
	}
	
	public function testLoadBatchArray_2() {
		$authors = $this->author->newQuery()->select()->fetchArrays();
		
		$this->relation->loadBatchArray([1,2,3], [], $authors);
		
		$this->assertArrayHasKey('comments', $authors[3]);
		$this->assertEquals(1, count($authors[1]['comments']));
		$this->assertEquals(1, count($authors[2]['comments']));
		$this->assertEquals(6, count($authors[3]['comments']));
	}
	
	/**
	 * Test loadBatchArray with a limit on the relation.
	 */
	public function testLoadBatchArray_3() {
		$this->relation->setLimit(3);
		
		$authors = $this->author->newQuery()->select()->fetchArrays();
		
		$this->relation->loadBatchArray([1,2,3], [], $authors);
		
		$this->assertArrayHasKey('comments', $authors[3]);
		$this->assertEquals(3, count($authors[3]['comments']));
	}
}

