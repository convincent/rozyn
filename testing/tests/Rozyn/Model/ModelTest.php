<?php

namespace Test\Rozyn\Model;

use Rozyn\Model\Model;
use Test\TestCase;
use Test\Model\Comment;
use Test\Model\Post;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class ModelTest extends TestCase {
	protected $model;
	protected $stub;
	
	public function setUp() {
		$this->model = new Post();
		$this->stub = $this->getMockBuilder(get_class($this->model));
	}
	
	public function tearDown() {
		unset($this->model);
	}
	
	public function testSetAndGet_1() {
		$this->model->set('title', 'Lorem Ipsum');
		
		$expected	= 'Lorem Ipsum';
		$result		= $this->model->get('title');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testSetAndGet_2() {
		$this->model->set('title', 'Lorem Ipsum');
		
		$expected	= 'Lorem Ipsum';
		$result		= $this->model->title;
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetRelation_1() {
		$expected	= 'Rozyn\Relation\HasMany';
		$result		= $this->model->getRelation('comments');
		
		$this->assertInstanceOf($expected, $result);
	}
	
	public function testGetRelation_2() {
		$this->setExpectedException('Rozyn\Relation\RelationNotFoundException');
		
		$this->model->getRelation('no_relation');
	}
	
	public function testGetRelation_3() {
		$this->setExpectedException('Rozyn\Relation\RelationNotFoundException');
		
		$this->model->getRelation('save');
	}
	
	public function testDirty_1() {
		$this->assertFalse($this->model->isDirty());
	}
	
	public function testDirty_2() {
		$this->model->set('title', 'Lorem Ipsum');
		
		$this->assertTrue($this->model->isDirty());
	}
	
	public function testDirty_3() {
		$this->model->forceSet('title', 'Lorem Ipsum');
		
		$this->assertFalse($this->model->isDirty());
	}
	
	public function testDirty_4() {
		$this->model->set('title', 'Lorem Ipsum');
		
		$this->model->clean();
		
		$this->assertFalse($this->model->isDirty());
	}
	
	public function testInferForeignKey() {
		$expected	= 'post_id';
		$result		= $this->model->inferForeignKey();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testInferTable() {
		$expected	= 'post';
		$result		= $this->model->inferTable();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetAliasedTable() {
		$expected	= 'post AS Post';
		$result		= $this->model->getAliasedTable();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetTable_1() {
		$expected	= 'post';
		$result		= $this->model->getTable();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetTable_2() {
		$this->model->setPrefix('prefix_');
			
		$expected	= 'prefix_post';
		$result		= $this->model->getTable();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetFields() {
		$expected	= ['id', 'author_id', 'title', 'content'];
		$result		= $this->model->getFields();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetStructure() {
		$expected	= ['id', 'author_id', 'title', 'content'];
		$result		= array_keys($this->model->getStructure());
		
		$this->assertEquals($expected, $result);
	}
	
	public function testHasField_1() {
		$this->assertTrue($this->model->hasField('title'));
	}
	
	public function testHasField_2() {
		$this->assertFalse($this->model->hasField('no_field'));
	}
	
	public function testGetData_1() {
		$this->model->set('title', 'Lorem Ipsum');
		
		$expected	= 'Lorem Ipsum';
		$result		= $this->model->getData('title');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetData_2() {
		$this->model->set('title', 'Lorem Ipsum');
		
		$expected	= ['title' => 'Lorem Ipsum'];
		$result		= $this->model->getData();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetData_3() {
		$expected	= [];
		$result		= $this->model->getData();
		
		$this->assertEquals($expected, $result);
	}
	
	/**
	 * Test whether or not saving a dirty new model works properly.
	 */
	public function testSave_1() {
		$mock = $this->stub->setMethods(['newQuery', 'exists', 'set', 'isDirty'])->getMock();
		
		$query = $this->getMockBuilder('Rozyn\Database\ModelQuery')
					  ->setConstructorArgs([$mock])
					  ->setMethods(['insert', 'values', 'execute'])
					  ->getMock();
		
		$query->expects($this->once())->method('insert')->will($this->returnValue($query));
		$query->expects($this->once())->method('values')->will($this->returnValue($query));
		
		$mock->expects($this->once())->method('isDirty')->will($this->returnValue(true));
		$mock->expects($this->once())->method('newQuery')->will($this->returnValue($query));
		$mock->expects($this->once())->method('exists')->will($this->returnValue(false));
		
		$mock->save();
	}
	
	/**
	 * Test whether or not saving a clean new model works properly.
	 */
	public function testSave_2() {
		$mock = $this->stub->setMethods(['newQuery', 'exists', 'set', 'isDirty'])->getMock();
		
		$query = $this->getMockBuilder('Rozyn\Database\ModelQuery')
					  ->setConstructorArgs([$mock])
					  ->setMethods(['insert', 'values', 'execute'])
					  ->getMock();
		
		$query->expects($this->once())->method('insert')->will($this->returnValue($query));
		$query->expects($this->once())->method('values')->will($this->returnValue($query));
		
		$mock->expects($this->once())->method('isDirty')->will($this->returnValue(false));
		$mock->expects($this->once())->method('newQuery')->will($this->returnValue($query));
		$mock->expects($this->exactly(2))->method('exists')->will($this->returnValue(false));
		
		$mock->save();
	}
	
	/**
	 * Test whether or not saving an existing model works properly.
	 */
	public function testSave_3() {
		$mock = $this->stub->setMethods(['newQuery', 'exists', 'set', 'getDirtyData', 'isDirty', 'id'])->getMock();
		
		$query = $this->getMockBuilder('Rozyn\Database\ModelQuery')
					  ->setConstructorArgs([$mock])
					  ->setMethods(['update', 'set', 'where', 'bind', 'execute'])
					  ->getMock();
		
		$query->expects($this->once())->method('update')->will($this->returnValue($query));
		$query->expects($this->once())->method('set')->will($this->returnValue($query));
		$query->expects($this->once())->method('where')->will($this->returnValue($query));
		$query->expects($this->once())->method('bind')->will($this->returnValue($query));
		
		$mock->expects($this->once())->method('isDirty')->will($this->returnValue(true));
		$mock->expects($this->once())->method('getDirtyData')->will($this->returnValue(['title' => 'test']));
		$mock->expects($this->once())->method('newQuery')->will($this->returnValue($query));
		$mock->expects($this->any())->method('exists')->will($this->returnValue(true));
		
		$mock->save();
	}
	
	/**
	 * Test whether or not saving a clean existing model does nothing.
	 */
	public function testSave_4() {
		$mock = $this->stub->setMethods(['isDirty', 'exists'])->getMock();
		
		$mock->expects($this->once())->method('isDirty')->will($this->returnValue(false));
		$mock->expects($this->once())->method('exists')->will($this->returnValue(true));
		$mock->expects($this->never())->method('newQuery');
		
		$mock->save();
	}
	
	/**
	 * Test if saveAll() saves both the current model and all of its related
	 * models.
	 */
	public function testSaveAll() {
		$mock = $this->stub->setMethods(['save', 'saveRelations'])->getMock();
		
		$mock->expects($this->once())->method('save');
		$mock->expects($this->once())->method('saveRelations');
		
		$mock->saveAll();
	}
	
	public function testGetRelationData_1() {
		$expected	= 'Rozyn\Model\Collection';
		$result		= $this->model->getRelationData('comments');
		
		$this->assertInstanceOf($expected, $result);
	}
	
	public function testGetRelationData_2() {
		$model = Post::read(1);
		
		$expected	= 2;
		$result		= $model->getRelationData('comments')->size();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testGetRelationData_3() {
		$this->setExpectedException('Rozyn\Relation\RelationNotFoundException');
		
		$this->model->getRelationData('no_relation');
	}
	
	/**
	 * Test whether or not a soft model is still forcefully deleted if $hard is
	 * set to true.
	 */
	public function testDelete_1() {
		$mock = $this->stub->setMethods(['id', 'isSoft', 'newQuery'])->getMock();
		
		$query = $this->getMockBuilder('Rozyn\Database\ModelQuery')
					  ->setConstructorArgs([$mock])
					  ->setMethods(['execute'])
					  ->getMock();
		
		$query->expects($this->once())->method('execute');
		
		$mock->expects($this->any())->method('id')->will($this->returnValue(1));
		$mock->expects($this->once())->method('isSoft')->will($this->returnValue(true));
		$mock->expects($this->once())->method('newQuery')->will($this->returnValue($query));
		
		$mock->delete(true);
	}
	
	/**
	 * Test whether or not a soft model is not actually deleted from the 
	 * database.
	 */
	public function testDelete_2() {
		$mock = $this->stub->setMethods(['id', 'isSoft', 'save'])->getMock();
		
		$mock->set(Model::SOFT_DELETE_FIELD, false);
		
		$mock->expects($this->any())->method('id')->will($this->returnValue(1));
		$mock->expects($this->once())->method('isSoft')->will($this->returnValue(true));
		$mock->expects($this->never())->method('newQuery');
		$mock->expects($this->once())->method('save');
		
		$mock->delete(false);
		
		$this->assertTrue($mock->get(Model::SOFT_DELETE_FIELD));
	}
	
	/**
	 * Test whether or not a hard model is actually deleted.
	 */
	public function testDelete_3() {
		$mock = $this->stub->setMethods(['id', 'isSoft', 'newQuery'])->getMock();
		
		$query = $this->getMockBuilder('Rozyn\Database\ModelQuery')
					  ->setConstructorArgs([$mock])
					  ->setMethods(['execute'])
					  ->getMock();
		
		$query->expects($this->once())->method('execute');
		
		$mock->expects($this->any())->method('id')->will($this->returnValue(1));
		$mock->expects($this->once())->method('isSoft')->will($this->returnValue(false));
		$mock->expects($this->once())->method('newQuery')->will($this->returnValue($query));
		
		$mock->delete(false);
	}
	
	public function testAddEagerRelations_1() {
		$this->assertEmpty($this->model->getEagerRelations());
		
		$this->model->addEagerRelations(['author']);
		
		$this->assertEquals(['author'], $this->model->getEagerRelations());
	}
	
	public function testAddEagerRelations_2() {
		$this->model->addEagerRelations(['author' => 'comments']);
		$this->assertEquals(['author' => 'comments'], $this->model->getEagerRelations());
		
		$this->model->addEagerRelations(['author' => 'posts']);
		$this->assertEquals(['author' => ['comments', 'posts']], $this->model->getEagerRelations());
	}
}

