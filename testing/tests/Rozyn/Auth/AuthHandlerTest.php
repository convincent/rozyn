<?php

namespace Test\Rozyn\Auth;

use Rozyn\Composition\DI;
use Test\TestCase;
use Rozyn\Auth\AuthHandler;
use Rozyn\Model\Auth\User;
use Rozyn\Model\Auth\Token;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class AuthHandlerTest extends TestCase {
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $encrypter;
	
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $randomizer;
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $session;
	
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $cookie;
	
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $request;
	
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $event;
	
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $hash;
	
	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject
	 */
	protected $client;
	
	/**
	 * @var \Rozyn\Auth\AuthHandler
	 */
	protected $auth;
	
	
	public function setUp() {
		$this->encrypter = $this->getMockBuilder('Rozyn\Encryption\Encrypter')
								->getMock();
		
		$this->randomizer = $this->getMockBuilder('Rozyn\Security\Randomizer')
								 ->getMock();
		
		$this->request = $this->getMockBuilder('Rozyn\Request\IncomingRequest')
							  ->disableOriginalConstructor()
							  ->getMock();
		
		$this->request->expects($this->any())->method('getIp')->will($this->returnValue('0.0.0.0'));
		
		$this->config = $this->getMockBuilder('Rozyn\Config\Config')
							 ->disableOriginalConstructor()
							 ->getMock();
		
		$this->session = $this->getMockBuilder('Rozyn\Session\SessionHandler')
							  ->disableOriginalConstructor()
							  ->setMethods(['has', 'read', 'write', 'clear', 'start', 'regenerateId', 'headersSent'])
							  ->getMock();
		
		$this->session->expects($this->any())->method('headersSent')->will($this->returnValue(false));
		
		$this->cookie = $this->getMockBuilder('Rozyn\Session\CookieHandler')
							 ->setConstructorArgs([$this->encrypter, $this->config])
							 ->setMethods(['has', 'read', 'write'])
							 ->getMock();
		
		$this->event = $this->getMockBuilder('Rozyn\Event\EventHandler')
							->setMethods(['fire'])
							->getMock();
		
		$this->hash = $this->getMockBuilder('Rozyn\Security\Hash')
						   ->setConstructorArgs([$this->randomizer])
						   ->setMethods(['verify'])
						   ->getMock();
		
		// Create a client object instance.
		$this->client = $this->getMockBuilder('Rozyn\Model\Auth\User')
							 ->setMethods(['getRelation'])
							 ->getMock();
		
		// Inject all our dependencies and create the AuthHandler.
		$this->auth = $this->getMockBuilder('Rozyn\Auth\AuthHandler')
						   ->setConstructorArgs([$this->request,
												 $this->session, 
												 $this->cookie, 
												 $this->event, 
												 $this->config,
												 $this->hash,
												 $this->client])
						   ->setMethods(['findUserById',
										 'findUserByAuthField',
										 'findToken'])
						   ->getMock();
	}
	
	public function testValidate_1() {
		$this->hash->expects($this->never())->method('verify');
		
		$this->assertFalse($this->auth->validate([]));
	}
	
	public function testValidate_2() {
		$this->hash->expects($this->never())->method('verify');
		
		$this->assertFalse($this->auth->validate(['email' => 'test@test.com']));
	}
	
	public function testValidate_3() {
		$this->auth->expects($this->once())->method('findUserByAuthField')->will($this->returnValue($this->client));
		$this->hash->expects($this->once())->method('verify')->will($this->returnValue(false));
		
		$this->assertFalse($this->auth->validate(['email' => 'test@test.com', 'password' => 'invalid']));
	}
	
	public function testValidate_4() {
		$this->auth->expects($this->once())->method('findUserByAuthField')->will($this->returnValue($this->client));		
		$this->hash->expects($this->once())->method('verify')->will($this->returnValue(true));
		
		$this->assertTrue($this->auth->validate(['email' => 'test@test.com', 'password' => 'valid']));
	}
	
	public function testAttempt_1() {
		$this->auth->expects($this->once())->method('findUserByAuthField')->will($this->returnValue($this->client));		
		$this->hash->expects($this->once())->method('verify')->will($this->returnValue(false));
		
		$this->assertFalse($this->auth->attempt(['email' => 'test@test.com', 'password' => 'valid']));
	}
	
	public function testAttempt_2() {
		$this->auth->expects($this->exactly(2))->method('findUserByAuthField')->will($this->returnValue($this->client));		
		$this->hash->expects($this->once())->method('verify')->will($this->returnValue(true));
		
		$this->session->expects($this->once())->method('write');
		$this->event->expects($this->once())->method('fire');
		
		$this->assertTrue($this->auth->attempt(['email' => 'test@test.com', 'password' => 'valid']));
	}
	
	/**
	 * Test if remember me functionality is working correctly.
	 */
	public function testRemember_1() {
		$token = $this->getMockBuilder('Rozyn\Model\Auth\Token')
					  ->setMethods(['save'])
					  ->getMock();
		
		$auth = $this->getAuthMock(['auth', 'client']);
		
		$auth->expects($this->any())->method('client')->will($this->returnValue(new User(['id' => 1])));
		$auth->expects($this->any())->method('auth')->will($this->returnValue(true));
					 
		$this->assertTrue($auth->remember($token));
	}
	
	public function testAuth_1() {
		$this->session->expects($this->once())->method('has')->with(AuthHandler::SESSION_CLIENT)->will($this->returnValue(false));
		
		$this->assertFalse($this->auth->auth());
	}
	
	public function testAuth_2() {
		$this->session->expects($this->once())->method('has')->with(AuthHandler::SESSION_CLIENT)->will($this->returnValue(true));
		
		$auth = $this->getAuthMock(['runTests']);
		
		$this->assertTrue($auth->auth());
	}

	/**
	 * Returns a mock of our AuthHandler class.
	 * 
	 * @param	string[]	$methods
	 * @return	\PHPUnit_Framework_MockObject_MockObject
	 */
	private function getAuthMock(array $methods = null) {
		return	$this->getMockBuilder('Rozyn\Auth\AuthHandler')
					 ->setConstructorArgs([	$this->request,
											$this->session, 
											$this->cookie, 
											$this->event, 
											$this->config,
											$this->hash,
											$this->client])
					 ->setMethods($methods)
					 ->getMock();
	}
}

