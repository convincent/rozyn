<?php

namespace Test\Rozyn\Filesystem;

use Test\TestCase;
use Rozyn\Filesystem\Image;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class ImageTest extends TestCase {
	public function setUp() {
		$this->file = new Image(__TESTING__ . DS . PUBLIC_DIR . DS . IMG_DIR . DS . 'nebula.jpg');
	}
	
	public function tearDown() { 
		unset($this->file);
	}
	
	public function testHeight() {
		// Make sure our height isn't initially the same as what we're trying to
		// set it to.
		$this->assertNotEquals($this->file->height(), 197);
		
		// Call the method that we're testing.
		$this->file->height(197);
		
		// Check that the height has been changed successfully and correctly.
		$this->assertEquals($this->file->height(), 197);
		
		// Check that the width has been changed accordingly as well.
		$this->assertEquals($this->file->width(), 322);
	}
	
	public function testWidth() {
		$this->assertNotEquals($this->file->width(), 322);
		
		$this->file->width(322);
		
		$this->assertEquals($this->file->width(), 322);
		$this->assertEquals($this->file->height(), 197);
	}
	
	public function testResize() {
		$this->file->resize(100, 100);
		
		$this->assertEquals($this->file->width(), 100);
		$this->assertEquals($this->file->height(), 100);
	}
	
	public function testResizeMax() {
		$this->file->resizeMax(322, 322);
		
		$this->assertEquals($this->file->width(), 322);
		$this->assertEquals($this->file->height(), 197);
	}
	
	public function testResizeMin() {
		$this->file->resizeMin(197, 197);
		
		$this->assertEquals($this->file->width(), 322);
		$this->assertEquals($this->file->height(), 197);
	}
}