<?php

namespace Test\Rozyn\Security;

use Test\TestCase;
use Rozyn\Security\Randomizer;
use Rozyn\Security\Hash;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class HashTest extends TestCase {
	
	public function setUp() {
		$this->randomizer = new Randomizer();
		$this->hash = new Hash($this->randomizer);
	}
	
	public function tearDown() { 
		unset($this->hash);
	}
	
	public function testHashVerifiesCorrectBlowfish() {
		$password	= 'test';
		$hash		= $this->hash->blowfish($password);
		
		$this->assertTrue($this->hash->verify($password, $hash));
	}
	
	public function testHashNotVerifiesIncorrectBlowfish() {
		$password	= 'test';
		$hash		= $this->hash->blowfish($password);
		
		$wrong		= '123';
		
		$this->assertFalse($this->hash->verify($wrong, $hash));
	}
	
	public function testHashVerifiesCorrectMd5() {
		$password	= 'test';
		$hash		= $this->hash->md5($password);
		
		$this->assertTrue($this->hash->verify($password, $hash));
	}
	
	public function testHashNotVerifiesIncorrectMd5() {
		$password	= 'test';
		$hash		= $this->hash->md5($password);
		
		$wrong		= '123';
		
		$this->assertFalse($this->hash->verify($wrong, $hash));
	}
}

