<?php

namespace Test\Rozyn\Encryption;

use Test\Model\Post;
use Test\TestCase;
use Rozyn\Composition\DI;
use Rozyn\Encryption\Encrypter;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class EncrypterTest extends TestCase {
	protected $model;
	
	public function setUp() {
		$this->encrypter = DI::getInstanceOf('Rozyn\Encryption\Encrypter');
	}
	
	public function tearDown() {
		unset($this->encrypter);
	}
	
	public function testEncrypterCanDecryptOwnMessages() {
		$plain  = 'message';
		$cipher = $this->encrypter->encrypt($plain);
		
		$this->assertEquals($plain, $this->encrypter->decrypt($cipher));
	}
	
	public function testEncrypterRecognizesInvalidMessages() {
		$plain  = 'message';
		$cipher = $this->encrypter->encrypt($plain);
		
		$i = rand(0, mb_strlen($cipher, '8bit') - 1);
		$cipher[$i] = $cipher[$i] ^ chr(1);
		
		$this->setExpectedException("Rozyn\Encryption\InvalidCipherTextException");
		$this->encrypter->decrypt($cipher);
	}
	
	public function testEncrypterCanEncryptObjects() {
		$object = new Post();
		$cipher = $this->encrypter->encrypt($object);
		
		$this->assertEquals($object, $this->encrypter->decrypt($cipher));
	}
}

