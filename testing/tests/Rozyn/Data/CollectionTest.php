<?php

namespace Test\Rozyn\Data;

use Test\TestCase;
use Rozyn\Data\Collection;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class CollectionTest extends TestCase {
	protected $model;
	
	public function setUp() {
		$this->collection = new Collection(['a' => 1, 'b' => 2]);
	}
	
	public function tearDown() {
		unset($this->collection);
	}
	
	public function testReverse() {
		$expected	= ['b' => 2, 'a' => 1];
		$result		= $this->collection->reverse()->toArray();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testDoubleReverse() {
		$expected	= $this->collection->toArray();
		$result		= $this->collection->reverse()->reverse()->toArray();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testSort() {
		$collection = new Collection(['b' => 9, 'a' => 5, 'c' => 12]);
		$compare	= function($a, $b) {
							return ($a > $b) ? 1 : -1;
					  };
		
		$expected	= ['a' => 5, 'b' => 9, 'c' => 12];
		$result		= $collection->sort($compare)->toArray();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testSortDescending() {
		$collection = new Collection(['b' => 9, 'a' => 5, 'c' => 12]);
		$compare	= function($a, $b) {
							return ($a > $b) ? 1 : -1;
					  };
		
		$expected	= array_reverse(['a' => 5, 'b' => 9, 'c' => 12]);
		$result		= $collection->sort($compare, ORDER_DESCENDING)->toArray();
		
		$this->assertEquals($expected, $result);
	}
}

