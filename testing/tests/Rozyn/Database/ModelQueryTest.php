<?php

namespace Test\Rozyn\Database;

use Test\Model\Post;
use Test\Model\Author;
use Test\Model\Comment;
use Test\DatabaseTestCase;
use Rozyn\Database\ModelQuery;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class ModelQueryTest extends DatabaseTestCase {
	/**
	 * @var	Rozyn\Database\ModelQuery
	 */
	protected $query;
	
	public function setUp() {
		$this->post = new Post();
		$this->author = new Author();
		$this->comment = new Comment();
		
		$this->query = new ModelQuery($this->post);
	}
	
	public function testFetchArray_1() {
		$expected	= 'Legolas Nulla';
		$result		= $this->query->select()->fetchArray();
		
		$this->assertEquals($expected, $result['title']);
	}
	
	public function testFetchArray_2() {
		$expected	= 'Arwen Noldori';
		$result		= $this->query->select()->whereEquals('Post.id', 2)->fetchArray();
		
		$this->assertEquals($expected, $result['title']);
	}
	
	public function testFetchArray_3() {
		$expected	= null;
		$result		= $this->query->select()->whereEquals('Post.id', 4)->fetchArray();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testWith_1() {
		$this->query->with(['author' => 'comments']);
		
		$expected	= ['comments'];
		$result		= $this->query->getNestedRelations('author');
		
		$this->assertEquals($expected, $result);
		
//		$this->query->with(['author' => 'posts']);
//		
//		$expected	= ['comments', 'posts'];
//		$result		= $this->query->getNestedRelations('author');
//		
//		$this->assertEquals($expected, $result);
	}
	
	/**
	 * Test objectifyRow() with a basic use case.
	 */
	public function testObjectifyRow_1() {
		$input = array(
			'id' => 100,
			'author_id' => 0,
			'title' => 'testObjectifyRow_1',
			'content' => 'testObjectifyRow_1'
		);
		 
		$result = $this->query->objectifyRow($input);
		
		$this->assertInstanceOf('Test\Model\Post', $result);
		$this->assertEquals($input, $result->getData());
	}
	
	/**
	 * Test objectifyRow() with related data.
	 */
	public function testObjectifyRow_2() {
		$input = array(
			'id' => 100,
			'author_id' => 100,
			'title' => 'testObjectifyRow_2',
			'content' => 'testObjectifyRow_2',
			'author' => array(
				'id' => 100,
				'name' => 'testObjectifyRow_2',
			)
		);
		 
		$result = $this->query->objectifyRow($input);
		
		$this->assertInstanceOf('Test\Model\Author', $result->getRelationData('author', false));
		$this->assertEquals($input['author'], $result->getRelationData('author')->getData());
	}
	
	/**
	 * Test objectifyRow() with model data that does not match the query's 
	 * model.
	 */
	public function testObjectifyRow_3() {
		$input = array(
			'id' => 100,
			'name' => 'testObjectifyRow_3',
		);
		 
		$result = $this->query->objectifyRow($input, $this->author);
		
		$this->assertInstanceOf('Test\Model\Author', $result);
		$this->assertEquals($input, $result->getData());
	}
	
	/**
	 * Test objectifyRow() with related data.
	 */
	public function testObjectifyRow_4() {
		$input = array(
			'id' => 100,
			'author_id' => 100,
			'title' => 'testObjectifyRow_4',
			'content' => 'testObjectifyRow_4',
			'author' => null,
		);
		 
		$result = $this->query->objectifyRow($input);
		
		$this->assertNull($result->getRelationData('author', false));
	}
	
	/**
	 * Test objectifyRow() with null as its only parameter.
	 */
	public function testObjectifyRow_5() {
		$result = $this->query->objectifyRow(null);
		
		$this->assertNull($result);
	}
	
	/**
	 * Test objectify() with a basic use case.
	 */
	public function testObjectify_1() {
		$input = array(
			100 => array(
				'id' => 100,
				'author_id' => 0,
				'title' => 'testObjectify_1_1',
				'content' => 'testObjectify_1_1'
			),
			
			101 => array(
				'id' => 101,
				'author_id' => 0,
				'title' => 'testObjectify_1_2',
				'content' => 'testObjectify_1_2'
			),
		);
		 
		$result = $this->query->objectify($input);
		
		$this->assertInstanceOf('Rozyn\Model\Collection', $result);
		$this->assertInstanceOf('Test\Model\Post', $result->first());
		$this->assertEquals($input[100], $result->shift()->getData());
		$this->assertEquals($input[101], $result->shift()->getData());
	}
	
	/**
	 * Test objectify() with related data.
	 */
	public function testObjectify_2() {
		$input = array(
			100 => array(
				'id' => 100,
				'author_id' => 100,
				'title' => 'testObjectify_2_1',
				'content' => 'testObjectify_2_1',
				'comments' => array(
					100 => array(
						'id' => 100,
						'author_id' => 0,
						'post_id' => 100,
						'content' => 'testObjectify_2_2',
					),
					
					101 => array(
						'id' => 101,
						'author_id' => 0,
						'post_id' => 100,
						'content' => 'testObjectify_2_2',
					),
				)
			),
			
			101 => array(
				'id' => 101,
				'author_id' => 0,
				'title' => 'testObjectify_2_3',
				'content' => 'testObjectify_2_3',
				'comments' => array(
					102 => array(
						'id' => 102,
						'author_id' => 0,
						'post_id' => 101,
						'content' => 'testObjectify_2_4',
					),
					
					103 => array(
						'id' => 103,
						'author_id' => 0,
						'post_id' => 101,
						'content' => 'testObjectify_2_4',
					),
				)
			),
		);
		 
		$result = $this->query->objectify($input);
		
		$first = $result->shift();
		$second = $result->shift();

		$this->assertInstanceOf('Test\Model\Comment', $first->getRelationData('comments')->first());
		$this->assertInstanceOf('Test\Model\Comment', $second->getRelationData('comments')->first());

		$this->assertInstanceOf('Rozyn\Model\Collection', $first->getRelationData('comments', false));
		$this->assertEquals($input[100]['comments'][100], $first->getRelationData('comments')->shift()->getData());
		$this->assertEquals($input[100]['comments'][101], $first->getRelationData('comments')->shift()->getData());
		
		$this->assertInstanceOf('Rozyn\Model\Collection', $second->getRelationData('comments', false));
		$this->assertEquals($input[101]['comments'][102], $second->getRelationData('comments')->shift()->getData());
		$this->assertEquals($input[101]['comments'][103], $second->getRelationData('comments')->shift()->getData());
	}
	
	/**
	 * Test objectify() with nested related data.
	 */
	public function testObjectify_3() {
		$input = array(
			100 => array(
				'id' => 100,
				'author_id' => 100,
				'title' => 'testObjectify_3_1',
				'content' => 'testObjectify_3_1',
				'comments' => array(
					100 => array(
						'id' => 100,
						'author_id' => 100,
						'post_id' => 100,
						'content' => 'testObjectify_3_2',
						'author' => array(
							'id' => 100,
							'name' => 'Author_1'
						)
					),
					
					101 => array(
						'id' => 101,
						'author_id' => 101,
						'post_id' => 100,
						'content' => 'testObjectify_3_2',
						'author' => array(
							'id' => 101,
							'name' => 'Author_2'
						)
					),
				)
			),
			
			101 => array(
				'id' => 101,
				'author_id' => 0,
				'title' => 'testObjectify_3_3',
				'content' => 'testObjectify_3_3',
				'comments' => array(
					102 => array(
						'id' => 102,
						'author_id' => 100,
						'post_id' => 101,
						'content' => 'testObjectify_3_4',
						'author' => array(
							'id' => 100,
							'name' => 'Author_1'
						)
					),
					
					103 => array(
						'id' => 103,
						'author_id' => 101,
						'post_id' => 101,
						'content' => 'testObjectify_3_4',
						'author' => array(
							'id' => 101,
							'name' => 'Author_2'
						)
					),
				)
			),
		);
		 
		$result = $this->query->objectify($input);
		
		$first = $result->shift();
		$second = $result->shift();
		
		$this->assertInstanceOf('Test\Model\Author', $first->getRelationData('comments', false)
														   ->first()
														   ->getRelationData('author', false));
		
		$this->assertEquals($input[100]['comments'][100]['author'], $first->getRelationData('comments')
																		  ->first()
																		  ->getRelationData('author')
																		  ->getData());
		
		$this->assertInstanceOf('Test\Model\Author', $second->getRelationData('comments', false)
														    ->first()
														    ->getRelationData('author', false));
		
		$this->assertEquals($input[101]['comments'][102]['author'], $second->getRelationData('comments')
																		   ->first()
																		   ->getRelationData('author')
																		   ->getData());
	}
	
	/**
	 * Test objectify() with a JoinModel.
	 */
	public function testObjectify_4() {
		$posts = Post::query()->select()->with('tags')->fetchModels();
		
		$this->assertArrayHasKey('created_at', $posts->first()->tags->first()->getJoinModel()->getData());
	}
}