<?php

namespace Test\Rozyn\Database;

use Test\DatabaseTestCase;
use Rozyn\Database\Query;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class QueryTest extends DatabaseTestCase {
	/**
	 * @var	\Rozyn\Database\ModelQuery	$query
	 */
	protected $query;
	
	public function setUp() {
		$this->query = new Query();
	}
	
	public function tearDown() { 
		unset($this->query);
	}
	
	public function testFetchList_1() {
		$this->setExpectedException('Rozyn\Database\QueryException');
		$this->query->select()->from('post')->fetchList();
	}
	
	public function testFetchList_2() {
		$expected	= ['1', '2', '3'];
		$result		= $this->query->select()->fields('id')->from('post')->orderBy('id', 'ASC')->fetchList();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testFetchList_3() {
		$expected	= ['Legolas Nulla', 'Arwen Noldori', 'Elrond Consilo'];
		$result		= $this->query->select()->fields('title')->from('post')->orderBy('id', 'ASC')->fetchList();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testFetchList_4() {
		$expected	= [1 => 'Legolas Nulla', 2 => 'Arwen Noldori', 3 => 'Elrond Consilo'];
		$result		= $this->query->select()->fields(['id', 'title'])->from('post')->orderBy('id', 'ASC')->fetchList();
		
		$this->assertEquals($expected, $result);
	}
	
	public function testFetchList_5() {
		$expected	= ['Legolas Nulla', 'Arwen Noldori', 'Elrond Consilo'];
		$result		= $this->query->select()->from('post')->orderBy('id', 'ASC')->fetchList('title');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testFetchList_6() {
		$expected	= [1 => 'Legolas Nulla', 2 => 'Arwen Noldori', 3 => 'Elrond Consilo'];
		$result		= $this->query->select()->from('post')->orderBy('id', 'ASC')->fetchList('title', 'id');
		
		$this->assertEquals($expected, $result);
	}
	
}

