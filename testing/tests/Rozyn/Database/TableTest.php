<?php

namespace Test\Rozyn\Database;

use Rozyn\Composition\DI;
use Test\DatabaseTestCase;
use Rozyn\Database\Table;
use Rozyn\Database\Database;
use Rozyn\Database\QueryException;
use Rozyn\Database\TableException;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class TableTest extends DatabaseTestCase {
	/**
	 * The name used for our test tables.
	 * 
	 * @var string
	 */
	protected $name;
	
	/**
	 * A Database instance.
	 * 
	 * @var \Rozyn\Database\Database
	 */
	protected $db;
	
	/**
	 * A Table instance.
	 * 
	 * @var \Rozyn\Database\Table
	 */
	protected $table;
	
	public function setUp() {
		$this->name = prefix($this->getName(), 'table_');
		
		$this->db	 = DI::getInstanceOf('Rozyn\Database\Database');
		$this->table = new Table($this->name);
		
		$this->table->drop();
		$this->table->addColumn('id', 'INT');
		$this->table->create();
	}
	
	public function tearDown() {
		unset($this->db);
		unset($this->table);
	}
	
	public function testCanDropTable() {
		$this->assertTrue($this->table->exists());
		
		$this->table->drop();
		
		$this->assertFalse($this->table->exists());
	}
	
	public function testCanCreateTable() {
		$this->assertTrue($this->table->exists());
	}
	
	public function testCanAddColumn() {
		$this->table->addColumn('test', 'VARCHAR(255)');
	}
	
	public function testCanAddColumnWithIndex() {
		$this->table->addColumn('test2', 'VARCHAR(255)');
		$this->table->index('test2');	
	}
	
	public function testCanDropColumn() {
		$this->table->addColumn('test3', 'VARCHAR(255)');
		
		$this->db->query("SELECT test3 FROM {$this->table->getName()}");
		$this->db->execute();
		
		$this->table->dropColumn('test3');
		
		try {
			$this->db->query("SELECT test3 FROM {$this->table->getName()}");
			$this->db->execute();
			
			$this->fail("Column test3 exists when it shouldn't.");
		} catch (\PDOException $e) {}
	}
	
	public function testCanChangeColumnDataType() {
		$this->table->addColumn('test4', 'VARCHAR(10)');
		$this->table->modifyColumn('test4', 'TEXT');
		
		$string = str_random(md5(uniqid()), 256);
			
		try {
			$this->db->query(str_format("INSERT INTO `{table}` (`id`, `test4`) VALUES (1, '{string}');", array(
				'table' => $this->table->getName(),
				'string' => $string
			)));
			
			$this->db->execute();
		} catch (QueryException $e) {
			vd($e->getMessage());
			
			$this->fail("Could not store a string of length 256 after converting column to TEXT type.");
		}
			
		$this->db->query(str_format("SELECT `test4` FROM `{table}` WHERE id = 1", array(
			'table' => $this->table->getName(),
		)));

		$result = $this->db->fetch();

		$this->assertEquals($result['test4'], $string);
	}
	
	public function testCanChangeColumnCollation() {
		$this->table->addColumn('test5', 'VARCHAR(255)');
		
		$collation = 'latin1_swedish_ci';
		$this->table->setColumnCollation('test5', $collation);
		
		$this->table->syncColumns();
		
		$this->assertEquals($this->table->getColumnCollation('test5'), $collation);
	}
	
	public function testCanCallCreateEvenWhenTableAlreadyExists() {
		$this->table->create();
		
		$this->assertTrue($this->table->exists());
		
		try {
			$this->table->create();
		} catch (TableException $e) {
			$this->fail('Trying to create a table that already exists throws Exception.');
		}
	}
}

