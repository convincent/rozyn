<?php

namespace Test\Rozyn\Helper;

use Test\TestCase;
use Rozyn\Helper\HtmlHelper;

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class HtmlHelperTest extends TestCase {
	/**
	 * Our HtmlHelper instance to be tested.
	 * 
	 * @var \Rozyn\Helper\HtmlHelper
	 */
	protected $html;
	
	public function setUp() {
		$this->html = new HtmlHelper();
	}
	
	public function tearDown() { 
		unset($this->html);
	}
	
	public function testStandardTag_1() {
		$expected	= '<p></p>';
		$result		= $this->html->tag('p');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testStandardTag_2() {
		$expected	= '<p>content</p>';
		$result		= $this->html->tag('p', 'content');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testStandardTag_3() {
		$expected	= '<p id="test" class="test">content</p>';
		$result		= $this->html->tag('p', 'content', ['id' => 'test', 'class' => 'test']);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testSelfClosedTag_1() {
		$expected	= '<br/>';
		$result		= $this->html->tag('br');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testSelfClosedTag_2() {
		$expected	= '<link href="#" type="text/css"/>';
		$result		= $this->html->tag('link', ['href' => '#', 'type' => 'text/css']);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testBooleanAttributes_1() {
		$expected	= '<input disabled="disabled"/>';
		$result		= $this->html->tag('input', ['disabled' => true]);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testBooleanAttributes_2() {
		$expected	= '<input />';
		$result		= $this->html->tag('input', ['disabled' => false]);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testStyleAttributes_1() {
		$expected	= '<p style="display:none;">content</p>';
		$result		= $this->html->tag('p', 'content', ['css' => ['display' => 'none']]);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testStyleAttributes_2() {
		$expected	= '<p style="background:#fff no-repeat cover;">content</p>';
		$result		= $this->html->tag('p', 'content', ['css' => ['background' => '#fff no-repeat cover']]);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testDataAttributes_1() {
		$expected	= '<p data-test="test" data-testing="testing">content</p>';
		$result		= preg_replace('/\s\s+/', ' ', $this->html->tag('p', 'content', ['data' => ['test' => 'test', 'testing' => 'testing']]));
		
		$this->assertEquals($expected, $result);
	}
	
	public function testPredefinedTag_a_1() {
		$expected	= '<a href="#">content</a>';
		$result		= $this->html->a('content', ['href' => '#']);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testPredefinedTag_css_1() {
		$expected	= '<link type="text/css" rel="stylesheet" href="file.css"/>';
		$result		= $this->html->css('file.css');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testPredefinedTag_js_1() {
		$expected	= '<script type="text/javascript" src="file.js"></script>';
		$result		= $this->html->js('file.js');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testPredefinedTag_img_1() {
		$expected	= '<img alt="" src="file.jpg"/>';
		$result		= $this->html->img('file.jpg');
		
		$this->assertEquals($expected, $result);
	}
	
	public function testPredefinedTag_img_2() {
		$expected	= '<img alt="test" src="file.jpg"/>';
		$result		= $this->html->img('file.jpg', ['alt' => 'test']);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testContentAsClosure_1() {
		$expected	= '<p>Test</p>';
		$result		= $this->html->tag('p', function() {
			return 'Test';
		});
		
		$this->assertEquals($expected, $result);
	}
	
	public function testContentAsClosure_2() {
		$expected	= '<p><span>Test</span></p>';
		$result		= $this->html->tag('p', function($html) {
			return $html->tag('span', 'Test');
		});
		
		$this->assertEquals($expected, $result);
	}
	
	public function testContentAsClosure_3() {
		$expected	= '<img src="" alt=""/>';
		$result		= $this->html->tag('img', function() {
			return 'Test';
		}, ['src' => '', 'alt' => '']);
		
		$this->assertEquals($expected, $result);
	}
	
	public function testMagicCall_1() {
		$expected	= '<p id="test" class="test">content</p>';
		$result		= $this->html->p('content', ['id' => 'test', 'class' => 'test']);
		
		$this->assertEquals($expected, $result);
	}
}