<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1486647063_add_additional_auth_groups extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		$result = (new Query())	->select(['name'])
								->from('auth_group')
								->whereEquals('name', 'admin')
								->fetch();
		
		if (!$result) {
			(new Query())	->insert()
							->into('auth_group')
							->fields(['id', 'name'])
							->values(['id' => 4, 'name' => 'admin'])
							->execute();
		}
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {
		
	}
}