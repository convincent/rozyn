<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1463233096_auth_fixes extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		$table = Table::instance('auth_resource');
		
		if ($table->exists() && $table->hasColumn('name')) {
			$table->dropPrimary();
			
			if ($table->hasColumn('id')) {
				$table->dropColumn('id');
			}
			
			$table->renameColumn('name', 'id')->primary('id')->push();
		} 
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {

	}
}