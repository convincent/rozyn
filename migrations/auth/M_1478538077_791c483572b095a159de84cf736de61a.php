<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1478538077_791c483572b095a159de84cf736de61a extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		$table = Table::instance('auth_user');
		$table->nullable(['username', 'firstname', 'lastname']);
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {
		$table = Table::instance('auth_user');
		$table->nullable(['username', 'firstname', 'lastname'], false);
	}
}