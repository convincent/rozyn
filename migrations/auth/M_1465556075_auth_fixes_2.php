<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

class M_1465556075_auth_fixes_2 extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		$table = Table::instance('auth_users_resources');
		
		$table->getColumn('resource_id')
			  ->setType('VARCHAR(255)')
			  ->setCollation('utf8_general_ci');
		
		$table->commitColumn('resource_id')
			  ->push();
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {

	}
}