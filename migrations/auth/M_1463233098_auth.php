<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

use Rozyn\Database\Query;
use Rozyn\Database\Table;
use Rozyn\Database\Column;
use Rozyn\Database\Migration;

use Rozyn\Facade\Hash;
use Rozyn\Facade\Random;

use Rozyn\Bin\MigrationException;

class M_1463233098_auth extends Migration {
	/**
	 * Rolls out the migration.
	 */
	public function rollOut() {
		$this->createTableAuthToken();
		$this->createTableAuthUser();
		$this->createTableAuthGroup();
		$this->createTableAuthResource();
		$this->createTableAuthSession();
		$this->createTableAuthAttempt();
		
		$this->createJoinTableAuthGroupsResources();
		$this->createJoinTableAuthUsersGroups();
		$this->createJoinTableAuthUsersResources();
		
		$this->createAuthGroupRecordSuperAdmin();
		$this->createAuthUserRecordSuperAdmin();
	}
	
	/**
	 * Create the auth session table.
	 */
	public function createTableAuthSession() {
		$table = Table::instance('auth_session');
		$table->drop();
		
		if (!$table->exists()) {
			$table	->primary('id')
					->integer('user_id')
					->string('token')
					->string('csrf')
					->string('ip')
					->timestamp('expires_at')
					->index('user_id')
					->unique('token')
					->unique('csrf')
					->index('expires_at')
					->index('ip')
					->timestamps()
					->create();
		}
	}
	
	/**
	 * Create the auth attempt table.
	 */
	public function createTableAuthAttempt() {
		$table = Table::instance('auth_attempt');
		$table->drop();
		
		if (!$table->exists()) {
			$table	->primary('id')
					->string('auth')
					->string('ip', 32)
					->boolean('success')
					->timestamp('created_at')
					->index('auth')
					->index('ip')
					->index('success')
					->index('created_at')
					->create();
		}
	}
	
	/**
	 * Create the auth group table.
	 */
	public function createTableAuthGroup() {
		$table = new Table('auth_group');
		$table->drop();
		
		if (!$table->exists()) {
			$table->primary('id')
				  ->string('name')
				  ->index('name')
				  ->create();
		}
	}
	
	/**
	 * Create the auth resource table.
	 */
	public function createTableAuthResource() {
		$table = new Table('auth_resource');
		$table->drop();
		
		if (!$table->exists()) {
			$table->string('name')
				  ->primary('name')
				  ->create();
		}
	}
	
	/**
	 * Create the resource join table.
	 */
	public function createJoinTableAuthUsersResources() {
		$table = new Table('auth_users_resources');
		if (!$table->exists()) {
			$table->primary('id')
				  ->integer('user_id')
				  ->string('resource_id')
				  ->index('user_id')
				  ->index('resource_id')
				  ->create();
		}
	}
	
	/**
	 * Create the resource join table.
	 */
	public function createJoinTableAuthGroupsResources() {
		$table = new Table('auth_groups_resources');
		if (!$table->exists()) {
			$table->primary('id')
				  ->integer('group_id')
				  ->string('resource_id')
				  ->index('group_id')
				  ->index('resource_id')
				  ->create();
		}
	}
	
	/**
	 * Create the resource join table.
	 */
	public function createJoinTableAuthUsersGroups() {
		$table = new Table('auth_users_groups');
		if (!$table->exists()) {
			$table->primary('id')
				  ->integer('user_id')
				  ->integer('group_id')
				  ->index('user_id')
				  ->index('group_id')
				  ->create();
		}
	}
	
	/**
	 * Creates the auth token table.
	 */
	public function createTableAuthToken() {
		$table = new Table('auth_token');
		if (!$table->exists()) {
			$table->primary('id')
				  ->integer('user_id')
				  ->string('session_token')
				  ->string('secret')
				  ->dateTime('created_at')
				  ->dateTime('expires_at')
				  ->soft()
				  ->index('user_id')
				  ->index('session_token')
				  ->index('secret')
				  ->index('created_at')
				  ->index('expires_at')
				  ->create();
		}
	}
	
	/**
	 * Creates the auth user table.
	 */
	public function createTableAuthUser() {
		$table = Table::instance('auth_user');
		if (!$table->exists()) {
			$table->primary('id')
				  ->string('username')
				  ->string('firstname')
				  ->string('lastname')
				  ->string('avatar')
				  ->string('email')
				  ->string('password')
				  ->soft()
				  ->nullable('avatar')
				  ->index('email')
				  ->create();
		}
	}
	
	/**
	 * Insert our superadmin group in the database.
	 */
	public function createAuthGroupRecordSuperAdmin() {
		$result = (new Query())	->select(['name'])
								->from('auth_group')
								->whereEquals('name', 'superadmin')
								->fetch();
		
		if (!$result) {
			(new Query())	->insert()
							->into('auth_group')
							->fields(['id', 'name'])
							->values(['id' => 5, 'name' => 'superadmin'])
							->execute();
		}
	}
	
	/**
	 * Insert our superadmin in the user table.
	 */
	public function createAuthUserRecordSuperAdmin() {
		$result = (new Query())	->select(['email'])
								->from('auth_user')
								->whereEquals('email', 'jeroen.mandersloot@gmail.com')
								->fetch();
		
		if (!$result) {

			// Create our root admin user.
			$password = Random::base64(32);
			$user = array(
				'password'		=> Hash::hash($password),
				'username'		=> 'superadmin',
				'firstname'		=> 'Jeroen',
				'lastname'		=> 'Mandersloot',
				'avatar'		=> '',
				'email'			=> 'jeroen.mandersloot@gmail.com',
			);

			(new Query())	->insert()
							->into('auth_user')
							->fields(['password', 'username', 'firstname', 'lastname', 'avatar', 'email'])
							->values($user)
							->execute();
			
			$admin = (new Query())	->select(['id'])
									->from('auth_user')
									->whereEquals('email', 'jeroen.mandersloot@gmail.com')
									->fetch();
			if (!$admin) {
				throw new MigrationException("Could not create superadmin.");
			}
			
			(new Query())	->insert()
							->into('auth_users_groups')
							->fields(['user_id', 'group_id'])
							->values(['user_id' => $admin['id'], 'group_id' => 5])
							->execute();

			// If the user was successfully saved, send ourselves an e-mail with the
			//password.
			$email = new \PHPMailer();
			$email->From      = $user['email'];
			$email->FromName  = $user['firstname'] . ' ' . $user['lastname'];
			$email->Subject   = 'Password for Rozyn project ' . basename(dirname(dirname(dirname(__FILE__))));
			$email->Body	  = $password;
			$email->addAddress($user['email']);

			$email->Send();
		}
	}
	
	/**
	 * Rolls back the migration, undoing all the changes made in the rollOut()
	 * method.
	 */
	public function rollBack() {

	}
}