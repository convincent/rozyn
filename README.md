# General

The Rozyn framework is a personal PHP framework that I developed for my own projects. It is not intended for widespread use, as it is somewhat limited in its support for different production environments. It is mainly meant to provide a robust framework that speeds up development for me, personally, in the environments that I mostly develop in.

It requires the following:

*	Apache server
*	MySQL database
*	PHP >= 5.4 (preferably >= 7.1)
*	Windows, Mac or Linux machine

Rozyn uses a Model-View-Controller (MVC) architecture for all its apps. The pipeline for an incoming request is rather straightforward:

*	Set up the app and load all configuration in `config/rozyn/bootstrap.php`
*	Match the request URI to one of the routes specified in `config/routes.php`
*	Instantiate the `Rozyn\Controller\Controller` subclass associated with the matched route.
*	Call the method associated with the matched route, passing any potential URL arguments along.
*	Render the template file associated with the matched route.
*	Render the view file associated with the controller and method.
*	Wrap the view inside the template and serve the response to the client.


# Directory structure

### /bin/
The `bin` directory contains files that are used for executing commands in the terminal. On a clean install it only includes the main `rozyn` command, which serves as an interface for a wide variety of core functions. 

Under Mac/Linux:
```sh
cd ~/path/to/rozyn/bin
./rozyn <command>
```

Or under Windows:
```sh
cd ~/path/to/rozyn/bin
php rozyn <function>
```

For a list of available functions, inspect the `src/Rozyn/Bin/Rozyn.php` file. The `rozyn` command allows you to execute each of the public methods in this class through the terminal. The most commonly used functions are:

*	`install` (creates a new project)
*	`update <project>` (updates the core rozyn files in a project)
*	`make_migration` (creates a new migration file)
*	`run_migrations` (runs all new migration files)

You can add your own functions by creating a new custom directory inside the `bin` directory which contains an `index.php` file. For example, if you named your custom directory `my_function`, you would be able to execute the PHP code in `bin/my_function/index.php` by running

```sh
cd ~/path/to/rozyn/bin
./rozyn my_function
```

### /cache/
The `cache` directory is one you should generally not have to worry about. It is only used to store cached files that can later be retrieved quickly. 

### /config/
The `config` directory contains the main configuration of the app. It contains several important subdirectories:

*	`config/env` contains config files that are specific to an environment, so that you can for example specify separate database connections for local and production environments.
*	`config/plugins` contains config files for plugins; you generally don't have to worry about this.
*	`config/rozyn` contains core config files that are required for the app to run; you should generally not edit this.

The core config files in `config/rozyn` take care of global configuration. A brief discussion of the various files follows below:

*	`config/rozyn/aliases.php` is used for flexible dependency injection, where we can map an abstract class to a specific class that should be used whenever an instance of the abstract class is requested.
*	`config/rozyn/autoload.php` sets up the auto loader.
*	`config/rozyn/bootstrap.php` is responsible for initializing the app and including all other config files.
*	`config/rozyn/config.php` contains an array of straightforward config options.
*	`config/rozyn/constants.php` defines several constants used throughout the core code.
*	`config/rozyn/debug.php` specifies how to deal with errors; on local and development environments error messages are printed to the screen directly, whereas for other environments they are logged to a file.
*	`config/rozyn/env.php` detects the current environment and sets the appropriate constants.
*	`config/rozyn/functions.php` contains several core procedural functions for dealing with strings, arrays, files, etc.
*	`config/rozyn/globals.php` defines a limited number of global variables.
*	`config/rozyn/lang.php` imports the translations for all languages supported by the app (i.e. those specified in the `/lang/` directory).
*	`config/rozyn/paths.php` defines constants for full paths to many of the directories so they can be used in an easy and consistent way.
*	`config/rozyn/validation.php` contains some standard validation methods.

All app-specific configuration files are stored directly under the `config` directory. These typically include:

*	`config/bootstrap.php`, which contains custom PHP code that needs to be executed on startup.
*	`config/config.php`, which contains additional configuration options for this app specifically.
*	`config/routes.php`, which contains all the routes for the app.

### /lang/
The `lang` directory contains all translation files. Each language gets its own subdirectory, e.g. `lang/nl` for Dutch, containing all translation files for that language specifically. Translation files can either be `.php` files that return a single array mapping keys to sentences, or plain `.txt` files where the first word on each line is a key and everything that follows constitutes the corresponding translation.

### /migrations/
The `migrations` directory contains all migration files for the app. You are free to create whatever directory structure inside; when rolling out migrations the entire directory is read recursively. Each migration is a `.php` file that contains a class with the same name as the file. This class must extend `Rozyn\Database\Migration` and therefore implement a `rollOut()` and `rollBack()` method. The first is used to run and apply the migration, whereas the second should revert these changes when run.

### /plugins/
The `plugins` directory contains parts of the app that are generic and can be used across multiple different apps if desired, but are not quite core-level. Currently there are 2 plugins: `plugins/rozadmyn` and `plugins/mediadmyn`, both related to admin panels as the names might suggest. Rozadmyn is a generic CMS implementation, whereas Mediadmyn contains a multimedia management system specifically for Rozadmyn. Inside each plugin's directory we find a directory structure similar to that of the main app, since each plugin can itself be considered a mini-app or widget.

### /public/
The `public` directory contains all the front-end files such as stylesheets, images, scripts and files that have been uploaded by users. It also contains its own `public/cache` directory, which contains minified CSS and JS files. Each time a page is rendered, all its stylesheets and script files are concatenated and minified in a single `.css` or `.js` file, respectively, and this file is then loaded instead. This reduces load time significantly. Every time the same page is loaded afterwards, the app checks whether any of the CSS or JS files have changed in the meantime, and if not, the same minified file is loaded without being regenerated.

### /src/
The `src` directory is the main component of the app. It contains all PHP source code for all the models, controllers and core functionality. A clean install of a Rozyn app comes with minimal controller and model files pre-packaged -- just enough to serve as a bare example. All controllers and models are stored in the `src/Controller` and `src/Model` directories, respectively. Every controller class must extend `Rozyn\Controller\Controller` and every model class must extend `Rozyn\Model\Model`. The core functionality of every app resides in the `src/Rozyn` subdirectory. 

The autoloader is configured to look for each class inside the `src` directory through a one-on-one correspondence between the namespace of the class and the location of the file in which it is defined. For example, the class `Rozyn\Bin\Rozyn` is expected to be defined in the file `src/Rozyn/Bin/Rozyn.php`.

There are two special kind of models: `Rozyn\Model\TranslatableModel` and `Rozyn\Model\TranslationModel`. As their names suggest, these are used for models that contain data that differs per language (e.g. the `Model\Page` and `Model\PageTranslation` classes contained in the `src/Model` subdirectory). These base model classes contain additional functionality to decouple language-independent logic from the language-dependent logic inside what would otherwise be a single model.

The core functionality in `src/Rozyn` is split among many different modules.  Below we briefly cover each of them:

*	`Auth`: logic for authenticating users.
*	`Bin`: code that can (indirectly) be executed from a terminal.
*	`Cache`: handles caching and provides support for different caching strategies.
*	`Composition`: support for flexible dependency injection.
*	`Controller`: contains the core controller classes from which all other controllers must inherit.
*	`Data`: contains several data structures.
*	`Database`: all classes that interact with the database.
*	`Encryption`: allows us to encrypt sensitive data that later needs to be decrypted.
*	`Error`: generic error handling.
*	`Event`: provides support for event listeners and handlers in PHP code.
*	`Facade`: ease-of-use static classes that serve as thin layers on top of non-static singleton classes.
*	`Filesystem`: any code related to manipulating the file system.
*	`Helper`: helper classes that can be used in views for easy HTML/DOM manipulation.
*	`I18n`: short for internationalization and provides multilingual support.
*	`Logging`: support for logging errors, warnings or other useful information.
*	`Model`: contains the core model classes from which all other models must inherit.
*	`Pagination`: support for displaying data in a paginated way.
*	`Plugin`: base classes for plugin classes defined in `/plugins/<plugin>/src/<plugin>/<plugin>.php`.
*	`Relation`: classes to define relations between models, such as one model belonging to another.
*	`Request`: code to handle incoming requests or create outgoing requests (sockets, curl, ...).
*	`Response`: wrapper classes that contain responses from our app and provide consistent interfaces for interacting with them.
*	`Routing`: contains all the logic pertaining to routes and routing.
*	`Security`: various classes that help make the app more secure.
*	`Session`: classes that keep track of client sessions.
*	`Validation`: contains logic for validating model data.

The `Rozyn\App` class defined in `src/Rozyn/App.php` represents an instance of the entire app running at the moment. After loading all configuration through the `config/rozyn/bootstrap.php` file, the app is started by calling `Rozyn\App->run()`.

### /templates/
The `templates` directory contains the backbone HTML templates in which individual views can be inserted later. Templates are useful for creating a general HTML structure for each web page and taking care of recurring elements such as footers, global stylesheets, meta tags, etc. Inside each `<template>.php` file you can use the variable `$this` to access the associated `Rozyn\Response\Response` instance.

### /testing/
The `testing` directory contains all unit tests and all other code and assets that are required for running them. Currently, our code coverage isn't that good, but something is better than nothing. You can run the unit tests by running the following on Mac/Linux (does not work on Windows):

```sh
cd ~/path/to/rozyn/bin
./rozyn test
```

### /views/
The `views` directory contains the HTML view files that are used to present the result of a controller method to the client. By default, the view file associated with a controller and method is located at `views/<controller>/<method>.php`. So for example, the view for `Controller\PageController->index()` is located at `views/page/index.php`.
Views can also be used independently from a controller to define blocks of code that can be reused across several other views. Inside each `<view>.php` file you can use the variable `$this` to access the associated `Rozyn\Response\View` instance. This is useful for accessing the view's helper methods by calling `$this->Html->...()`.


# Design

### Database interaction
I have created my own ORM and included it in the core of Rozyn to easily communicate with the database. I work almost exclusively with MySQL databases, so at the moment there is only support for those. 

In the `src/Rozyn/Database` directory you will find a lot of `...Statement.php` files, each of which contains a class that deals specifically with SQL statements of a particular kind. You should rarely have to work with them directly, as the `Rozyn\Database\Query` gives a nice interface that deals with the different `Rozyn\Database\Statement` subclasses. A special query subclass that is used very often in Rozyn apps is `Rozyn\Database\ModelQuery`, which can be used to easily generate proper SQL queries that take into account a specific model definition.

### Dependency Injection & Singletons
Each `Rozyn\Composition\...Factory` class can be used to generate instances of a particular class. The `Rozyn\Composition\DI` is a more generic dependency injection (hence the name) class that can generate instances for any other class, but it is mainly used to properly handle singleton classes.

As a design choice we prefer not to work with static classes as they are difficult to unit test, among other problems. However, some classes by their nature only require a single instance throughout the app. This is where the `Rozyn\Composition\DI` class comes in handy. Whenever a class is requested, this dependency injector checks whether or not it is a singleton (by inspecting the `$singleton` property of said class). If it is, it stores a reference to that instance so that the next time that class is requested, the same instance as before is returned. 

Rozyn is set up in such a way that it can recursively load dependencies for each requested class. As an example, take the `Rozyn\Routing\Router` class. Its constructor excepts, among other arguments, a `Rozyn\Session\SessionHandler` instance to provide CSRF protection. However, the session handler has its own dependencies, such as a `Rozyn\Encryption\Encrypter` instance. As long as no circular dependencies exist, our dependency injector internally finds all dependencies and the order in which to instantiate them so that it can successfully instantiate the top-level requested class.

