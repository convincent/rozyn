<?php $this->script('rozyn/class', 'rozyn/jquery', '*'); ?>
<?php $this->style('rozyn/reset',  '*');  ?>

<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->getStyleSheetTags(); ?>

		<title><?php echo $this->getTitle(); ?></title>
		
		<script type="text/javascript">
			var __ROOT__ = '<?php echo purify(__WWW_ROOT__, PURIFY_JS); ?>';
		</script>
	</head>

	<body>
		<div id="wrapper">
			<div id="content">
				<?php echo $this->getContent(); ?>
			</div>
		</div>

		<?php echo $this->getScriptTags(); ?>
	</body>
</html>