<?php

/**
 * @author		J.V. Mandersloot
 * @version 	0.0.1
 * @desc 		Loads all the files that have to be loaded manually, like
 *				plugins or third-party files that we decide to use in our app.
 */

use Rozyn\Facade\App;
use Rozyn\Facade\Lang;
use Rozyn\Composition\DI;



/*******************************************************************************
 * I. ALIASES
 * 
 * This section is used to specify aliases for our Dependency Injector (DI). It
 * will store these aliases so that it knows which class to instantiate when
 * a certain (possibly abstract) class is required. Concrete classes don't
 * necessarily have to have an alias, because they themselves can be
 * instantiated in the absence of any alises, but abstract classes or interfaces
 * that form dependencies for some other classes ARE required to have an alias 
 * associated with them so that our DI knows which class to inject whenever such
 * a dependency presents itself.
 * 
 * You can specify new aliases by calling the mapClass() method on the static
 * DI class. The first argument is the original class name that the DI will try
 * to load, the second argument is the class that should be substituted for it.
 * 
 * An optional third argument tells the DI whether or not the class should be 
 * treated as a singleton. The default is set to false, but an additional check
 * will always be performed anyway to see if the protected $singleton class
 * variable is set to true. The main advantage you can get by telling the DI
 * here that it's dealing with a singleton class is that it won't perform the
 * check above, since it has already established that it's a singleton class, so
 * there's no need to inspect the class any further.
 *******************************************************************************/



/*******************************************************************************
 * IV. LANGUAGES / TRANSLATIONS
 * 
 * Our app supports multiple languages natively. Below you can specify your own
 * custom translation files. The app comes with a folder called "lang" which
 * is used by default. All .php files in this folder will be treated as
 * translation files and should return an array with these translations. 
 * 
 * Use the string that identifies the text independent of locale as the key with
 * the corresponding value being the translation in a given locale. The locale
 * is determined through the file structure. For example, a translation file
 * called "translations.php" stored in the folder /lang/en/us/ will have its
 * locale set to en_US, etc.
 *******************************************************************************/
			



/*******************************************************************************
 * V. PLUGINS
 * 
 * Some plugins are only needed for certain actions, meaning you don't really
 * have to include them every single time a page on your site is requested. In
 * those cases, it's enough to load the plugin whenever that action is called.
 * 
 * However, some plugins are meant to be included on every single page, and in
 * those cases it's a good idea to load these plugins while we're booting up.
 * 
 * You can manually load a plugin by calling App::plug(<plugin-name>). The app
 * will then try to load that plugin's bootstrap file to determine what to do
 * with this plugin. By default, this bootstrap file is assumed to be called
 * bootstrap.php. In case one of your plugins has a bootstrap file with a 
 * different name, you can pass the name of the file as the second argument
 * to your App::plug() call.
 * 
 *******************************************************************************/

App::plug('Rozadmyn');
App::plug('Mediadmyn');