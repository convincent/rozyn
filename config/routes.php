<?php

use Rozyn\Routing\Router;

$router->group(['controller' => 'PageController'], function(Router $router) {
	$router->connect(array(
		'name'		=> 'home',
		'format'	=> '/',
		'method'	=> 'index',
	));
	
	$router->connect(array(
		'name'		=> 'test',
		'format'	=> '/test',
		'method'	=> 'test',
	));
	
	$router->post(array(
		'name'		=> 'post',
		'format'	=> '/post',
		'method'	=> 'post',
	));
	
	$router->connect(array(
		'name'		=> 'bad',
		'format'	=> '/bad',
		'method'	=> 'bad',
	));
	
	$router->get(array(
		'name'		=> 'xml',
		'format'	=> '/xml',
		'method'	=> 'xml',
		'type'		=> 'xml',
		'file'		=> 'rss',
	));
});