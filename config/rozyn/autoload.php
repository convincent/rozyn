<?php

/**
 * @author		J.V. Mandersloot
 * @version 	0.0.1
 * 
 * An autoloader for your classes makes your code much more readable since you
 * don't have to manually require() each file that contains a class anymore.
 * 
 * Below we define some (helper) functions that take care of class autoloading
 * for us.
 */

/**
 * Returns the include path to the file containing the specified (namespaced) 
 * class path. If the file could not be deduced from the $className, a boolean
 * value of FALSE is returned instead.
 * 
 * @param	string	$className
 * @return	string|false
 */
function get_class_path($className) {
	// Create a relative path for the supposed class file location that is to be
	// appended to a number of base paths that we're going to check.
	$fileRelPath = str_replace('\\', DS, $className) . '.php';
	
	// Check our default source file directory.
	if (file_exists($file = __SRC__ . DS . $fileRelPath)) {
		return $file;
	}
	
	// If the class file could not be found there, try the plugins directory.
	if (file_exists($file = __PLUGINS__ . DS . camel2snake(current(explode('\\', $className))) . DS . SRC_DIR . DS . $fileRelPath)) {
		return $file;
	}
	
	// If it wasn't there either, check our testing directory.
	if (file_exists($file = __TESTING__ . DS . $fileRelPath)) {
		return $file;
	}
	
	// If no file was found, then it's safe to conclude the file doesn't exist.
	// In that case, return false.
	return false;
}



/**
 * Loads the class with the specified namespaced class name.
 * 
 * @param	string	$className
 */
function include_class($className) {
	// Try to guess the location of the file containing the class definition.
	if (false !== ($path = get_class_path($className))) {
		// If a file is found, include it.
		require_once $path;
	}
}

// Register our custom function for our class autoloader.
spl_autoload_register('include_class', true, true);