<?php

/**
 * Copyright (C) 2016 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

// Define the different sort of environments that we accept.
$environments = array(
	'ENV_PRODUCTION'	=> 'production',
	'ENV_DEVELOPMENT'	=> 'development',
	'ENV_TESTING'		=> 'testing',
	'ENV_LOCAL'			=> 'local',
	'ENV_CLI'			=> 'cli',
);

// Make each environment a constant.
define('ENV_PRODUCTION', 'production');
define('ENV_DEVELOPMENT', 'development');
define('ENV_TESTING', 'testing');
define('ENV_LOCAL', 'local');
define('ENV_CLI', 'cli');

// Automatically detect our environment. First we check if a file has been 
// uploaded that forces our app into a certain environment.
foreach ($environments as $constant => $value) {
	if (is_file($file = dirname(dirname(dirname(__FILE__))) . DS . strtoupper(substr($constant, 4)))) {
		$environment = $value;
		break;
	}
}

// If no such file is found, we assume that we're on production so that if 
// anything goes wrong in our further automatic determination process, at least 
// the error reporting settings will be set to the bare minimum, meaning we
// don't risk accidentally spilling sensitive information to people who aren't 
// meant to see it.
if (!isset($environment)) {
	$environment = ENV_PRODUCTION;
	
	if (isset($_SERVER['HTTP_HOST']) && preg_match('/(^((https?:\/\/)?localhost)|\.dev$)/', $_SERVER['HTTP_HOST'])) {
		$environment = ENV_LOCAL;
	} elseif(isset($_SERVER['HTTP_HOST']) && preg_match('/^((https?:\/\/)?dev\.)/', $_SERVER['HTTP_HOST'])) {
		$environment = ENV_DEVELOPMENT;
	} elseif (defined('PHPUNIT_TEST_SUITE')) {
		$environment = ENV_TESTING;
	} elseif (defined('ROZYN_BIN_SCRIPT')) {
		$environment = ENV_CLI;
	}
}

define('ENV', $environment);