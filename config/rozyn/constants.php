<?php

// The name of the GET parameter in which the requested URL is stored.
define('REQUEST_PARAM', 'q');

// Event names
define('EVENT_QUERY_EXECUTED',		'EVENT_QUERY_EXECUTED');
define('EVENT_VIEW_CONSTRUCTED',	'EVENT_VIEW_CONSTRUCTED');
define('EVENT_LOGIN',				'EVENT_LOGIN');
define('EVENT_LOGOUT',				'EVENT_LOGOUT');

// The name of the Relation for a TranslatableModel that holds its translations.
define('SINGLE_TRANSLATION_RELATION', 'translation');

// The name of the Relation for a TranslatableModel that holds all its possible
// translations (for all languages).
define('MULTIPLE_TRANSLATION_RELATION', 'translations');

// Purify strategies
define('PURIFY_NONE',		0);
define('PURIFY_HTML',		1);
define('PURIFY_HTML_ATTR',	2);
define('PURIFY_URL',		4);
define('PURIFY_JS',			8);
define('PURIFY_ALL',		15);

// Commonly used DATETIME formats.
define('DATETIME_FORMAT_SQL', 'Y-m-d H:i:s');

// Order directions
define('ORDER_ASCENDING',	1);
define('ORDER_DESCENDING',	0);

// Debug levels
define('DEBUG_MODE_NONE',		0);
define('DEBUG_MODE_ERRORS',		1);
define('DEBUG_MODE_CONSOLE',	2);
define('DEBUG_MODE_LOG_FILE',	4);
define('DEBUG_MODE_ALL',		7);