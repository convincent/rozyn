<?php

use Rozyn\Facade\Validator;
use Rozyn\Model\Collection as ModelCollection;


/*******************************************************************************
 * I. RULES
 * 
 * Below we define several basic validation rules that can then be used to 
 * validate attributes of your models. You can always add your own custom rules
 * later on, but these rules below are so common that I judged it best to enable
 * them by default.
 * 
 *******************************************************************************/

Validator::addRules(array(
	'obligatory' => function($v = null) {
		if ($v instanceof ModelCollection) {
			return !$v->isEmpty();
		}

		if (is_string($v)) {
			return strlen($v) !== 0;
		}

		return $v !== null;
	},
		
	'min' => function($v, $min) {
		return is_int($v) && $v >= $min || is_string($v) && strlen($v) >= $min;
	},
		
	'max' => function($v, $max) {
		return is_int($v) && $v <= $max || is_string($v) && strlen($v) <= $max;
	}
));