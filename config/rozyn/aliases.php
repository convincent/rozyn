<?php

/**
 * Copyright (C) 2015 Convincent - All Rights Reserved
 * 
 * @author	Jeroen Mandersloot
 * 
 * You may not use, distribute or modify this code under
 * any circumstance without explicit written permission
 * from the original author.
 */

/*******************************************************************************
 * I. ALIASES
 * 
 * This section is used to specify aliases for our Dependency Injector (DI). It
 * will store these aliases so that it knows which class to instantiate when
 * a certain (possibly abstract) class is required. Concrete classes don't
 * necessarily have to have an alias, because they themselves can be
 * instantiated in the absence of any alises, but abstract classes or interfaces
 * that form dependencies for some other classes ARE required to have an alias 
 * associated with them so that our DI knows which class to inject whenever such
 * a dependency presents itself.
 * 
 * You can specify new aliases by calling the mapClass() method on the static
 * DI class. The first argument is the original class name that the DI will try
 * to load, the second argument is the class that should be substituted for it.
 * 
 * An optional third argument tells the DI whether or not the class should be 
 * treated as a singleton. The default is set to false, but an additional check
 * will always be performed anyway to see if the protected $singleton class
 * variable is set to true. The main advantage you can get by telling the DI
 * here that it's dealing with a singleton class is that it won't perform the
 * check above, since it has already established that it's a singleton class, so
 * there's no need to inspect the class any further.
 *******************************************************************************/

use Rozyn\Composition\DI;

DI::mapClass('Rozyn\Auth\Authenticable', 'Rozyn\Auth\AuthHandler');
DI::mapClass('Rozyn\Cache\Cache', (function_exists('apc_add')) ? 'Rozyn\Cache\ApcCache' : 'Rozyn\Cache\FileCache');

if (!check_php_version('7.1.0')) {
    DI::mapClass('Rozyn\Encryption\Encrypter', 'Rozyn\Encryption\DeprecatedEncrypter');
}