<?php



/*******************************************************************************
 * I. FOLDERS
 * 
 * First, we define a couple of commonly used folder names. These aren't full
 * paths to these folders, just the name of the individual folders, which we
 * will later use as building blocks for full paths to these folders or other
 * (sub)folders. 
 * 
 * Beware: Some of these folders are used in multiple places, so if you change
 * their name here, you'll have to change the name of all system folders that 
 * share its name through the application.
 * 
 *******************************************************************************/

define('PROJECT_DIR',	basename(dirname(dirname(dirname(__FILE__)))));

define('CORE_DIR'		,	'Rozyn');
define('CONTROLLERS_DIR',	'Controller');
define('MODELS_DIR'		,	'Model');
define('HELPERS_DIR'	,	'Helper');

define('LOGS_DIR'		,	'logs');
define('BIN_DIR'		,	'bin');
define('BOOTSTRAP_DIR'	,	'bootstrap');
define('COMPILED_DIR'	,	'compiled');
define('PUBLIC_DIR'		,	'public');
define('CSS_DIR'		,	'css');
define('IMG_DIR'		,	'img');
define('JS_DIR'			,	'js');
define('PLUGINS_DIR'	,	'plugins');
define('SRC_DIR'		,	'src');
define('VIEWS_DIR'		,	'views');
define('TEMPLATES_DIR'	,	'templates');
define('CACHE_DIR'		,	'cache');
define('CONFIG_DIR'		,	'config');
define('MIGRATIONS_DIR' ,	'migrations');
define('VENDOR_DIR'		,	'vendor');
define('TESTING_DIR'	,	'testing');
define('TESTS_DIR'		,	'tests');
define('INTEGRATION_DIR',	'integration');
define('LANG_DIR'		,	'lang');
define('CUSTOM_DIR'		,	'custom');
define('UPLOADS_DIR'	,	'uploads');
define('FINISHED_DIR'	,	'_finished');

/*******************************************************************************
 * II. ALIASES
 * 
 * We can use aliases for some of our public directories so we don't expose them
 * to our clients. All they will see is one of the aliases specified below, but
 * the true location of the file or folder will remain a mystery to them.
 * 
 * This can be useful because it keeps the average client completely in the dark
 * about the actual file system behind our application.
 * 
 *******************************************************************************/

// No aliases yet



/*******************************************************************************
 * III. DIRECTORY PATHS
 * 
 * Below we define a number paths to commonly used directories. You shouldn't 
 * need to change anything here, since everything should be determined correctly
 * automatically based on server variables and the folder names you defined
 * previously. 
 * 
 * However, if you'd like to move certain directories around, you might need to
 * change some things here to make sure those directories can still be found.
 * 
 * We prefix and suffix each directory constant with double underscores (__) so
 * that they are easily distinguished from other constants. PHP also uses this
 * naming convention for some of their magic constants, so beware of potential
 * clashes if you decide to add your own constants below.
 * 
 *******************************************************************************/

// Define some magic paths
define('__DOC_ROOT__'	,	(!empty($_SERVER['DOCUMENT_ROOT'])) ? $_SERVER['DOCUMENT_ROOT'] : dirname(dirname(dirname(dirname(__FILE__)))));
define('__HOST__'		,	(isset($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : 'localhost');
define('__ROOT__'		, 	dirname(dirname(dirname(__FILE__))));
define('__SITES__'		, 	dirname(__ROOT__));

// Core app directories
define('__SRC__'		,	__ROOT__ . DS . SRC_DIR);
define('__LOGS__'		,	__ROOT__ . DS . LOGS_DIR);
define('__TESTING__'	,	__ROOT__ . DS . TESTING_DIR);
define('__PUBLIC__'		,	__ROOT__ . DS . PUBLIC_DIR);
define('__MIGRATIONS__'	,	__ROOT__ . DS . MIGRATIONS_DIR);
define('__CONFIG__'		,	__ROOT__ . DS . CONFIG_DIR);
define('__BOOTSTRAP__'	,	__ROOT__ . DS . BOOTSTRAP_DIR);
define('__CACHE__'		,	__ROOT__ . DS . CACHE_DIR);
define('__COMPILED__'	,	__CACHE__ . DS . COMPILED_DIR);
define('__PLUGINS__'	,	__ROOT__ . DS . PLUGINS_DIR);
define('__LANG__'		,	__ROOT__ . DS . LANG_DIR);

// Source code directories
define('__CORE__'		,	__SRC__ . DS . CORE_DIR);
define('__CONTROLLERS__',	__SRC__ . DS . CONTROLLERS_DIR);
define('__MODELS__'		,	__SRC__ . DS . MODELS_DIR);
define('__HELPERS__'	,	__SRC__ . DS . HELPERS_DIR);

// Pseudo-public directories. These are directories that contain files that are
// in a way presented directly to the user, but contain PHP code and so might
// not be suitable to be marked public. Some of these used to be public, but
// are not anymore now. This is why there are some if statements here, because
// we still want to support older projects that still list these files as public.
define('__VIEWS__'		,	(file_exists(__ROOT__ . DS . VIEWS_DIR))		? __ROOT__ . DS . VIEWS_DIR		: __PUBLIC__ . DS . VIEWS_DIR);
define('__TEMPLATES__'	,	(file_exists(__ROOT__ . DS . TEMPLATES_DIR))	? __ROOT__ . DS . TEMPLATES_DIR	: __PUBLIC__ . DS . TEMPLATES_DIR);

// Public directories
define('__JS__'			,	__PUBLIC__ . DS . JS_DIR);
define('__IMG__'		,	__PUBLIC__ . DS . IMG_DIR);
define('__CSS__'		,	__PUBLIC__ . DS . CSS_DIR);

// Public cache directories.
define('__PUBLIC_CACHE__'	,	__PUBLIC__ . DS . CACHE_DIR);
define('__CACHE_CSS__'		,	__CACHE__ . DS . CSS_DIR);
define('__CACHE_JS__'		,	__CACHE__ . DS . JS_DIR);

// Bin directories
define('__BIN_TEMPLATES__', __ROOT__ . DS . BIN_DIR . DS . '_templates');

// Other directories
define('__VENDOR__'		,	__ROOT__ . DS . VENDOR_DIR);

// WWW folder paths
define('__WWW_ROOT__'		,	(!in_array(ENV, [ENV_TESTING, ENV_CLI])) ? str_replace('/index.php', '', getenv('SCRIPT_NAME')) : URI_SEPARATOR . basename(__ROOT__));
define('__WWW_PUBLIC__'		,	__WWW_ROOT__ . URI_SEPARATOR . PUBLIC_DIR);
define('__WWW_PLUGINS__'	,	__WWW_ROOT__ . URI_SEPARATOR . PLUGINS_DIR);
define('__WWW_CACHE__'		,	__WWW_PUBLIC__ . URI_SEPARATOR . CACHE_DIR);
define('__WWW_JS__'			,	__WWW_PUBLIC__ . URI_SEPARATOR . JS_DIR);
define('__WWW_IMG__'		,	__WWW_PUBLIC__ . URI_SEPARATOR . IMG_DIR);
define('__WWW_CSS__'		,	__WWW_PUBLIC__ . URI_SEPARATOR . CSS_DIR);
define('__WWW_CACHE_CSS__'	,	__WWW_CACHE__ . URI_SEPARATOR . CSS_DIR);
define('__WWW_CACHE_JS__'	,	__WWW_CACHE__ . URI_SEPARATOR . JS_DIR);



/*******************************************************************************
 * IV. FILE PATHS
 * 
 * There are a few files that are extremely important throughout our application
 * so we decided to define their locations as constants as well. This makes them
 * easy to find throughout our application.
 * 
 * We prefix and suffix each file path with a single underscore (_) so that they
 * are easily identifiable and distinguishable from other constants, for example
 * directory constants (which use double underscores).
 * 
 *******************************************************************************/

define('_ROUTES_'			,	__CONFIG__ . DS . 'routes.php');
define('_VENDOR_AUTOLOAD_'	,	__VENDOR__ . DS . 'autoload.php');



/*******************************************************************************
 * V. NAMESPACES
 * 
 * Even though namespaces aren't technically part of our file system, it's still
 * handy to define some commonly used namespace paths as well. 
 * 
 * IMPORTANT: Make sure to include a trailing backslash for each namespace, 
 * otherwise errors might occur.
 * 
 *******************************************************************************/

define('NS_CONTROLLERS'	,	'Controller\\');
define('NS_MODELS'		,	'Model\\');

define('NS_HELPERS'		,	'Rozyn\Helper\\');
define('NS_RELATIONS'	,	'Rozyn\Relation\\');