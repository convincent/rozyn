<?php

use Rozyn\Facade\Config;
use Rozyn\Facade\Session;



/*******************************************************************************
 * I. SETUP
 * 
 * Next up we do the initial setup for our application, the most basic things
 * really. We define some constants that are most important throughout our 
 * application, handle environmental logic, etc.
 * 
 * We provide automatic detection of environment based on some generic rules
 * we have defined ourselves, but if you need to override these rules because
 * your server(s)/development environments are configured differently, feel
 * free to do so. These generic rules are meant mostly as a means to deploy
 * our application with as little extra work as possible.
 * 
 *******************************************************************************/

// Register the time at which the app started running.
define('START_TIME', microtime(true));

// The directory separator used by the host.
define('DS', DIRECTORY_SEPARATOR);

// The directory separator, escaped in such a way that it can be used in a 
// pattern for any of the preg_* functions.
define('PREG_DS', preg_quote(DS, '/'));

// The URI separator (generally the same and system-independent)
define('URI_SEPARATOR', '/');

// The URI separator, escaped in such a way that it can be used in a 
// pattern for any of the preg_* functions.
define('PREG_URI_SEPARATOR', preg_quote(URI_SEPARATOR, '/'));

// Determine the environment we're on.
require __DIR__ . DS . 'env.php';

// Set our default encoding to UTF-8. You can always override this later on.
header('Content-type: text/html; charset=utf-8');


/*******************************************************************************
 * II. CONFIG
 * 
 * Load our various config files. Feel free to edit them to your liking. Most of
 * the current values are default values that work well enough with most 
 * configurations of any application, but if you need to tweak a few of them to
 * in order for your application to work properly, there should be little to no
 * risk in changing up some of these values.
 * 
 * Just make sure to always read the comments carefully so that you know what
 * kind of values you can specify without breaking the application.
 * 
 *******************************************************************************/

// We use a lot of different paths in our application, which we all define in a
// paths.php file. This way, you'll only ever have to edit the name of a folder 
// once in order for that change to take effect throughout the entire app.
require __DIR__ . DIRECTORY_SEPARATOR . 'paths.php';

// Load our other App constants.
require __DIR__ . DIRECTORY_SEPARATOR . 'constants.php';

// Load composer's autoload file if it exists.
if (file_exists(_VENDOR_AUTOLOAD_)) {
	require _VENDOR_AUTOLOAD_;
}

// Set up our class autoloader.
require __DIR__ . DS . 'autoload.php';

// Load initial default configuration for our app.
Config::import(__DIR__ . DS . 'config.php');

// Now that we've loaded our base config files, we can load some core functions 
// that we have defined for use within this application to make life easier.
require __DIR__ . DS . 'functions.php';

// Load our environment-dependent configuration files.
$envConfigDir = __CONFIG__ . DS . 'env' . DS . ENV;
if (is_dir($envConfigDir)) {
	if (false !== ($handle = opendir($envConfigDir))) {
		while (false !== ($file = readdir($handle))) {
			if (strtolower(substr($file, -4)) === '.php') {
				$config = require $envConfigDir . DS . $file;
				
				if (is_array($config)) {
					Config::write($config);
				}
			}
		}		

		// Clean up the memory. 
		unset($config);

		closedir($handle);
	}
}

// Load our custom configuration files.
if (file_exists(__CONFIG__ . DS . 'config.php')) {
	Config::import(__CONFIG__ . DS . 'config.php');
}

// Initialize our error reporting/debugging functionality.
require __DIR__ . DS . 'debug.php';

// Prepare all the default validation rules.
require __DIR__ . DS . 'validation.php';

// Set any system-defined class aliases.
require __DIR__ . DS . 'aliases.php';

// Set up our internationalization files.
require __DIR__ . DS . 'lang.php';

// Finally, load a custom setup file where you can handle any logic that wasn't
// handled automatically by this bootstrap file.
require __CONFIG__ . DS . 'bootstrap.php';

// Set up our global variables that we use throughout the app. This include
// should be last so that all other configuration options have already been
// loaded and can thus be assigned to global variables.
require __DIR__ . DS . 'globals.php';