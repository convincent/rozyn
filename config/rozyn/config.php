<?php

use Rozyn\Security\Sanitizer;

return array(
	/**
	 * The default charset for this site.
	 * 
	 * @var string
	 */
	'default_charset' => 'UTF-8',

	/**
	 * The default language for this site.
	 * 
	 * @var string
	 */
	'default_language' => 'nl',
	
	/**
	 * Specify whether or not to enable CSRF protection on each POST request.
	 * 
	 * @var boolean
	 */
	'csrf_protection' => true,
	
	/**
	 * Specify the session timeout in seconds.
	 * 
	 * @var int
	 */
	'session_timeout' => 15 * 60,
	
	/**
	 * Specify whether or not session data should be encrypted.
	 * 
	 * @var	boolean
	 */
	'encrypt_sessions' => false,
	
	/**
	 * Specify whether or not to allow IP addresses to change mid-session. Most
	 * of the time, this is a good indicator of whether or not a session has
	 * been hijacked, but there are cases where false negatives can be reported.
	 * For example, if the client is browsing your site via a proxy server or if
	 * their ISP for some reason changes their IP address from time to time.
	 * 
	 * Alternatively, you could choose to only enable this check for a select
	 * few auth groups. This way, regular users could browse your site through a
	 * proxy just fine, but users with administrative privileges would have to 
	 * make sure they're on a connection where their IP address does not change.
	 * 
	 * @var	boolean|array
	 */
	'force_static_ip' => false,
	
	/**
	 * Specifies whether or not to log sessions in our database.
	 * 
	 * @var	boolean
	 */
	'log_sessions' => true,
	
	/**
	 * Specify whether or not to force HTTPS for all pages.
	 * 
	 * @var boolean
	 */
	'force_ssl' => false,
	
	/**
	 * Specify any filters that are automatically applied to any user input.
	 * 
	 * @var	null|string|string[]
	 */
	'input_filters' => Sanitizer::FILTER_HTML,
	
	/**
	 * Specify whether or not database queries should be logged.
	 * 
	 * @var	boolean
	 */
	'log_queries' => false
);