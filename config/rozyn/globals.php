<?php

use Rozyn\Composition\DI;
use Rozyn\Facade\Lang;
use Rozyn\Facade\Config;

/*******************************************************************************
 * GLOBALS
 * 
 * Yes, I know, global variables are terrible. However, I believe if you use 
 * them responsibly and safely, they can serve a very real purpose. In this App,
 * I define a couple of global variables that are meant provide easier access to 
 * the variables stored in data-containing classes (like the Config class or the
 * I18nHandler class). To protect these variables, most of them are limited to
 * specific functions defined in our functions.php file. They should not be used
 * directly in other parts of the code. 
 * 
 *******************************************************************************/

/**
 * Holds all the configuration options for this site.
 * 
 * @var \Rozyn\Config\Config
 */
$GLOBALS['config'] = DI::getInstanceOf('Rozyn\Config\Config');

/**
 * Holds all the translations for this site.
 * 
 * @var string[]
 */
$GLOBALS['translations'] = Lang::getTranslations();

/**
 * Holds the default language for this site.
 * 
 * @var string
 */
$GLOBALS['default_language'] = Config::read('default_language', 'nl');

/**
 * Holds the current site language.
 * 
 * @var string
 */
$GLOBALS['language'] = $GLOBALS['default_language'];

/**
 * Holds a reference to the Router for this site.
 * 
 * @var \Rozyn\Routing\Router
 */
$GLOBALS['router'] = DI::getInstanceOf('Rozyn\Routing\Router');

/**
 * Holds a reference to the Sanitizer singleton used to sanitize user input.
 * 
 * @var	\Rozyn\Security\Sanitizer
 */
$GLOBALS['sanitizer'] = DI::getInstanceOf('Rozyn\Security\Sanitizer');