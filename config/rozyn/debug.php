<?php

use Rozyn\Facade\Shutdown;
use Rozyn\Error\PHPRuntimeErrorException;

// If automatically setting our debug mode based on the environment's config
// files failed for whatever reason, try another way of automatically setting
// the debug mode, using a switch statement. This ensures that a debug mode
// will always be set, because even if no enviroment is specified, the default
// case in the switch statement will still be executed.
if (!defined('DEBUG_MODE')) {
	$env = (defined('ENV')) ? ENV : null;
	
	switch ($env) {
		case ENV_DEVELOPMENT:
		case ENV_TESTING:
		case ENV_LOCAL:
			$debug_mode = DEBUG_MODE_ALL;
			break;

		default:
			$debug_mode = DEBUG_MODE_NONE;
			break;
	}
	
	define('DEBUG_MODE', $debug_mode);
}

/**
 * Whenever a fatal error is encountered that needs to be neatly conveyed to our
 * end user, we call our custom shutdown_handler method. This method takes an
 * Exception as an optional argument (if omitted, the last error thrown by PHP
 * is converted to an Exception to be used in its place instead) that is passed
 * to a custom route responsible for displayed proper error messages.
 * 
 * This method also logs the Exception to our error log so that even if no
 * precise error message is shown to the end user, we can always go back and 
 * see what exactly went wrong.
 * 
 * @param	\Exception	$exception
 */
function shutdown_handler(Exception $exception = null) {
	try {
		Shutdown::setException($exception);
		$response = Shutdown::handle();
		$response->write();
	} catch (Exception $exception) {
		if (DEBUG_MODE & DEBUG_MODE_ERRORS) {
			http_response_code($exception->getCode());
			die(nl2br($exception->__toString()));
		} else {
			http_response_code(500);
			die("An unknown error occured. Please contact the administrator to resolve this issue.");
		}
	}
}

/**
 * A function that converts a PHP error into a PHPRuntimeErrorException.
 * 
 * @param	int		$errno
 * @param	string	$errstr
 * @param	string	$errfile
 * @param	int		$errline
 * @throws	\Rozyn\Error\PHPRuntimeErrorException
 */
function error_handler($errno, $errstr, $errfile, $errline) {
	$e = new PHPRuntimeErrorException($errstr . ' (' . $errfile . ' on line ' . $errline . ')', $errno);
	
	throw $e;
}

// We set our error handler to the above function so that every error that we
// encounter will be automatically converted to our desired custom Exception
// class.
set_error_handler('error_handler');

// Now that we know for a fact that the debug mode is set, we specify our error
// handlers based on the value of the debug mode.
if (DEBUG_MODE & DEBUG_MODE_ERRORS) {
	// If necessary, turn on error reporting. We want to do this as early as 
	// possible, since it reduces the risk of errors slipping through unnoticed 
	// in the remainder of our code
	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(-1);
} 

else {	
	// Turn off error reporting so that we don't accidentally leak information
	// to the users.
	ini_set('display_startup_errors', 0);
	ini_set('display_errors', 0);
	error_reporting(0);
	
	// Register a shutdown handler which handles unexpected fatal errors we can't
	// catch through our Exception handler.
	register_shutdown_function('shutdown_handler');
}