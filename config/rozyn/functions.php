<?php

use Rozyn\Security\Sanitizer;
use Rozyn\Routing\Route;
use Rozyn\Facade\Config;
use Rozyn\Facade\Lang;
use Rozyn\Facade\App;
use Rozyn\I18n\Locale;
use Rozyn\Composition\DI;


/*******************************************************************************
 * I. STRINGS
 * 
 * Even though PHP already has a lot of string manipulation functions, there are 
 * still a couple of simple functions that are missing. We've included a couple 
 * of functions that we want to use multiple times throughout the application
 * below.
 * 
 *******************************************************************************/

/**
 * Returns whether or not $haystack begins with $needle. If $needle is an
 * array, $haystack is checked against each element in that array. If $haystack
 * starts with at least one of the elements in $needle, a boolean value of TRUE
 * is returned. Otherwise, FALSE is returned.
 * 
 * @param 	string			$haystack
 * @param 	array|string	$needle
 * @return 	boolean
 */
function starts_with($haystack, $needle) {
	foreach ((is_string($needle)) ? [$needle] : $needle as $v) {
		if ($v === '' || strpos($haystack, $v) === 0) {
			return true;
		}
	}

	return false;
}

/**
 * Returns whether or not $haystack ends with $needle. If $needle is an array, 
 * $haystack is checked against each element in that array. If $haystack ends
 * with at least one of the elements in $needle, a boolean value of TRUE is 
 * returned. Otherwise, FALSE is returned.
 * 
 * @param 	string			$haystack
 * @param 	array|string	$needle
 * @return 	boolean
 */
function ends_with($haystack, $needle) {
	foreach ((is_string($needle)) ? [$needle] : $needle as $v) {
		if (substr($haystack, -strlen($v)) === $v) {
			return true;
		}
	}

	return false;
}

/**
 * Returns the original string, prefixed with $prefix. If the original string
 * already started with $prefix, the original string is returned unchanged.
 * 
 * @param 	string	$str
 * @param 	string	$prefix
 * @return 	string
 */
function prefix($str, $prefix) {
	return (starts_with($str, $prefix)) ? $str : $prefix . $str;
}

/**
 * Returns the original string, suffixed with $suffix. If the original string
 * already ended with $suffix, the original string is returned unchanged.
 * 
 * @param 	string	$str
 * @param 	string	$suffix
 * @return 	string
 */
function suffix($str, $suffix) {
	return (ends_with($str, $suffix)) ? $str : $str . $suffix;
}

/**
 * Converts a camel cased string to a snake cased string. You can provide an
 * optional second parameter which will used as the snake character, ie: the 
 * character that separates two words from each other. By default, an
 * underscore (_) is used as snake character.
 * 
 * @param 	string	$str
 * @param 	string	$snake
 * @return 	string	type
 */
function camel2snake($str, $snake = '_') {
	return ltrim(strtolower(preg_replace('/[A-Z]/', $snake . '$0', $str)), $snake);
}

/**
 * Converts a snake cased string to a camel cased string. You can provide an
 * optional second parameter which will used as the snake character, ie: the 
 * character that separates two words from each other. By default, an
 * underscore (_) is used as snake character.
 * 
 * @param 	string	$str
 * @param 	string	$snake
 * @return 	string	type
 */
function snake2camel($str, $snake = '_') {
	return str_replace(' ', '', ucwords(str_replace($snake, ' ', $str)));
}

/**
 * Takes a parameterized string as its first argument with the parameters
 * denoted by accolades ({}) or a colon (:). 
 * The second argument of this function is an array where each key should 
 * correspond with a parameter name (without the accolades) where that key's
 * value should be the string with which the parameter should be replaced.
 * 
 * So for example: str_format("{0} :name", [0 => 'Hello', 'name' => 'world']) 
 * would return "Hello world".
 * 
 * @param 	string	$str
 * @param 	array	$vars
 * @return 	string
 */
function str_format($str, array $vars) {
	$replace = [];
	foreach ($vars as $key => $value) {
		$replace["{{$key}}"] = $value;
	}
	
	return strtr($str, $replace);
}

/**
 * Replaces the first instance of each element in $search if $search is an 
 * array, or just the first instance of $search if it is a string, with each
 * corresponding element in the $replace array, or $replace if it is a string in
 * $subject and returns the resulting string.
 * 
 * @param 	string|array	$search
 * @param 	string|array	$replace
 * @param 	string			$subject
 * @return 	string
 */
function str_replace_once($search, $replace, $subject) {
	if (!is_array($search)) {
		$search = [$search];
	}

	if (!is_array($replace)) {
		$replace = [$replace];
	}

	$replacementCount = count($replace);
	foreach ($search as $i => $s) {
		$subject = preg_replace('/' . preg_quote($s, '/') . '/', $replace[min($i, $replacementCount - 1)], $subject, 1);
	}

	return $subject;
}

/**
 * Applies base64 encoding on a string but removes the +, / and = symbols so 
 * that the resulting string is fully alphanumeric. Note that because of this, 
 * you cannot decode the resulting string and expect to get the original string 
 * back.
 * 
 * @param	string	$string
 * @param	array	$remove
 * @return	string
 */
function base64_alphanumeric($string, $remove = ['+', '=', '/']) {
	return str_replace($remove, '', base64_encode($string));
}

/**
 * A very simple function to generate a random string, possibly based on a salt.
 * 
 * @param 	mixed	$salt
 * @param 	int		$len
 * @param 	string	$prefix
 * @return 	string
 */
function str_random($salt = '', $len = 32, $prefix = '') {
	return $prefix . substr(md5(uniqid() . json_encode($salt)), 0, $len);
}

/**
 * Converts <br /> tags in a string to newlines. Basically the reverse function
 * of nl2br.
 * 
 * @param 	string
 * @return 	string
 */
function br2nl($str) {
	return preg_replace('/<br\s*(\/)?>/', "\r\n", $str);
}

/**
 * Slugifies the given string so that it can safely be used in URLs.
 * 
 * See also: http://stackoverflow.com/questions/14114411/remove-all-special-characters-from-a-string
 * 
 * @param 	string	$str
 * @param 	string	$slug
 * @return 	string
 */
function slugify($str, $slug = '-') {
	$utf8 = array(
		'/[áàâãªä]/u' => 'a',
		'/[ÁÀÂÃÄ]/u' => 'A',
		'/[ÍÌÎÏ]/u' => 'I',
		'/[íìîï]/u' => 'i',
		'/[éèêë]/u' => 'e',
		'/[ÉÈÊË]/u' => 'E',
		'/[óòôõºö]/u' => 'o',
		'/[ÓÒÔÕÖ]/u' => 'O',
		'/[úùûü]/u' => 'u',
		'/[ÚÙÛÜ]/u' => 'U',
		'/ç/' => 'c',
		'/Ç/' => 'C',
		'/ñ/' => 'n',
		'/Ñ/' => 'N',
		'/–/' => '-', // UTF-8 hyphen to "normal" hyphen
		'/[’‘‹›‚]/u' => ' ', // Literally a single quote
		'/[“”«»„]/u' => ' ', // Double quote
		'/ /' => ' ', // nonbreaking space (equiv. to 0x160)
	);

	return preg_replace('/[^a-z0-9' . $slug . '_-]/', '', str_replace(' ', $slug, strtolower(
				preg_replace(array_keys($utf8), array_values($utf8), $str))));
}

/**
 * Ellipsifies the given string so that it's only $len characters long. All
 * further characters are truncated and replaced by an ellipsis (...).
 * 
 * @param 	string	$str
 * @param 	int		$len
 * @return 	string
 */
function ellipsify($str, $len = null) {
	if ($len === null || strlen($str) <= $len) {
		return $str;
	}

	return trim(substr($str, 0, $len)) . '...';
}

/**
 * Tries to intelligently guess the plural of a given word (in English).
 * 
 * @param 	string	$word
 * @return 	string
 */
function pluralize($word) {
	if (ends_with($word, ['s', 'x'])) {
		return $word . 'es';
	}

	if (ends_with($word, 'y')) {
		return preg_replace('/y$/', 'ies', $word);
	}

	return $word . 's';
}

/**
 * Tries to intelligently guess the singular of a given word (in English).
 * 
 * @param 	string	$word
 * @return 	string
 */
function singularize($word) {
	if (ends_with($word, 'ies')) {
		return preg_replace('/ies$/', 'y', $word);
	}

	return preg_replace('/e?s$/', '', $word);
}

/**
 * Checks if $data represents a serialized string.
 * 
 * https://core.trac.wordpress.org/browser/tags/4.4.2/src/wp-includes/functions.php#L0
 * 
 * @param 	mixed	$data
 * @param 	boolean	$strict
 * @return 	boolean
 */
function is_serialized($data, $strict = true) {
	if (!is_string($data)) {
		return false;
	}
	
	$data = trim($data);
	
	if ('N;' == $data) {
		return true;
	}
	
	if (strlen($data) < 4) {
		return false;
	}
	
	if (':' !== $data[1]) {
		return false;
	}
	
	if ($strict) {
		$lastc = substr($data, -1);
		if (';' !== $lastc && '}' !== $lastc) {
			return false;
		}
	} else {
		$semicolon = strpos($data, ';');
		$brace = strpos($data, '}');
		// Either ; or } must exist.
		if (false === $semicolon && false === $brace)
			return false;
		
		// But neither must be in the first X characters.
		if (false !== $semicolon && $semicolon < 3)
			return false;
		
		if (false !== $brace && $brace < 4)
			return false;
	}
	
	$token = $data[0];
	switch ($token) {
		case 's' :
			if ($strict) {
				if ('"' !== substr($data, -2, 1)) {
					return false;
				}
			} 
			
			elseif (false === strpos($data, '"')) {
				return false;
			}
			
		// or else fall through
		case 'a' :
		case 'O' :
			return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
			
		case 'b' :
		case 'i' :
		case 'd' :
			$end = $strict ? '$' : '';
			return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
	}
	
	return false;
}

if (!function_exists('random_bytes')) {
	/**
	 * Generates a string with random bytes.
	 * 
	 * @param	int		$length
	 * @param	string	
	 */
	function random_bytes($length) {
		return openssl_random_pseudo_bytes($length);
	}
}

/*******************************************************************************
 * II. ARRAYS
 * 
 * All of our custom array-related functions are listed below. Nothing special,
 * because PHP already has so many array-related functions implemented natively,
 * but nonetheless there are still a few things we could implement.
 * 
 *******************************************************************************/

/**
 * Returns the first element of the passed array. If a callback is specified,
 * that callback is used as a filter function that should return true or false. 
 * In that case, the first element that passes the filter function is returned. 
 * If not a single element matches the filter function, null is returned.
 * 
 * @param 	array		$array
 * @param 	callable	$cb
 * @return 	mixed
 */
function array_first(array $array, callable $cb = null) {
	foreach ($array as $key => $value) {
		if (null === $cb || $cb($key, $value)) {
			return $value;
		}
	}

	return null;
}

/**
 * Flattens a multidimensional array using a specified character to separate
 * different levels. By default a dot (.) is used.
 * 
 * @param 	mixed	$array
 * @param 	char	$char
 * @param 	string	$base
 * @return 	array
 */
function array_flatten($array, $char = '.', $base = '') {
	$res = [];

	if (is_array($array)) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$res = array_merge($res, array_flatten($v, $char, $base . $k . $char));
			} else {
				$res[$base . $k] = $v;
			}
		}
	}

	return $res;
}

/**
 * Inflates a 1-dimensional array and turns it into a multidimensional array
 * using the specified character as a separator between levels.
 * 
 * @param 	mixed	$array
 * @param 	char	$char
 * @param 	string	$base
 * @return 	array
 */
function array_inflate($array, $char = '.', $base = '') {
	$res = [];

	foreach ($array as $k => $v) {
		$parts = explode($char, $k);
		$leaf = array_pop($parts);
		$parent = &$res;

		foreach ($parts as $part) {
			if (!isset($parent[$part]) || !is_array($parent[$part])) {
				$parent[$part] = [];
			}

			$parent = &$parent[$part];
		}


		$parent[$leaf] = $v;
	}

	return $res;
}

/**
 * Implodes both the keys and values of an array.
 * 
 * @param 	string	$glue
 * @param 	array	$array
 * @param 	string	$keyGlue
 * @return 	string
 */
function implode_all($glue, array $array, $keyGlue = ':') {
	$res = [];
	foreach ($array as $key => $value) {
		$res[] = $key . $keyGlue . $value;
	}

	return implode($glue, $array);
}

/**
 * Returns true if the array is associative.
 * 
 * @param 	array	$array
 * @return 	boolean
 */
function array_is_assoc(array $array) {
	return (bool) count(array_filter(array_keys($array), 'is_string'));
}

/**
 * Converts an array to a valid Xml document. This codeis heavily inspired by 
 * the answer at the following pages:
 * 
 * http://stackoverflow.com/questions/1397036/how-to-convert-array-to-simplexml
 * http://stackoverflow.com/revisions/5965940/2
 * 
 * @param 	array				$data
 * @param 	SimpleXMLElement	$xml
 * @return 	SimpleXMLElement
 */
function array_to_xml(array $data, SimpleXMLElement &$xml = null) {
	if ($xml === null) {
		$xml = new SimpleXMLElement('<?xml version="1.0"?><root></root>');
	}

	foreach ($data as $key => $value) {
		if (is_array($value)) {
			if (is_numeric($key)) {
				array_to_xml($value, $xml);
			} else {
				$subnode = $xml->addChild($key);
				array_to_xml($value, $subnode);
			}
		} else {
			if (is_numeric($key)) {
				$key = 'item' . $key;
			}

			$xml->addChild("{$key}", htmlspecialchars("{$value}"));
		}
	}

	return $xml;
}



/*******************************************************************************
 * III. CLASSES / OBJECTS
 * 
 * Below are helper functions for classes and/or objects in our application. We
 * generally don't want every single class to extend a custom class to provide
 * each class with some basic functionality, because that unnecessarily clutters
 * the code of our classes. 
 * 
 * Instead, it's easier to write some procedural functions that can interact
 * with classes in some way. That way, you can write these functions without
 * having to alter class code or having to extend some sort of master parent
 * class.
 * 
 *******************************************************************************/

/**
 * Returns the basename of the class, without its namespace.
 *
 * @param 	Object|string	$object
 * @return 	string
 */
function get_class_name($object) {
	return basename(str_replace('\\', '/', (is_string($object)) ?
				$object :
				get_class($object)));
}

/**
 * Returns the value of a single Doc Comment annotation for a class, a class 
 * method or a class property.
 * 
 * @param 	Reflector	$reflector
 * @param 	string		$tag
 * @return 	string|string[]
 */
function get_annotation(Reflector $reflector, $tag) {
	$annotations = get_annotations($reflector, $tag);
	
	if (empty($annotations)) {
		return false;
	}
	
	$result = array_pop($annotations);
	
	if (trim($result) === '') {
		return true;
	}
	
	return $result;
}

/**
 * Returns the annotation values for the given tag as an array.
 * 
 * @param 	Reflector	$reflector
 * @param 	string		$tag
 */
function get_annotations(Reflector $reflector, $tag) {
	// The array which will hold any matches from the regex that is used to 
	// find the annotation in the method's Doc Comment.
	$matches = [];

	// Make sure the reflector supports the method we need to inspect the Doc 
	// Comment.
	if (!method_exists($reflector, 'getDocComment')) {
		return false;
	}
	
	$pattern = "/@" . preg_quote($tag, '/') . "(?:(?:\t|\s)+([^" . PHP_EOL . "]+))?" . PHP_EOL . "/";
	
	// Look for the annotation and, if present, its associated value.
	preg_match_all(	$pattern, 
					$reflector->getDocComment(), 
					$matches);
	
	return $matches[1];
}



/*******************************************************************************
 * IV. DIRECTORIES / FILES 
 * 
 * Below are some helper functions that should make it easier to interact with
 * the file system of your application. Functions to easily traverse directories,
 * parse files, etc. 
 * 
 *******************************************************************************/

/**
 * Returns the memory required to open a specified image with PHP (in bytes).
 * 
 * @param 	string	$path
 * @param 	float	$tweak
 * @return 	int
 */
function getrawimagesize($path, $tweak = 1.9) {
	$info = getimagesize($path);

	$memory = $tweak *
		$info[0] *
		$info[1] *
		$info['channels'] *
		$info['bits'] / 8;

	return intval($memory);
}

/**
 * Helper function to create transparent images.
 * 
 * @param 	int		$width
 * @param 	int		$height
 * @return 	resource
 */
function imagecreatetruecolortransparent($width, $height) {
	$image = imagecreatetruecolor($width, $height);
	imagealphablending($image, false);
	imagesavealpha($image, true);
	$transparent = imagecolorallocatealpha($image, 255, 255, 255, 127);
	imagefilledrectangle($image, 0, 0, $width, $height, $transparent);

	return $image;
}

/**
 * Executes the passed callback on each filename in the given directory. If 
 * $recurse is set to true, this method will be called recursively on each
 * subdirectory.
 * 
 * If no callback is specified, a simple array containing all the filenames in 
 * the directory. Both "." and ".." will be skipped entirely by this function.
 * 
 * The callback, if provided, can take up to three arguments. The first argument
 * is the filename of the file for the current iteration. The second argument
 * is the directory's name. The third argument is the full path to the file.
 * 
 * @param 	string		$dir
 * @param 	callable	$cb
 * @param 	boolean		$recurse
 * @return 	array
 */
function dir_map($dir, $cb = null, $recurse = false) {
	$res = [];

	if (false !== ($handle = opendir($dir))) {
		while (false !== ($file = readdir($handle))) {
			if ($file !== '.' && $file !== '..') {
				$path = $dir . DS . $file;

				if (is_file($path) || !$recurse && is_dir($path)) {
					$res[$file] = ($cb === null) ? $file : $cb($file, $dir, $path);
				} elseif ($recurse && is_dir($path)) {
					$res[$file] = dir_map($path, $cb, true);
				}
			}
		}

		closedir($handle);
	}

	return $res;
}

/**
 * Deletes a directory and all files in it.
 * 
 * @param 	string	$dir
 */
function unlink_dir($dir) {
	dir_map($dir, function($file, $dir, $path) {
		if (is_file($path)) {
			unlink($path);
		}
		
		elseif (is_dir($path)) {
			unlink_dir($path);
		}
	});
	
	rmdir($dir);
}

/**
 * Makes sure that a particular directory exists. If it doesn't exist, it will
 * create it (and any parent directories that don't exist yet either).
 * 
 * @param 	string	$path
 */
function mkpath($path, $mode = 0777, $context = null) {
	if (!is_dir($path)) {
		mkdir($path, $mode, true, $context);
	}
	
	return true;
}

/**
 * Get the specified directory's modification time.
 * 
 * @param 	string	$path
 * @return 	int
 */
function dirmtime($path) {
	if (is_dir($path)) {
		$res = 0;
		dir_map($path, function($file, $dir, $path) use (&$res) {
			if (is_file($path)) {
				$res = max($res, filemtime($path));
			}
		}, true);

		return $res;
	}
	
	throw new Rozyn\Filesystem\DirectoryNotFoundException($path);
}

/**
 * Parses the given file through PHP's parser by including the file. An optional
 * array containing variables that should be loaded in this function's scope can
 * be passed as a second argument. This way, you can pass certain variables to 
 * the PHP code that will be executed in the file.
 * 
 * @param 	string	$path
 * @param 	array	$vars
 * @return 	string
 */
function pf($path, array $vars = []) {
	if (file_exists($path)) {
		ob_start();
		extract($vars);
		require $path;
		return ob_get_clean();
	}

	return null;
}

/**
 * An alias for the pf() function.
 * 
 * @param 	string	$path
 * @param 	array	$vars
 * @return 	string
 */
function parse_file($path, array $vars = []) {
	return pf($path);
}

/**
 * Returns whether or not a view file exists.
 * 
 * @param 	string	$file
 * @return 	boolean
 */
function view_exists($file) {
	$view = DI::getProxyInstanceOf('Rozyn\Response\View');

	$view->setFile($file);

	return $view->getFile()->exists();
}



/*******************************************************************************
 * V. ROUTING / REQUESTS
 * 
 * These functions all help dealing with routing and/or requests more easily. 
 * They provide an easy way of generating certain URLs, read/convert aliased
 * URLs, etc.
 * 
 *******************************************************************************/

/**
 * Takes a filename or a path as its first argument and returns an absolute URL
 * to that asset.
 * 
 * @param 	string	$file
 * @return 	string
 */
function asset($file) {
	if (!starts_with($file, 'http')) {
		$file = (starts_with($file, URI_SEPARATOR)) ? 
					__WWW_ROOT__ . $file :
					__WWW_PUBLIC__ . URI_SEPARATOR . $file;
	}

	return strtr($file, ["'" => '%27', ' ' => '%20', '(' => '%28', ')' => '%29']);
}

/**
 * Simplified version of asset() for plugin assets. You can leave out the plugin
 * name in which case this function will try to deduce from which plugin this 
 * function was called. If no plugin is found, a PluginException is thrown. 
 * 
 * @param 	string	$file
 * @param 	string	$plugin
 * @return 	string
 * @throws	\Rozyn\Plugin\PluginException
 */
function passet($file, $plugin, $ext = null) {
	// Call our base asset function with the newly constructed paths.
	// The null element in the array forces the path to start with a /, but
	// writing it this way seemed more elegant than concatenating strings
	// outside of the implode call.
	return asset(URI_SEPARATOR . PLUGINS_DIR . URI_SEPARATOR . $plugin . URI_SEPARATOR . PUBLIC_DIR . URI_SEPARATOR . $file, $ext);
}

/**
 * Returns an absolute URL for the given named route if it's stored in our 
 * Router. An array containing parameters that should be passed on to the 
 * Route can be passed as an optional second argument.
 * 
 * @param 	string	$name
 * @param 	array	$params
 * @return 	string
 */
function route($name, array $params = []) {
	// Retrieve our Router object from our global variables.
	$router = globals('router');
	
	// If for some reason the global router could not be found, use our DI to
	// retrieve our Router instance.
	if ($router === null) {
		$router = DI::getInstanceOf('Rozyn\Routing\Router');
	}
	
	// Initialize our $route variable, which will hold the Route object matching
	// the requested name.
	$route = null;
	
	// Check if the current language of the site matches the default language.
	// If not, we prepend this language to the route's name and try to retrieve
	// the route matching that name instead. If it can't be found, we default
	// back to the route name that was actually requested.
	$language = site_language();
	if (globals('default_language') !== $language) {
		$route = $router->getRoute($language . '_' . $name);
	}

	// If no route was found up until this point, fall back on the default
	// behaviour of requesting the route matching the exact name as it was
	// requested.
	if ($route === null) {
		$route = $router->getRoute($name);
	}
	
	// Return the URL matching the found route.
	if ($route instanceof Route) {
		return $route->url($params);
	}
	
	// If the name is a string that is already a valid URL, simply return it.
	if (is_string($name) && strpos($name, URI_SEPARATOR) !== false) {
		return $name;
	}
	
	// In other cases, simply return an empty string.
	return '';
}

/**
 * Returns the Route object that was matched to the current Request.
 * 
 * @return 	\Rozyn\Routing\Route
 */
function get_current_route() {
	return App::getRoute();
}

/**
 * Merges two parts of a path together, getting rid of any potential overlap in
 * the middle part of the resulting path.
 * 
 * @param 	string	$head
 * @param 	string	$tail
 * @param 	string	$ds
 * @return 	string
 */
function merge_paths($head, $tail, $ds = DS) {
	$leadingDs  = (starts_with($head, $ds)) ? $ds : '';
	$trailingDs = (ends_with($tail, $ds)) ? $ds : '';
					
	$hcomps = explode($ds, trim($head, $ds));
	$tcomps = explode($ds, trim($tail, $ds));
	$hcount = count($hcomps);
	
	$res = null;
	foreach ($hcomps as $i => $hcomp) {
		if ($tcomps[0] === $hcomp && array_slice($hcomps, $i) === array_slice($tcomps, 0, $hcount - $i)) {
			$res = array_merge(array_slice($hcomps, 0, $i), $tcomps);
		}
	}
	
	if ($res === null) {
		$res = array_merge($hcomps, $tcomps);
	}
	
	return $leadingDs . implode($ds, $res) . $trailingDs;
}

/**
 * Returns whether or not the given path is absolute.
 * 
 * @param   string  $path
 * @return  boolean
 */
function is_absolute_path($path) {
    return is_os_windows() && preg_match('/^[a-zA-Z]:($|' . PREG_DS . ')/', $path) || starts_with($path, DS);
}



/* * *****************************************************************************
 * VI. DEBUG / LOGGING
 * 
 * In development environments it can be highly useful to have a bunch of 
 * functions available to you to help you debug certain parts of the application
 * or to provide additional information about certain pieces of code (the total 
 * execution time of your application for example). That's what the functions
 * below are for!
 * 
 * ***************************************************************************** */

/**
 * A global variable that holds a float number representing the server time at 
 * an arbitrary point during code execution. This global variable can then later
 * be used to determine how much time has passed since it was set, thus providing
 * useful information about the execution time of a certain piece of code.
 * 
 * @var 	float
 */
$b_start_time;

/**
 * Another global variable that holds the memory usage at an arbitrary point
 * during code execution. This variable is used to later calculate how much
 * additional memory has been used up after a certain point in the code.
 * 
 * @var 	int
 */
$b_start_mem;

/**
 * Executes the passed callback function $n times and prints the amount of time
 * it took to execute it that many times. The default value for $n is 100, but 
 * feel free to increase or decrease the number at will. Just beware of heavy
 * functions that take quite a while.
 * 
 * An optional third argument can be passed. This argument specifies whether or
 * not to exit the entire script after execution has completed.
 * 
 * @param 	callable	$cb
 * @param 	int			$n
 * @param 	boolean		$exit
 */
function bench(callable $cb, $n = 100, $exit = true) {
	b_start();

	for ($i = 0; $i < $n; $i++) {
		$cb();
	}

	b_end($exit);
}

/**
 * Starts a benchmark, ie: sets the value of our global $b_start_time variable
 * to the current server time in microseconds.
 * 
 * @global float $b_start_time
 */
function b_start() {
	global $b_start_time;
	global $b_start_mem;
	global $b_start_included_files;

	$b_start_included_files = get_included_files();
	$b_start_time = microtime(true);
	$b_start_mem = memory_get_usage();
}

/**
 * Ends a benchmark by printing the total execution time since b_start() was last
 * called. An optional boolean argument can be provided that, when set to true,
 * will exit the entire script after printing out the benchmark results.
 * 
 * @global	float	$b_start_time
 * @param 	boolean	$exit
 */
function b_end($exit = true) {
	b_coach();
	
	global $b_start_included_files;
	
	$output = '';
	$new_files = array_diff(get_included_files(), $b_start_included_files);
	if (!empty($new_files)) {
		$output .= "Included files: " . PHP_EOL . "- " . implode(PHP_EOL . "- ", $new_files);
	}

	echo "<pre>{$output}</pre>";
	
	if ($exit) {
		exit;
	}
}

/**
 * While benchmarking some function, this function can be used to print the
 * amount of time that has passed since the benchmark was started. An optional
 * argument can be passed which, if set, will be printed along with the time
 * that has passed so far.
 * 
 * Note: For security purposes, this function is only enabled for development 
 * environments.
 * 
 * @global	float	$b_start_time
 * @param 	string	$marker
 */
function b_coach($marker = null) {
	if (ENV !== ENV_DEVELOPMENT && ENV !== ENV_LOCAL) {
		return;
	}

	global $b_start_time;
	global $b_start_mem;

	$time = microtime(true) - $b_start_time . ' seconds';

	$mem = memory_get_usage() - $b_start_mem - PHP_INT_SIZE * 8;

	if ($mem < 1024) {
		$mem .= " bytes";
	} elseif ($mem < 1048576) {
		$mem = round($mem / 1024, 3) . " kB";
	} else {
		$mem = round($mem / 1048576, 3) . " MB";
	}

	$output = '';
	
	$output .= ($marker ? $marker . ':' : '') . PHP_EOL;
	$output .= "Time: \t" . $time . PHP_EOL;
	$output .= "Mem: \t" . $mem . PHP_EOL;

	echo "<pre>{$output}</pre>";
}

/**
 * A shorthand alias for var_dump(), only this function also wraps the output of 
 * var_dump() in <pre> tags so that the result is easily readable on HTML
 * pages without having to load the source code each time first.
 * 
 * @param 	mixed	$var
 */
function vd($var) {
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}

/**
 * A shorthand alias for print_r(),  only this function also wraps the output of 
 * print_r() in <pre> tags so that the result is easily readable on HTML
 * pages without having to load the source code each time first.
 * 
 * @param 	mixed	$var
 */
function pr($var) {
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

/**
 * Returns the value for the debug mode currently set for our application. If
 * for whatever reason no debug mode is set, DEBUG_MODE_NONE will be returned
 * by default to prevent any code from accidentally printing out debug info
 * when that shouldn't be allowed. Better to be safe than sorry!
 * 
 * @return 	int
 */
function debug_mode() {
	return (defined('DEBUG_MODE')) ? DEBUG_MODE : DEBUG_MODE_NONE;
}

/**
 * Returns whether or not we should display errors.
 * 
 * @return 	boolean
 */
function display_errors() {
	return debug_mode() & DEBUG_MODE_ERRORS;
}

/**
 * Returns the time (in seconds) that the script has been running.
 * 
 * @return 	float
 */
function runtime() {
	return microtime(true) - START_TIME;
}

/* * *****************************************************************************
 * VII. TRANSLATIONS / LOCALE
 * 
 * We use a couple of global functions to easily retrieve information about the
 * client's current language, region, etc. so that we can make our website 
 * respond to those settings.
 * 
 * ***************************************************************************** */

/**
 * Returns the language of the current page. This could be one of many things:
 * most often it will be the global system default. However, for some sites, it
 * may be possible for their users to change the language they want to view the
 * site it. In that case the value will most likely be retrieved from stored
 * session data. In other cases, a particular site area may always be rendered
 * in a particular language (think about pages like http://www.example.com/en/
 * for example).
 * 
 * This function is meant to give you one single and simple way of determining
 * which language your content should be rendered in.
 * 
 * If a parameter is specified, the site language is set to that language instead.
 * 
 * @param 	string	$language
 * @return 	string
 */
function site_language($language = null) {
	if (null !== $language) {
		$GLOBALS['language'] = $language;
	}
	
	return globals('language') ?: Config::read('default_language');
}

/**
 * A shorthand alias for site_language().
 * 
 * @return 	string
 */
function lang() {
	return site_language();
}

/**
 * Executes a callback in the context of a certain language and returns the
 * result.
 * 
 * @param 	callable	$callback
 * @param 	string		$language
 * @return 	mixed
 */
function run_language(callable $callback, $language = null) {
	// If no language was specified, simply run the given callback in the 
	// context of the current language. If a language was specified, cache it so
	// that we can set the language back to its original value after we're done 
	// running the callback function.
	if ($language === null || $language === ($current = site_language())) {
		return $callback();
	}
	
	// For now, set the site language to the specified $language.
	site_language($language);
	
	// Execute our callback in the new language.
	$res = $callback();
	
	// Reset our language to its original value.
	site_language($current);
	
	// Return our callback result.
	return $res;
}

/**
 * Returns all languages that are supported by this website.
 * 
 * @return 	array
 */
function site_languages() {
	$languages = [];
	$locale = new Locale();

	dir_map(__LANG__, function($file, $dir) use (&$languages, $locale) {
		$structure = preg_replace('/^' . preg_quote(__LANG__, '/') . PREG_DS . '?/', '', $dir);
		$locale->parse(str_replace(DS, '_', $structure));
		
		$format = $locale->format();
		
		$languages[$format] = $format;
	}, true);
	
	return array_values($languages);
}

/**
 * Translates a given label.
 * 
 * An optional second argument containing variables used in the label can be 
 * passed along to insert variable data into the translated string.
 * 
 * An optional third argument specifying the destination language forces the 
 * label to be translated to a specific language. If it is omitted, the default 
 * site language is used instead.
 * 
 * If the second argument is a string, the method assumes that this string is
 * meant to identify the language.
 * 
 * @param 	string	$label
 * @param 	array	$vars
 * @param 	string	$lang
 * @return 	string
 */
function translate($label, $vars = [], $lang = null) {
	// Check if a language to which the given label should be translated was
	// explicitly specified.
	if (null === $lang) {
		// If not, use the site's default language.
		$lang = site_language();
	}
	
	// Retrieve our global translations array.
	$translations = globals('translations');
	
	// If loading the translations array failed for whatever reason (maybe it
	// hasn't been fully computed yet?), run our label through our 
	// internationalization handler which will then take care of all the 
	// translation logic.
	if (null === $translations) {
		return Lang::translate($label, $lang, $vars);	
	}
	
	// If a translations array was retrieved, look up our label in there under
	// the right language. If the label was not found, no translation exists,
	// so we return the label as is.
	if (!isset($translations[$lang][$label])) {
		return $label;
	}

	// If the label was found, we translate it and afterwards we insert any 
	// variables in the translated text.
	return (empty($vars)) ? 
				$translations[$lang][$label] :
				str_format($translations[$lang][$label], $vars);
}

/**
 * A shorthand alias for translate().
 * 
 * @param 	string	$label
 * @param 	array	$vars
 * @param 	string	$lang
 * @return 	string
 */
function __($label, $vars = [], $lang = null) {
	return translate($label, $vars, $lang);
}

/**
 * Runs the given arguments through translate() and prints the result.
 * 
 * @param 	string	$label
 * @param 	array	$vars
 * @param 	string	$lang
 * @return 	string
 */
function ___($label, $vars = [], $lang = null) {
	echo translate($label, $vars, $lang);
}

/**
 * A shorthand alias for translate().
 * 
 * @param 	string	$label
 * @param 	array	$vars
 * @param 	string	$lang
 * @return 	string
 */
function t($label, $vars = [], $lang = null) {
	return translate($label, $vars, $lang);
}

/**
 * Returns whether or not the site supports multiple languages.
 * 
 * @return 	boolean
 */
function is_multilingual() {
	return is_dir(__LANG__);
}



/* * *****************************************************************************
 * VIII. SECURITY / SANITIZING
 * 
 * It's extremely important to make sure everything we output to the client has
 * been purified/sanitized. This way we drastically reduce the risks of XSS,
 * CSRF or other attacks that could harm us or our clients. The functions below
 * help sanitizing different types of data for different types of contexts,
 * be it inside <script> tags (which is almost always a bad idea, unless the
 * output is a quoted data value) or inside other HTML tags.
 * 
 * Some of these functions can also be used to escape strings for SQL use, 
 * although our Database interface already handles this for you.
 * 
 * ***************************************************************************** */

/**
 * Takes any sort of variable as a parameter and normalizes its contents.
 * 
 * @param 	mixed	$var
 * @return 	mixed
 */
function normalize($var) {
	if (is_array($var)) {
		foreach ($var as $k => $e) {
			$var[$k] = normalize($e);
		}

		return $var;
	}

	if (is_string($var) && class_exists('Normalizer') && !Normalizer::isNormalized($var)) {
		return Normalizer::normalize($var);
	}

	return $var;
}

/**
 * Takes a value that needs to be purified as its first argument and an integer
 * representing the different strategies that should be used for the purification
 * process as its second argument. After running the value through the necessary
 * purification helper functions, the result is returned.
 * 
 * The $strategy variable should be a bit combination of the PURIFY_* constants
 * we have defined in the config.php file for this application.
 * 
 * @param 	string|array	$var
 * @param 	int				$strategy
 * @return 	string|array
 */
function purify($var, $strategy) {
	$filters = [];
	
	if ($strategy & PURIFY_HTML) {
		$filters[] = Sanitizer::FILTER_HTML;
	}

	if ($strategy & PURIFY_HTML_ATTR) {
		$filters[] = Sanitizer::FILTER_HTML;
	}

	if ($strategy & PURIFY_URL) {
		$filters[] = Sanitizer::FILTER_URL;
	}

	if ($strategy & PURIFY_JS) {
		$filters[] = Sanitizer::FILTER_JS;
	}

	$sanitizer = globals('sanitizer');
	
	if (null === $sanitizer) {
		$sanitizer = DI::getInstanceOf('Rozyn\Security\Sanitizer');
	}
	
	return $sanitizer->sanitize($var, array_unique($filters));
}

/**
 * Purifies the contents of the variable so that it can safely be used inside
 * your HTML templates.
 * 
 * @param 	string	$var
 * @return 	string
 */
function purify_html($var) {
	return purify($var, PURIFY_HTML);
}

/**
 * Purifies the contents of the variable so that it can safely be used as a
 * value for an HTML element's attribute.
 * 
 * @param 	string	$var
 * @return 	string
 */
function purify_html_attr($var) {
	return purify($var, PURIFY_HTML);
}

/**
 * Purifies the contents of the variable so that it can safely be used inside a
 * URL string.
 * 
 * @param 	string	$var
 * @return 	string
 */
function purify_url($var) {
	return purify($var, PURIFY_URL);
}

/**
 * Purifies the contents of the variable so that it can safely be used inside
 * JavaScript code as long as the variable is only used as a quoted data value.
 * 
 * @param 	string	$var
 * @return 	string
 */
function purify_js($var) {
	return purify($var, PURIFY_JS);
}

/**
 * Returns the secret key for this application.
 * 
 * @return 	string
 */
function secret_key() {
	$keyfile = __CONFIG__ . DS . '_key';

	if (!file_exists($keyfile)) {
		file_put_contents($keyfile, Crypto::CreateNewRandomKey());
	}

	if (!Config::has('key')) {
		Config::write('key', file_get_contents($keyfile));
	}

	return Config::read('key');
}



/*******************************************************************************
 * IX. DATES AND TIMES
 * 
 * Dates and times are notoriously hard to work with, so below are some helper
 * functions that can be used to make things a little bit easier.
 * 
 *******************************************************************************/

/**
 * Returns the current datetime in the specified format. If no format is given,
 * the default SQL format is used.
 * 
 * @param 	string	$format
 * @return 	string
 */
function now($format = null) {
	if (!$format) {
		$format = DATETIME_FORMAT_SQL;
	}

	return date($format, time());
}



/*******************************************************************************
 * X. MISCELLANEOUS
 * 
 * Some functions are a bit harder to categorize than others, so they were put
 * under this Miscellaneous category, which basically encompasses all random 
 * functions we didn't know where else to file them under.
 * 
 *******************************************************************************/

/**
 * Returns the memory usage as a string, including the unit.
 * 
 * @return	string
 */
function memory() {
	$memory = memory_get_usage();

	if ($mem_usage < 1024) {
		return $mem_usage . " bytes";
	} elseif ($mem_usage < 1048576) {
		return round($mem_usage / 1024, 2) . " kb";
	} else {
		return round($mem_usage / 1048576, 2) . " mb";
	}
}

/**
 * Return the value of a global variable or a specified default value if the 
 * global does not exist.
 * 
 * @param	string	$var
 * @param	mixed	$default
 * @return	mixed
 */
function globals($var, $default = null) {
	if (isset($GLOBALS[$var])) {
		return $GLOBALS[$var];
	}
	
	if (isset($GLOBALS['_' . $var])) {
		return $GLOBALS['_' . $var];
	}
	
	return $default;
}

/**
 * Returns a configuration option or a specified default value if the config
 * option does not exist.
 * 
 * @param	string	$key
 * @param	mixed	$default
 * @return	mixed
 */
function config($key, $default = null) {
	$config = globals('config');
	
	if ($config === null) {
		$config = DI::getInstanceOf('Rozyn\Config\Config');
	}
	
	return $config->read($key, $default);
}

/**
 * Check that the PHP version on this server passes a version comparison.
 * 
 * @param   string  $version
 * @param   string  $comparison
 * @return  boolean
 */
function check_php_version($version, $comparison = ">=") {
    return version_compare(phpversion(), $version, $comparison);
}

/**
 * Returns the Operating System that this PHP is installed on.
 * 
 * @return  string
 */
function get_os() {
    return PHP_OS;
}

/**
 * Returns whether or not PHP is running on a Windows machine.
 * 
 * @return  string
 */
function is_os_windows() {
    return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
}

/**
 * Returns the stack trace as either a string or an array.
 * 
 * @param	boolean	$asArray
 * @return	array
 */
function get_stack_trace($asArray = false) {
	$e = new \Exception();
	
	return ($asArray) ? $e->getTrace() : $e->getTraceAsString();
}