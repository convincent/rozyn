var Form;
 
(function($) {
	Form = Base.extend({
		/**
		 * The Form constructor. 
		 * 
		 * @param	jQuery	element
		 * @param	dict	settings
		 * @return	void
		 */
		init: function(element, settings, validator) {
			var defaults = {
				ajax: false,
				validation: true
			};
			
			this.disabled = false;
			this.element  = element;
			this.settings = $.extend({}, defaults, settings);
			
			this.validator = (typeof(validator) === 'undefined') ? new Validator() : validator;
			
			/**
			 * Holds the reference to the active coachmark in this form.
			 * 
			 * @var	{Popout}
			 */
			this.coachmark;
			
			/**
			 * Holds all the references to the active datetime popout in this form.
			 * 
			 * @var	{Popout}
			 */
			this.datetime;
					
			// Make the Form responsive.
			this.respond();
		},
		
		/**
		 * Looks up a child element of this form.
		 * 
		 * @return	s {jQuery}
		 */
		find: function(s) {
			return this.element.find(s);
		},
		
		/**
		 * Our master respond() method which calls all other "submethods" to
		 * make each part of our form responsive.
		 * 
		 * @return	void
		 */
		respond: function() {
			this.respondForm();
			this.respondInputs();
			
			if (this.isAjax()) {
				this.respondAjax();
			}
		},
		
		/**
		 * Generic responsiveness for the form as a whole.
		 * 
		 * @return	s	{void}
		 */
		respondForm: function() {
			var frm = this;
			
			this.find('.button.submit').on('click', function(e) {
				e.preventDefault();
				
				frm.getElement().submit();
			});
			
			this.element.on('submit', function(e) {
				frm.fire('beforesubmit', [this]);
				
				if (!frm.canSubmit()) {
					e.preventDefault();
					return false;
				}
				
				frm.fire('submit', [this]);
			});
		},
		
		/**
		 * Takes care of making all the different inputs in the form responsive.
		 * 
		 * @return	void
		 */
		respondInputs: function() {
			var self = this;
			
			this.getInputs().each(function() {
				if (self.isInputObligatory($(this))) {
					var label = self.getInputLabel($(this));
					if (label !== null) {
						label.after($('<span>').addClass('obligatory'));
					}
				}
				
				$(this).on('focus', function(e) {
					self.clearError($(this));
				});
			});
			
			this.respondFileUploads();
		},
		
		/**
		 * Makes all the file upload fields responsive. This method first
		 * performs some generic logic that should be applied to every single
		 * file input. Afterwards, submethods are called to handle specific
		 * types of file inputs (for example: image file uploads, excel file
		 * uploads, etc.)
		 * 
		 * @return	void
		 */
		respondFileUploads: function() {
			this.element.find('input[type=file]').each(function() {
				// Replace each file input with a placeholder that can be styled
				// more easily. We bind the newly created placeholder element to
				// a variable only after we've wrapped it around our file input,
				// because if you do it before that, there are some bugs if you
				// try to call functions on that element later on. This way, we
				// circumvent all those potential bugs.
				$(this).wrap($('<div>').addClass('form-element-wrapper'));
			   
				var name		= $(this).attr('name');
				var wrapper		= $(this).parent();
				var placeholder = $('<div>').addClass('form-element')
											.addClass($(this).attr('type'))
											.addClass($(this).attr('class'));
					
				if ($(this).attr('multiple')) {
					if ($(this).slice(-2) !== "[]") {
						name += "[]";
						$(this).attr('name', name);
					}
				}
									
				if ($(this).data('value')) {
					placeholder.html($(this).data('value'));
				}
									
				for (var x in $(this).data()) {
					placeholder.attr("data-" + x, $(this).data(x));
				}
				
				placeholder.insertBefore($(this));
				
				// Bind a focus handler to the input so that we can add a 
				// "focus" class to the placeholder when the actual input is
				// being focused. This allows us to still apply focus styles,
				// even though the placeholder itself isn't actually being
				// focused.
				$(this).on('focus', function(e) {
					placeholder.addClass('focus');
				});
				
				// Remove focus when the input itself loses focus.
				$(this).on('focusout', function(e) {
					placeholder.removeClass('focus');
				});
				
				// Bind a change handler that checks whether or not the input
				// has a file selected. This allows us to toggle a class on the
				// placeholder which indicates if the input has a file selected.
				$(this).on('change', function(e) {
					placeholder.toggleClass('empty', $(this)[0].files.length === 0);
					
					// We also want to update the file upload text field based
					// on the names of the uploaded files, so we have to keep
					// track of them.
					var names = [];

					// Check if a valid file has been specified and if so, loop 
					// through them all.
					for (i = 0; i < $(this)[0].files.length; i++) {
						// Add the file name to our names array.
						names.push($(this)[0].files[i].name);
					}
					
					// Add the names to the placeholder's inner HTML.
					placeholder.text(names.join(', '));
				});
				
				placeholder.toggleClass('empty', !placeholder.html());
			});
			
			// Call several submethods to handle different kinds of file-type 
			// uploads.
			this.respondImageFileUploads();
		},
		
		/**
		 * Makes the image file upload fields responsive. This is a submethod
		 * that should only be called after respondFileUploads() has been called
		 * previously.
		 * 
		 * @return	void
		 */
		respondImageFileUploads: function() {
			// Handle some image-specific logic.
			this.element.find('input[type=file].image').each(function() {
				// The preview variable holds the wrapper element for any 
				// preview images of the uploaded files.
				var preview;
				
				// The element that wraps around the file input.
				var wrapper = $(this).parent();
				
				// The placeholder for the input
				var placeholder = wrapper.find('.form-element').first();

				// If the input has a data-preview attribute, parse its value as
				// a jQuery selector and use the matching element as the preview
				//  wrapper.
				if ($(this).data('preview')) {
					preview = $($(this).data('preview'));
				} 

				// If the input doesn't contain a data-preview attribute, we
				// create a new <div> element on the fly to serve as our preview
				// wrapper. We prepend this <div> to our file input placeholder 
				// element.
				else {
					preview = $('<div>').addClass('preview');
					preview.insertBefore(wrapper);
				}
				
				// Show any images that were previously stored.
				if ($(this).data('src')) {
					var images = $(this).data('src').split('|');
					
					for (var i = images.length - 1; i >= 0; i--) {
						preview.append($('<img />').addClass('old').attr({
							alt: '',
							src: images[i]
						}));
					}
				}

				// We want to update the previews each time the file input's
				// value is changed, so we specify a callback for that.
				$(this).on('change', function(e) {
					// If this image input supports multiple images, only remove
					// the images that were added on this page, leaving all the
					// images that were stored earlier intact.
					if ($(this).attr('multiple')) {
						preview.find('img.new').remove();
					} 
					
					// If this is a single image input, replace the old image
					// with the new one regardless of when the old image was
					// added.
					else {
						preview.find('img').remove();
					}
					
					// Check if a valid file has been specified and if so, loop 
					// through them all, since each of them.
					for (i = 0; i < $(this)[0].files.length; i++) {
						// Extract the current file.
						var file = $(this)[0].files[i];
						
						// Create the image tag that will hold the preview of 
						// this particular file. Add a "new" class to distinguish
						// it from other images that were already stored and are
						// simply being displayed to give the client the option
						// of deleting them manually.
						var img	 = $('<img />').addClass('new');

						// Add the <img> tag to our previously established
						// preview wrapper and set its file to the file we
						// are currently processing.
						preview.append(img);
						img[0].file = file;

						// We need to specify an onload callback for this
						// file which makes it so that the "src" attribute 
						// of the <img> tag is updated to load the image file.
						var reader = new FileReader();
						reader.onload = (function(img) {
							return function(e) {
								img.src = e.target.result;
							};
						})(img[0]);

						reader.readAsDataURL(file);
					}
				});
			});
		},
		
		/**
		 * Makes the form submit its data through an AJAX request.
		 * 
		 * @return	s {void}
		 */
		respondAjax: function() {
			// Store a reference to this object so that we can use it in our 
			// callback functions below.
			var frm = this;
			
			this.element.on('submit', function(e) {
				e.preventDefault();
				
				// Make sure the form can in fact be submitted.
				if (!frm.canSubmit()) {
					return false;
				}
				
				var formData = new FormData(frm.element.get(0));
				
				// Take care of file inputs.
				$(this).find('input[type=file]').each(function() {
					// Extract the input name and the associated files first.
					var name  = $(this).attr('name'),
						files = $(this).get(0).files;
					
					// If we're dealing with multiple files on a single input,
					// make sure that the name ends with "[]".
					if (files.length > 1 && !(new RegExp("\[\]$").test(name))) {
						name += '[]';
					}
					
					// Loop through all the files for this input and add them to
					// our formData object.
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						
						formData.append(name, file, file.name);
					}
				});
				
				// Inspired by http://stackoverflow.com/questions/2320069/jquery-ajax-file-upload/10811427#10811427
				$.ajax({
					url:  frm.element.attr('action') || window.location,
					type: frm.element.attr('method').toUpperCase() || 'POST',
					
					// Custom xhr callback to allow for progress updates.
					xhr: function() {
						myXhr = $.ajaxSettings.xhr();
						// First make sure the upload property actually exists.
						if (myXhr.upload) {
							myXhr.upload.addEventListener('progress', function(e) {
								frm.fire('progress', [e.loaded, e.total, e.timeStamp, e]);
							}, false);
						}
						
						return myXhr;
					},
					
					// Ajax events
					success: function(data, textStatus, jqXHR) {
						frm.fire('success', [data, textStatus, jqXHR]);
					},
					
					error: function(jqXHR, textStatus, errorThrown) {
						frm.fire('error', [textStatus, jqXHR, errorThrown]);
					},
					
					// Form data
					data: formData,
					
					// Options to tell JQuery not to process data or worry about 
					// content-type
					cache: false,
					contentType: false,
					processData: false
				}, 'json');
				
				frm.disable();
			});
			
			frm.on('success error', function() {
				frm.enable();
			});
		},
		
		/**
		 * Returns whether or not the form may be submitted.
		 * 
		 * @return	s {Boolean}
		 */
		canSubmit: function() {
			return this.isEnabled() && (!this.hasValidation() || this.validate(true));
		},
		
		/**
		 * Disables the form.
		 */
		disable: function() {
			this.disabled = true;
			
			this.element.find('input[type=submit]').addClass('loading').attr('disabled', 'disabled');
		},
		
		/**
		 * Enables the form.
		 */
		enable: function() {
			this.disabled = false;
			
			this.element.find('input[type=submit]').removeClass('loading').removeAttr('disabled');
		},
		
		/**
		 * Returns true if the form is disabled.
		 * 
		 * @return	s {Boolean}
		 */
		isDisabled: function() {
			return !!this.disabled;
		},
		
		/**
		 * Returns true if the form is enabled.
		 * 
		 * @return	s {Boolean}
		 */
		isEnabled: function() {
			return !this.disabled;
		},
		
		/**
		 * Validates the entire form and returns true if all inputs pass their
		 * validation rules. If the mark argument is set to TRUE, inputs that
		 * don't pass their validation rules are marked as faulty. Otherwise no
		 * visual representation of errors is given.
		 * 
		 * @param	{Boolean}	mark
		 * @return	s {Boolean}
		 */
		validate: function(mark) {
			var self = this,
				res  = true;
			
			this.getInputs().each(function() {
				res = self.validateInput($(this), mark) && res;
			});
			
			return res;
		},
		
		/**
		 * Validates a single input
		 * 
		 * @param	{jQuery}	input
		 * @param	{Boolean}	mark
		 * @return	s {Boolean}
		 */
		validateInput: function(input, mark) {
			var self = this,
				res	 = true;
			
			if (!this.getValidator().checkInput(input)) {
				res = false;

				if (mark) {
					this.markError(input);
				}
			}
			
			return res;
		},
		
		/**
		 * Returns the Validator object used to validate inputs in this form.
		 * 
		 * @return	s {Validator}
		 */
		getValidator: function() {
			return this.validator;
		},
		
		/**
		 * Returns whether or not validation is enabled for this form.
		 * 
		 * @return	s {Boolean}
		 */
		hasValidation: function() {
			return this.settings.validation;
		},
		
		/**
		 * Marks the given input as faulty.
		 * 
		 * @param	{jQuery}	input
		 * @return	s {void}
		 */
		markError: function(input) {
			input.addClass('error');
			input.siblings('.form-element').addClass('error');
			input.closest('.form-block').addClass('error');
		},
		
		/**
		 * Clears the input of any error indicators.
		 * 
		 * @return	s {void}
		 */
		clearError: function(input) {
			input.removeClass('error');
			input.siblings('.form-element').removeClass('error');
			input.closest('.form-block').removeClass('error');
		},
		
		/**
		 * Scrolls to the first error in the form.
		 * 
		 * @return	s {void}
		 */
		jumpToFirstError: function() {
			var element = this.find('.error').first();
			
			if (element.size()) {
				$('body, html').animate({
					scrollTop: element.offset().top,
				}, 300);
			}
		},
		
		/**
		 * Returns whether or not the specified input is obligatory.
		 * 
		 * @param	{jQuery}	input
		 * @return	s {Boolean}
		 */
		isInputObligatory: function(input) {
			return input.data('validation') && input.data('validation').split('|').indexOf('obligatory') > -1;
		},
		
		/**
		 * Returns whether or not the form is meant to be sent through an AJAX
		 * request.
		 * 
		 * @return	s	{Boolean}
		 */
		isAjax: function() {
			return !!this.settings.ajax;
		},
		
		/**
		 * Returns a jQuery collection that represents all form elements in this
		 * form.
		 * 
		 * @return	s {jQuery}
		 */
		getInputs: function() {
			return this.getElement().find('input, textarea, select');
		},
		
		/**
		 * Returns a jQuery representation of the label that corresponds to the
		 * specified input element.
		 * 
		 * @param	{jQuery}		input
		 * @return	s {jQuery}
		 */
		getInputLabel: function(input) {
			// Check to see if the input contains a sibling that is a label.
			var label = null;
			input.siblings('label').each(function() {
				if ($(this).attr('for') === input.attr('id')) {
					label = $(this);
				}
			});
			
			if (label !== null) {
				return label;
			}
			
			// If no label has been found up until now, just check all the
			// labels in the same form block.
			var block	= input.closest('.form-block'),
				res		= null;
		
			if (block.size()) {
				block.find('label').each(function() {
					if ($(this).attr('for') === input.attr('id')) {
						res = $(this);
					}

					return false;
				});
			}
			
			return res;
		},
		
		/**
		 * Returns a jQuery object that represents this form.
		 * 
		 * @return	s	{jQuery}
		 */
		getElement: function() {
			return this.element;
		}
	});
})(jQuery);