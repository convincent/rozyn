var Slider = Base.extend({
	init: function(element, options) {
		this.setElement(element);
		this.sliding = false;

		var defaults = {
			initialActive: 0,
			slideDuration: 0,
			autoSlide: 2500,
			animate: function(self, index, duration) {
				self.getActiveSlide().fadeOut(duration, function() {
					self.fire('betweenslide', [self, index]);
					self.getSlide(index).fadeIn(duration, function() {
						self.setActive(index);
						self.sliding = false;

						self.fire('afterslide', [this]);
					});
				});
			}
		};

		if (typeof(options) === 'undefined') {
			options = {};
		}

		for (var x in defaults) {
			this[x] = (typeof(options[x]) !== 'undefined') ? options[x] : defaults[x];
		}

		if (this.autoSlide) {
			this.initAutoSlide();
		}
		
		this.getSlides().each(function() {
			link = $(this).find('.slide-button');
			if (link.size() && link.attr('href')[0] === '#') {
				link.on('click', function(e) {
					e.preventDefault();
					$('html, body').animate({
						scrollTop: $($(this).attr('href')).offset().top
					}, 300);
				});
			}
		});

		this.setActive(this.initialActive);

		this.respondNavigation();
	},

	setActive: function(index) {
		this.fire('change', [this, index]);

		this.getSlide(index).addClass('active').siblings().removeClass('active');
	},

	next: function() {
		this.slideTo((this.getActiveIndex() + 1) % this.size());
	},

	prev: function() {
		this.slideTo((this.getActiveIndex() - 1 + this.size()) % this.size());
	},

	slideTo: function(index, duration) {
		if (index === this.getActiveIndex())
			return false;

		if (!this.sliding) {
			this.sliding = true;
			var self	 = this;

			if (this.autoSlide) {
				this.initAutoSlide();
			}
			
			if (typeof(duration) === 'undefined') {
				var duration = this.slideDuration;
			}

			this.fire('beforeslide', [this, index]);

			if (duration === 0 || duration === false || duration === null) {
				// Hide the old slide.
				this.getActiveSlide().hide();
				
				// Even though we're not actually sliding, we're technically in between slides, so fire that event now.
				this.fire('betweenslide', [this, index]);
				
				// Mark the new slide as active.
				this.setActive(index);
				
				// Show the new slide.
				this.getActiveSlide().show();
				
				// We're done "sliding", so free the sliding variable.
				this.sliding = false;

				// Fire our afterslide event, even though no sliding actually took place.
				this.fire('afterslide', [this]);
			}

			else {
				this.animate(this, index, duration);
			}
		}
	},

	getSlides: function() {
		if (typeof(this.slides) === 'undefined') {
			this.slides = this.find('.slide').first().parent().children('.slide');
		}

		return this.slides;
	},

	getSlide: function(index) {
		return this.getSlides().eq(index);
	},

	getActiveSlide: function() {
		return this.getSlides().filter('.active');
	},

	getActiveIndex: function() {
		return this.getActiveSlide().prevAll('.slide').size();
	},

	size: function() {
		return this.getSlides().size();
	},

	initAutoSlide: function() {
		var self = this;

		if (typeof(this.autoSlideInterval) !== 'undefined')
			clearInterval(this.autoSlideInterval);

		this.autoSlideInterval = setInterval(function() {
			self.next();
		}, this.autoSlide);
	},

	respondNavigation: function() {
		var self = this;

		this.find('.slider-prev').on('click', function(e) {
			self.prev();
			e.preventDefault();
			return false;
		});

		this.find('.slider-next').on('click', function(e) {
			self.next();
			e.preventDefault();
			return false;
		});
	},
	
	setElement: function(element) {
		this.element = element;
	},

	getElement: function() {
		return this.element;
	},

	html: function(content) {
		if (typeof(content) === 'undefined') {
			return this.element.html();
		}

		this.element.html('').append(content);

		return this;
	},

	remove: function() {
		this.getElement().remove();
	},

	find: function(q) {
		return this.element.find(q);
	},
});