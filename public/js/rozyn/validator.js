var Validator;
		
(function($) {
	Validator = Base.extend({
		init: function() {
			/**
			 * The input element currently being validated.
			 * 
			 * @var	{jQuery}
			 */
			this.input;

			/**
			 * A dictionary containing all validation rules that are registered to this
			 * Validator.
			 * 
			 * @var	{dict}
			 */
			this.rules = {
				obligatory: function(v) {
					return !!v.trim().length;
				},

				hex: function(v) {
					return v.match(/^\#[a-fA-F0-9]{6}$/);
				},

				int: function(v) {
					return v.match(/^\d*$/);
				},

				email: function(v) {
					return !v.length || (v.match(/^\S+@\S+$/) && v.match(/^[\x00-\x7F]+$/));
				},

				phone: function(v) {
					return v.match(/^[0-9+ ()-]*$/);
				},

				equals: function(v, sel) {
					return v === (($(sel).size()) ? ($(sel).val() || $(sel).html()) : sel);
				},

				differs: function(v, sel) {
					return !v || v != (($(sel).size()) ? ($(sel).val() || $(sel).html()) : sel);
				},

				minWords: function(v, min) {
					return v.split(' ').length >= parseInt(min);
				},

				maxLength: function(v, len) {
					return v.length <= len;
				},
			};
		},
	
		/**
		 * Checks a given value against a particular set of rules.
		 * 
		 * @param	{mixed}		v
		 * @param	{Array}		rules
		 * @return	s {Boolean}
		 */
		check: function(v, rules) {
			for (x in rules) {
				var rule   = rules[x],
					params = [v];

				if ((pos = rule.indexOf(':')) > -1) {
					params = params.concat(rule.substr(pos + 1).split(','));
					rule   = rule.substr(0, pos);
				}

				if (rule in this.rules && typeof(this.rules[rule]) === 'function' && !this.rules[rule].apply(null, params)) {
					return false;
				}
			}

			return true;
		},

		/**
		 * Takes a particular input element and runs its value through all the
		 * validation rules that have been set on it using the data-validation
		 * attribute. Before checking if all the validation rules are indeed not
		 * violated, the input element is stored inside an object variable so that
		 * it can also be accessed from within each individual rule's callback
		 * without having to be passed along as a parameter each time. Just note
		 * that if a rule's callback is called directly without invoking the 
		 * checkInput method, the this.input variable may be null, so the rule's
		 * callback may throw an error in case you implicitly take it for granted
		 * that this variable will always contain a proper reference to an input
		 * element.
		 * 
		 * @param	{jQuery}	input
		 * @return	s {Boolean}
		 */
		checkInput: function(input) {
			this.input = input;
			
			value = input.val();
			if ((input.attr('type') === 'checkbox' || input.attr('type') === 'radio') && !input.is(':checked')) {
				value = '';
			}

			var res = (input.data('validation')) ? this.check(value, input.data('validation').split('|')) : true;

			this.input = null;
			
			return res;
		}
	});
})(jQuery);