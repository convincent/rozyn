var Overlay = Base.extend({
	init: function(content) {
		this.setContent(content);

		this.setId('id_' + (Math.random() + '').replace('0.', '')); //uniqid

		this.fadeDuration  = 500;
	},

	respond: function() {
		var self = this;

		this.element.on('click', function(e) {
			self.die();
		});

		this.contentElement.children().on('click', function(e) {
			e.stopImmediatePropagation();
		});
	},

	show: function(duration, callback) {
		if (this.isActive())
			return;

		var self = this;

		if (typeof(duration) === 'undefined') {
			var duration = this.fadeDuration;
		}

		if (this.isAnyActive()) {
			var active = this.getInstanceFromBody();

			active.hideContent(duration, function() {
				active.hide(0, function() {
					self.show(0);
					self.hideContent(0);
					self.showContent(self.fadeDuration);
					self.respond();
				});
			});
		} else {
			this.setInstanceOnBody();
			this.contentElement = $('<div>').attr('id', 'overlay-content')
											.append(this.getContent());
									
			this.setElement($('<div>').attr('id', 'overlay-wrapper')
										.append(this.contentElement)
										.css('display', 'none'));
					
			// Calculate the top offset.
			var offset = $(document).scrollTop();
			
			// Mark the HTML tag as containing an overlay.
			$('html').addClass('overlay');
			
			// Do the same for the BODY tag and set the previously calculated
			// top offset on it. This is necessary bceause the overlay class
			// forces a fixed positining on the BODY element, which means it
			// needs an appropriate top offset to still show the same section
			// that was shown before the overlay was initialized.
			$('body').addClass('overlay').css('top', -offset).append(this.getElement());

			this.fire('beforeshow', [this]);

			this.getElement().fadeIn(duration, function() {
				if (typeof(callback) !== 'undefined') {
					callback(self);
				}

				self.fire('aftershow', [self]);

				self.respond();
			});
		}
	},

	hide: function(duration, callback) {
		if (!this.isActive())
			return;

		if (typeof(duration) === 'undefined') {
			var duration = this.fadeDuration;
		}

		var self = this;

		this.fire('beforehide', [this]);

		this.getElement().fadeOut(duration, function() {
			self.remove();
			self.removeInstanceFromBody();
			
			var offset = -parseInt($('body').css('top').replace(/px$/, ''));

			// Remove the overlay class from the HTML and BODY tags.
			$('body, html').removeClass('overlay');
			
			// We don't need the top offset on the body anymore, so reset that.
			$('body').css('top', '');
			
			// Scroll back to where the user was on the page before they opened
			// the overlay.
			$(document).scrollTop(offset);

			if (typeof(callback) !== 'undefined') {
				callback(self);
			}

			self.fire('afterhide', [self]);
		});
	},

	showContent: function(duration, callback) {
		var self = this;

		this.fire('beforeshowcontent', [this]);

		if (typeof(duration) === 'undefined') {
			var duration = this.fadeDuration;
		}

		this.getContent().fadeIn(duration, function() {
			if (typeof(callback) !== 'undefined') {
				callback(self);
			}

			self.fire('aftershowcontent', [self]);
		});
	},

	hideContent: function(duration, callback) {
		var self = this;

		this.fire('beforehidecontent', [this]);

		if (typeof(duration) === 'undefined') {
			var duration = this.fadeDuration;
		}

		this.getContent().fadeOut(duration, function() {
			if (typeof(callback) !== 'undefined') {
				callback(self);
			}

			self.fire('afterhidecontent', [self]);
		});
	},

	die: function() {
		this.hide(this.fadeDuration);
	},

	find: function(q) {
		return (this.content) ? this.content.find(q) : this.element.find(q);
	},

	setContent: function(content) {
		if (typeof(content) === 'undefined') {
			var content = '';
		}
		
		if (this.isActive()) {
			this.content = content;
			this.html(content);
			this.respond();
		}

		else {
			this.content = content || '';
		}
	},

	getContent: function() {
		return this.content;
	},

	isActive: function() {
		return (this.isAnyActive() && this.equals(this.getInstanceFromBody()));
	},

	isAnyActive: function() {
		return $('body').hasClass('overlay');
	},

	getInstanceFromBody: function() {
		return $('body').data('_overlay');
	},

	setInstanceOnBody: function() {
		$('body').data('_overlay', this);
	},

	removeInstanceFromBody: function() {
		$('body').data('_overlay', null);
	},

	equals: function(otherOverlay) {
		return this.getId() === otherOverlay.getId();
	},

	setId: function(id) {
		this._id = id;
	},

	getId: function() {
		return this._id;
	},
	
	setElement: function(element) {
		this.element = element;
	},

	getElement: function() {
		return this.element;
	},

	getContentElement: function() {
		return this.contentElement;
	},

	html: function(content) {
		if (typeof(content) === 'undefined') {
			return this.contentElement.html();
		}

		this.contentElement.html('').append(content);

		return this;
	},

	remove: function() {
		this.getElement().remove();
	}
});