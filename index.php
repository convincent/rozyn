<?php

use Rozyn\Facade\App;

/*******************************************************************************
 * I. SANITY CHECKS
 * 
 * Here we perform some basic checks to make sure that the server is
 * capable of running our code. It's important to check for these things,
 * because we haven't specified our error reporting yet, so it is absolutely
 * essential we test for all possible errors beforehand so that we can 
 * display meaningful error messages to ourselves and our visitors in case
 * something goes wrong in this phase of the script.
 * 
 *******************************************************************************/

// Make sure we aren't looping this file for some weird reason. This could happen
// due to an invalid Controller View path. For example, if a Controller's view
// is mapped to this index.php file for whatever reason, an infinite loop would
// occur.
if (defined('DS')) {
	echo 'Infinite loop detected, probably caused by invalid Controller View.';
	exit(1);
}

// First, we perform some basic tests to see if the server can actually support
// our code.
define('MIN_PHP_VERSION', '5.4.0');

// Make sure we can actually compare PHP versions.
if (!function_exists('version_compare') || 
	!function_exists('phpversion') || 
	!defined('PHP_VERSION')) {
	
		die('PHP VERSION TOO LOW');
}

// Make sure that the PHP version on this server can support our code by 
// comparing our specified minimum PHP version with the PHP version currently
// running.
if (!version_compare(phpversion(), MIN_PHP_VERSION, ">=")) {
	
		die('PHP VERSION TOO LOW, USING VERSION ' . PHP_VERSION);
}

// Include our main bootstrap file which takes care of booting the App.
require __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'rozyn' . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Run our application.
App::run();